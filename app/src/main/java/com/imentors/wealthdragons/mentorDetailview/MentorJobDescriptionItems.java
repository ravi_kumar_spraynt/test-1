package com.imentors.wealthdragons.mentorDetailview;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.CourseNewItemsAdapter;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class MentorJobDescriptionItems extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvNewItemHeading, mTvJobRole, mTvComapany, mTvDate, mTvPlace, mTvJobDescriptionHeading, mTvJobDescription, mTvJobRoleHeading, mTvDesiganation;
    private RecyclerView mItemListRecyclerView;
    private CourseNewItemsAdapter mCourseNewItemsAdapter = null;
    private View mLineBelowHeading, mLineDivider;
    private ImageView mIvDegree;


    public MentorJobDescriptionItems(Context context) {
        super(context);
        mContext = context;
    }

    public MentorJobDescriptionItems(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorJobDescriptionItems(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorJobDescriptionItems(Context context, DashBoardMentorDetails.JobHistory newItemList, String listTitle, String newItemType, boolean showDivider) {
        super(context);
        initViews(context);
        bindView(newItemList, listTitle, newItemType, showDivider);
    }

    //  set data in view
    private void bindView(DashBoardMentorDetails.JobHistory newItemList, String listTitle, String newItemType, boolean showDivider) {

        if (TextUtils.isEmpty(listTitle)) {
            mTvNewItemHeading.setVisibility(GONE);
            mLineBelowHeading.setVisibility(GONE);

        } else {
            mLineBelowHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setText(listTitle);
        }

        if (showDivider) {
            mLineDivider.setVisibility(VISIBLE);
        } else {
            mLineDivider.setVisibility(GONE);

        }


        if (!TextUtils.isEmpty(newItemList.getCompany())) {
            mTvComapany.setText(newItemList.getCompany());
        } else {
            mTvComapany.setVisibility(GONE);
        }


        if (!TextUtils.isEmpty(newItemList.getDesignation())) {
            mTvDesiganation.setText(newItemList.getDesignation());
        } else {
            mTvDesiganation.setVisibility(GONE);

        }
        if (TextUtils.isEmpty(newItemList.getJob_logo())) {
            mIvDegree.setVisibility(View.GONE);
        } else {
            Glide.with(mContext).load(newItemList.getJob_logo()).into(mIvDegree);

        }

        if (!TextUtils.isEmpty(newItemList.getDescription_heading())) {
            mTvJobDescriptionHeading.setText(newItemList.getDescription_heading());
        } else {
            mTvJobDescriptionHeading.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(newItemList.getStart_date()) && !TextUtils.isEmpty(newItemList.getStart_date())) {
            mTvDate.setText(newItemList.getStart_date() + " - " + newItemList.getEnd_date());
        } else {
            mTvDate.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(newItemList.getCity()) && !TextUtils.isEmpty(newItemList.getCountry())) {
            mTvPlace.setText(newItemList.getCity() + ", " + newItemList.getCountry());
        } else {
            mTvPlace.setVisibility(GONE);
        }

        if (!TextUtils.isEmpty(newItemList.getRole_heading())) {
            mTvJobRoleHeading.setText(newItemList.getRole_heading());
        } else {
            mTvJobRoleHeading.setVisibility(GONE);
        }


        if (!TextUtils.isEmpty(newItemList.getDescription_text())) {
            mTvJobDescription.setText(newItemList.getDescription_text());
        } else {
            mTvJobDescription.setVisibility(GONE);
        }


        if (newItemList.getJobRole() != null && newItemList.getJobRole().size()>0) {
            mCourseNewItemsAdapter = new CourseNewItemsAdapter(newItemList.getJobRole(), newItemType);

            mItemListRecyclerView.setAdapter(mCourseNewItemsAdapter);

        }


    }

    //  initialize course detail description view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.mentor_job_history, this);
        mContext = context;

        mTvNewItemHeading = view.findViewById(R.id.tv_detail_title);
        mLineBelowHeading = view.findViewById(R.id.view_below_heading);
        mIvDegree = view.findViewById(R.id.iv_degree);
        mTvDesiganation = view.findViewById(R.id.tv_designation);
        mItemListRecyclerView = view.findViewById(R.id.listView);
        mTvComapany = view.findViewById(R.id.tv_company);
        mTvDate = view.findViewById(R.id.tv_job_date);
        mTvPlace = view.findViewById(R.id.tv_job_place);
        mTvJobDescriptionHeading = view.findViewById(R.id.tv_job_description_heading);
        mTvJobDescription = view.findViewById(R.id.tv_job_description);
        mTvJobRoleHeading = view.findViewById(R.id.tv_job_role_heading);
        mLineDivider = view.findViewById(R.id.view_divider);
        mItemListRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mItemListRecyclerView.setNestedScrollingEnabled(false);
    }


}
