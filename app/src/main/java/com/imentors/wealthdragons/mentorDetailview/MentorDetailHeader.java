package com.imentors.wealthdragons.mentorDetailview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MentorDetailHeader extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvMentorTitle, mTvMentorPunchLine;
    private FrameLayout mBtShare;
    private boolean mIsDownload;


    public MentorDetailHeader(Context context) {
        super(context);
        mContext = context;
    }

    public MentorDetailHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorDetailHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorDetailHeader(Context context, DashBoardMentorDetails dashBoardDetails, boolean isDownload) {
        super(context);
        mIsDownload = isDownload;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardMentorDetails dashBoardDetails) {

        if(dashBoardDetails.isSocial_sharing() && !mIsDownload){
            mBtShare.setVisibility(View.VISIBLE);
        }else{
            mBtShare.setVisibility(View.GONE);
        }

        mTvMentorTitle.setText(dashBoardDetails.getProfile().getName());
        mTvMentorPunchLine.setText(dashBoardDetails.getProfile().getPunch_line());



        mBtShare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIsDownload) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }else{

                    mBtShare.setEnabled(false);
                    Utils.callEventLogApi("clicked <b>Share</b> from <b>" + dashBoardDetails.getProfile().getName() + "</b> " + Constants.TYPE_MENTOR + " type");
                    Utils.shareArticleUrl(dashBoardDetails.getProfile().getShare_url(), mContext, mContext.getString(R.string.share_expert));
                }

            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if(hasWindowFocus){
            mBtShare.setEnabled(true);
        }
    }

    // initialize mentor detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);



        View view = inflater.inflate(R.layout.mentor_detail_header, this);
        mContext = context;

        // heading
        mTvMentorTitle = view.findViewById(R.id.tv_mentor_name);
        mTvMentorPunchLine = view.findViewById(R.id.tv_mentor_punch_line);
        mBtShare = view.findViewById(R.id.fl_btn_share);


    }

}
