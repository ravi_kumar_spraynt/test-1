package com.imentors.wealthdragons.mentorDetailview;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2.Status;
import com.tonyodev.fetch2core.Extras;
import com.tonyodev.fetch2core.Func;
import com.tonyodev.fetch2core.MutableExtras;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MentorDigitalVideo extends LinearLayout {


    private Context mContext;


    private ImageView mIvVideoPlay;
    private ImageView mIvCourseImage;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private FrameLayout mFullScreenButton, mDialogFullScreenButton;
    private ImageView mFullScreenIcon;
    private SimpleExoPlayer mPlayer;
    private DashBoardMentorDetails mDashBoardDetails;

    private long mResumePosition;
    private String mVideoUrl;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen, mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private PlayerView mDialogplayerView;
    private boolean isPotrait;
    private boolean isLandScape;
    private WealthDragonTextView mVideoError;
    private ImageButton mPlayImageButton;

    private String mVideoNameForListenerLog;
    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private RelativeLayout mLlHeadingContainer;
    private TextView mTvEClassNameSmallVideo;
    private ProgressBar mProgressBar, mDownloadProgressBar, mDownloadBar;
    private String mImageUrl, mBannerImagePath;
    private RelativeLayout rlFrameError;
    private boolean isBufferering, mVideoStoppped, mIsVideoDownloaded, mIsVideoDownloading, mIsDownloaded;
    private Activity mActivity;
    private MentorDigitalVideo.OnVideoPlayListener onVideoPlayListener = null;
    private ImageView mDownloadIcon, mDownloadGrey;
    private com.tonyodev.fetch2.Request mRequset;
    private Fetch mFetch;
    private List<Download> mDownLoadItemList;
    private String mID, mSavedGroupImage, mGroupImageUrl;
    private WealthDragonTextView mTitle;


    public MentorDigitalVideo(Context context) {
        super(context);
        mContext = context;
    }

    public MentorDigitalVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MentorDigitalVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public MentorDigitalVideo(Context context, DashBoardMentorDetails dashBoardDetails,
                              Activity activity, OnVideoPlayListener videoPlayListener, boolean isDownloaded) {
        super(context);
        mActivity = activity;
        onVideoPlayListener = videoPlayListener;
        mFetch = Utils.getFetchInstance();
        mIsDownloaded = isDownloaded;
        initViews(context);
        bindView(dashBoardDetails);
    }

    public interface OnVideoPlayListener {
        void onVideoPlayed();

    }

    //  set data in view
    private void bindView(final DashBoardMentorDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;

        mID = mDashBoardDetails.getProfile().getId();

        mRequset = new com.tonyodev.fetch2.Request(dashBoardDetails.getProfile().getDigi_intro_video(), mContext.getApplicationContext().getFilesDir() + "/video/" + dashBoardDetails.getProfile().getId());

        mFetch.addListener(fetchListener);

        showDonwloadIcon();

        mVideoNameForListenerLog = "VIP CLUB";
        mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);


        if (!TextUtils.isEmpty(mDashBoardDetails.getProfile().getDigi_intro_video())
                && !mDashBoardDetails.getProfile().getDigi_intro_video().equals("NoVideoExist")) {

            mTitle.setVisibility(VISIBLE);
            mTitle.setText("Download " + dashBoardDetails.getProfile().getName() + " Digital Intro Video");

        }
        else
        {
            mTitle.setVisibility(GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getProfile().getExpert_intro_video()) && !dashBoardDetails.getProfile().getExpert_intro_video().equals("NoVideoExist")) {
            // add only intro video in list
            mVideoUrl = dashBoardDetails.getProfile().getExpert_intro_video();
            mVideoUrls.add(mVideoUrl);
        }


        mImageUrl = dashBoardDetails.getProfile().getIntro_video_banner();
        mGroupImageUrl = dashBoardDetails.getProfile().getBanner();

        Glide.with(mContext).load(dashBoardDetails.getProfile().getIntro_video_banner()).dontAnimate().listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                rlFrameError.setVisibility(VISIBLE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        }).into(mIvCourseImage);


        if (!TextUtils.isEmpty(dashBoardDetails.getProfile().getDigi_intro_video()) && !dashBoardDetails.getProfile().getDigi_intro_video().equals("NoVideoExist")) {
            mIvVideoPlay.setVisibility(View.VISIBLE);
            mExoPlayerView.setVisibility(View.VISIBLE);
            mVideoError.setVisibility(INVISIBLE);
            rlFrameError.setVisibility(INVISIBLE);

            initFullscreenButton();
            initFullscreenDialog();

        } else {
            mIvCourseImage.setVisibility(INVISIBLE);

            mIvVideoPlay.setVisibility(View.INVISIBLE);
            rlFrameError.setVisibility(VISIBLE);

        }

        mResumePosition = AppPreferences.getVideoResumePosition(mDashBoardDetails.getProfile().getId());

        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {
                    initFullscreenDialog();
                    if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                        Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

                    } else {

                        mIvVideoPlay.setVisibility(GONE);
                        if (mIsDownloaded && !TextUtils.isEmpty(dashBoardDetails.getProfile().getSavedVideoAddress())) {
                            mVideoUrl = dashBoardDetails.getProfile().getSavedVideoAddress();
                            initExoPlayer();
                        } else {
                            callVideoUrl(dashBoardDetails.getProfile().getId());
                        }

                        Utils.callEventLogApi("clicked <b> play button </b> of  " + dashBoardDetails.getProfile().getName());

                    }
                } else {

                    if (mIsDownloaded) {

                        if(mDownloadGrey.getVisibility() == VISIBLE){
                            mVideoUrl = dashBoardDetails.getProfile().getSavedVideoAddress();
                            mIvVideoPlay.setVisibility(GONE);
                            initExoPlayer();
                        }else{
                            Toast.makeText(getApplicationContext(), R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();
                        }



                    } else {
                        if (isLandScape) {
                            closeFullscreenDialog();
                        }
                        Utils.showNetworkError(mContext, new NoConnectionError());
                    }


                }
            }
        });


        mDownloadIcon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(mContext)) {

                    mIsVideoDownloading = true;
                    mDownloadProgressBar.setProgress(0);
                    mDownloadProgressBar.setVisibility(GONE);
                    mDownloadIcon.setVisibility(GONE);
                    mDownloadBar.setVisibility(VISIBLE);

                    Toast.makeText(getApplicationContext(), R.string.download_added, Toast.LENGTH_SHORT).show();
                    Utils.callEventLogApi("downloading started  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");

                    new MentorDigitalVideo.startGroupImageDownload().execute();

                    new MentorDigitalVideo.startImageDownload().execute();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.error_api_no_internet_connection, Toast.LENGTH_SHORT).show();

                }


            }
        });

        mDownloadGrey.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading removed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadIcon.setVisibility(VISIBLE);
                        mDownloadBar.setVisibility(GONE);
                        mDownloadProgressBar.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.remove), getContext().getString(R.string.remove_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));


            }
        });

        mDownloadBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading cancelled  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadIcon.setVisibility(VISIBLE);
                        mDownloadBar.setVisibility(GONE);
                        mDownloadProgressBar.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));


            }
        });

        mDownloadProgressBar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                    @Override
                    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
                        Toast.makeText(getContext(), R.string.download_canceled, Toast.LENGTH_SHORT).show();
                        Utils.callEventLogApi("downloading cancelled  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");


                        mFetch.delete(mRequset.getId());
                        mDownloadIcon.setVisibility(VISIBLE);
                        mDownloadProgressBar.setVisibility(GONE);
                        mDownloadBar.setVisibility(GONE);
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                        dialog.dismiss();
                    }
                }, getContext(), getContext().getString(R.string.cancel), getContext().getString(R.string.cancel_message), getContext().getString(R.string.yes), getContext().getString(R.string.no));

            }
        });


    }

    private void showDonwloadIcon() {

        mFetch.getDownloads(new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {
                mDownLoadItemList = result;
                checkForCourse();
            }
        });


    }

    private void checkForCourse() {


        if (!TextUtils.isEmpty(mDashBoardDetails.getProfile().getDigi_intro_video())
                && !mDashBoardDetails.getProfile().getDigi_intro_video().equals("NoVideoExist")) {
            if (mDownLoadItemList != null && mDownLoadItemList.size() > 0) {
                for (Download downloadTask : mDownLoadItemList) {

                    if (downloadTask.getExtras().getString(Constants.ID, "1").equals(mDashBoardDetails.getProfile().getId() + "Digital")) {
                        if (downloadTask.getStatus() == Status.COMPLETED) {
                            mIsVideoDownloaded = true;

                            mDashBoardDetails.getProfile().setSavedVideoAddress(downloadTask.getFile());

                            mDownloadProgressBar.setVisibility(GONE);
                            mDownloadIcon.setVisibility(GONE);
                            mDownloadGrey.setVisibility(VISIBLE);
                        } else if (downloadTask.getStatus() == Status.FAILED
                                || downloadTask.getStatus() == Status.CANCELLED
                                || downloadTask.getStatus() == Status.DELETED
                                || downloadTask.getStatus() == Status.REMOVED
                        ) {
                            mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                            mDownloadProgressBar.setVisibility(GONE);
                            mDownloadIcon.setVisibility(VISIBLE);
                            mDownloadGrey.setVisibility(GONE);
                        } else {
                            mDashBoardDetails.getProfile().setSavedVideoAddress(downloadTask.getFile());
                            mIsVideoDownloading = true;
                            mDownloadProgressBar.setVisibility(VISIBLE);
                            mDownloadProgressBar.setProgress(downloadTask.getProgress());
                            mDownloadIcon.setVisibility(GONE);
                            mDownloadGrey.setVisibility(GONE);
                        }
                        break;
                    } else {
                        mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                        mDownloadProgressBar.setVisibility(GONE);
                        mDownloadIcon.setVisibility(VISIBLE);
                        mDownloadGrey.setVisibility(GONE);
                    }
                }
            } else {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mDownloadProgressBar.setVisibility(GONE);
                mDownloadIcon.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
            }
        } else {
            mDashBoardDetails.getProfile().setSavedVideoAddress(null);

            mDownloadProgressBar.setVisibility(GONE);
            mDownloadIcon.setVisibility(GONE);
            mDownloadGrey.setVisibility(GONE);
        }


    }

    //  initialize course detail description view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.mentor_digital_video, this);
        mContext = context;


        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mDownloadBar = view.findViewById(R.id.progressBar);

        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mExoPlayerView = findViewById(R.id.exoplayer);
        rlFrameError = view.findViewById(R.id.frame_error);
        mDownloadIcon = view.findViewById(R.id.iv_download_orange);
        mDownloadGrey = view.findViewById(R.id.iv_download_grey);
        mDownloadProgressBar = view.findViewById(R.id.download_bar);
        mTitle = view.findViewById(R.id.tv_title);


        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);


    }


    private void bufferingListener() {
        if (mExoPlayerView.getVisibility() == View.INVISIBLE) {
            mProgressBar.setVisibility(VISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
        }
    }


    private void bufferingStopListener() {
        if (mIsVideoError) {
            mPlayImageButton.setActivated(false);
            mVideoError.setVisibility(VISIBLE);
            rlFrameError.setVisibility(VISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(INVISIBLE);
        } else {
            isBufferering = false;

            if (mExoPlayerView.getVisibility() == View.INVISIBLE) {

                mProgressBar.setVisibility(INVISIBLE);
                mExoPlayerView.setVisibility(VISIBLE);
                mExoPlayerView.setControllerAutoShow(true);
                mExoPlayerView.setUseController(true);
                mIvCourseImage.setVisibility(INVISIBLE);
                mIvCourseImage.setVisibility(INVISIBLE);

            }
        }
    }


    private void videoErrorListener() {
        mIsVideoError = true;
        if (Utils.isNetworkAvailable(mContext)) {

            if (mPlayer != null) {
                if (mPlayer.getPlayWhenReady()) {
                    try {
                        if (isLandScape) {
                            closeFullscreenDialog();
                        }

                        mPlayImageButton.setActivated(false);
                        mIvCourseImage.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        rlFrameError.setVisibility(VISIBLE);

                        mExoPlayerView.setVisibility(View.INVISIBLE);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {

            if (mIsDownloaded) {
                Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

            } else {
                if (isLandScape) {
                    closeFullscreenDialog();
                }
                Utils.showNetworkError(mContext, new NoConnectionError());
            }


        }
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);

        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + "Digital Coaching");

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setVisibility(GONE);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        TextView tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText("VIP CLUB");
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);


        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + "Digital Coaching");

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

        if (mPlayer.getPlaybackState() == Player.STATE_ENDED) {

            mExoPlayerView.setVisibility(GONE);
            mVideoError.setVisibility(View.INVISIBLE);
            rlFrameError.setVisibility(INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mIvCourseImage.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(mDashBoardDetails.getProfile().getIntro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);
            mIvVideoPlay.setVisibility(VISIBLE);
        }

    }

    public void stopVideo() {
        mVideoStoppped = true;
        pauseVideo();

        if (mVideoUrls.size() > 0) {

            if (mPlayer != null) {
                mPlayer.setPlayWhenReady(false);
            }

            if (mExoPlayerView.getVisibility() == View.VISIBLE) {
                mProgressBar.setVisibility(INVISIBLE);
                mExoPlayerView.setVisibility(INVISIBLE);
                mIvCourseImage.setVisibility(VISIBLE);
                mIvVideoPlay.setVisibility(VISIBLE);

                Glide.with(mContext).load(mDashBoardDetails.getProfile().getIntro_video_banner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);
            }
        }

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    mNextVideo.setVisibility(GONE);
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }

    public void pressFullScreenDialogButton() {

        mDialogFullScreenButton.performClick();

    }

    public void pauseVideo() {
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences


        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching", false);

        mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardDetails.getProfile().getDigi_intro_video_seektime());


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();


        mPlayer.clearVideoSurface();

        Utils.callEventLogApi("started video of <b>" + mVideoNameForListenerLog + "</b>");


        mExoPlayerView.setPlayer(mPlayer);


        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {


            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


                if (!mVideoStoppped) {


                    if (playWhenReady) {
                        onVideoPlayListener.onVideoPlayed();
                    } else {
                        Utils.callEventLogApi("paused <b>" + mVideoNameForListenerLog + "</b> mentor profile video ");
                    }


                    if (playbackState == Player.STATE_BUFFERING) {
//                    bufferingListener();
                        mProgressBar.setVisibility(VISIBLE);

                    } else {
                        mProgressBar.setVisibility(INVISIBLE);
                        mExoPlayerView.setVisibility(VISIBLE);
                        mExoPlayerView.setControllerAutoShow(true);
                        mExoPlayerView.setUseController(true);
                        mIvCourseImage.setVisibility(INVISIBLE);
//                    bufferingStopListener();
                    }


                    if (!((BaseActivity) mActivity).isFragmentInBackStack(new DashBoardDetailFragment())) {
                        Utils.releaseDetailScreenPlayer();
                    }

                }

                mVideoStoppped = false;

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");

                } catch (Exception e) {
                    e.printStackTrace();

                }
                videoErrorListener();


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch (reason) {
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mVideoNameForListenerLog + "</b>");

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                    bufferingListener();
                } else {
                    bufferingStopListener();
                }
            }
        });


//        mExoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
//            @Override
//            public void onVisibilityChange(int visibility) {
//                if(visibility == View.VISIBLE){
//                    mDownloadIcon.setVisibility(GONE);
//                    mDownloadProgressBar.setVisibility(GONE);
//                    mDownloadBar.setVisibility(GONE);
//
//                }else{
//
//                    showDonwloadIcon();
//
//                    if(mIsVideoDownloaded){
//                        mDownloadIcon.setVisibility(GONE);
//                        mDownloadProgressBar.setVisibility(GONE);
//
//                    }else {
//                        if (mDownloadProgressBar.getVisibility() == View.VISIBLE || mIsVideoDownloading) {
//                            mDownloadIcon.setVisibility(GONE);
//                            mDownloadProgressBar.setVisibility(VISIBLE);
//
//                        } else {
//                            mDownloadIcon.setVisibility(VISIBLE);
//                            mDownloadProgressBar.setVisibility(GONE);
//
//
//                        }
//                    }

//                }
//            }
//        });


        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        mPlayer.seekTo(0, mResumePosition);


        mPlayer.setPlayWhenReady(true);

    }


    private void callVideoUrl(final String mItemId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer();
                } catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = mVideoUrl;
                    initExoPlayer();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = mVideoUrl;

                initExoPlayer();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.ITEM_TYPE, "DigitalVideoIntro");
                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    public VideoData getCurrentVideoData() {
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    mResumePosition = 0;
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                    mResumePosition = Math.max(0, mPlayer.getContentPosition());
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("DigiIntro");

                videoDataToBeSent.setVideoId(mDashBoardDetails.getProfile().getId());
                AppPreferences.setVideoResumePosition(mDashBoardDetails.getProfile().getId(), mResumePosition);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }


    class startImageDownload extends AsyncTask<Void, Void, Void> {


        File outputFile;


        @Override
        protected void onPostExecute(Void aVoid) {


            DownloadFile mDownLoadTask = new DownloadFile(
                    mRequset.getFile(),
                    null,
                    mDashBoardDetails.getProfile().getPunch_line(),
                    mDashBoardDetails.getProfile().getName(),
                    0,
                    null,
                    mDashBoardDetails.getProfile().getDigi_intro_video(),
                    mDashBoardDetails.getProfile().getId() + "Digital",
                    Constants.TYPE_MENTOR,
                    mDashBoardDetails.getProfile().getIntro_video_banner(),
                    mBannerImagePath,
                    null,
                    mDashBoardDetails.getProfile().getName(),
                    mSavedGroupImage,
                    Constants.TYPE_MENTOR

            );


            mRequset.setGroupId(Integer.valueOf(mDashBoardDetails.getProfile().getId()));

            mRequset.setExtras(getExtrasForRequest(mDownLoadTask));
            Utils.addItemToFetchList(mRequset);


            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                URL url = new URL(mImageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(), mDashBoardDetails.getProfile().getId() + "Digital");//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mBannerImagePath = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(mDashBoardDetails.getProfile().getId() + "Digital", Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }

            return null;
        }
    }

    private Extras getExtrasForRequest(DownloadFile request) {
        final MutableExtras extras = new MutableExtras();
        if (!TextUtils.isEmpty(request.getTitle())) {
            extras.putString(Constants.TITLE, request.getTitle());

        }
        if (!TextUtils.isEmpty(request.getId())) {
            extras.putString(Constants.ID, request.getId());

        }

        if (!TextUtils.isEmpty(request.getmBannerAddress())) {
            extras.putString(Constants.BANNER_PATH, request.getmBannerAddress());

        }

        if (!TextUtils.isEmpty(request.getDescription())) {
            extras.putString(Constants.PUNCH_LINE, request.getDescription());

        }

        if (!TextUtils.isEmpty(request.getMentorName())) {
            extras.putString(Constants.MENTOR_NAME, request.getMentorName());

        }

        if (!TextUtils.isEmpty(request.getVideoUrl())) {
            extras.putString(Constants.VIDEO_URL, request.getVideoUrl());

        }

        if (!TextUtils.isEmpty(request.getBanner())) {
            extras.putString(Constants.IMAGE_URL, request.getBanner());

        }

        if (!TextUtils.isEmpty(request.getVideo_address())) {
            extras.putString(Constants.VIDEO_PATH, request.getVideo_address());

        }

        if (!TextUtils.isEmpty(request.getmGroupBannerAddress())) {
            extras.putString(Constants.GROUP_IMAGE_PATH, request.getmGroupBannerAddress());

        }

        if (!TextUtils.isEmpty(request.getmGroupTitle())) {
            extras.putString(Constants.GROUP_TITLE, request.getmGroupTitle());

        }

        if (!TextUtils.isEmpty(request.getmGroupType())) {
            extras.putString(Constants.GROUP_TYPE, request.getmGroupType());

        }

        extras.putString(Constants.TYPE, request.getType());

        extras.putString(Constants.DATA, new Gson().toJson(mDashBoardDetails));

        if (!TextUtils.isEmpty(request.getmVideoDuration())) {
            extras.putString(Constants.VIDEO_DURATION, request.getmVideoDuration());
        }
        return extras;
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID + "Digital")) {

                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());
                Toast.makeText(getApplicationContext(), R.string.download_complete, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading completed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");

                mDownloadProgressBar.setVisibility(GONE);
                mDownloadIcon.setVisibility(GONE);
                mDownloadBar.setVisibility(GONE);
                mDownloadGrey.setVisibility(VISIBLE);


            }

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            if (download.getExtras().getString(Constants.ID, "0").equals(mID + "Digital")) {

                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());

                Toast.makeText(getApplicationContext(), R.string.download_failed, Toast.LENGTH_SHORT).show();
                Utils.callEventLogApi("downloading failed  <b>" + mDashBoardDetails.getProfile().getName() + " </b>" + "from mentor detail");
                mDownloadIcon.setVisibility(VISIBLE);
                mDownloadProgressBar.setVisibility(GONE);
                mDownloadBar.setVisibility(GONE);
                mDownloadGrey.setVisibility(GONE);

            }

        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID + "Digital")) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(download.getFile());

                updateProgress(download);
            }

        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

        }


        @Override
        public void onRemoved(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID + "Digital")) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mDownloadProgressBar.setVisibility(GONE);
                mDownloadIcon.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
                mDownloadBar.setVisibility(GONE);

            }
        }

        @Override
        public void onDeleted(@NotNull Download download) {

            if (download.getExtras().getString(Constants.ID, "0").equals(mID + "Digital")) {
                mDashBoardDetails.getProfile().setSavedVideoAddress(null);

                mDownloadProgressBar.setVisibility(GONE);
                mDownloadIcon.setVisibility(VISIBLE);
                mDownloadGrey.setVisibility(GONE);
                mDownloadBar.setVisibility(GONE);


            }
        }

    };

    private void updateProgress(Download progress) {

        mDownloadBar.setVisibility(GONE);
        mDownloadProgressBar.setVisibility(VISIBLE);
        mDownloadIcon.setVisibility(GONE);
        mDownloadGrey.setVisibility(GONE);
        mDownloadProgressBar.setProgress(progress.getProgress());

    }


    private class startGroupImageDownload extends AsyncTask<Void, Void, Void> {

        private File outputFile = null;

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                URL url = new URL(mGroupImageUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection


                outputFile = new File(getApplicationContext().getFilesDir(), mID);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }


                mSavedGroupImage = outputFile.getPath();


                FileOutputStream fos = getApplicationContext().openFileOutput(mID, Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {


                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());


            }
            return null;
        }

    }


}
