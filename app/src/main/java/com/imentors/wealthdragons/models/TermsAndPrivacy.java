package com.imentors.wealthdragons.models;

import android.text.Html;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

public class TermsAndPrivacy implements Serializable {

    private String title;
    private String effective_date;
    private List<Content> content;

    public String getTitle() {
        return title;
    }

    public String getEffectiveDate() {
        return effective_date;
    }

    public List<Content> getContent() {
        return content;
    }


    public class Content {

        private String heading;
        private String description;

        public String getHeading() {
            return heading;
        }

        public String getDescription() {
            return description;
        }
    }
}
