package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.utils.Utils;

import java.io.Serializable;

public class Slider implements Serializable{

    private String id;
    private String heading;
    private String description;
    private String banner;
    private String big_banner;
    private String low_qty_big_banner;
    private String low_qty_banner;

    public String getHeading() {
        if(!TextUtils.isEmpty(heading)) {
            return Utils.removeExtraCharacterFromString(heading);
        }else{
            return null;
        }
    }

    public String getDescription() {
        if(!TextUtils.isEmpty(description)) {
            return Utils.removeExtraCharacterFromString(description);
        }else{
            return null;
        }
    }

    public String getBanner() {
        return banner;
    }

    public String getBig_banner() {
        return big_banner;
    }

    public String getLow_qty_banner() {
        return low_qty_banner;
    }

    public String getLow_qty_big_banner() {
        return low_qty_big_banner;
    }
}
