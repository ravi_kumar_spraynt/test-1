package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class DeepLink implements Serializable{


   private String id;

   private String type;


   public String getType() {
      return type;
   }

   public String getId() {
      return id;
   }
}


