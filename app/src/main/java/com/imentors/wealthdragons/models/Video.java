package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;

public class Video implements Parcelable ,Serializable {

    private String title;
    private String banner;
    private String mobile_banner;
    private String mentor_id;
    private String id;
    private String video_id;
    private String video_ordering;
    private String taste_preference_id;
    private String free_paid;
    private String punch_line;
    private String mentor_name;
    private String mobile_small_banner;
    private String episode;
    private String subscription;
    private String category;
    private String mobile_big_banner;
    private String mobile_small_banner_status;
    private String mobile_big_banner_status;
    private String modified;
    private String created;
    private String status;
    private String featured;
    private String ordering;
    private String alis;
    private String type;
    private String temp_dir_name;
    private String video_name;
    private String video_status;
    private String low_qty_mobile_small_banner;
    private String low_qty_mobile_big_banner;
    private String banner_low_qty;
    private String mobile_small_banner_detail;
    private String low_qty_mobile_small_banner_detail;
    private String wishlist;
    private String layout;



    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getTitle() {
        return title;
    }

    public String getBanner() {
        return banner;
    }

    public String getMobile_banner() {
        return mobile_banner;
    }

    public String getMentor_id() {
        return mentor_id;
    }

    public String getId() {
        return id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public String getVideo_ordering() {
        return video_ordering;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getFree_paid() {
        return free_paid;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getMentor_name() {
        return mentor_name;
    }

    public String getMobile_small_banner() {
        return mobile_small_banner;
    }

    public String getEpisode() {
        return episode;
    }

    public boolean isEpisode(){
        if(!TextUtils.isEmpty(getEpisode())) {
            return getEpisode().equals(WealthDragonsOnlineApplication.sharedInstance().getString(R.string.yes));
        }else{
            return false;
        }
    }

    public String getSubscription() {
        return subscription;
    }



    public boolean isFreePaid(){
        if(!TextUtils.isEmpty(getFree_paid())){
            return getFree_paid().equals("Free");
        }
            return false;
    }



    protected Video(Parcel in) {

        this.title = in.readString();
        this.banner = in.readString();
        this.mobile_banner = in.readString();
        this.mentor_id = in.readString();
        this.id = in.readString();
        this.video_id = in.readString();
        this.video_ordering = in.readString();
        this.taste_preference_id = in.readString();
        this.free_paid = in.readString();
        this.punch_line = in.readString();
        this.mentor_name = in.readString();
        this.mobile_small_banner = in.readString();
        this.episode = in.readString();
        this.subscription = in.readString();
        this.category = in.readString();
        this.mobile_big_banner = in.readString();
        this.mobile_small_banner_status = in.readString();
        this.mobile_big_banner_status = in.readString();
        this.modified = in.readString();
        this.created = in.readString();
        this.status = in.readString();
        this.featured = in.readString();
        this.ordering = in.readString();
        this.alis = in.readString();
        this.type = in.readString();
        this.temp_dir_name = in.readString();
        this.video_name = in.readString();
        this.video_status = in.readString();
        this.low_qty_mobile_small_banner = in.readString();
        this.low_qty_mobile_big_banner = in.readString();
        this.banner_low_qty = in.readString();
        this.low_qty_mobile_small_banner_detail = in.readString();
        this.mobile_small_banner_detail = in.readString();

    }

    public static final Creator<Video> CREATOR = new Creator<Video>() {
        @Override
        public Video createFromParcel(Parcel in) {
            return new Video(in);
        }

        @Override
        public Video[] newArray(int size) {
            return new Video[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.title);
        dest.writeString(this.banner);
        dest.writeString(this.mobile_banner);
        dest.writeString(this.mentor_id);
        dest.writeString(this.id);
        dest.writeString(this.video_id);
        dest.writeString(this.video_ordering);
        dest.writeString(this.taste_preference_id);
        dest.writeString(this.free_paid);
        dest.writeString(this.punch_line);
        dest.writeString(this.mentor_name);
        dest.writeString(this.mobile_small_banner);
        dest.writeString(this.episode);
        dest.writeString(this.subscription);
        dest.writeString(this.category);
        dest.writeString(this.mobile_big_banner);
        dest.writeString(this.mobile_small_banner_status);
        dest.writeString(this.mobile_big_banner_status);
        dest.writeString(this.modified);
        dest.writeString(this.created);
        dest.writeString(this.status);
        dest.writeString(this.featured);
        dest.writeString(this.ordering);
        dest.writeString(this.alis);
        dest.writeString(this.type);
        dest.writeString(this.temp_dir_name);
        dest.writeString(this.video_name);
        dest.writeString(this.video_status);
        dest.writeString(this.low_qty_mobile_small_banner);
        dest.writeString(this.low_qty_mobile_big_banner);
        dest.writeString(this.banner_low_qty);
        dest.writeString(this.low_qty_mobile_small_banner_detail);
        dest.writeString(this.mobile_small_banner_detail);

    }

    public String getCategory() {
        return category;
    }

    public String getMobile_big_banner() {
        return mobile_big_banner;
    }

    public String getMobile_small_banner_status() {
        return mobile_small_banner_status;
    }

    public String getMobile_big_banner_status() {
        return mobile_big_banner_status;
    }

    public String getModified() {
        return modified;
    }

    public String getCreated() {
        return created;
    }

    public String getStatus() {
        return status;
    }

    public String getFeatured() {
        return featured;
    }

    public String getOrdering() {
        return ordering;
    }

    public String getAlis() {
        return alis;
    }

    public String getType() {
        return type;
    }

    public String getTemp_dir_name() {
        return temp_dir_name;
    }

    public String getVideo_name() {
        return video_name;
    }

    public String getVideo_status() {
        return video_status;
    }

    public String getLow_qty_banner() {
        return low_qty_mobile_small_banner;
    }

    public String getLow_qty_bigBanner() {
        return low_qty_mobile_big_banner;
    }


    public String getBanner_low_qty() {
        return banner_low_qty;
    }

    public String getLow_qty_mobile_small_banner_detail() {
        return low_qty_mobile_small_banner_detail;
    }

    public String getMobile_small_banner_detail() {
        return mobile_small_banner_detail;
    }

    public String getLayout() {
        return layout;
    }
}
