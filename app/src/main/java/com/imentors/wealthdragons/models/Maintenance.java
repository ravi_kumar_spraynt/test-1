package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class Maintenance implements Serializable{

    private String heading;
    private String logo;
    private String status;
    private String text;

    public String getHeading() {
        return heading;
    }

    public String getLogo() {
        return logo;
    }

    public String getStatus() {
        return status;
    }

    public String getText() {
        return text;
    }
}
