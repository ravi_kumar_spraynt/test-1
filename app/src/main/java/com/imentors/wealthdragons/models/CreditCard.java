package com.imentors.wealthdragons.models;

public class CreditCard {

    private String card_no;
    private String ex_month;
    private String ex_year;
    private String cvv;
    private String id;
    private String card_type;


    public String getCard_no() {
        return card_no;
    }

    public String getEx_month() {
        return ex_month;
    }

    public String getCvv() {
        return cvv;
    }

    public String getEx_year() {
        return ex_year;
    }

    public String getId() {
        return id;
    }

    public String getCard_type() {
        return card_type;
    }
}
