package com.imentors.wealthdragons.models;

import android.content.Context;
import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Categories implements Serializable{

    private List<Category> Category;
    private UserData UserData;
    private String is_subscribed;
    private String free_member;
    private int remaining_days;
    private boolean is_static_content = false;
    private boolean categories;
    private boolean my_wishlist;
    private boolean messages;
    private boolean tast_preference;
    private boolean about_us;
    private boolean settings;
    private boolean payment_history;
    private boolean my_badges;
    private boolean subscription_tab;
    private boolean customer_support;
    private boolean purchase;
    private boolean subscription_pop_up;
    private String subs_popup_duration;



    public Categories(List<Category> category , UserData userData , String is_subscribed , String  free_member , int remaining_days ){
        this.Category = category;
        this.UserData = userData;
        this.is_subscribed = is_subscribed;
        this.free_member = free_member;
        this.remaining_days = remaining_days;

        // set true when you are sending static categories
        this.is_static_content = true;
    }

    public Categories(){

    }

    public List<com.imentors.wealthdragons.models.Category> getCategory() {

        return Category;
    }


    public List<com.imentors.wealthdragons.models.Category> getAllCategory() {

        List<Category> onlineCategory = new ArrayList<>();


        if(isCategories()){
            onlineCategory.addAll(getCategory());
        }


        if(!is_static_content) {
            onlineCategory.addAll(Utils.addStaticCategories(AppPreferences.getCategories()));
        }
        return onlineCategory;
    }


    public int getStaticCategoriesSize(){
        return Utils.addStaticCategories(AppPreferences.getCategories()).size();
    }

    public Categories.UserData getUserData() {
        return UserData;
    }

    public String getIsSubscribed() {
        return is_subscribed;
    }

    public String getFreeMember() {
        return free_member;
    }

    public int getRemainingDays() {
        return remaining_days;
    }

    public boolean isIs_static_content() {
        if(!isCategories()){
            return true;
        }
        return is_static_content;
    }

    public boolean isCategories() {
        return categories;
    }

    public boolean isMy_wishlist() {
        return my_wishlist;
    }

    public boolean isMessages() {
        return messages;
    }

    public boolean isTast_preference() {
        return tast_preference;
    }

    public boolean isAbout_us() {
        return about_us;
    }

    public boolean isSubscription_pop_up() {
        return subscription_pop_up;
    }

    public String getSubs_popup_duration() {
        return subs_popup_duration;
    }


    public class UserData implements Serializable{
        private String user_name;
        private String user_image;
        private int unread_messages;

        public String getUserName() {
            return user_name;
        }

        public String getUserImage() {
            return user_image;
        }

        public int getUnreadMessages() {
            return unread_messages;
        }
    }

    public String getIs_subscribed() {
        return is_subscribed;
    }

    public String getFree_member() {
        return free_member;
    }

    public int getRemaining_days() {
        return remaining_days;
    }

    public boolean isSettings() {
        return settings;
    }

    public boolean isPayment_history() {
        return payment_history;
    }

    public boolean isMy_badges() {
        return my_badges;
    }

    public boolean isSubscription_tab() {
        return subscription_tab;
    }

    public boolean isCustomer_support() {
        return customer_support;
    }

    public boolean isPurchased() {
        return purchase;
    }

    public boolean showSubscriptionPopUp(){
        // checking for free and lifetime user
        User user = AppPreferences.getUser();
        if(("7".equals(user.getUserGroupId()))||("8".equals(user.getUserGroupId()))) {
            return false;
        }else{
            // checking for subscription
            String isSubscribed;
            if(TextUtils.isEmpty(getIs_subscribed())){
                isSubscribed =  AppPreferences.getCategories().is_subscribed;
            }else{
                isSubscribed = getIsSubscribed();
            }
            if (isSubscribed.equals(WealthDragonsOnlineApplication.sharedInstance().getString(R.string.no)) && AppPreferences.getSubscriptionPopUpState()) {
                return isSubscription_pop_up();
            }
        }
        return false;


    }

}
