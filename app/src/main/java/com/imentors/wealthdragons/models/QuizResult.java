package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class QuizResult implements Serializable {



    private QuizResultData Result;

    public QuizResultData getQuiz() {
        return Result;
    }


    public class QuizResultData implements Serializable{
        private String quiz_title;
        private String course_title;
        private List<Attempt> Attempt;


        public String getCourse_title() {
            return course_title;
        }


        public List<QuizResult.Attempt> getAttempt() {
            return Attempt;
        }

        public String getQuiz_title() {
            return quiz_title;
        }
    }

    public class Attempt implements Serializable{
        private String Heading;
        private int Correct;
        private int Incorrect;
        private List<Question> Question;

        public List<QuizResult.Question> getQuestion() {
            return Question;
        }

        public int getIncorrect() {
            return Incorrect;
        }

        public int getCorrect() {
            return Correct;
        }

        public String getHeading() {
            return Heading;
        }
    }


    public class Question implements Serializable{
        private String type;
        private String question;
        private String answer;
        private String correct_answer;

        public String getType() {
            return type;
        }

        public String getQuestion() {
            return question;
        }

        public String getAnswer() {
            return answer;
        }


        public String getCorrect_answer() {
            return correct_answer;
        }
    }

}
