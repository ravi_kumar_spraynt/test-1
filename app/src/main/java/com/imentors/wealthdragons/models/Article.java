package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.io.Serializable;

public class Article implements Parcelable,Serializable {

    private String id;
    private String title;
    private String mentor_id;
    private String taste_preference_id;
    private String punch_line;
    private String free_paid;
    private String banner;
    private String banner_status;
    private String mobile_banner;
    private String cost;
    private String discount_cost;
    private String final_cost;
    private String mentor_name;
    private String tag_line;
    private String mentor_banner;
    private float rating;
    private int rating_persons;
    private float percentage;
    private String purchase;
    private String product_id;
    private String reference_name;
    private String subscription;
    private String subscribe_msg;
    private String course_id;
    private String low_qty_banner;
    private String banner_low_qty;
    private String detail_banner;
    private String low_qty_detail_banner;
    private String big_banner;
    private String low_qty_big_banner;
    private String currency;
    private String pre_booking_date;
    private String pre_booking_status;
    private String push_live;
    private String wishlist;
    private String layout;
    private String savedResponse;
    private boolean isDownloaded;


    public Article(String id, String name, String banner, String mentor_name, String punch_line, String savedResponse, boolean isDownloaed){
        this.id = id;
        this.title = name;
        this.banner = banner;
        this.mentor_name = mentor_name;
        this.punch_line = punch_line;
        this.savedResponse = savedResponse;
        this.isDownloaded = isDownloaed;
    }


    public String getWishlist() {
        return wishlist;
    }



    public String getPush_live() {
        return push_live;
    }

    public String getPre_booking_date() {
        return pre_booking_date;
    }

    public String getPre_booking_status() {
        return pre_booking_status;
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMentor_id() {
        return mentor_id;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getFree_paid() {
        return free_paid;
    }

    public String getBanner() {
        return banner;
    }

    public String getBanner_status() {
        return banner_status;
    }

    public String getMobile_banner() {
        return mobile_banner;
    }

    public String getCost() {
        return cost;
    }

    public String getDiscount_cost() {
        return discount_cost;
    }

    public String getFinal_cost() {
        return final_cost;
    }

    public String getMentor_name() {
        return mentor_name;
    }

    public String getTag_line() {
        return tag_line;
    }

    public String getMentor_banner() {
        return mentor_banner;
    }

    public float getRating() {
        return rating;
    }

    public int getRating_persons() {
        return rating_persons;
    }

    public float getPercentage() {
        return percentage;
    }

    public String getPurchase() {
        return purchase;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getReference_name() {
        return reference_name;
    }

    public String getSubscription() {
        return subscription;
    }

    public String getSubscribe_msg() {
        return subscribe_msg;
    }

    public String getCategory() {
        return category;
    }

    private String category;



    public boolean isDiscountAvailable(){

        return !TextUtils.isEmpty(getFinal_cost()) && !TextUtils.isEmpty(getCost()) && !TextUtils.equals(getCost(), getFinal_cost()) && !TextUtils.isEmpty(free_paid) && TextUtils.equals("Paid", free_paid);
    }



    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    protected Article(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.mentor_id = in.readString();
        this.taste_preference_id = in.readString();
        this.punch_line = in.readString();
        this.free_paid = in.readString();
        this.banner = in.readString();
        this.banner_status = in.readString();
        this.mobile_banner = in.readString();
        this.cost = in.readString();
        this.discount_cost = in.readString();
        this.final_cost = in.readString();
        this.mentor_name = in.readString();
        this.tag_line = in.readString();
        this.mentor_banner = in.readString();
        this.purchase = in.readString();
        this.product_id = in.readString();
        this.reference_name = in.readString();
        this.subscription = in.readString();
        this.subscribe_msg = in.readString();
        this.category = in.readString();
        this.rating = in.readFloat();
        this.rating_persons = in.readInt();
        this.percentage = in.readFloat();
        this.course_id = in.readString();
        this.low_qty_banner = in.readString();
        this.banner_low_qty = in.readString();
        this.big_banner = in.readString();
        this.low_qty_big_banner = in.readString();
        this.low_qty_detail_banner = in.readString();
        this.detail_banner = in.readString();
        this.currency = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if(!TextUtils.isEmpty(this.id)){
            dest.writeString(this.id);
        }
        if(!TextUtils.isEmpty(this.title)) {
            dest.writeString(this.title);
        }
        if(!TextUtils.isEmpty(this.mentor_id)) {
            dest.writeString(this.mentor_id);

        }
        if(!TextUtils.isEmpty(this.taste_preference_id)) {
            dest.writeString(this.taste_preference_id);

        }
        if(!TextUtils.isEmpty(this.punch_line)) {
            dest.writeString(this.punch_line);

        }
        if(!TextUtils.isEmpty(this.free_paid)) {
            dest.writeString(this.free_paid);

        }
        if(!TextUtils.isEmpty(this.banner)) {
            dest.writeString(this.banner);
        }
        if(!TextUtils.isEmpty(this.banner_status)) {
            dest.writeString(this.banner_status);
        }
        if(!TextUtils.isEmpty(this.mobile_banner)) {
            dest.writeString(this.mobile_banner);

        }
        if(!TextUtils.isEmpty(this.cost)) {
            dest.writeString(this.cost);

        }
        if(!TextUtils.isEmpty(this.discount_cost)) {
            dest.writeString(this.discount_cost);

        }
        if(!TextUtils.isEmpty(this.final_cost)) {
            dest.writeString(this.final_cost);

        }
        if(!TextUtils.isEmpty(this.mentor_name)) {
            dest.writeString(this.mentor_name);

        }
        if(!TextUtils.isEmpty(this.tag_line)) {
            dest.writeString(this.tag_line);


        }
        if(!TextUtils.isEmpty(this.mentor_banner)) {
            dest.writeString(this.mentor_banner);

        }
        if(!TextUtils.isEmpty(this.purchase)) {
            dest.writeString(this.purchase);

        }
        if(!TextUtils.isEmpty(this.product_id)) {
            dest.writeString(this.product_id);

        }
        if(!TextUtils.isEmpty(this.reference_name)) {
            dest.writeString(this.reference_name);

        }
        if(!TextUtils.isEmpty(this.subscription)) {
            dest.writeString(this.subscription);

        }
        if(!TextUtils.isEmpty(this.subscribe_msg)) {
            dest.writeString(this.subscribe_msg);

        }
        if(!TextUtils.isEmpty(this.category)) {
            dest.writeString(this.category);

        }
        dest.writeFloat(this.rating);

        dest.writeInt(this.rating_persons);

        dest.writeFloat(this.percentage);

        dest.writeString(this.course_id);

        dest.writeString(this.low_qty_banner);

        dest.writeString(this.banner_low_qty);

        dest.writeString(this.big_banner);
        dest.writeString(this.low_qty_big_banner);
        dest.writeString(this.detail_banner);
        dest.writeString(this.low_qty_detail_banner);
        dest.writeString(this.currency);

    }

    public String getCourse_id() {
        return course_id;
    }

    public String getLow_qty_banner() {
        return low_qty_banner;
    }

    public String getBanner_low_qty() {
        return banner_low_qty;
    }

    public String getDetail_banner() {
        return detail_banner;
    }

    public String getLow_qty_detail_banner() {
        return low_qty_detail_banner;
    }

    public String getBig_banner() {
        return big_banner;
    }

    public String getLow_qty_big_banner() {
        return low_qty_big_banner;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getLayout() {
        return layout;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public String getSavedResponse() {
        return savedResponse;
    }
}
