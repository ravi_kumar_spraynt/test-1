package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class SearchAutoComplete implements Serializable{

   private String id;
   private String name;
   private String type;
   private String tick;
   private String icon;

   public String getId() {
      return id;
   }

   public String getName() {
      return name;
   }

   public String getType() {
      return type;
   }

   public String getTick() {
      return tick;
   }

   public String getIcon() {
      return icon;
   }
}


