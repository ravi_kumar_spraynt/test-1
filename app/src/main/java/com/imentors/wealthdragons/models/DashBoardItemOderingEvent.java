package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.List;

public class DashBoardItemOderingEvent extends DashBoardItemOdering{

    private String banner;

    private List<Event> items;

    private List<Event> Events;

    private List<Event> events;

    private List<Video> videos;




    public List<Event> getItems() {
        return items;
    }

    private List<com.imentors.wealthdragons.models.SubCategory> SubCategory;

    public List<com.imentors.wealthdragons.models.SubCategory> getSubCategory() {
        return SubCategory;
    }

    public String getBanner() {
        return banner;
    }

    public List<Event> getEvents() {
        return Events;
    }

    public List<Event> getSmallLeterEvents() {
        return events;
    }


    public String getTitle(){
        return  WealthDragonsOnlineApplication.sharedInstance().getString(R.string.events);
    }

    public List<Video> getVideos() {
        return videos;
    }



}
