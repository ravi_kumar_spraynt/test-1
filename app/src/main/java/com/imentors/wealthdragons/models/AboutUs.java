package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class AboutUs implements Serializable{


   private String version;

   public String getVersion() {
      return version;
   }

   public String getAbout_us_text() {
      return about_us_text;
   }

   private String about_us_text;


}


