package com.imentors.wealthdragons.models;

import android.text.TextUtils;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

public class Feeds implements Serializable {


    private FeedData Feeds;

    public FeedData getFeeds() {
        return Feeds;
    }


    public class FeedData extends DashBoardItemOdering implements Serializable{
        private List<Feed> Feeds;
        private String next_video;

        public boolean isAutoPlay(){
            return !TextUtils.isEmpty(next_video) && TextUtils.equals(next_video, "Auto Play");
        }

        public List<Feed> getFeeds() {
            return Feeds;
        }

    }


    public class Feed implements Serializable {
        private String title;
        private String banner;
        private String id;
        private String video;
        private String created;
        private String duration;
        private String seektime;

        public String getId() {
            return id;
        }

        public String getBanner() {
            return banner;
        }

        public String getTitle() {
            return title;
        }

        public String getVideo() {
            return video;
        }

        public String getCreated() {
            return created;
        }

        public String getDuration() {
            return duration;
        }

        public String getSeektime() {
            return seektime;
        }
    }
}

