package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class EclassesType implements Serializable {

    private String type;
    private String banner;


    public String getBanner() {
        return banner;
    }

    public String getType() {
        return type;
    }
}
