package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import java.io.Serializable;

public class Settings implements Serializable {

    private String data_use;
    private String next_episode;

    public String getData_use() {
        return data_use;
    }

    public String getNext_episode() {
        return next_episode;
    }
}
