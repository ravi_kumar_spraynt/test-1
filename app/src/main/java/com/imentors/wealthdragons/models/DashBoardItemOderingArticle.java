package com.imentors.wealthdragons.models;

import java.util.List;

public class DashBoardItemOderingArticle extends DashBoardItemOdering{

    private String banner;
    private List<com.imentors.wealthdragons.models.SubCategory> SubCategory;
    private List<Article> Courses;
    private List<Article> Programmes;

    private List<Article> items;

    public List<Article> getItems() {
        return items;
    }

    public List<Article> getCourses() {
        return Courses;
    }

    public List<com.imentors.wealthdragons.models.SubCategory> getSubCategory() {
        return SubCategory;
    }

    public String getBanner() {
        return banner;
    }

    public List<Article> getProgrammes() {
        return Programmes;
    }
}
