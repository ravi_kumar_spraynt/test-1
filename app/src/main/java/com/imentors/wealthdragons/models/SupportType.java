package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class SupportType implements Serializable {



    private List<ExistingSupportMessage> Message;


    private String id;
    private String title;
    private String ordering;
    private String status;
    private String created;
    private String modified;

    public List<ExistingSupportMessage> getMessage() {
        return Message;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getOrdering() {
        return ordering;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }



    public static class ExistingSupportMessage implements Serializable{


        public ExistingSupportMessage(String message){
            this.msg_by = "Me";
            this.contact_message = message;
            this.date = "now";
        }

        private String id;
        private String user_id;
        private String parent_id;
        private String ticket_no;
        private String full_name;
        private String contact_email;
        private String contact_mobile;
        private String service_code;
        private String support_type_id;
        private String contact_message;
        private String country;
        private String status;
        private String document;
        private String message_status;
        private String created_by;
        private String created;
        private String modified;
        private String support_type_title;
        private String msg_by;
        private String attachments;
        private String date;
        private int no_replies;


        public String getId() {
            return id;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getParent_id() {
            return parent_id;
        }

        public String getTicket_no() {
            return ticket_no;
        }

        public String getFull_name() {
            return full_name;
        }

        public String getContact_email() {
            return contact_email;
        }

        public String getContact_mobile() {
            return contact_mobile;
        }

        public String getService_code() {
            return service_code;
        }

        public String getSupport_type_id() {
            return support_type_id;
        }

        public String getContact_message() {
            return contact_message;
        }

        public String getCountry() {
            return country;
        }

        public String getStatus() {
            return status;
        }

        public String getDocument() {
            return document;
        }

        public String getDate() {
            String datetext = null;
            if(!TextUtils.isEmpty(date)){
                if(date.equals("now")){
                    datetext = WealthDragonsOnlineApplication.sharedInstance().getString(R.string.posted)+" "+date;
                }else{
                    datetext = WealthDragonsOnlineApplication.sharedInstance().getString(R.string.posted_on)+" "+date;

                }
            }
            return datetext;
        }

        public String getMessage_status() {

            if(message_status.equals("1")){
                return  WealthDragonsOnlineApplication.sharedInstance().getString(R.string.open);
            }else{
                return  WealthDragonsOnlineApplication.sharedInstance().getString(R.string.closed);
            }

        }

        public String getCreated_by() {

            return  created_by;

        }

        public String getCreated() {
            return created;
        }

        public String getModified() {
            return modified;
        }

        public String getSupport_type_title() {
            return support_type_title;
        }

        public String getMsg_by() {
            return msg_by;
        }

        public String getAttachments() {
            return attachments;
        }

        public int getNo_replies() {
            return no_replies;
        }
    }



}
