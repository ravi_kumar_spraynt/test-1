package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class DashBoardPlanSetting implements Serializable{

    private String subs_btn_name;
    private String tag_line;
    private String id;
    private String paid_item_short_alert;

    public String getSubs_btn_name() {
        return subs_btn_name;
    }

    public String getTag_line() {
        return tag_line;
    }

    public String getId() {
        return id;
    }

    public String getPaid_item_short_alert() {
        return paid_item_short_alert;
    }

    public String getCommon_btn() {
        return common_btn;
    }

    private String common_btn;

}
