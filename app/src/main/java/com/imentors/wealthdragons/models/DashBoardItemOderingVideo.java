package com.imentors.wealthdragons.models;

import java.util.List;

public class DashBoardItemOderingVideo extends DashBoardItemOdering {


    private String banner;
    private List<com.imentors.wealthdragons.models.SubCategory> SubCategory;
    private List<Video> Videos;
    private List<Video> videos;


    public List<Video> getItems() {
        return items;
    }

    private List<Video> items;


    public List<Video> getVideos() {
        return Videos;
    }

    public List<Video> getSmallLetterVideos() {
        return videos;
    }


    public List<com.imentors.wealthdragons.models.SubCategory> getSubCategory() {
        return SubCategory;
    }

    public String getBanner() {
        return banner;
    }
}
