package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class DashBoardVideos implements Serializable{


   private List<DashBoardItemOderingVideo> All_Videos;

   private boolean show_free_video_name;
   private boolean show_free_video_mentor_name;
   private boolean wishlist;


   // layout type mentor
   public boolean isMentorTypeLayoutVisible(){
       return isShow_free_video_mentor_name() || isShow_free_video_name();
   }


   public List<DashBoardItemOderingVideo> getAll_Videos() {
      return All_Videos;
   }

   public boolean isShow_free_video_name() {
      return show_free_video_name;
   }

   public boolean isShow_free_video_mentor_name() {
      return show_free_video_mentor_name;
   }

   public boolean isWishlist() {
      return wishlist;
   }
}


