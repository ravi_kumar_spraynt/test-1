package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class DashBoard  implements Serializable{


   private List<DashBoardSlider> Slider;
   private List<DashBoardItemOdering> HeadingOrdering;
   private DashBoardPlanSetting PlanSetting;
   private String is_subscribed;
   private boolean show_ratings;
   private boolean show_course_name;
   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean isSldingAnimation;
   private boolean show_free_video_name;
   private boolean show_free_video_mentor_name;
   private boolean show_mentor_mentor_name;
   private boolean show_mentor_mentor_tagline;
   private boolean course;
   private boolean mentor;
   private boolean purchase;
   private String content_view;
   private boolean video;
   private boolean wishlist;
   private String category_title;
   private List<SubCategory> Subcategories;
   private String next_episode;



   public List<DashBoardSlider> getSlider() {
      return Slider;
   }

   public List<DashBoardItemOdering> getHeadingOrdering() {
      return HeadingOrdering;
   }

   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public String getIsSubscribed() {
      return is_subscribed;
   }

   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }


   public boolean isShowMentorMentorTagline() {
      return show_mentor_mentor_tagline;
   }

   public boolean isShowMentorMentorName() {
      return show_mentor_mentor_name;
   }

   public boolean isShowFreeVideoName() {
      return show_free_video_name;
   }

   public boolean isShowFreeVideoMentorName() {
      return show_free_video_mentor_name;
   }


   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();

   }

   public boolean isSldingAnimation() {
      if(WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)){

         // start slider animation if count is greater than 4 in tablet
          return getSlider().size() > 2;
      }


      else{

         // start slider animation if count is greater than 2 in mobile
          return getSlider().size() > 1;
      }

   }


   public boolean isVideoLayoutVisible(){
       return isShowFreeVideoName() || isShowFreeVideoMentorName();
   }


   // layout type mentor
   public boolean isMentorTypeLayoutVisible(){
       return isShowMentorMentorName() || isShowMentorMentorTagline();
   }


   public boolean isCourseTabVisible() {
      return course;
   }

   public boolean isMentorTabVisible() {
      return mentor;
   }

   public boolean isPurchaseTabVisible() {
      return purchase;
   }

   public boolean isVideoTabVisible() {
      return video;
   }

   public String getCategory_title() {
      return category_title;
   }

   public List<SubCategory> getSubcategories() {
      return Subcategories;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public String getContent_view() {
      return content_view;
   }

   public String getContetnType(){
      if(!TextUtils.isEmpty(getContent_view())){
         switch(getContent_view()){
            case "MentorView":
               return Constants.TYPE_MENTOR;
            case "CourseView":
               return Constants.TYPE_COURSE;
            default:
               return Constants.TYPE_OLD;
         }
      }

      return Constants.TYPE_OLD;

   }

   public String getNext_episode() {
      return next_episode;
   }
}


