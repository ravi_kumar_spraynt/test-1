package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class CourseItemListModel implements Serializable{


   private String text;
   private String heading;
   private String question;
   private String answer;
   private String description;


   public String getText() {
      text = text.replace("\n", "<br>");
      text = text.replace("\t", "  ");
      return text;
   }

   public String getHeading() {
      heading = heading.replace("\n", "<br>");
      heading = heading.replace("\t", "  ");
      return heading;
   }

   public String getQuestion() {
      question = question.replace("\n", "<br>");
      question = question.replace("\t", "  ");
      return question;
   }

   public String getAnswer() {
      answer = answer.replace("\n", "<br>");
      answer = answer.replace("\t", "  ");
      return answer;
   }

   public String getDescription() {
      description = description.replace("\n", "<br>");
      description = description.replace("\t", "  ");
      return description;
   }
}


