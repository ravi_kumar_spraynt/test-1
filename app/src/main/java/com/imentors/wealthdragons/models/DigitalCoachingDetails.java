package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class DigitalCoachingDetails implements Serializable{

   private String main_heading;
   private List<Content> Content;
   private String currency;
   private String cost;
   private String digi_intro_video;
   private String intro_video_banner;
   private String mentor_id;
   private String digi_intro_video_duration;
   private String digi_intro_video_seektime;
   private String digital_coaching_btn_name;


   public String getMentor_id() {
      return mentor_id;
   }

   public String getMain_heading() {
      return main_heading;
   }

   public List<DigitalCoachingDetails.Content> getContent() {
      return Content;
   }

   public String getCurrency() {
      return currency;
   }

   public String getCost() {
      return cost;
   }

   public String getDigi_intro_video() {
      return digi_intro_video;
   }

   public String getIntro_video_banner() {
      return intro_video_banner;
   }

   public String getDigi_intro_video_seektime() {
      return digi_intro_video_seektime;
   }

   public String getDigi_intro_video_duration() {
      return digi_intro_video_duration;
   }

   public void setDigi_intro_video_duration(String digi_intro_video_duration) {
      this.digi_intro_video_duration = digi_intro_video_duration;
   }

   public void setDigi_intro_video_seektime(String digi_intro_video_seektime) {
      this.digi_intro_video_seektime = digi_intro_video_seektime;
   }

   public String getDigital_coaching_btn_name() {
      return digital_coaching_btn_name;
   }


   public static class Content{
      private String heading;
      private String description;
      private String icon;
      private String status;


      public String getHeading() {
         return heading;
      }

      public String getDescription() {
         return description;
      }

      public String getIcon() {
         return icon;
      }

      public String getStatus() {
         return status;
      }
   }

}


