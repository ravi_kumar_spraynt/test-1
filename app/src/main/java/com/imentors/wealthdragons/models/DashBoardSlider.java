package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;

public class DashBoardSlider implements Parcelable {

    private String id;
    private String heading;
    private String description;
    private String mobile_banner;
    private String type;
    private String taste_preference_id;
    private String item_id;
    private String low_qty_banner;
    private String mobile_banner_low_qty;

    public String getId() {
        return id;
    }

    public String getHeading() {
        return heading;
    }

    public String getDescription() {
        return description;
    }

    public String getMobile_banner() {
        return mobile_banner;
    }

    public String getType() {
        return type;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    private String video_id;



    protected DashBoardSlider(Parcel in) {

        this.heading = in.readString();
        this.description = in.readString();
        this.mobile_banner = in.readString();
        this.id = in.readString();
        this.video_id = in.readString();
        this.taste_preference_id = in.readString();
        this.type = in.readString();
        this.item_id = in.readString();
        this.low_qty_banner = in.readString();
        this.mobile_banner_low_qty = in.readString();

    }

    public static final Creator<DashBoardSlider> CREATOR = new Creator<DashBoardSlider>() {
        @Override
        public DashBoardSlider createFromParcel(Parcel in) {
            return new DashBoardSlider(in);
        }

        @Override
        public DashBoardSlider[] newArray(int size) {
            return new DashBoardSlider[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(this.mobile_banner);
        dest.writeString(this.id);
        dest.writeString(this.video_id);
        dest.writeString(this.taste_preference_id);
        dest.writeString(this.heading);
        dest.writeString(this.description);
        dest.writeString(this.type);
        dest.writeString(this.item_id);
        dest.writeString(this.low_qty_banner);
        dest.writeString(this.mobile_banner_low_qty);

    }

    public String getLow_qty_banner() {
        return low_qty_banner;
    }

    public String getMobile_banner_low_qty() {
        return mobile_banner_low_qty;
    }
}
