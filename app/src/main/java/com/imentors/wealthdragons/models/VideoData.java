package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.utils.AppPreferences;

import java.io.Serializable;
import java.util.List;

public class VideoData implements Serializable{

   String videoId;
   String time;
   String type;

   public String getVideoId() {
      return videoId;
   }

   public void setVideoId(String videoId) {
      this.videoId = videoId;
   }

   public String getTime() {
      return time;
   }

   public void setTime(String time) {
      this.time = time;
   }

   public String getType() {
      return type;
   }

   public void setType(String type) {
      this.type = type;
   }

   public String getCountry() {
      return AppPreferences.getUser().getCountry();
   }

}


