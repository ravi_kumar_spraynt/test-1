package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class Quiz implements Serializable {



    private QuizData Quiz;

    public QuizData getQuiz() {
        return Quiz;
    }


    public class QuizData implements Serializable{
        private String title;
        private String course_title;
        private String current_page;
        private List<Question> questions;

        public String getTitle() {
            return title;
        }

        public String getCourse_title() {
            return course_title;
        }

        public String getCurrent_page() {
            return current_page;
        }

        public List<Question> getQuestions() {
            return questions;
        }


    }

    public class Question implements Serializable{
        private String type;
        private String question;
        private String answer;
        private String id;
        private List<String> option;

        public String getType() {
            return type;
        }

        public String getQuestion() {
            return question;
        }

        public String getAnswer() {
            return answer;
        }

        public String getId() {
            return id;
        }

        public List<String> getOption() {
            return option;
        }
    }




}
