package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.dialogFragment.ServiceTypeFragment;

import java.io.Serializable;
import java.util.List;

public class PaymentHistory implements Serializable {


    private List<OrderData> orderData;

    public List<OrderData> getOrderData() {
        return orderData;
    }


    public class OrderData implements Serializable{
        private String id;
        private String invoice_id;
        private String created;
        private String title;
        private String date;
        private String transaction_key;
        private String modified;
        private String status;
        private String subscription_type;
        private String user_id;
        private String item_id;
        private String item_name;
        private String token;
        private String receipt;
        private String email;
        private String coupon_code;
        private String message;
        private String currency;
        private String price;
        private String save_money;
        private String org_price;
        private String pack_start_date_str;
        private String pack_end_date;
        private String pack_end_date_str;
        private String payment_method;
        private String payment_by;
        private String cc_approve;
        private String type;

        public String getInvoice_id() {
            return invoice_id;
        }

        public String getCreated() {
            return created;
        }

        public String getTitle() {
            return title;
        }

        public String getDate() {
            return date;
        }

        public String getTransaction_key() {
            return transaction_key;
        }

        public String getModified() {
            return modified;
        }

        public String getStatus() {
            return status;
        }

        public String getSubscription_type() {
            return subscription_type;
        }

        public String getUser_id() {
            return user_id;
        }

        public String getItem_id() {
            return item_id;
        }

        public String getItem_name() {
            return item_name;
        }

        public String getToken() {
            return token;
        }

        public String getReceipt() {
            return receipt;
        }

        public String getEmail() {
            return email;
        }

        public String getCoupon_code() {
            return coupon_code;
        }

        public String getMessage() {
            return message;
        }

        public String getCurrency() {
            return currency;
        }

        public String getCurrencyIcon(){
            if(TextUtils.isEmpty(getCurrency())){
                if(currency.equals("usd")){
                    return "$ ";
                }else{
                    return "£ ";
                }
            }else{
                return "$ ";
            }

        }

        public String getPrice() {
            return price;
        }

        public String getSave_money() {
            return save_money;
        }

        public String getOrg_price() {
            return org_price;
        }

        public String getPack_start_date_str() {
            return pack_start_date_str;
        }

        public String getPack_end_date() {
            return pack_end_date;
        }

        public String getPack_end_date_str() {
            return pack_end_date_str;
        }

        public String getPayment_method() {
            return payment_method;
        }

        public String getPayment_by() {
            return payment_by;
        }

        public String getCc_approve() {
            return cc_approve;
        }

        public String getId() {
            return id;
        }


        public String getType() {
            return type;
        }
    }




}
