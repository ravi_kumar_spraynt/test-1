package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class Badges implements Serializable {


    private List<Badge> Badge;

    public List<Badges.Badge> getBadge() {
        return Badge;
    }


    public class Badge implements Serializable{
        private String login_user_name;
        private String bedge_id;
        private String course_title;
        private String url;
        private String badge;

        public String getLogin_user_name() {
            return login_user_name;
        }

        public String getBedge_id() {
            return bedge_id;
        }

        public String getCourse_title() {
            return course_title;
        }

        public String getUrl() {
            return url;
        }

        public String getBadge() {
            return badge;
        }
    }




}
