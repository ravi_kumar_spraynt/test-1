package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Mentors implements Parcelable ,Serializable{

    private String id;
    private String name;
    private String tag_line;
    private String punch_line;
    private String banner;
    private String taste_preference_id;
    private String low_qty_banner;
    private String wishlist;
    private String layout;
    private String savedResponse;
    private boolean isDownloaded;



    public Mentors(String id, String name, String banner, String punch_line, String savedResponse,boolean isDownloaded){
        this.id = id;
        this.name = name;
        this.banner = banner;
        this.punch_line = punch_line;
        this.savedResponse = savedResponse;
        this.isDownloaded = isDownloaded;
    }


    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTag_line() {
        return tag_line;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getLow_qty_banner(){
        return low_qty_banner;
    }

    public String getBanner() {
        return banner;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getCategory() {
        return category;
    }

    private String category;


    protected Mentors(Parcel in) {
     this.id = in.readString();
     this.name = in.readString();
     this.tag_line = in.readString();
     this.punch_line = in.readString();
     this.banner = in.readString();
     this.category = in.readString();
     this.taste_preference_id = in.readString();
     this.low_qty_banner = in.readString();
    }

    public static final Creator<Mentors> CREATOR = new Creator<Mentors>() {
        @Override
        public Mentors createFromParcel(Parcel in) {
            return new Mentors(in);
        }

        @Override
        public Mentors[] newArray(int size) {
            return new Mentors[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.tag_line);
        dest.writeString(this.banner);
        dest.writeString(this.punch_line);
        dest.writeString(this.category);
        dest.writeString(this.taste_preference_id);
        dest.writeString(this.low_qty_banner);

    }

    public String getLayout() {
        return layout;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public String getSavedResponse() {
        return savedResponse;
    }
}
