package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;

public class DashBoardCourseDetails implements Serializable{


   private boolean show_ratings;
   private boolean show_course_name;

   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean social_sharing;
   private boolean wishlist;
   private boolean student_enroll_section;
   private DashBoardPlanSetting PlanSetting;
   private CourseDetail CourseDetail;
   private CourseDetail ProgrammeDetail;
   private String setting;
   private boolean is_intro_video;


   public boolean isEclassVisible() {
      // checking for free and lifetime user
      // subscribe case is currently removed
//      User user = AppPreferences.getUser();
//      if(("7".equals(user.getUserGroupId()))||("8".equals(user.getUserGroupId()))) {
//          return  true;
//      }else{
//         return getCourseDetail().getSubscription().equals(WealthDragonsOnlineApplication.sharedInstance().getString(R.string.yes));
//      }

       return !getCourseDetail().getFree_paid().equals("Paid") || !getCourseDetail().getPurchase().equals("No");
   }


      public String getSetting() {
      return setting;
   }

   public boolean isAutoPlay(){
       return getSetting().equals(WealthDragonsOnlineApplication.sharedInstance().getString(R.string.auto_play));
   }





   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }


   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();
   }


   public com.imentors.wealthdragons.models.CourseDetail getCourseDetail() {
      // to handle programme condition

      if(CourseDetail != null){
         return CourseDetail;
      }else{
         return ProgrammeDetail;
      }

   }


   public boolean isProgrammes(){
      if(ProgrammeDetail != null){
          return !TextUtils.isEmpty(ProgrammeDetail.getBanner());
      }else{
         return false;
      }
   }

   public Boolean isStudent_enroll_section() {
      return student_enroll_section;
   }

   public boolean isSocial_sharing() {
      return social_sharing;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public boolean isBelowVideoVisible(){
      if(isWishlist() || getCourseDetail().isSubscription_btn() || !isEclassVisible() ){
         if(getCourseDetail().getPush_live().equals("2")) {
             return getCourseDetail().getItem_pre_booking_status().equals("Yes") || isWishlist();
         }

         return true;
      }else{
         return false;
      }
   }

   public boolean isIs_intro_video() {
      return is_intro_video;
   }

}


