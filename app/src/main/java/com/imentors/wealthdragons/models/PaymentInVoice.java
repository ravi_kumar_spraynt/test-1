package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import java.io.Serializable;

public class PaymentInVoice implements Serializable{

   public String getId() {
      return id;
   }

   public String getInvoice_id() {
      return invoice_id;
   }

   public String getTransaction_key() {
      return transaction_key;
   }

   public String getType() {
      return type;
   }

   public String getUser_id() {
      return user_id;
   }

   public String getItem_id() {
      return item_id;
   }

   public String getItem_name() {
      return item_name;
   }

   public String getToken() {
      return token;
   }

   public String getReceipt() {
      return receipt;
   }

   public String getEmail() {
      return email;
   }

   public String getCoupon_code() {
      return coupon_code;
   }

   public String getMessage() {
      return message;
   }

   public String getCurrency() {
      return currency;
   }

   public String getPrice() {
      return price;
   }

   public String getSave_money() {
      return save_money;
   }

   public String getOrg_price() {
      return org_price;
   }

   public String getSubscription_type() {
      return subscription_type;
   }

   public String getPack_start_date() {
      return pack_start_date;
   }

   public String getPack_start_date_str() {
      return pack_start_date_str;
   }

   public String getPack_end_date() {
      return pack_end_date;
   }

   public String getPack_end_date_str() {
      return pack_end_date_str;
   }

   public String getPayment_method() {
      return payment_method;
   }

   public String getPayment_by() {
      return payment_by;
   }

   public String getCc_approve() {
      return cc_approve;
   }

   public String getStatus() {
      return status;
   }

   public String getCreated() {
      return created;
   }

   public String getModified() {
      return modified;
   }

   public String getFirstname() {
      return firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public String getShipping_address() {
      return shipping_address;
   }

   public String getShipping_town() {
      return shipping_town;
   }

   public String getShipping_county() {
      return shipping_county;
   }

   public String getShipping_zipcode() {
      return shipping_zipcode;
   }

   public String getShipping_phone() {
      return shipping_phone;
   }

   public String getDate() {
      return date;
   }

   private String id;
   private String invoice_id;
   private String transaction_key;
   private String type;
   private String user_id;
   private String item_id;
   private String item_name;
   private String token;
   private String receipt;
   private String email;
   private String coupon_code;
   private String message;
   private String currency;
   private String price;
   private String save_money;
   private String org_price;
   private String subscription_type;
   private String pack_start_date;
   private String pack_start_date_str;
   private String pack_end_date;
   private String pack_end_date_str;
   private String payment_method;
   private String payment_by;
   private String cc_approve;
   private String status;
   private String created;
   private String modified;
   private String firstname;
   private String lastname;
   private String shipping_address;
   private String shipping_town;
   private String shipping_county;
   private String shipping_zipcode;
   private String shipping_phone;
   private String date;
   private String pdf_file;


   public String getCurrencyIcon(){
      if(TextUtils.isEmpty(getCurrency())){
         if(currency.equals("usd")){
            return "$ ";
         }else{
            return "£ ";
         }
      }else{
         return "$ ";
      }

   }

   public boolean isShippingDetailsVisible(){
       return !TextUtils.isEmpty(shipping_address) || !TextUtils.isEmpty(shipping_county) || !TextUtils.isEmpty(shipping_town) || !TextUtils.isEmpty(shipping_phone) || !TextUtils.isEmpty(shipping_zipcode);
   }

   public String getPdf_file() {
      return pdf_file;
   }
}


