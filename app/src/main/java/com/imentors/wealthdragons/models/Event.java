package com.imentors.wealthdragons.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Event implements Parcelable ,Serializable {

    private String id;
    private String title;
    private String mentor_id;
    private String taste_preference_id;
    private String punch_line;
    private String free_paid;
    private String banner;
    private String mobile_banner;
    private String cost;
    private String discount_cost;
    private String start_date;
    private String end_date;
    private String final_cost;
    private String mentors_name;
    private String purchase;
    private String product_id;
    private String reference_name;
    private String subscribe_msg;
    private String subscription;
    private String category;
    private String unique_id;
    private String channel_code;
    private String alis;
    private String description;
    private String banner_status;
    private String detail_banner;
    private String detail_banner_status;
    private String mobile_banner_status;
    private String mobile_detail_banner;
    private String mobile_detail_banner_status;
    private String pre_book_cost;
    private String pre_booking_date;
    private String zone;
    private String city;
    private String country;
    private String address;
    private String duration;
    private String video_before_converted;
    private String resolution;
    private String streme_rate;
    private String directory_name;
    private String seat_no;
    private String seo_title;
    private String seo_description;
    private String seo_keywords;
    private String robots;
    private String server;
    private String notification_send;
    private String ordering;
    private String status;
    private String created;
    private String modified;
    private String push_notification_message;
    private String language_id;
    private String featured;
    private String chat;
    private String wishlist;
    private String share_url;
    private String taste_prefrence;
    private String video;
    private String seek_time;
    private String low_qty_banner;
    private String layout;

    public String getUnique_id() {
        return unique_id;
    }

    public String getChannel_code() {
        return channel_code;
    }

    public String getAlis() {
        return alis;
    }

    public String getDescription() {
        return description;
    }

    public String getBanner_status() {
        return banner_status;
    }

    public String getDetail_banner() {
        return detail_banner;
    }

    public String getDetail_banner_status() {
        return detail_banner_status;
    }

    public String getMobile_banner_status() {
        return mobile_banner_status;
    }

    public String getMobile_detail_banner() {
        return mobile_detail_banner;
    }

    public String getMobile_detail_banner_status() {
        return mobile_detail_banner_status;
    }

    public String getPre_book_cost() {
        return pre_book_cost;
    }

    public String getPre_booking_date() {
        return pre_booking_date;
    }

    public String getZone() {
        return zone;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getAddress() {
        return address;
    }

    public String getDuration() {
        return duration;
    }

    public String getVideo_before_converted() {
        return video_before_converted;
    }

    public String getResolution() {
        return resolution;
    }

    public String getStreme_rate() {
        return streme_rate;
    }

    public String getDirectory_name() {
        return directory_name;
    }

    public String getSeat_no() {
        return seat_no;
    }

    public String getSeo_title() {
        return seo_title;
    }

    public String getSeo_description() {
        return seo_description;
    }

    public String getSeo_keywords() {
        return seo_keywords;
    }

    public String getRobots() {
        return robots;
    }

    public String getServer() {
        return server;
    }

    public String getNotification_send() {
        return notification_send;
    }

    public String getOrdering() {
        return ordering;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }

    public String getPush_notification_message() {
        return push_notification_message;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public String getFeatured() {
        return featured;
    }

    public String getChat() {
        return chat;
    }

    public String getWishlist() {
        return wishlist;
    }

    public void setWishlist(String wishlist) {
        this.wishlist = wishlist;
    }

    public String getShare_url() {
        return share_url;
    }

    public String getTaste_prefrence() {
        return taste_prefrence;
    }

    public String getVideo() {
        return video;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getMentor_id() {
        return mentor_id;
    }

    public String getTaste_preference_id() {
        return taste_preference_id;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getFree_paid() {
        return free_paid;
    }

    public String getBanner() {
        return banner;
    }


    public String getMobile_banner() {
        return mobile_banner;
    }

    public String getCost() {
        return cost;
    }

    public String getDiscount_cost() {
        return discount_cost;
    }

    public String getFinal_cost() {
        return final_cost;
    }

    public String getMentors_name() {
        return mentors_name;
    }



    public String getPurchase() {
        return purchase;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getReference_name() {
        return reference_name;
    }

    public String getSubscription() {
        return subscription;
    }

    public String getSubscribe_msg() {
        return subscribe_msg;
    }

    public String getCategory() {
        return category;
    }
    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }




    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    protected Event(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.mentor_id = in.readString();
        this.taste_preference_id = in.readString();
        this.punch_line = in.readString();
        this.free_paid = in.readString();
        this.banner = in.readString();
        this.mobile_banner = in.readString();
        this.cost = in.readString();
        this.discount_cost = in.readString();
        this.final_cost = in.readString();
        this.mentors_name = in.readString();
        this.purchase = in.readString();
        this.product_id = in.readString();
        this.reference_name = in.readString();
        this.subscription = in.readString();
        this.subscribe_msg = in.readString();
        this.category = in.readString();
        this.start_date=in.readString();
        this.end_date=in.readString();
        this.unique_id=in.readString();
        this.channel_code=in.readString();
        this.alis=in.readString();
        this.description=in.readString();
        this.banner_status=in.readString();
        this.detail_banner=in.readString();
        this.detail_banner_status=in.readString();
        this.mobile_banner_status=in.readString();
        this.mobile_detail_banner=in.readString();
        this.mobile_detail_banner_status=in.readString();
        this.pre_book_cost=in.readString();
        this.pre_booking_date=in.readString();
        this.zone=in.readString();
        this.city=in.readString();
        this.country=in.readString();
        this.address=in.readString();
        this.duration=in.readString();
        this.video_before_converted=in.readString();
        this.resolution=in.readString();
        this.streme_rate=in.readString();
        this.directory_name=in.readString();
        this.seat_no=in.readString();
        this.seo_title=in.readString();
        this.seo_description=in.readString();
        this.seo_keywords=in.readString();
        this.robots=in.readString();
        this.server=in.readString();
        this.notification_send=in.readString();
        this.ordering=in.readString();
        this.status=in.readString();
        this.created=in.readString();
        this.modified=in.readString();
        this.push_notification_message=in.readString();
        this.language_id=in.readString();
        this.featured=in.readString();
        this.chat=in.readString();
        this.wishlist=in.readString();
        this.share_url=in.readString();
        this.taste_prefrence=in.readString();
        this.video=in.readString();
        this.low_qty_banner = in.readString();

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeString(this.mentor_id);
        dest.writeString(this.taste_preference_id);
        dest.writeString(this.punch_line);
        dest.writeString(this.free_paid);
        dest.writeString(this.banner);
        dest.writeString(this.mobile_banner);
        dest.writeString(this.cost);
        dest.writeString(this.discount_cost);
        dest.writeString(this.final_cost);
        dest.writeString(this.mentors_name);
        dest.writeString(this.purchase);
        dest.writeString(this.product_id);
        dest.writeString(this.reference_name);
        dest.writeString(this.subscription);
        dest.writeString(this.subscribe_msg);
        dest.writeString(this.category);
        dest.writeString(this.start_date);
        dest.writeString(this.end_date);
        dest.writeString(this.unique_id);
        dest.writeString(this.channel_code);
        dest.writeString(this.alis);
        dest.writeString(this.description);
        dest.writeString(this.banner_status);
        dest.writeString(this.detail_banner);
        dest.writeString(this.detail_banner_status);
        dest.writeString(this.mobile_banner_status);
        dest.writeString(this.mobile_detail_banner);
        dest.writeString(this.mobile_detail_banner_status);
        dest.writeString(this.pre_book_cost);
        dest.writeString(this.pre_booking_date);
        dest.writeString(this.zone);
        dest.writeString(this.city);
        dest.writeString(this.country);
        dest.writeString(this.address);
        dest.writeString(this.duration);
        dest.writeString(this.video_before_converted);
        dest.writeString(this.resolution);
        dest.writeString(this.streme_rate);
        dest.writeString(this.directory_name);
        dest.writeString(this.seat_no);
        dest.writeString(this.seo_title);
        dest.writeString(this.seo_description);
        dest.writeString(this.seo_keywords);
        dest.writeString(this.robots);
        dest.writeString(this.server);
        dest.writeString(this.notification_send);
        dest.writeString(this.ordering);
        dest.writeString(this.status);
        dest.writeString(this.created);
        dest.writeString(this.modified);
        dest.writeString(this.push_notification_message);
        dest.writeString(this.language_id);
        dest.writeString(this.featured);
        dest.writeString(this.chat);
        dest.writeString(this.wishlist);
        dest.writeString(this.share_url);
        dest.writeString(this.taste_prefrence);
        dest.writeString(this.video);
        dest.writeString(this.low_qty_banner);


    }

    public String getSeek_time() {
        return seek_time;
    }

    public String getLow_qty_banner() {
        return low_qty_banner;
    }

    public String getLayout() {
        return layout;
    }
}
