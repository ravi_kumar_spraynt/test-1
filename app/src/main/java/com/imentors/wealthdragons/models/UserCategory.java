package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class UserCategory implements Serializable{

   private List<category> category;
   private String category_choose;
   private String status;


   public List<UserCategory.category> getCategory() {
      return category;
   }

   public String getCategory_choose() {
      return category_choose;
   }

   public String getStatus() {
      return status;
   }

   public static class category{

      private String id;
      private String title;
      private String banner_three;

   public String getId() {
      return id;
   }

   public String getTitle() {
      return title;
   }

   public String getBanner_three() {
      return banner_three;
   }
   }
}


