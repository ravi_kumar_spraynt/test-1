package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class MaintainceNotification implements Serializable{


   private String main_status;

   private String status;

   private String notification_text;


   public String getNotification_text() {
      return notification_text;
   }

   public String getStatus() {
      return status;
   }

   public String getMain_status() {
      return main_status;
   }
}


