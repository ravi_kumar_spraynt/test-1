package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class DashBoardEventDetails implements Serializable{




   private boolean social_sharing;
   private boolean wishlist;
   private boolean subscription;
   private DashBoardPlanSetting PlanSetting;
   private Event Event;
   private List<Days> Days;

   public boolean isSubscription() {
      return subscription;
   }

   public com.imentors.wealthdragons.models.Event getEvent() {
      return Event;
   }

   public List<DashBoardEventDetails.Days> getDays() {
      return Days;
   }

   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public boolean isBelowVideoVisible(){
       return isWishlist() || isSubscription();
   }


   public boolean isSocial_sharing() {
      return social_sharing;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public boolean getIs_subscribed() {
      return subscription;
   }


   public static class Days{


      private String start_time;
      private String start_time_am;
      private String date;
      private String end_time;
      private String end_time_am;
      private String url;
      private String event_start_time;
      private String event_end_time;

      public String getStart_time() {
         return start_time;
      }

      public String getStart_time_am() {
         return start_time_am;
      }

      public String getDate() {
         return date;
      }

      public String getEnd_time() {
         return end_time;
      }

      public String getEnd_time_am() {
         return end_time_am;
      }

      public String getUrl() {
         return url;
      }

      public String getEvent_start_time() {
         return event_start_time;
      }

      public String getEvent_end_time() {
         return event_end_time;
      }
   }




}


