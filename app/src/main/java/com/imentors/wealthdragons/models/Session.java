package com.imentors.wealthdragons.models;

import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created on 7/3/17.
 */

public class Session {

    //access token
    private String access_token;

    private static SharedPreferences mSharedPreferences;

    //user
    private User user;
    


    /**
     * Provides Access Token
     *
     * @return String Access Token
     */

    public String getAccessToken() {
        return access_token;
    }




    /**
     * Provides Current user
     *
     * @return User
     */
    public User getCurrentUser() {
        return user;
    }

    /**
     * Provides LogIn Status
     *
     * @return boolean
     */
    public boolean isLoggedIn() {

        return !(TextUtils.isEmpty(access_token) || user == null);

    }
}
