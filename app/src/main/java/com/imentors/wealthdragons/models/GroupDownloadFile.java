package com.imentors.wealthdragons.models;

import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Status;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GroupDownloadFile implements Serializable{


   private String mId;
   private String mType;
   private String mGroupTitle;
   private String mGroupBannerAddress;
   private Status mStatus;
   private int mTotalItem;
   private String mSavedResponse;
   private List<Download> mDownloadItemList = new ArrayList<>();

   public GroupDownloadFile(
            int id,
            String type,
            int totalItem,
            String groupTitle,
            String groupBannerAddress,
            Status status,
            String response
   ){
     this.mId = String.valueOf(id);
      this.mType = type;
      this.mGroupTitle = groupTitle;
      this.mGroupBannerAddress = groupBannerAddress;
      this.mStatus = status;
      this.mTotalItem = totalItem;
      this.mSavedResponse = response;

   }


   public String getmId() {
      return mId;
   }

   public String getmType() {
      return mType;
   }

   public String getmGroupTitle() {
      return mGroupTitle;
   }

   public String getmGroupBannerAddress() {
      return mGroupBannerAddress;
   }

   public Status getmStatus() {
      return mStatus;
   }

   public int getmTotalItem() {
      return mTotalItem;
   }

   public List<Download> getmDownloadItemList() {
      return mDownloadItemList;
   }

   public void setmDownloadItemList(List<Download> mDownloadItemList) {
      this.mDownloadItemList = mDownloadItemList;
   }
   public void addDownloadItemList(Download download) {
      mDownloadItemList.add(download);
   }

   public String getmSavedResponse() {
      return mSavedResponse;
   }
}


