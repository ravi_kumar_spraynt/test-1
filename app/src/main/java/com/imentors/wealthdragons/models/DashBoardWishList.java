package com.imentors.wealthdragons.models;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.io.Serializable;
import java.util.List;

public class DashBoardWishList implements Serializable{


   private boolean show_ratings;
   private boolean show_course_name;
   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean wishlist;
   private DashBoardPlanSetting PlanSetting;
   private CourseDetail.RelatedCourses FeaturedCourse;

   private RelatedProgramme FeaturedProgramme;
   private DashBoardItemOderingEvent Event;
   private DashBoardItemOderingEvent Video;
   private DashBoardItemOderingMentors Expert;

//   private CourseDetail.RelatedCourses LiveEventsFree;
//   private CourseDetail.RelatedCourses LiveEventsPaid;

   public CourseDetail.RelatedCourses getFeaturedCourse() {
      return FeaturedCourse;
   }

   public RelatedProgramme getFeaturedProgramme() {
      return FeaturedProgramme;
   }



   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }

   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();
   }

   public DashBoardItemOderingEvent getEvent() {
      return Event;
   }

   public DashBoardItemOderingEvent getVideo() {
      return Video;
   }

   public DashBoardItemOderingMentors getExpert() {
      return Expert;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public class RelatedProgramme implements Serializable{
      private int total_items;
      private int total_page;
      private int current_page;
      private int next_page;
      private int per_page_items;
      private List<Article> programmes;

      public int getTotal_items() {
         return total_items;
      }

      public int getTotal_page() {
         return total_page;
      }

      public int getCurrent_page() {
         return current_page;
      }

      public int getNext_page() {
         return next_page;
      }

      public int getPer_page_items() {
         return per_page_items;
      }

      public List<Article> getProgrammes() {
         return programmes;
      }

      public String getTitle(){
         if(programmes.size()>1) {
            return WealthDragonsOnlineApplication.sharedInstance().getString(R.string.featured_programmes);
         }
         return WealthDragonsOnlineApplication.sharedInstance().getString(R.string.featured_programme);
      }

   }


}


