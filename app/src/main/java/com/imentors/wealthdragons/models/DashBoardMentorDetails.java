package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class DashBoardMentorDetails implements Serializable{


   private boolean show_ratings;
   private boolean show_course_name;

   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean show_free_video_name;
   private boolean show_free_video_mentor_name;

   private boolean social_sharing;
   private boolean wishlist;
   private String is_subscribed;
   private DashBoardPlanSetting PlanSetting;
   private Profile Profile;
   private CourseDetail.RelatedCourses Courses;
   private CourseDetail.RelatedProgramme Programmes;
   private DashBoardVideoDetails.RelatedVideos Videos;
   private DashBoardItemOderingEvent Events;
   private RelatedMentors Mentor;
   private DashBoardItemOderingDigital DigitalCoaching;



   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }

   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }

   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();
   }


   public boolean isSocial_sharing() {
      return social_sharing;
   }

   public boolean isWishlist() {
      return wishlist;
   }

   public boolean isShow_free_video_mentor_name() {
      return show_free_video_mentor_name;
   }

   public boolean isShow_free_video_name() {
      return show_free_video_name;
   }

   public String getIs_subscribed() {
      return is_subscribed;
   }

   public DashBoardMentorDetails.Profile getProfile() {
      return Profile;
   }

   public CourseDetail.RelatedCourses getCourses() {
      return Courses;
   }

   public CourseDetail.RelatedProgramme getProgrammes() {
      return Programmes;
   }

   public DashBoardVideoDetails.RelatedVideos getVideos() {
      return Videos;
   }

   public DashBoardItemOderingEvent getEvents() {
      return Events;
   }

   public RelatedMentors getMentors() {
      return Mentor;
   }

   public DashBoardItemOderingDigital getDigitalCoaching() {
      return DigitalCoaching;
   }


   public static class Profile{

      private String name;
      private String id;
      private String tag_line;
      private String punch_line;
      private String slug;
      private String short_details;
      private String long_details;
      private String facebook;
      private String twitter;
      private String google;
      private String linkedin;
      private String share_url;
      private String rating;
      private int rating_persons;
      private String banner;
      private String wishlist;
      private int seek_time;
      private List<JobHistory> JobHistory;
      private List<Education> Educations;
      private List<CourseItemListModel> SkillEndorsements;
      private List<CourseItemListModel> HonorsAwards;
      private String expert_intro_video;
      private String expert_intro_video_banner;
      private String digi_subscription;
      private String digi_coaching_cost;
      private String product_id;
      private String join_digi_btn;
      private String currency;
      private List<Announcement> Announcements;
      private String normal_subscription;
      private String feeds_btn;
      private String expert_intro_video_duration;
      private String expert_intro_video_seektime;
      private boolean expert_subscription_btn;
      private String live_session_btn;
      private String live_session_row_id;
      private String live_digi_coaching_id;
      private String digital_coaching_btn_name;
      private String digital_coaching_heading;
      private String digital_coaching_description;
      private List<Ranking> SectionOrdering;
      private String digi_intro_video;
      private String intro_video_banner;
      private String digital_coaching_section_heading;
      private String digital_coaching_section_heading_orange;
      private String about_mentor_black;
      private String about_mentor_orange;
      private String digi_intro_video_seektime;
      private String digi_coaching_screen;
      private String savedVideoAddress;



      public boolean isDigiDescriptionVisible(){
          return !digital_coaching_description.isEmpty() && !digital_coaching_heading.isEmpty();
      }


      public void setExpert_intro_video_seektime(String expert_intro_video_seektime) {
         this.expert_intro_video_seektime = expert_intro_video_seektime;
      }

      public String getExpert_intro_video_seektime() {
         return expert_intro_video_seektime;
      }

      public String getExpert_intro_video_duration() {
         return expert_intro_video_duration;
      }

      public String getFeeds_btn() {
         return feeds_btn;
      }

      public String getNormal_subscription() {
         return normal_subscription;
      }
      private String next_video;

      public String getWishlist() {
         return wishlist;
      }

      public String getName() {
         return name;
      }

      public String getId() {
         return id;
      }


      public String getTag_line() {
         return tag_line;
      }

      public String getPunch_line() {
         return punch_line;
      }

      public String getSlug() {
         return slug;
      }

      public String getShort_details() {
         short_details = short_details.replace("\n", "<br>");
         short_details = short_details.replace("\t", "  ");
         return short_details;
      }

      public String getLong_details() {
         long_details = long_details.replace("\n", "<br>");
         long_details = long_details.replace("\t", "  ");
         return long_details;
      }

      public String getFacebook() {
         return facebook;
      }

      public String getTwitter() {
         return twitter;
      }

      public String getGoogle() {
         return google;
      }

      public String getLinkedin() {
         return linkedin;
      }

      public String getShare_url() {
         return share_url;
      }

      public String getRating() {
         return rating;
      }

      public int getRating_persons() {
         return rating_persons;
      }

      public String getBanner() {
         return banner;
      }


      public List<Education> getEducations() {
         return Educations;
      }

      public List<CourseItemListModel> getSkillEndorsements() {
         return SkillEndorsements;
      }

      public List<CourseItemListModel> getHonorsAwards() {
         return HonorsAwards;
      }

      public List<DashBoardMentorDetails.JobHistory> getJobHistory() {
         return JobHistory;
      }

      public String getExpert_intro_video() {
         return expert_intro_video;
      }

      public int getSeek_time() {
         return seek_time;
      }

      public String getExpert_intro_video_banner() {
         return expert_intro_video_banner;
      }

      public String getDigi_subscription() {
         return digi_subscription;
      }

      public String getDigi_coaching_cost() {
         return digi_coaching_cost;
      }

      public String getProduct_id() {
         return product_id;
      }

      public String getCurrency() {
         return currency;
      }

      public List<Announcement> getAnnouncements() {
         return Announcements;
      }

      public String getJoin_digi_btn() {
         return join_digi_btn;
      }

      public String getNext_video() {
         return next_video;
      }

      public boolean getExpert_subscription_btn() {
         return expert_subscription_btn;
      }

      public String getLive_session_btn() {
         return live_session_btn;
      }

      public String getLive_session_row_id() {
         return live_session_row_id;
      }

      public String getLive_digi_coaching_id() {
         return live_digi_coaching_id;
      }

      public void setWishlist(String wishlist) {
         this.wishlist = wishlist;
      }

      public String getDigital_coaching_btn_name() {
         return digital_coaching_btn_name;
      }

      public String getDigital_coaching_heading() {
         return digital_coaching_heading;
      }

      public String getDigital_coaching_description() {
         return digital_coaching_description;
      }

      public List<Ranking> getSectionOrdering() {
         return SectionOrdering;
      }

      public String getDigi_intro_video() {
         return digi_intro_video;
      }

      public String getIntro_video_banner() {
         return intro_video_banner;
      }

      public String getDigital_coaching_section_heading() {
         return digital_coaching_section_heading;
      }

      public String getDigital_coaching_section_heading_orange() {
         return digital_coaching_section_heading_orange;
      }

      public String getAboutMentorBlack() {
         return about_mentor_black;
      }


      public String getAboutMentorOrange() {
         return about_mentor_orange;
      }

      public String getDigi_intro_video_seektime() {
         return digi_intro_video_seektime;
      }

      public String getDigi_coaching_screen() {
         return digi_coaching_screen;
      }

      public String getSavedVideoAddress() {
         return savedVideoAddress;
      }

      public void setSavedVideoAddress(String savedVideoAddress) {
         this.savedVideoAddress = savedVideoAddress;
      }
   }

   public static class RelatedEvents{
      private int total_items;
      private int total_page;
      private int current_page;
      private int next_page;
      private int per_page_items;
      private List<Event> events;

      public int getTotal_items() {
         return total_items;
      }

      public int getTotal_page() {
         return total_page;
      }

      public int getCurrent_page() {
         return current_page;
      }

      public int getNext_page() {
         return next_page;
      }

      public int getPer_page_items() {
         return per_page_items;
      }


      public List<Event> getEvents() {
         return events;
      }
   }



   public static class Ranking{
      private String id;
      private String title;

      public String getId() {
         return id;
      }

      public String getTitle() {
         return title;
      }
   }

   public static class JobHistory{
      private String designation;
      private String company;
      private String city;
      private String country_id;
      private String start_date;
      private String end_date;
      private String description_heading;
      private String role_heading;
      private String banner;
      private String banner_status;
      private String id;
      private String job_logo;
      private String description_text;
      private String country;
      private List<CourseItemListModel> JobRole;
      private boolean join_digi_btn;


      public String getDesignation() {
         return designation;
      }

      public String getCompany() {
         return company;
      }

      public String getCity() {
         return city;
      }

      public String getCountry_id() {
         return country_id;
      }

      public String getStart_date() {
         return start_date;
      }

      public String getEnd_date() {
         return end_date;
      }

      public String getDescription_heading() {
         return description_heading;
      }

      public String getRole_heading() {
         return role_heading;
      }

      public String getBanner() {
         return banner;
      }

      public String getBanner_status() {
         return banner_status;
      }

      public String getId() {
         return id;
      }

      public String getJob_logo() {
         return job_logo;
      }

      public List<CourseItemListModel> getJobRole() {
         return JobRole;
      }

      public String getDescription_text() {
         return description_text;
      }

      public String getCountry() {
         return country;
      }

      public boolean isJoin_digi_btn() {
         return join_digi_btn;
      }
   }



   public static class RelatedMentors{
      private int total_items;
      private int total_page;
      private int current_page;
      private int next_page;
      private int per_page_items;
      private List<Mentors> mentors;

      public int getTotal_items() {
         return total_items;
      }

      public int getTotal_page() {
         return total_page;
      }

      public int getCurrent_page() {
         return current_page;
      }

      public int getNext_page() {
         return next_page;
      }

      public int getPer_page_items() {
         return per_page_items;
      }


      public List<Mentors> getMentors() {
         return mentors;
      }
   }

   public static class Education{
      private String degree_name;
      private String organisation_name;
      private String education_from;
      private String education_to;
      private String description;
      private String job_logo;

      public String getDegree_name() {
         return degree_name;
      }

      public String getOrganisation_name() {
         return organisation_name;
      }

      public String getEducation_from() {
         return education_from;
      }

      public String getEducation_to() {
         return education_to;
      }

      public String getDescription() {
         return description;
      }

      public String getJob_logo() {
         return job_logo;
      }
   }

   public static class Announcement{
      private String title;
      private String banner;
      private String id;
      private String video;

      public String getTitle() {
         return title;
      }

      public String getId() {
         return id;
      }

      public String getBanner() {
         return banner;
      }

      public String getVideo() {
         return video;
      }
   }

}


