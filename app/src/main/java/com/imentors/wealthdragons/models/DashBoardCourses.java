package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class DashBoardCourses implements Serializable{


   private List<DashBoardItemOderingArticle> All_Course;
   private List<DashBoardItemOderingArticle> All_Programme;
   private CourseDetail.RelatedCourses FeaturedCourse;


   private boolean show_ratings;
   private boolean show_course_name;

   private boolean show_mentor_name;
   private boolean show_mentor_image;
   private boolean show_mentor_tagline;
   private boolean show_view_detail_btn;
   private boolean wishlist;
   private String is_subscribed;

   private DashBoardPlanSetting PlanSetting;


   public List<DashBoardItemOderingArticle> getAll_Course() {
      return All_Course;
   }


   public String getIs_subscribed() {
      return is_subscribed;
   }

   public DashBoardPlanSetting getPlanSetting() {
      return PlanSetting;
   }



   public boolean isShowRatings() {
      return show_ratings;
   }

   public boolean isShowCourseName() {
      return show_course_name;
   }

   public boolean isShowMentorName() {
      return show_mentor_name;
   }

   public boolean isShowMentorImage() {
      return show_mentor_image;
   }

   public boolean isShowMentorTagline() {
      return show_mentor_tagline;
   }

   public boolean isShowViewDetailBtn() {
      return show_view_detail_btn;
   }

   public boolean isMentorLayoutVisible(){

       return isShowMentorImage() || isShowMentorName() || isShowMentorTagline();

   }


   public List<DashBoardItemOderingArticle> getAll_Programme() {
      return All_Programme;
   }

   public CourseDetail.RelatedCourses getFeaturedCourse() {
      return FeaturedCourse;
   }

   public boolean isWishlist() {
      return wishlist;
   }
}


