package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class MentorDetail implements Serializable {

    private String id;
    private String name;
    private String tag_line;
    private String punch_line;
    private String banner;
    private String short_details;
    private String long_details;
    private String facebook;
    private String twitter;
    private String google;
    private String linkedin;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTag_line() {
        return tag_line;
    }

    public String getPunch_line() {
        return punch_line;
    }

    public String getBanner() {
        return banner;
    }

    public String getShort_details() {
        short_details = short_details.replace("\n", "<br>");
        short_details = short_details.replace("\t", "  ");

        return short_details;
    }

    public String getLong_details() {
        long_details = long_details.replace("\n", "<br>");
        long_details = long_details.replace("\t", "  ");


        return long_details;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getGoogle() {
        return google;
    }

    public String getLinkedin() {
        return linkedin;
    }
}
