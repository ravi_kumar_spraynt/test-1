package com.imentors.wealthdragons.models;

import java.io.Serializable;
import java.util.List;

public class Messages implements Serializable {


    private List<Notification> Notifications;

    public List<Notification> getNotifications() {
        return Notifications;
    }


    public class Notification{
        private String id;
        private String message;
        private String created;
        private String title;
        private String date;
        private String seen;
        private String modified;
        private String status;
        private String type;

        public String getId() {
            return id;
        }

        public String getMessage() {
            return message;
        }

        public String getCreated() {
            return created;
        }

        public String getTitle() {
            return title;
        }

        public String getDate() {
            return date;
        }

        public String getSeen() {
            return seen;
        }

        public void setSeen(String seen) {
            this.seen = seen;
        }
    }




}
