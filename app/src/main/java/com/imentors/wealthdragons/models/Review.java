package com.imentors.wealthdragons.models;

import java.io.Serializable;

public class Review implements Serializable {

    private String id;
    private String type;
    private String product_id;
    private String user_id;
    private String heading;
    private String rating;
    private String review;
    private String status;
    private String created;
    private String modified;
    private String user_name;
    private String user_banner;
    private String ago;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getHeading() {
        return heading;
    }

    public String getRating() {
        return rating;
    }

    public String getReview() {
        return review;
    }

    public String getStatus() {
        return status;
    }

    public String getCreated() {
        return created;
    }

    public String getModified() {
        return modified;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_banner() {
        return user_banner;
    }

    public String getAgo() {
        return ago;
    }
}
