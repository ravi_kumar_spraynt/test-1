package com.imentors.wealthdragons.models;

import android.text.TextUtils;

import java.io.Serializable;

public class Subscription implements Serializable {

    private String main_heading;
    private String sub_heading;
    private String content_heading;
    private String course_heading;
    private String course;
    private String wdo_tv_heading;
    private String wdo_tv;
    private String other_benifits_heading;
    private String other_benifits;
    private String footer_heading;
    private String footer_text;
    private String terms_and_conditions_line;
    private String button_text;
    private String currency;


    public String getCurrency() {
        return currency;
    }

    public String getMainHeading() {
        return main_heading;
    }

    public String getSubHeading() {
        return sub_heading;
    }

    public String getContentHeading() {
        return content_heading;
    }

    public String getCourseHeading() {
        return course_heading;
    }

    public String getCourse() {
        return course;
    }

    public String getWdoTvHeading() {
        return wdo_tv_heading;
    }

    public String getWdoTv() {
        return wdo_tv;
    }

    public String getOtherBenifitsHeading() {
        return other_benifits_heading;
    }

    public String getOtherBenifits() {
        return other_benifits;
    }

    public String getFooterHeading() {
        return footer_heading;
    }

    public String getFooterText() {
        return footer_text;
    }

    public String getTermsAndConditionsLine() {
        return terms_and_conditions_line;
    }

    public String getButtonText() {
        return button_text;
    }

    public boolean isButtonVisible(){
        return !TextUtils.isEmpty(getButtonText()) ;
    }
}
