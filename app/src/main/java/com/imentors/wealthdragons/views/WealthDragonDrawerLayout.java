package com.imentors.wealthdragons.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;

public class WealthDragonDrawerLayout extends DrawerLayout {
    public WealthDragonDrawerLayout(@NonNull Context context) {
        super(context);
    }

    public WealthDragonDrawerLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public WealthDragonDrawerLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {


        final View drawerView = getChildAt( 1 );
        final ViewConfiguration config = ViewConfiguration.get( getContext() );


        // empty area beside menu fragment where slider should enabled
        int rightArea = getWidth() - drawerView.getWidth();


        if(isDrawerOpen(drawerView) ){

            // click on empty area and slider should behave naturally
            if (rightArea>=ev.getX()){
                return super.onInterceptTouchEvent( ev );
            }else{
                // click on menu fragment where slide should be disabled
                return false;
            }

        }else{
            return super.onInterceptTouchEvent( ev );
        }


    }


    }




