package com.imentors.wealthdragons.views;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.SearchListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.PagentaionItem;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import java.util.Map;

public class SearchTabView extends LinearLayout implements SearchListAdapter.OnItemClickListener, PagingListener {

    private RecyclerView mRecyclerView;
    private WealthDragonTextView mRecylerViewHeading, mTxtViewAll;
    private SearchListAdapter searchListAdapter;
    private ImageView mHeadingImage;
    private LinearLayout mHeadingContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private String mSearchKey;
    private ArticleClickListener mArticleClickListeners;
    private MentorClickListener mMentorClickListener;
    private VideoClickListener mVideoClickListener;
    private boolean mIsProgrammeType;
    private String mSearchType, mSearchId;


    private static final int MAX_ITEM_COUNT = 1;


    public SearchTabView(Context context) {
        super(context);
    }

    public SearchTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SearchTabView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public SearchTabView(Context context, DashBoardItemOderingMentors mentorList, String searchKey, String type, String id) {
        super(context);
        mSearchType = type;
        mSearchId = id;
        initViews(context);
        setMentorList(context, mentorList, searchKey);

    }

    //  set mentor list in adapter
    private void setMentorList(Context context, DashBoardItemOderingMentors mentorList, String searchKey) {


        searchListAdapter = new SearchListAdapter(this, this, context);

        searchListAdapter.setItems(null, mentorList.getItems(), null, null, mentorList.getTitle(), mentorList.getIcon(), mentorList.getNext_page(), mentorList.getTotal_items(), searchKey, null,mIsProgrammeType);

        mSearchKey = searchKey;

        mRecyclerView.setAdapter(searchListAdapter);

        if (mentorList.getItems().size() == mentorList.getLimit()) {
            mTxtViewAll.setVisibility(View.VISIBLE);

        } else {
            mTxtViewAll.setVisibility(View.GONE);

        }


    }


    public SearchTabView(Context context, DashBoardItemOderingVideo videoList, String searchKey, String type, String id) {
        super(context);
        mSearchType = type;
        mSearchId = id;
        initViews(context);
        setVideoList(context, videoList, searchKey);
    }

    //  set video list in adapter
    private void setVideoList(Context context, DashBoardItemOderingVideo videoList, String searchKey) {


        searchListAdapter = new SearchListAdapter(this, this, context);

        searchListAdapter.setItems(null, null, videoList.getItems(), null, videoList.getTitle(), videoList.getIcon(), videoList.getNext_page(), videoList.getTotal_items(), searchKey, null,mIsProgrammeType);

        mRecyclerView.setAdapter(searchListAdapter);

        mSearchKey = searchKey;

        if (videoList.getItems().size() == videoList.getLimit()) {
            mTxtViewAll.setVisibility(View.VISIBLE);

        } else {
            mTxtViewAll.setVisibility(View.GONE);

        }


    }


    public SearchTabView(Context context, DashBoardItemOderingArticle articleList, String searchKey, String seeAll, boolean isProgramme, String type, String id) {
        super(context);
        mSearchType = type;
        mSearchId = id;
        initViews(context);
        mIsProgrammeType = isProgramme;
        setArticleList(context, articleList, searchKey, seeAll);
    }

    //  set article list in adapter
    private void setArticleList(Context context, DashBoardItemOderingArticle articleList, String searchKey, String seeAll) {


        searchListAdapter = new SearchListAdapter(this, this, context);

        searchListAdapter.setItems(articleList.getItems(), null, null, null, articleList.getTitle(), articleList.getIcon(), articleList.getNext_page(), articleList.getTotal_items(), searchKey, seeAll,mIsProgrammeType);


        mRecyclerView.setAdapter(searchListAdapter);
        mSearchKey = searchKey;

        if (articleList.getItems().size() == articleList.getLimit()) {
            mTxtViewAll.setVisibility(View.VISIBLE);

        } else {
            mTxtViewAll.setVisibility(View.GONE);

        }


    }

    public SearchTabView(Context context, DashBoardItemOderingEvent eventList, String searchKey, String type, String id) {
        super(context);
        mSearchType = type;
        mSearchId = id;
        initViews(context);

        setEventList(context, eventList, searchKey);
    }

    //  set event list in adapter
    private void setEventList(Context context, DashBoardItemOderingEvent eventList, String searchKey) {


        searchListAdapter = new SearchListAdapter(this, this, context);
        searchListAdapter.setItems(null, null, null, eventList.getItems(), eventList.getTitle(), eventList.getIcon(), eventList.getNext_page(), eventList.getTotal_items(), searchKey, null,mIsProgrammeType);


        mRecyclerView.setAdapter(searchListAdapter);
        mSearchKey = searchKey;

        if (eventList.getItems().size() == eventList.getLimit()) {
            mTxtViewAll.setVisibility(View.VISIBLE);
        } else {
            mTxtViewAll.setVisibility(View.GONE);

        }


    }


    private void initViews(final Context context) {


        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dashboard_item_view, this);

        if (context instanceof ArticleClickListener) {
            mArticleClickListeners = (ArticleClickListener) context;
        }

        if (context instanceof MentorClickListener) {
            mMentorClickListener = (MentorClickListener) context;
        }


        if (context instanceof VideoClickListener) {
            mVideoClickListener = (VideoClickListener) context;
        }

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLinearLayoutManager.setInitialPrefetchItemCount(8);
        mLinearLayoutManager.setItemPrefetchEnabled(true);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecylerViewHeading = view.findViewById(R.id.txt_header_name);
        mHeadingImage = view.findViewById(R.id.image_view_header);
        mHeadingContainer = view.findViewById(R.id.ll_header_view);
        mTxtViewAll = view.findViewById(R.id.txt_view_all);


        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

    }

    @Override
    public void onArticleClick(Article item, int position) {
        if(item.getLayout().equals(Constants.TYPE_PROGRAMMES)){

            mArticleClickListeners.onArticleClick(Constants.TYPE_PROGRAMMES, item.getId());

        } else {

            mArticleClickListeners.onArticleClick(Constants.TYPE_COURSE, item.getId());

        }
    }

    @Override
    public void onMentorClick(Mentors item, int position) {

        mMentorClickListener.onMentorClick(Constants.TYPE_MENTOR, item.getId());

    }

    @Override
    public void onVideoClick(Video item, int position) {

        if (item.isEpisode()) {
            mVideoClickListener.onVideoClick(Constants.TYPE_EPISODE, item.getVideo_id(), item.getId());
        } else {
            mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO, item.getId(), null);
        }
    }

    @Override
    public void onEventClick(Event item, int position) {

    }


    //  set header in view
    @Override
    public void setHeader(String title, String imageHeader, int listSize) {
        if (TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setVisibility(View.GONE);
            mHeadingContainer.setVisibility(View.GONE);
        } else {
            mHeadingContainer.setVisibility(View.VISIBLE);
            mRecylerViewHeading.setVisibility(View.VISIBLE);
            mRecylerViewHeading.setText(title);
        }

        mTxtViewAll.setVisibility(View.GONE);


        if (TextUtils.isEmpty(imageHeader)) {
            mHeadingImage.setVisibility(View.GONE);
        } else {
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(imageHeader).into(mHeadingImage);
        }
    }


    //TODO implemented maintenance mode
    public void callSeeAllPaginationApi(final int pageNumber, final String tasteId, String api, final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getContext());
                        return;
                    }
                    addDataInAdapter(response, viewType);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if (!Utils.setErrorDialog(response, getContext())) {
                            searchListAdapter.setServerError();
                        }
                    } catch (Exception e1) {
                        Utils.callEventLogApi("getting <b> Pagination</b> error");

                        e1.printStackTrace();
                        searchListAdapter.setServerError();
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b> Pagination</b> error");

                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                } else {
                    searchListAdapter.setServerError();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);



                if (pageNumber != 0) {
                    params.put(Keys.PAGE_NO, String.valueOf(pageNumber));
                }


                if (!TextUtils.isEmpty(mSearchType)) {
                    switch (mSearchType) {
                        case Constants.TYPE_TAG:
                            params.put(Keys.ID, mSearchId);
                            break;
                        case Constants.TYPE_EXPERT:
                            params.put(Keys.ID, mSearchId);
                            break;
                        default:
                            if (!TextUtils.isEmpty(tasteId)) {
                                params.put(Keys.SEARCH_KEY, tasteId);
                            }
                            break;
                    }
                }else{
                    if (!TextUtils.isEmpty(tasteId)) {
                        params.put(Keys.SEARCH_KEY, tasteId);
                    }
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String viewType, String seeAllType) {

        switch (viewType) {
            case Constants.TYPE_COURSE:
                if (!TextUtils.isEmpty(seeAllType)) {
                    switch (seeAllType) {
                        // for different types of courses
                        case Constants.ALL_PROGRAMMES_KEY:
                            // for different types of search type
                            if (!TextUtils.isEmpty(mSearchType)) {
                                switch (mSearchType) {
                                    case Constants.TYPE_EXPERT:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_PROGRAMME_PAGINATION_API, viewType);
                                        break;

                                    case Constants.TYPE_TAG:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_PROGRAMME_PAGINATION_API, viewType);
                                        break;

                                    default:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_PROGRAMME_API, viewType);
                                        break;

                                }
                            }else {
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_PROGRAMME_API, viewType);
                            }
                            break;
                        case Constants.ALL_UPCOMING_COURSE:
                            // for different types of search type
                            if (!TextUtils.isEmpty(mSearchType)) {
                                switch (mSearchType) {
                                    case Constants.TYPE_EXPERT:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_UPCOMING_COURSE_PAGINATION_API, viewType);
                                        break;

                                    case Constants.TYPE_TAG:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_UPCOMING_COURSE_PAGINATION_API, viewType);
                                        break;

                                    default:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_UPCOMING_COURSE_API, viewType);
                                        break;

                                }
                            }else {
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_UPCOMING_COURSE_API, viewType);
                            }
                            break;

                        default:
                            if (!TextUtils.isEmpty(mSearchType)) {
                                switch (mSearchType) {
                                    case Constants.TYPE_EXPERT:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_COURSE_PAGINATION_API, viewType);
                                        break;

                                    case Constants.TYPE_TAG:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_COURSE_PAGINATION_API, viewType);
                                        break;

                                    case Constants.TYPE_CHAPTER:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_CHAPTER_COURSE_PAGINATION_API, viewType);
                                        break;

                                    default:
                                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_COURSE_API, viewType);
                                        break;

                                }
                            }else {
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_COURSE_API, viewType);
                            }
                            break;
                    }
                } else {
                    if (!TextUtils.isEmpty(mSearchType)) {
                        switch (mSearchType) {
                            case Constants.TYPE_CHAPTER:
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_CHAPTER_COURSE_PAGINATION_API, viewType);
                                break;

                            case Constants.TYPE_EXPERT:
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_COURSE_PAGINATION_API, viewType);
                                break;

                            case Constants.TYPE_TAG:
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_COURSE_PAGINATION_API, viewType);
                                break;

                            default:
                                callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_COURSE_API, viewType);
                                break;

                        }
                    }else {
                        callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_COURSE_API, viewType);
                    }
                }

                break;

            case Constants.TYPE_MENTOR:
                if (!TextUtils.isEmpty(mSearchType)) {
                    switch (mSearchType) {
                        case Constants.TYPE_TAG:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_MENTOR_PAGINATION_API, viewType);
                            break;
                        default:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_API, viewType);
                            break;
                    }
                }else {
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_API, viewType);
                }
                break;

            case Constants.TYPE_VIDEO:
                if (!TextUtils.isEmpty(mSearchType)) {
                    switch (mSearchType) {
                        case Constants.TYPE_TAG:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_TAG_VIDEO_PAGINATION_API, viewType);
                            break;
                        case Constants.TYPE_EXPERT:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_MENTOR_VIDEO_PAGINATION_API, viewType);
                            break;
                        default:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_VIDEO_API, viewType);
                            break;
                    }
                }else {
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.SEARCH_VIDEO_API, viewType);
                }

                break;

            default:
                break;
        }
    }


    private void addDataInAdapter(String response, String viewType) {




            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
            final Gson gson = gsonBuilder.create();

            // Parse JSON to Java
            final PagentaionItem dashBoardPagenationData = gson.fromJson(response, PagentaionItem.class);

            //  get heading ordering from dashboard pagination data
            DashBoardItemOdering dashBoard = dashBoardPagenationData.getHeadingOrdering();

            switch (viewType) {
                case Constants.TYPE_COURSE:
                    DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoard;
                    if (dashBoardItemOderingArticle.getItems().size() > 0) {

                        searchListAdapter.addItems(dashBoardItemOderingArticle.getItems(), null, null, null, dashBoardItemOderingArticle.getNext_page(), dashBoardItemOderingArticle.getTotal_items(), mSearchKey);

                    }
                    break;
                case Constants.TYPE_MENTOR:
                    DashBoardItemOderingMentors dashBoardItemOderingMentor = (DashBoardItemOderingMentors) dashBoard;
                    if (dashBoardItemOderingMentor.getItems().size() > 0) {

                        searchListAdapter.addItems(null, dashBoardItemOderingMentor.getItems(), null, null, dashBoardItemOderingMentor.getNext_page(), dashBoardItemOderingMentor.getTotal_items(), mSearchKey);

                    }
                    break;
                case Constants.TYPE_VIDEO:
                    DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoard;
                    if (dashBoardItemOderingVideo.getItems().size() > 0) {

                        searchListAdapter.addItems(null, null, dashBoardItemOderingVideo.getItems(), null, dashBoardItemOderingVideo.getNext_page(), dashBoardItemOderingVideo.getTotal_items(), mSearchKey);

                    }
                    break;
                default:
                    break;
            }

    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mArticleClickListeners = null;
        mMentorClickListener = null;
        mVideoClickListener = null;

    }
}
