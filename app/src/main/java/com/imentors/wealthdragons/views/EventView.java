package com.imentors.wealthdragons.views;

import android.app.Activity;
import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.LruCache;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.utils.Utils;


public class EventView extends LinearLayout {


    private Context mContext;
    private ImageView mEventImage;
    private WealthDragonTextView   mEventTitle,  mEventStartDate , mEventEndDate , mCostAmount , mEventStartDateView , mEventEndDateView;

    public EventView(Context context) {
        super(context);
        mContext = context;
    }

    public EventView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EventView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mEventTitle = findViewById(R.id.tv_article_title);
        mEventImage = findViewById(R.id.iv_articleImage);
        mEventStartDate = findViewById(R.id.tv_start_date);
        mEventEndDate = findViewById(R.id.tv_end_date);
        mCostAmount = findViewById(R.id.tv_cost_button);
        mEventEndDateView = findViewById(R.id.tv_end_date_view);
        mEventStartDateView = findViewById(R.id.tv_start_date_view);
    }

    // Set values to the list items
    public void bind(Event event , LruCache memCache){

        String eventTitle = event.getTitle();
        String eventStartDate = event.getStart_date();
        String eventEndDate = event.getEnd_date();


        //  set data in view
        if(!TextUtils.isEmpty(eventTitle)){
            mEventTitle.setText(eventTitle);
        }

        if(!TextUtils.isEmpty(eventStartDate)){
            mEventStartDate.setText(Utils.getEventDate(eventStartDate));
        }

        if(!TextUtils.isEmpty(eventEndDate)){
            mEventEndDate.setText(Utils.getEventDate(eventEndDate));
        }



        mEventImage.getLayoutParams().width = (int) Utils.getCourseImageWidth((Activity) mContext);
        mEventImage.getLayoutParams().height = (int) Utils.getCourseImageHeight((Activity) mContext);



        double height = mEventImage.getLayoutParams().height;
        double width = mEventImage.getLayoutParams().width;

        //  get image from utils
        String newImageUrl=Utils.getImageUrl(event.getBanner(),width,height);
        String thumbImage=Utils.getImageUrl(event.getLow_qty_banner(),width,height);


        Glide.with(mContext).load(newImageUrl).thumbnail(Glide.with(mContext).load(thumbImage).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.no_img_video).into(mEventImage);


        mEventEndDateView.setText(mContext.getString(R.string.end_date));
        mEventStartDateView.setText(mContext.getString(R.string.start_date));

    }


}
