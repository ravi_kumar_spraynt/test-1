package com.imentors.wealthdragons.views;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;

public class FooterViewHolder extends RecyclerView.ViewHolder {

    private final View mContentView;

    private final ProgressBar mProgressBar;

    private final TextView mTitleView;

    private final TextView mActionView;

    private final CardView mCardView;

    public FooterViewHolder(@NonNull View view) {
        super(view);
        mCardView = view.findViewById(R.id.card_view);
        mContentView = view.findViewById(R.id.layout_content);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mTitleView = view.findViewById(R.id.txt_title);
        mActionView = view.findViewById(R.id.txt_action);
    }



    //  show loading
    public void showLoading(int height) {
        if(height ==0){
            mCardView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else{
            mCardView.getLayoutParams().height = height;
        }
        mCardView.setCardBackgroundColor(Color.TRANSPARENT);
        mCardView.setCardElevation(0);
        mContentView.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
    }




    // show no internet Error
    public void showNoInternetError(int height) {
        if(height ==0){
            mCardView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else{
            mCardView.getLayoutParams().height = height;
        }
        mCardView.setCardBackgroundColor(Color.WHITE);
        mCardView.setCardElevation(6);
        mProgressBar.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
        mTitleView.setText(R.string.error_api_no_internet_connection);
        mActionView.setText(R.string.tap_to_reload);
    }


    //  show server error
    public void showServerError(int height) {
        if(height ==0){
            mCardView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else{
            mCardView.getLayoutParams().height = height;
        }
        mCardView.setCardBackgroundColor(Color.WHITE);
        mCardView.setCardElevation(6);
        mProgressBar.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
        mTitleView.setText(R.string.error);
        mActionView.setText(R.string.tap_to_reload);
    }


    public void showNoContent(boolean isBookmarkScreen) {
        mCardView.setCardBackgroundColor(Color.TRANSPARENT);
        mCardView.setCardElevation(0);
        mProgressBar.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
    }

    public void showNoContent(int titleResId, int actionTitleResId) {
        mCardView.setCardBackgroundColor(Color.TRANSPARENT);
        mCardView.setCardElevation(0);
        mProgressBar.setVisibility(View.GONE);
        mContentView.setVisibility(View.VISIBLE);
        mTitleView.setText(titleResId);
        mActionView.setText(actionTitleResId);
    }
}