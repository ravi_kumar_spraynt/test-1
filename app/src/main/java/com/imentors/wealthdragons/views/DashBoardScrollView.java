package com.imentors.wealthdragons.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

public class DashBoardScrollView extends NestedScrollView {


    private float startX;
    private float startY;
    private boolean mIsScrolling;

    public DashBoardScrollView(@NonNull Context context) {
        super(context);
    }

    public DashBoardScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DashBoardScrollView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        float diffX = 0;
        float diffY = 0;



        final int action = event.getAction();

        // Always handle the case of the touch gesture being complete.
        if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
            // Release the scroll.
            mIsScrolling = false;
            return false;// Do not intercept touch event, let the child handle it
        }




        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startX = event.getX();
                startY = event.getY();
                mIsScrolling = false;

                break;

            case MotionEvent.ACTION_MOVE:
                float currentX = event.getX();
                float currentY = event.getY();
                diffX = Math.abs(currentX - startX);
                diffY = Math.abs(currentY - startY);


                if(diffY > diffX + 150){
                    mIsScrolling = true;
                }else mIsScrolling = diffX == 0 && diffY > 0;

                return mIsScrolling;


        }

        return super.onInterceptTouchEvent(event);


    }
}
