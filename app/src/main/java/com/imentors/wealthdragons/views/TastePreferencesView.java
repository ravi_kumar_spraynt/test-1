package com.imentors.wealthdragons.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.TastePreferencesListAdapter;
import com.imentors.wealthdragons.interfaces.TasteClickListener;
import com.imentors.wealthdragons.models.Category;


import static com.imentors.wealthdragons.adapters.TastePreferencesListAdapter.NO_TASTE;
import static com.imentors.wealthdragons.adapters.TastePreferencesListAdapter.TASTE_OFTEN;

public class TastePreferencesView extends LinearLayout{

    private RecyclerView mRecyclerView ;
    private WealthDragonTextView mTvGroupTasteName;
    private LinearLayout mLLTasteHeadingContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private TastePreferencesListAdapter mTastePreferncesListApdater;
    private boolean isSubCategoryListVisible = false;
    private Context mContext;
    private ImageView mIvExpandable , mIvNever , mIvOften;
    private onExpandClick onExpandClickListener;
    // for subcategory
    private TasteClickListener mSubTasteClickListener;

    // for header taste Listener
    private TasteClickListener mTasteClickListener;



    public TastePreferencesView(Context context) {
        super(context);
    }

    public TastePreferencesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TastePreferencesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TastePreferencesView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public TastePreferencesView(Context context , Category attempt , onExpandClick expandClick , int position , boolean isSubPreferenceListShown, TasteClickListener tasteClickListener){
        super(context);
        onExpandClickListener = expandClick;
        mTasteClickListener = tasteClickListener;
        mSubTasteClickListener = tasteClickListener;
        initViews(context);
        setQuiz(context,attempt,position,isSubPreferenceListShown);
    }

    //  set quiz in adapter
    private void setQuiz(Context context, final Category category , final int position , final boolean isSubPreferenceListShown) {


        if(category.getSub().size()>0) {
            mIvExpandable.setVisibility(VISIBLE);
            mLLTasteHeadingContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            mIvExpandable.setColorFilter(ContextCompat.getColor(mContext,R.color.colorWhite));
            mTastePreferncesListApdater = new TastePreferencesListAdapter(category.getSub(), context, mSubTasteClickListener, position);
            mRecyclerView.setAdapter(mTastePreferncesListApdater);
        }else{
            mIvExpandable.setVisibility(INVISIBLE);
            mIvExpandable.setColorFilter(ContextCompat.getColor(mContext,R.color.colorWhite));
            mLLTasteHeadingContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));

        }

        isSubCategoryListVisible = isSubPreferenceListShown;

        mTvGroupTasteName.setText(category.getTitle());

        changeHeaderPreferenceView(isSubPreferenceListShown);


        if(isSubPreferenceListShown){
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
        }else{
            hideSubCategory();
        }


        mLLTasteHeadingContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(category.getSub().size()>0) {

                    if (isSubCategoryListVisible) {
                        onExpandClickListener.onExpandClick(position, false);
                    } else {
                        onExpandClickListener.onExpandClick(position, true);
                    }
                }
            }
        });



        switch (category.getTaste()){
            case NO_TASTE:
                mIvNever.setImageResource(R.drawable.radio_check_active);
                mIvNever.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblue));
                mIvOften.setImageResource(R.drawable.radio_check_inactive);
                break;
            case TASTE_OFTEN:
                mIvNever.setImageResource(R.drawable.radio_check_inactive);
                mIvOften.setImageResource(R.drawable.radio_check_active);
                mIvOften.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblue));

                break;
            default:
                mIvNever.setImageResource(R.drawable.radio_check_active);
                mIvNever.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblue));
                mIvOften.setImageResource(R.drawable.radio_check_inactive);
                break;

        }


        mIvNever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // -1 if it is not sub Category click just to differentiate
                mTasteClickListener.onTasteClick(category.getId(),TastePreferencesListAdapter.TASTE_NEVER,position,-1);
                mIvOften.setImageResource(R.drawable.radio_check_inactive);
                mIvNever.setImageResource(R.drawable.radio_check_active);
                mIvNever.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblue));

            }
        });

        mIvOften.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // -1 if it is not sub Category click just to differentiate
                mTasteClickListener.onTasteClick(category.getId(), TASTE_OFTEN,position,-1);
                mIvOften.setImageResource(R.drawable.radio_check_active);
                mIvOften.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblue));
                mIvNever.setImageResource(R.drawable.radio_check_inactive);

            }
        });


    }

    //  changing the header
    private void changeHeaderPreferenceView(boolean isSubPreferenceListShown) {
        if(isSubPreferenceListShown) {
            mIvExpandable.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
//            mLLTasteHeadingContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            mTvGroupTasteName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));

        }else{
            mIvExpandable.setImageResource(R.drawable.ic_arrow_drop_down_black);
            mIvExpandable.setColorFilter(ContextCompat.getColor(mContext,R.color.colorWhite));
//            mLLTasteHeadingContainer.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            mTvGroupTasteName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));

        }

        }


        //  initialize view
    private void initViews(Context context){

        if(context instanceof TasteClickListener){
            mSubTasteClickListener = (TasteClickListener) context;
            mTasteClickListener = (TasteClickListener) context;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.taste_preferences_list_item, this);
        mContext= context;

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mIvExpandable = view.findViewById(R.id.iv_expandable_icon);

        mLLTasteHeadingContainer = view.findViewById(R.id.ll_taste_heading_container);

        mTvGroupTasteName = view.findViewById(R.id.tv_taste_group);
        mIvNever = view.findViewById(R.id.iv_taste_never);
        mIvOften = view.findViewById(R.id.iv_taste_often);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);


    }


    // hide sub category
    private void hideSubCategory() {
        mRecyclerView.setVisibility(View.GONE);
    }



    /**
     * different click handled by onItemClick
     */
    public interface onExpandClick {
        void onExpandClick(int position, boolean isExpanded );
    }




}
