package com.imentors.wealthdragons.views;

import android.graphics.Canvas;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.adapters.DownLoadDetailGroupListAdapter;
import com.imentors.wealthdragons.adapters.DownLoadDetailListAdapter;

public class DeleteSwipeRecyclerView extends ItemTouchHelper.SimpleCallback {
    private DeleteSwipeRecyclerView.RecyclerItemTouchHelperListener listener;


    public DeleteSwipeRecyclerView(int dragDirs, int swipeDirs, DeleteSwipeRecyclerView.RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;

    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        listener.onSwiped(viewHolder, i, viewHolder.getAdapterPosition());

    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        final View foregroundView;

        if (viewHolder instanceof DownLoadDetailListAdapter.MyViewHolder) {
            foregroundView = ((DownLoadDetailListAdapter.MyViewHolder) viewHolder).mViewForeground;
        } else {
            foregroundView = ((DownLoadDetailGroupListAdapter.MyViewHolder) viewHolder).mViewForeground;
        }

        getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        final View foregroundView;

        if (viewHolder instanceof DownLoadDetailListAdapter.MyViewHolder) {
            foregroundView = ((DownLoadDetailListAdapter.MyViewHolder) viewHolder).mViewForeground;
        } else {
            foregroundView = ((DownLoadDetailGroupListAdapter.MyViewHolder) viewHolder).mViewForeground;
        }

        getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY,
                actionState, isCurrentlyActive);
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {

        final View foregroundView;

        if (viewHolder != null) {
            if (viewHolder instanceof DownLoadDetailListAdapter.MyViewHolder) {
                foregroundView = ((DownLoadDetailListAdapter.MyViewHolder) viewHolder).mViewForeground;
            } else {
                foregroundView = ((DownLoadDetailGroupListAdapter.MyViewHolder) viewHolder).mViewForeground;
            }


            getDefaultUIUtil().onSelected(foregroundView);

        }
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {

        final View foregroundView;

        if (viewHolder instanceof DownLoadDetailListAdapter.MyViewHolder) {
            foregroundView = ((DownLoadDetailListAdapter.MyViewHolder) viewHolder).mViewForeground;
        } else {
            foregroundView = ((DownLoadDetailGroupListAdapter.MyViewHolder) viewHolder).mViewForeground;
        }

        getDefaultUIUtil().clearView(foregroundView);
    }


    public interface RecyclerItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position);
    }


}


