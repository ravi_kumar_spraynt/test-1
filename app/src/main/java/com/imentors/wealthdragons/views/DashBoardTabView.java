package com.imentors.wealthdragons.views;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.DashBoardListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.EventClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.interfaces.SeeAllClickListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.PagentaionItem;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Map;

public class DashBoardTabView extends LinearLayout implements DashBoardListAdapter.OnItemClickListener,PagingListener{

    private RecyclerView mRecyclerView;
    private WealthDragonTextView mRecylerViewHeading , mTxtViewAll,mHeading;
    private DashBoardListAdapter mDashBoardListAdapter;
    private ImageView mHeadingImage;
    private LinearLayout mHeadingContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private SeeAllClickListener mSeeAllClickListeners;
    private ArticleClickListener mArticleClickListeners;
    private MentorClickListener mMentorClickListener;
    private VideoClickListener mVideoClickListener;
    private EventClickListener mEventClickListener;
    private boolean mIsProgrammeType;
    // for log purpose
    private String mTitle,mTitleHeading;
    private Context mContext;

    private static final int  MAX_ITEM_COUNT = 1;
    private float startX;
    private float startY;
    private String mCategoryViewType;


    public DashBoardTabView(Context context) {
        super(context);
    }

    public DashBoardTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DashBoardTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DashBoardTabView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public DashBoardTabView(Context context , DashBoardItemOderingMentors mentorList, String categoryViewType){
        super(context);
        initViews(context,mentorList);
        setMentorList(context,mentorList);
        mCategoryViewType = categoryViewType;
        
    }

    //  set mentor data in view
    private void setMentorList(Context context, DashBoardItemOderingMentors mentorList ) {


        mDashBoardListAdapter = new DashBoardListAdapter(this,this,context);

        mDashBoardListAdapter.setItems(null,mentorList.getItems(),null,null,mentorList.getTitle(),mentorList.getPre_fix(),mentorList.getIcon(),mentorList.getLimit(),mentorList.getNext_page(),mentorList.getTotal_items(),mentorList.getCat_id(),Constants.TYPE_MENTOR);

        mRecyclerView.setAdapter(mDashBoardListAdapter);

        if(mentorList.getItems().size() > mentorList.getLimit()){
            mTxtViewAll.setVisibility(View.VISIBLE);

        }else{
            mTxtViewAll.setVisibility(View.GONE);

        }


    }

    public DashBoardTabView(Context context , DashBoardItemOderingVideo videoList){
        super(context);
        initViews(context,videoList);
        setVideoList(context, videoList);
    }

    //  set video data in view
    private void setVideoList(Context context, DashBoardItemOderingVideo videoList) {


        mDashBoardListAdapter = new DashBoardListAdapter(this,this,context);

        mDashBoardListAdapter.setItems(null,null,videoList.getItems(),null,videoList.getTitle(),videoList.getPre_fix(),videoList.getIcon(),videoList.getLimit(),videoList.getNext_page(),videoList.getTotal_items(),null,Constants.TYPE_VIDEO);

        mRecyclerView.setAdapter(mDashBoardListAdapter);

        if(videoList.getItems().size() > videoList.getLimit()){
            mTxtViewAll.setVisibility(View.VISIBLE);

        }else{
            mTxtViewAll.setVisibility(View.GONE);

        }

    }


    public DashBoardTabView(Context context , DashBoardItemOderingArticle articleList , boolean isProgrammeType){
        super(context);
        initViews(context,articleList);
        mIsProgrammeType = isProgrammeType;

        setArticleList(context,articleList);
    }

    // set article data in view
    private void setArticleList(Context context, DashBoardItemOderingArticle articleList) {


        mDashBoardListAdapter = new DashBoardListAdapter(this,this,context);

        if(mIsProgrammeType){
            mDashBoardListAdapter.setItems(articleList.getItems(),null,null,null,articleList.getTitle(),articleList.getPre_fix(),articleList.getIcon(),articleList.getLimit(),articleList.getNext_page(),articleList.getTotal_items(),articleList.getCat_id(),Constants.TYPE_PROGRAMMES);

        }
        else if(articleList.getSee_all().equals(Constants.ALL_UPCOMING_COURSE)){

            mDashBoardListAdapter.setItems(articleList.getItems(),null,null,null,articleList.getTitle(),articleList.getPre_fix(),articleList.getIcon(),articleList.getLimit(),articleList.getNext_page(),articleList.getTotal_items(),articleList.getCat_id(),Constants.ALL_UPCOMING_COURSE);

        } else if(articleList.getSee_all().equals(Constants.ON_SALE_KEY)){

            mDashBoardListAdapter.setItems(articleList.getItems(),null,null,null,articleList.getTitle(),articleList.getPre_fix(),articleList.getIcon(),articleList.getLimit(),articleList.getNext_page(),articleList.getTotal_items(),articleList.getCat_id(),Constants.ON_SALE_KEY);
        }
        else if(articleList.getSee_all().equals(Constants.COURSES_KEY)){
            mDashBoardListAdapter.setItems(articleList.getItems(),null,null,null,articleList.getTitle(),articleList.getPre_fix(),articleList.getIcon(),articleList.getLimit(),articleList.getNext_page(),articleList.getTotal_items(),articleList.getCat_id(),Constants.COURSES_KEY);
        }else{
            mDashBoardListAdapter.setItems(articleList.getItems(),null,null,null,articleList.getTitle(),articleList.getPre_fix(),articleList.getIcon(),articleList.getLimit(),articleList.getNext_page(),articleList.getTotal_items(),articleList.getCat_id(),Constants.TYPE_COURSE);

        }

        mRecyclerView.setAdapter(mDashBoardListAdapter);


        if(articleList.getItems().size() > articleList.getLimit()){
            mTxtViewAll.setVisibility(View.VISIBLE);

        }else{
            mTxtViewAll.setVisibility(View.GONE);

        }

    }

    public DashBoardTabView(Context context , DashBoardItemOderingEvent eventList ){
        super(context);
        initViews(context,eventList);

        setEventList(context,eventList);
    }

    //  set event data in view
    private void setEventList(Context context, DashBoardItemOderingEvent eventList) {


        mDashBoardListAdapter = new DashBoardListAdapter(this,this,context);

        mDashBoardListAdapter.setItems(null,null,null,eventList.getItems(),eventList.getTitle(),eventList.getPre_fix(),eventList.getIcon(),eventList.getLimit(),eventList.getNext_page(),eventList.getTotal_items(),null,Constants.TYPE_EVENT);

        mRecyclerView.setAdapter(mDashBoardListAdapter);

        if(eventList.getItems().size() > eventList.getLimit()){
            mTxtViewAll.setVisibility(View.VISIBLE);
        }else{
            mTxtViewAll.setVisibility(View.GONE);

        }


    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mSeeAllClickListeners = null;
        mArticleClickListeners = null;
        mMentorClickListener = null;
        mVideoClickListener = null;
        mEventClickListener = null;
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(ListUpdateListener);

    }


    //  initialize view
    private void initViews(final Context context , final DashBoardItemOdering dashBoardItemOdering ){

        mContext = context;

        if(context instanceof SeeAllClickListener){
            mSeeAllClickListeners = (SeeAllClickListener) context;
        }

        if(context instanceof ArticleClickListener){
            mArticleClickListeners = (ArticleClickListener) context;
        }

        if(context instanceof MentorClickListener){
            mMentorClickListener = (MentorClickListener) context;
        }

        if(context instanceof VideoClickListener){
            mVideoClickListener = (VideoClickListener) context;
        }



        if(context instanceof EventClickListener){
            mEventClickListener = (EventClickListener) context;
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dashboard_item_view, this);

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLinearLayoutManager.setInitialPrefetchItemCount(8);
        mLinearLayoutManager.setItemPrefetchEnabled(true);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecylerViewHeading = view.findViewById(R.id.txt_header_name);
        mHeadingImage = view.findViewById(R.id.image_view_header);
        mHeadingContainer = view.findViewById(R.id.ll_header_view);
        mTxtViewAll = view.findViewById(R.id.txt_view_all);
        mHeading=view.findViewById(R.id.tv_header);

        mTxtViewAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mSeeAllClickListeners.onSeeAllClicked(dashBoardItemOdering.getSee_all(),dashBoardItemOdering);
                Utils.callEventLogApi("visited <b>"+mTitle + "</b> see all");

            }
        });

        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        LocalBroadcastManager.getInstance(context).registerReceiver(ListUpdateListener,new IntentFilter(Constants.BROADCAST_DASHBOARD_TAB_LIST_UPDATED));
    }

    //  clicked article items
    @Override
    public void onArticleClick(Article item, int position) {


        // only programme type contain course_id not course type
        if(item.getLayout().equals(Constants.TYPE_PROGRAMMES)){
            mArticleClickListeners.onArticleClick(Constants.TYPE_PROGRAMMES,item.getId());

        }else{
            mArticleClickListeners.onArticleClick(Constants.TYPE_COURSE,item.getId());

        }

    }

    //  clicked mentor items
    @Override
    public void onMentorClick(Mentors item, int position) {

        mMentorClickListener.onMentorClick(Constants.TYPE_MENTOR,item.getId());
    }

    //  clicked video items
    @Override
    public void onVideoClick(Video item, int position) {

        if(item.isEpisode()){
            mVideoClickListener.onVideoClick(Constants.TYPE_EPISODE,item.getVideo_id(),item.getId());
        }else{
            mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO,item.getId(),null);
        }
    }

    //  clicked event items
    @Override
    public void onEventClick(Event item, int position) {
        mEventClickListener.onEventClick(Constants.TYPE_EVENT,item.getId());
    }


    //  set header
    @Override
    public void setHeader(String title,String heading, String imageHeader, int listSize , int viewAllVisibilityLimit) {
        if(TextUtils.isEmpty(title) && TextUtils.isEmpty(heading)){
            mRecylerViewHeading.setVisibility(View.GONE);
            mHeading.setVisibility(GONE);
            mHeadingContainer.setVisibility(View.GONE);
        }else {
            mTitle = title;
            mTitleHeading=heading;
            mRecylerViewHeading.setTextColor(getResources().getColor(R.color.btnBGcolorOrange));
            mHeadingContainer.setVisibility(View.VISIBLE);
            mRecylerViewHeading.setVisibility(View.VISIBLE);
            mRecylerViewHeading.setText(mTitle);
            mHeading.setVisibility(VISIBLE);
            mHeading.setText(mTitleHeading);



            if(listSize > viewAllVisibilityLimit){
                mTxtViewAll.setVisibility(View.VISIBLE);
            }else{
                mTxtViewAll.setVisibility(View.GONE);
            }
        }

        if(TextUtils.isEmpty(imageHeader)){
            mHeadingImage.setVisibility(View.GONE);
//            mHeading.setPadding(Utils.dpToPx(2), 0, Utils.dpToPx(4), 0);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(imageHeader).into(mHeadingImage);
        }
    }



    private BroadcastReceiver ListUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mDashBoardListAdapter.clearData();
        }
    };


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String viewType, String seeAllType) {


        switch (viewType) {
            case Constants.TYPE_COURSE:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_COURSE_PAGENATION, viewType);
                break;

            case Constants.TYPE_MENTOR:

                if(TextUtils.isEmpty(tasteId)){
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_MENTOR_PAGENATION, viewType);

                }else {
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_NEW_MENTOR_PAGENATION, viewType);

                }

                break;

            case Constants.TYPE_VIDEO:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_VIDEO_PAGENATION, viewType);
                break;

            case Constants.TYPE_PROGRAMMES:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_PROGRAMME_PAGENATION, viewType);
                break;

            case Constants.ALL_UPCOMING_COURSE:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_UPCOMING_COURSE_PAGENATION, viewType);
                break;

            case Constants.COURSES_KEY:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.DASHBOARD_FEATURED_COURSE_PAGENATION, viewType);
                break;

            case Constants.ON_SALE_KEY:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.ON_SALE_PAGINATION, viewType);
                break;



        }


    }


    public void callSeeAllPaginationApi(final int pageNumber , final String id , String api, final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    addDataInAdapter(response,viewType);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if(!Utils.setErrorDialog(response,getContext())){
                            setErrorMessage();
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                        Utils.callEventLogApi("getting <b> Pagination</b> error");

                        setErrorMessage();
                    }

                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b> Pagination</b> error");
                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                }else{
                    setErrorMessage();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(pageNumber !=0){
                    params.put(Keys.PAGE_NO,String.valueOf(pageNumber));
                }

                if(!TextUtils.isEmpty(id)){
                    params.put(Keys.CAT_ID, id);
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }
    // add data in adapter
    private void addDataInAdapter(String response , String viewType) {

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final PagentaionItem dashBoardPagenationData = gson.fromJson(response, PagentaionItem.class);

        DashBoardItemOdering dashBoard = dashBoardPagenationData.getHeadingOrdering();

        switch (viewType){


            case Constants.TYPE_MENTOR:
                DashBoardItemOderingMentors dashBoardItemOderingMentor = (DashBoardItemOderingMentors) dashBoard;
                if(dashBoardItemOderingMentor.getItems().size()>0) {

                    mDashBoardListAdapter.addItems(null,dashBoardItemOderingMentor.getItems(),null,null,dashBoardItemOderingMentor.getNext_page(),dashBoardItemOderingMentor.getTotal_items());

                }

                break;
            case Constants.TYPE_VIDEO:
                DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoard;
                if(dashBoardItemOderingVideo.getItems().size()>0) {

                    mDashBoardListAdapter.addItems(null,null,dashBoardItemOderingVideo.getItems(),null,dashBoardItemOderingVideo.getNext_page(),dashBoardItemOderingVideo.getTotal_items());

                }

                break;

            default:

                DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoard;
                if(dashBoardItemOderingArticle.getItems().size()>0) {

                    mDashBoardListAdapter.addItems(dashBoardItemOderingArticle.getItems(), null, null, null, dashBoardItemOderingArticle.getNext_page(), dashBoardItemOderingArticle.getTotal_items());
                }
                break;
        }


    }
    //  set error message
    private void setErrorMessage() {

        mDashBoardListAdapter.setServerError();
    }


}
