package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.imentors.wealthdragons.R;

import java.util.ArrayList;
import java.util.List;

public class ExplosionView extends View {


    private Paint mPaint;
    private Context mContext;
    private List<Path> pathList;
    private float radius;

    public ExplosionView(Context context) {
        super(context);
        mContext = context;
        init();

    }

    public ExplosionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();

    }

    public ExplosionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }


    private void init(){
        setLayerType(LAYER_TYPE_SOFTWARE, null);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(ContextCompat.getColor(mContext, R.color.colorWhite));
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        radius = getWidth()/2;
        pathList = new ArrayList<>();

//        for(int i=0;i<12;i++){
//            Path path = new Path();
//            path.moveTo((float)(getWidth()/2), (float)(getHeight()/2));
//            path.lineTo(radius+(float) (radius * Math.cos(30 * i)),radius+(float) (radius * Math.sin(30 * i)));
//            path.close();
//            pathList.add(path);
//        }

        Path path = new Path();
        path.moveTo(radius, radius);
        float  x = radius+(float) (radius * Math.cos(30));
        float y  = radius+(float) (radius * Math.sin(30 ));
        path.lineTo(250,250);
        pathList.add(path);

//        canvas.drawCircle(getWidth()/2, getHeight()/2, radius, mPaint);

//        for(int i=0;i<12;i++){
//            if(i%2==0){
//                mPaint.setColor(ContextCompat.getColor(mContext, R.color.colorWhite));
//            }else{
//                mPaint.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
//            }
//
//            canvas.drawPath(pathList.get(i),mPaint);
//
//        }



        canvas.drawPath(path,mPaint);
        path.reset();
    }
}
