package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.braintreepayments.cardform.view.ExpirationDateEditText;


/**
 * An {@link android.widget.EditText} for entering dates, used for card expiration dates.
 * Will automatically format input as it is entered.
 */
public class CardFormExpirationDateEditText extends ExpirationDateEditText implements ViewUtils.ViewExtension {


    public CardFormExpirationDateEditText(Context context) {
        super(context);
        init(context, null);    }

    public CardFormExpirationDateEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public CardFormExpirationDateEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        ViewUtils.init(this, context, attrs);

    }


    @Override
    public void onInit(Typeface typeface) {
        if (typeface != null) {
            this.setTypeface(typeface);
        }
    }
}
