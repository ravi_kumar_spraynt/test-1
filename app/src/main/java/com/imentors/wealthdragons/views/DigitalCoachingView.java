package com.imentors.wealthdragons.views;

import android.app.Activity;
import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;


import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.models.DigitalCoaching;

import com.imentors.wealthdragons.utils.Utils;



public class DigitalCoachingView extends LinearLayout {


    private ImageView mIvDigitalCoaching;
    private WealthDragonTextView mTvDigitalCoachingName, mTvDigitalCoachingDetailHeading, mTvDigitalCoachingDetailDescription, mStripFreePaid;
    private Context mContext;
    private String mItemId;

    public DigitalCoachingView(Context context) {
        super(context);
        mContext = context;
    }

    public DigitalCoachingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

    }

    public DigitalCoachingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

    }

    public DigitalCoachingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mIvDigitalCoaching = findViewById(R.id.iv_video_image);
        mTvDigitalCoachingName = findViewById(R.id.tv_digital_detail_name);
        mStripFreePaid = findViewById(R.id.tv_strip_detail);

        mTvDigitalCoachingDetailHeading = findViewById(R.id.tv_digital_detail_heading);
    }

    /**
     * /**
     * bind the view with article item
     *
     * @param item article item
     */
    public void bind(final DigitalCoaching item, final String tabPosition, final String isSubscribed, boolean isDownloaded) {


        mTvDigitalCoachingName.setText(item.getTitle());

        if (!TextUtils.isEmpty(item.getDescription()) && !item.getDescription().equals(".")) {
            mTvDigitalCoachingDetailHeading.setVisibility(VISIBLE);
            mTvDigitalCoachingDetailHeading.setText(item.getDescription());

        }else{
            mTvDigitalCoachingDetailHeading.setVisibility(GONE);

        }


        mIvDigitalCoaching.getLayoutParams().width = (int) Utils.getDigitalImageWidth((Activity) mContext);
        mIvDigitalCoaching.getLayoutParams().height = (int) Utils.getDigitalImageHeight((Activity) mContext);

        if(isDownloaded){
            Glide.with(mContext).load(item.getmDownloadBanner()).error(R.drawable.no_img_video).dontAnimate().into(mIvDigitalCoaching);


        }else{
            double height = mIvDigitalCoaching.getLayoutParams().height;
            double width = mIvDigitalCoaching.getLayoutParams().width;

            String bigImageUrl = Utils.getImageUrl(item.getBanner(), width, height);

            Glide.with(mContext).load(bigImageUrl).error(R.drawable.no_img_video).dontAnimate().into(mIvDigitalCoaching);
        }




        if (!TextUtils.isEmpty(item.getFree_paid()) && item.getFree_paid().equals("Free") ) {
            mTvDigitalCoachingDetailHeading.setVisibility(VISIBLE);
            mStripFreePaid.setVisibility(VISIBLE);
            mStripFreePaid.setText(item.getFreePaidText());

        }else{
            mStripFreePaid.setVisibility(GONE);

        }
    }


}





