package com.imentors.wealthdragons.views;


import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Dheeraj Singh on 11/27/17.
 */

public class EmptyViewHolder extends RecyclerView.ViewHolder {
    public EmptyViewHolder(@NonNull View view) {
        super(view);
    }
}