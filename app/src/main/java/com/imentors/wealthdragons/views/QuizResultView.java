package com.imentors.wealthdragons.views;

import android.content.Context;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.adapters.QuizResultListAdapter;

import com.imentors.wealthdragons.models.QuizResult;

import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QuizResultView extends LinearLayout {

    private RecyclerView mRecyclerView ;
    private WealthDragonTextView mTvDateTitle , mTvCorrectAnswerCount , mTvIncorrectAnswerCount;
    private LinearLayout mLLDateContainer,mLlBelowHiddenLayout;
    private LinearLayoutManager mLinearLayoutManager;
    private QuizResultListAdapter mQuizListAdapter;
    private boolean isTablet,isSubCategoryListVisible = false;
    private Context mContext;
    private ImageView mIvExpandable;
    private onExpandClick onExpandClickListener;



    public QuizResultView(Context context) {
        super(context);
    }

    public QuizResultView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public QuizResultView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public QuizResultView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public QuizResultView(Context context , QuizResult.Attempt attempt ,onExpandClick expandClick ,int position , boolean isQuestionListShown){
        super(context);
        onExpandClickListener = expandClick;
        initViews(context);
        setQuiz(context,attempt,position,isQuestionListShown);
    }

    //  set quiz in adapter
    private void setQuiz(Context context, final QuizResult.Attempt attempt , final int position , final boolean isQuestionListShown) {

        mTvDateTitle.setText(attempt.getHeading());


        mQuizListAdapter = new QuizResultListAdapter(attempt.getQuestion(),context);
        mRecyclerView.setAdapter(mQuizListAdapter);



        if(isQuestionListShown){
            mRecyclerView.setVisibility(View.VISIBLE);
            mLlBelowHiddenLayout.setVisibility(VISIBLE);
            mIvExpandable.setImageResource(R.drawable.ic_remove);

            mRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));

            mTvCorrectAnswerCount.setText(mContext.getString(R.string.correct_answers_count)+" "+attempt.getCorrect());
            mTvIncorrectAnswerCount.setText(mContext.getString(R.string.incorrect_answers)+" "+attempt.getIncorrect());
        }else{
            hideSubCategory();
            mIvExpandable.setImageResource(R.drawable.ic_add);
        }


        mLLDateContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked on <b>Expand Icon </b>");

                if(isQuestionListShown){
                    onExpandClickListener.onExpandClick(-1);

                }else {
                    onExpandClickListener.onExpandClick(position);
                }
            }
        });

    }

    //  initialize view
    private void initViews(Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.quiz_result_list_item, this);
        mContext= context;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mIvExpandable = view.findViewById(R.id.iv_expandable_icon);

        mLLDateContainer = view.findViewById(R.id.ll_date_container);
        mTvDateTitle = view.findViewById(R.id.tv_quiz_date);
        mTvCorrectAnswerCount = view.findViewById(R.id.tv_correct_answer);
        mTvIncorrectAnswerCount = view.findViewById(R.id.tv_incorrect_answer);
        mLlBelowHiddenLayout = view.findViewById(R.id.ll_correct_answer);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setNestedScrollingEnabled(false);


    }


    //  hide sub category
    private void hideSubCategory() {
        mRecyclerView.setVisibility(View.GONE);
        mLlBelowHiddenLayout.setVisibility(GONE);
        isSubCategoryListVisible = !isSubCategoryListVisible;
    }


    /**
     * different click handled by onItemClick
     */
    public interface onExpandClick {
        void onExpandClick(int position);
    }


}
