package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Typeface;

import android.util.AttributeSet;

import com.braintreepayments.cardform.view.CardEditText;

/**
 * An {@link android.widget.EditText} that displays Card icons based on the number entered.
 */
public class CardFormEditText extends CardEditText implements ViewUtils.ViewExtension {

    @Override
    public void onInit(Typeface typeface) {
        if (typeface != null) {
            this.setTypeface(typeface);
        }
    }

    public CardFormEditText(Context context) {
        super(context);
        init(context, null);
    }

    public CardFormEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CardFormEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context,attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        ViewUtils.init(this, context, attrs);

    }
}
