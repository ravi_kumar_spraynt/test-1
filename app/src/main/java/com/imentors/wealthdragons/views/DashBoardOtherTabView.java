package com.imentors.wealthdragons.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.CoursesTabListAdapter;
import com.imentors.wealthdragons.adapters.MentorsTabListAdapter;
import com.imentors.wealthdragons.adapters.SeeAllCoursesTabListAdapter;
import com.imentors.wealthdragons.adapters.SeeAllEventsTabListAdapter;
import com.imentors.wealthdragons.adapters.SubCategoryListAdapter;
import com.imentors.wealthdragons.adapters.VideosTabListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.EventClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.PagentaionItem;
import com.imentors.wealthdragons.models.SubCategory;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DashBoardOtherTabView extends LinearLayout implements MentorsTabListAdapter.OnItemClickListener,VideosTabListAdapter.OnItemClickListener,CoursesTabListAdapter.OnItemClickListener,SeeAllCoursesTabListAdapter.OnItemClickListener,PagingListener,SubCategoryListAdapter.onSubCategoryClick , SeeAllEventsTabListAdapter.OnItemClickListener{

    private RecyclerView mRecyclerView ,mSubCategoryRecyclerView;
    private WealthDragonTextView mRecylerViewHeading , mTxtViewAll , mSubCategoryName;
    private MentorsTabListAdapter mMentorsTabListAdapter;
    private VideosTabListAdapter mVideoTabListAdapter;
    private SeeAllEventsTabListAdapter mSeeAllEventsTabListAdapter;
    private ImageView mHeadingImage , mSubCategoryUpIcon;
    private LinearLayout mHeadingContainer,mSubCategoryContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private String mTasteId;
    private List<SubCategory> mSubCategoryList = new ArrayList<>();
    private SubCategoryListAdapter mSubCategoryListAdapter;
    private boolean isTablet,isSubCategoryListVisible = false,mIsProgrammeType;
    private Context mContext;
    private boolean isSubCategoryFilter = false;
    private ArticleClickListener mArticleClickListeners;
    private MentorClickListener mMentorClickListener;
    private VideoClickListener mVideoClickListener;
    private EventClickListener mEventClickListener;

    private static final int  MAX_ITEM_COUNT = 1;
    private CoursesTabListAdapter mCoursesTabListAdapter;
    private SeeAllCoursesTabListAdapter mSeeAllCoursesTabListAdapter;


    public DashBoardOtherTabView(Context context) {
        super(context);
    }

    public DashBoardOtherTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DashBoardOtherTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DashBoardOtherTabView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public DashBoardOtherTabView(Context context , DashBoardItemOderingMentors mentorList, int itemCountOnServer , String tasteId , int nextPage ){
        super(context);
        mTasteId = tasteId;
        mSubCategoryList.add(new SubCategory(mTasteId,"Show All"));
        if(mentorList.getSubCategory().size()>0) {
            mSubCategoryList.addAll(mentorList.getSubCategory());
        }
        initViews(context);
        setMentorList(context,mentorList,itemCountOnServer,tasteId,nextPage);

    }

    //  set Recyler View Heading in mentor list
    private void setMentorList(Context context, final DashBoardItemOderingMentors mentorList , int itemCountOnServer , String tasteId , int nextPage) {
        mRecylerViewHeading.setVisibility(View.VISIBLE);

        mRecylerViewHeading.setText(mentorList.getTitle());

        if(TextUtils.isEmpty(mentorList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(mentorList.getBanner()).into(mHeadingImage);
        }



        mMentorsTabListAdapter = new MentorsTabListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(this,this,context,false);


        mMentorsTabListAdapter.setItems(mentorList.getMentors(),Constants.MENTOR_TAB,nextPage,itemCountOnServer,tasteId);

        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_MENTOR,null);


        mLinearLayoutManager.setInitialPrefetchItemCount(7);

        mRecyclerView.setAdapter(mMentorsTabListAdapter);

        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);


    }


    public DashBoardOtherTabView(Context context , DashBoardItemOderingVideo videoList , int itemCountOnServer , String tasteId , int nextPage){
        super(context);
        mTasteId = tasteId;
        mSubCategoryList.add(new SubCategory(mTasteId,"Show All"));
        if(videoList.getSubCategory().size()>0) {
            mSubCategoryList.addAll(videoList.getSubCategory());
        }
        initViews(context);
        setVideoList(context, videoList,itemCountOnServer,tasteId,nextPage);
    }

    //  set Recyler View Heading in video list
    private void setVideoList(Context context, DashBoardItemOderingVideo videoList, int itemCountOnServer , String tasteId , int nextPage) {
        mRecylerViewHeading.setVisibility(View.VISIBLE);

        mRecylerViewHeading.setText(videoList.getTitle());

        if(TextUtils.isEmpty(videoList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(videoList.getBanner()).into(mHeadingImage);
        }


        mVideoTabListAdapter = new VideosTabListAdapter(this,this,context);

        mSubCategoryListAdapter = new SubCategoryListAdapter(this,this,context,false);


        mVideoTabListAdapter.setItems(videoList.getVideos(),nextPage,itemCountOnServer,tasteId);

        mSubCategoryListAdapter.setItems(mSubCategoryList,null,Constants.TYPE_VIDEO,null);


        mLinearLayoutManager.setInitialPrefetchItemCount(7);

        mRecyclerView.setAdapter(mVideoTabListAdapter);

        mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);


    }


    public DashBoardOtherTabView(Context context , DashBoardItemOderingEvent articleList , boolean isSeeAll, int itemCountOnServer, String seeAllApiKey , String tasteId , int nextPage ){
        super(context);
        mTasteId = tasteId;
        mSubCategoryList.add(new SubCategory(mTasteId,"Show All"));
        if(articleList.getSubCategory().size()>0) {
            mSubCategoryList.addAll(articleList.getSubCategory());
        }
        initViews(context);
        setEventList(context, articleList ,isSeeAll,itemCountOnServer,seeAllApiKey,tasteId,nextPage);
    }

    //  set Recyler View Heading in event list
    private void setEventList(Context context, DashBoardItemOderingEvent articleList, boolean isSeeAll , int itemCountOnServer,String seeAllApiKey , String tasteId , int nextPage) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);
        mRecylerViewHeading.setText(articleList.getTitle());
        if(TextUtils.isEmpty(articleList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(articleList.getBanner()).into(mHeadingImage);
        }


            mSeeAllEventsTabListAdapter = new SeeAllEventsTabListAdapter(this,this, context);

            mSubCategoryListAdapter = new SubCategoryListAdapter(this,this,context,false);

            mSeeAllEventsTabListAdapter.setItems(articleList.getEvents(), Constants.SEE_ALL_COURSES_TAB,nextPage,itemCountOnServer,tasteId,seeAllApiKey);

            mSubCategoryListAdapter.setItems(mSubCategoryList,seeAllApiKey,Constants.TYPE_SEE_ALL_EVENTS,null);



            mLinearLayoutManager.setInitialPrefetchItemCount(7);

            mRecyclerView.setAdapter(mSeeAllEventsTabListAdapter);

            mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);




    }

    public DashBoardOtherTabView(Context context , DashBoardItemOderingArticle articleList , boolean isSeeAll, int itemCountOnServer,String seeAllApiKey , String tasteId , int nextPage, boolean isProgrammeType){
        super(context);
        mTasteId = tasteId;
        mIsProgrammeType = isProgrammeType;
        mSubCategoryList.add(new SubCategory(mTasteId,"Show All"));
        if(articleList.getSubCategory().size()>0) {
            mSubCategoryList.addAll(articleList.getSubCategory());
        }
        initViews(context);
        setArticleList(context, articleList ,isSeeAll,itemCountOnServer,seeAllApiKey,tasteId,nextPage);
    }

    //  set Recyler View Heading in article list
    private void setArticleList(Context context, DashBoardItemOderingArticle articleList, boolean isSeeAll , int itemCountOnServer,String seeAllApiKey , String tasteId , int nextPage) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);
        mRecylerViewHeading.setText(articleList.getTitle());
        if(TextUtils.isEmpty(articleList.getBanner())){
            mHeadingImage.setVisibility(View.GONE);
        }else{
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(articleList.getBanner()).into(mHeadingImage);
        }



        if(isSeeAll){

            mSeeAllCoursesTabListAdapter = new SeeAllCoursesTabListAdapter(this,this, context);

            mSubCategoryListAdapter = new SubCategoryListAdapter(this,this,context,false);


            if(mIsProgrammeType){
                mSeeAllCoursesTabListAdapter.setItems(articleList.getProgrammes(), Constants.SEE_ALL_COURSES_TAB,nextPage,itemCountOnServer,tasteId,seeAllApiKey,mIsProgrammeType);
            }else{
                mSeeAllCoursesTabListAdapter.setItems(articleList.getCourses(), Constants.SEE_ALL_COURSES_TAB,nextPage,itemCountOnServer,tasteId,seeAllApiKey,mIsProgrammeType);

            }


            mSubCategoryListAdapter.setItems(mSubCategoryList,seeAllApiKey,Constants.TYPE_SEE_ALL_COURSES,null);



            mLinearLayoutManager.setInitialPrefetchItemCount(7);

            mRecyclerView.setAdapter(mSeeAllCoursesTabListAdapter);

            mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);



        } else {
            mCoursesTabListAdapter = new CoursesTabListAdapter(this,this, context);

            mSubCategoryListAdapter = new SubCategoryListAdapter(this,this,context,false);


            mCoursesTabListAdapter.setItems(articleList.getCourses(),nextPage,itemCountOnServer,tasteId,mIsProgrammeType);
            mSubCategoryListAdapter.setItems(mSubCategoryList,seeAllApiKey,Constants.TYPE_COURSE,null);



            mLinearLayoutManager.setInitialPrefetchItemCount(7);

            mRecyclerView.setAdapter(mCoursesTabListAdapter);

            mSubCategoryRecyclerView.setAdapter(mSubCategoryListAdapter);



        }
    }



    private void initViews(Context context){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dashboard_item_view, this);
        mContext= context;

        if(context instanceof ArticleClickListener){
            mArticleClickListeners = (ArticleClickListener) context;
        }

        if(context instanceof MentorClickListener){
            mMentorClickListener = (MentorClickListener) context;
        }

        if(context instanceof VideoClickListener){
            mVideoClickListener = (VideoClickListener) context;
        }


        if(context instanceof EventClickListener){
            mEventClickListener = (EventClickListener) context;
        }


        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLinearLayoutManager.setInitialPrefetchItemCount(8);
        mLinearLayoutManager.setItemPrefetchEnabled(true);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecylerViewHeading = view.findViewById(R.id.txt_header_name);
        mHeadingImage = view.findViewById(R.id.image_view_header);
        mHeadingContainer = view.findViewById(R.id.ll_header_view);
        mTxtViewAll = view.findViewById(R.id.txt_view_all);
        mSubCategoryContainer = view.findViewById(R.id.subcategory_container);
        mSubCategoryName= view.findViewById(R.id.tv_subcategory);
        mSubCategoryRecyclerView = view.findViewById(R.id.recycler_view_subcategory);
        mSubCategoryUpIcon = view.findViewById(R.id.iv_subcategory_up_icon);
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);


        // show subcategory in case of more than single subcategory
        if(mSubCategoryList.size()>2){

            mSubCategoryContainer.setVisibility(View.VISIBLE);
            mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
        }

        mSubCategoryContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b> "+mSubCategoryList.get(0).getTitle()+"  subcategory</b> from " +mRecylerViewHeading.getText());

                if(isSubCategoryListVisible){
                    hideSubCategory();
                }else{
                    mSubCategoryRecyclerView.setVisibility(View.VISIBLE);
                    mSubCategoryUpIcon.setVisibility(View.VISIBLE);

                    if(isTablet) {
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
                    }else{
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
                    }
                    mSubCategoryRecyclerView.requestLayout();
                    isSubCategoryListVisible = !isSubCategoryListVisible;

                }

            }
        });



        mTxtViewAll.setVisibility(View.GONE);

        mRecyclerView.setNestedScrollingEnabled(false);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        LocalBroadcastManager.getInstance(context).registerReceiver(ListUpdateListener,new IntentFilter(Constants.BROADCAST_OTHER_TAB_LIST_UPDATED));


    }

    //  hide sub category
    private void hideSubCategory() {
        mSubCategoryRecyclerView.setVisibility(View.GONE);
        mSubCategoryUpIcon.setVisibility(View.GONE);
        isSubCategoryListVisible = !isSubCategoryListVisible;
    }

    //  clicked mentor items
    @Override
    public void onMentorClick(Mentors item, int position) {
        mMentorClickListener.onMentorClick(Constants.TYPE_MENTOR,item.getId());

    }

    //  clicked video items
    @Override
    public void onVideoClick(Video item, int position) {
        if(item.isEpisode()){
            mVideoClickListener.onVideoClick(Constants.TYPE_EPISODE,item.getVideo_id(),item.getId());
        }else{
            mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO,item.getId(),null);
        }
    }

    //  clicked article items
    @Override
    public void onArticleClick(Article item, int position) {
        if(item.getLayout().equals(Constants.TYPE_PROGRAMMES)){
            mArticleClickListeners.onArticleClick(Constants.TYPE_PROGRAMMES,item.getId());

        }else{
            mArticleClickListeners.onArticleClick(Constants.TYPE_COURSE,item.getId());

        }    }

    //  clicked event items
    @Override
    public void onEventClick(Event item, int position) {
        mEventClickListener.onEventClick(Constants.TYPE_EVENT,item.getId());
    }

    @Override
    public void setHeader(String title, String imageHeader, int listSize , int tabPosition) {

    }

    //  removed title
    private void removeTitle() {
        mRecylerViewHeading.setVisibility(View.GONE);
        mHeadingContainer.setVisibility(View.GONE);
        mHeadingImage.setVisibility(View.GONE);
        mTxtViewAll.setVisibility(View.GONE);

    }


    private BroadcastReceiver ListUpdateListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int position = intent.getIntExtra(Constants.TAB_POSITION,1);

            if(position == Constants.MENTOR_TAB) {
                if(mMentorsTabListAdapter!=null) {
                    mMentorsTabListAdapter.clearData(position);
                    removeTitle();
                    hideSubCategory();

                }

            }

            if(position == Constants.VIDEO_TAB){
                if(mVideoTabListAdapter!=null) {

                    mVideoTabListAdapter.clearData(position);
                    removeTitle();
                    hideSubCategory();


                }
            }

            if(position == Constants.COURSE_TAB ){
                if(mCoursesTabListAdapter!=null) {
                    mCoursesTabListAdapter.clearData(position);
                    removeTitle();
                    hideSubCategory();

                }
            }

            if(position == Constants.SEE_ALL_COURSES_TAB){
                if(mSeeAllCoursesTabListAdapter!=null) {
                    mSeeAllCoursesTabListAdapter.clearData(position);
                    removeTitle();
                    hideSubCategory();

                }
            }



            if(position == Constants.SEE_ALL_EVENT_TAB){
                if(mSeeAllEventsTabListAdapter!=null) {
                    mSeeAllEventsTabListAdapter.clearData(position);
                    removeTitle();
                    hideSubCategory();

                }
            }
        }
    };


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String viewType,String seeAllType) {

        switch (viewType){
            case Constants.TYPE_COURSE:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.COURSE_PAGENATION,viewType);
                break;

            case Constants.TYPE_MENTOR:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.MENTOR_PAGENATION,viewType);
                break;

            case Constants.TYPE_VIDEO:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.VIDEO_PAGENTATION,viewType);
                break;

            case Constants.TYPE_SEE_ALL_EVENTS:
                callSeeAllPaginationApi(pageNumber,tasteId, Api.EVENT_PAGENTATION,viewType);
                break;

            default:
                switch (seeAllType){
                    // for different types of courses
                    case Constants.ALL_PROGRAMMES_KEY:
                        callSeeAllPaginationApi(pageNumber,tasteId, Api.PROGRAMME_PAGENTATION,viewType);
                        break;
                    case Constants.ALL_UPCOMING_COURSE:
                        callSeeAllPaginationApi(pageNumber,tasteId, Api.UPCOMING_COURSE_PAGENTATION,viewType);
                        break;
                    default:
                        callSeeAllPaginationApi(pageNumber,tasteId, Api.ON_SALE_PAGENTATION,viewType);
                        break;
                }

                break;
        }

    }

    public void callSeeAllPaginationApi(final int pageNumber , final String tasteId , String api , final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    addDataInAdapter(response,viewType,tasteId);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if(!Utils.setErrorDialog(response,getContext())){
                            setErrorMessage(viewType);
                        }
                    } catch (Exception e1) {
                        Utils.callEventLogApi("getting <b> Pagination</b> error");

                        e1.printStackTrace();
                        setErrorMessage(viewType);
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b> Pagination</b> error");
                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                }else{
                    setErrorMessage(viewType);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(!TextUtils.isEmpty(tasteId)){
                    params.put(Keys.TASTE_ID,tasteId);
                }

                if(pageNumber !=0){
                    params.put(Keys.PAGE_NO,String.valueOf(pageNumber));
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void addDataInAdapter(String response ,String viewType,String tasteId){

        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final PagentaionItem dashBoardPagenationData = gson.fromJson(response, PagentaionItem.class);

        DashBoardItemOdering dashBoard = dashBoardPagenationData.getHeadingOrdering();

        switch (viewType){

            case Constants.TYPE_SEE_ALL_COURSES:
                DashBoardItemOderingArticle dashBoardItemOderingSeeAllArticle = (DashBoardItemOderingArticle) dashBoard;
                if(dashBoardItemOderingSeeAllArticle.getItems().size()>0) {

                    mSeeAllCoursesTabListAdapter.addItems(dashBoardItemOderingSeeAllArticle.getItems(),dashBoardItemOderingSeeAllArticle.getNext_page(),dashBoardItemOderingSeeAllArticle.getTotal_items(),tasteId,dashBoardItemOderingSeeAllArticle.getSee_all());

                }

            break;

            case Constants.TYPE_COURSE:
                DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoard;
                if(dashBoardItemOderingArticle.getItems().size()>0) {

                    mCoursesTabListAdapter.addItems(dashBoardItemOderingArticle.getItems(),dashBoardItemOderingArticle.getNext_page(),dashBoardItemOderingArticle.getTotal_items(),tasteId);

                }
                break;
            case Constants.TYPE_MENTOR:
                DashBoardItemOderingMentors dashBoardItemOderingMentor = (DashBoardItemOderingMentors) dashBoard;
                if(dashBoardItemOderingMentor.getItems().size()>0) {

                    mMentorsTabListAdapter.addItems(dashBoardItemOderingMentor.getItems(),dashBoardItemOderingMentor.getNext_page(),dashBoardItemOderingMentor.getTotal_items(),tasteId);

                }
                isSubCategoryFilter = !isSubCategoryFilter;

                break;
            case Constants.TYPE_VIDEO:
                DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoard;
                if(dashBoardItemOderingVideo.getItems().size()>0) {
                    mVideoTabListAdapter.addItems(dashBoardItemOderingVideo.getItems(),dashBoardItemOderingVideo.getNext_page(),dashBoardItemOderingVideo.getTotal_items(),tasteId);
                }
                break;

            case Constants.TYPE_SEE_ALL_EVENTS:
                DashBoardItemOderingEvent dashBoardItemOderingEvent = (DashBoardItemOderingEvent) dashBoard;
                if(dashBoardItemOderingEvent.getItems().size()>0) {
                    mSeeAllEventsTabListAdapter.addItems(dashBoardItemOderingEvent.getItems(),dashBoardItemOderingEvent.getNext_page(),dashBoardItemOderingEvent.getTotal_items(),tasteId,dashBoardItemOderingEvent.getSee_all());
                }

                break;
            default:
                break;
        }
    }

    //  calling server error
    private void setErrorMessage(String viewType) {
        switch (viewType) {

            case Constants.TYPE_SEE_ALL_COURSES:
                mSeeAllCoursesTabListAdapter.setServerError();
                break;

            case Constants.TYPE_COURSE:
                mCoursesTabListAdapter.setServerError();
                break;

            case Constants.TYPE_MENTOR:
                mMentorsTabListAdapter.setServerError();
                break;

            case Constants.TYPE_VIDEO:
                mVideoTabListAdapter.setServerError();
                break;

            case Constants.TYPE_SEE_ALL_EVENTS:
                mSeeAllEventsTabListAdapter.setServerError();
                break;
        }
    }

    @Override
    public void onSubCategoryItemClick(SubCategory item , String viewType) {

        hideSubCategory();
        Utils.callEventLogApi("clicked <b> "+item.getTitle()+"  subcategory</b> from " +mRecylerViewHeading.getText());

        isSubCategoryFilter = true;

        mSubCategoryName.setText(item.getTitle());


        // clear list to reload again
        switch (viewType) {

            case Constants.TYPE_SEE_ALL_COURSES:
                mSeeAllCoursesTabListAdapter.isSubCategoryFilterData();
                mSeeAllCoursesTabListAdapter.clearData(Constants.SEE_ALL_COURSES_TAB);
                break;

            case Constants.TYPE_COURSE:
                mCoursesTabListAdapter.isSubCategoryFilterData();
                mCoursesTabListAdapter.clearData(Constants.COURSE_TAB);
                break;
            case Constants.TYPE_MENTOR:
                mMentorsTabListAdapter.isSubCategoryFilterData();
                mMentorsTabListAdapter.clearData(Constants.MENTOR_TAB);

                break;
            case Constants.TYPE_VIDEO:
                mVideoTabListAdapter.isSubCategoryFilterData();
                mVideoTabListAdapter.clearData(Constants.VIDEO_TAB);
                break;

            case Constants.TYPE_SEE_ALL_EVENTS:
                mSeeAllEventsTabListAdapter.isSubCategoryFilterData();
                mSeeAllEventsTabListAdapter.clearData(Constants.SEE_ALL_EVENT_TAB);
                break;
        }

    }

    @Override
    public void onSubCategorySeeAllFilterClick(SubCategory item, String parentId) {
        // only used in see All fragment
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mArticleClickListeners = null;
        mMentorClickListener = null;
        mVideoClickListener = null;
        mEventClickListener = null;
    }
}
