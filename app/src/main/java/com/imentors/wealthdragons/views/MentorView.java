package com.imentors.wealthdragons.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.DashBoard;

import com.imentors.wealthdragons.models.DashBoardMentors;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.io.File;
import java.util.Map;

public class MentorView extends LinearLayout{


    private ImageView mMentorImage,mMentorWishlist;
    private TextView mMentorName , mMentorDetials;
    private Context mContext;
    private boolean isWishlistSelected = false,mIsMentor=true;
    private String mItemId;

    public MentorView(Context context) {
        super(context);
        mContext= context;
    }

    public MentorView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext= context;

    }

    public MentorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext= context;

    }

    public MentorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext= context;

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
            mMentorImage = findViewById(R.id.iv_mentor_image);
            mMentorName = findViewById(R.id.tv_mentor_name);
            mMentorDetials = findViewById(R.id.tv_mentor_course_detail);
            mMentorWishlist=findViewById(R.id.iv_add_wishlist);
    }

    /**
     * /**
     * bind the view with article item
     *
     * @param item            article item
     */
    public void bind(final Mentors item , LruCache memCache, final int tabPosition, boolean isDownloaded) {

        String mentorName = item.getName();
        String mentorDetails = item.getPunch_line();
        String mentorDetailsTagLine = item.getTag_line();
        mItemId=item.getId();

        if(isDownloaded){
            mMentorName.setText(mentorName);
//            mMentorDetials.setText(mentorDetails);
            mMentorDetials.setVisibility(GONE);
            mMentorWishlist.setVisibility(GONE);
            mMentorImage.getLayoutParams().width = (int) Utils.getMentorImageWidth((Activity) mContext);
            mMentorImage.getLayoutParams().height = (int) Utils.getMentorImageHeight((Activity) mContext);
            Glide.with(mContext).load(Uri.fromFile(new File(item.getBanner()))).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.no_img_mentor).into(mMentorImage);

        }else{
            if(!TextUtils.isEmpty(item.getWishlist()) && item.getWishlist().equals("Yes")){
                mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                isWishlistSelected = true;

            }else{
                mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                isWishlistSelected = false;
            }


            mMentorWishlist.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isWishlistSelected ) {
                        mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                        callWishListApi(mItemId,item);

                    }else{
                        mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                        callWishListApi(mItemId,item);

                    }
                    isWishlistSelected=!isWishlistSelected;
                }
            });


            if(!TextUtils.isEmpty(mentorName)){
                mMentorName.setText(mentorName);
            }

            if(!TextUtils.isEmpty(mentorDetails)){
                mMentorDetials.setText(mentorDetails);
            }else{
                mMentorDetials.setText(mentorDetailsTagLine);
            }



            mMentorImage.getLayoutParams().width = (int) Utils.getMentorImageWidth((Activity) mContext);
            mMentorImage.getLayoutParams().height = (int) Utils.getMentorImageHeight((Activity) mContext);

            double height = mMentorImage.getLayoutParams().height;
            double width = mMentorImage.getLayoutParams().width;

            String newImageUrl=Utils.getImageUrl(item.getBanner(),width,height);
            String thumbImage=Utils.getImageUrl(item.getLow_qty_banner(),width,height);


            Glide.with(mContext).load(item.getBanner()).thumbnail(Glide.with(mContext).load(thumbImage).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).error(R.drawable.no_img_mentor).into(mMentorImage);


            if(tabPosition == Constants.MENTOR_TAB){

                //  get mentor dashboard from app preferences
                DashBoardMentors dashBoard = AppPreferences.getmMentorDashBoard();

                if(dashBoard.isWishlist()){
                    mMentorWishlist.setVisibility(VISIBLE);
                }else{
                    mMentorWishlist.setVisibility(GONE);
                }


                if (dashBoard.getAll_Mentors() != null) {

                    if (!dashBoard.isMentorTypeLayoutVisible()) {
                        mMentorName.setVisibility(View.GONE);
                        mMentorDetials.setVisibility(View.GONE);
                    } else {

                        if (dashBoard.isShowMentorMentorTagline()) {
                            mMentorDetials.setVisibility(View.VISIBLE);
                        } else {
                            mMentorDetials.setVisibility(View.GONE);

                        }

                        if (dashBoard.isShowMentorMentorName()) {
                            mMentorName.setVisibility(View.VISIBLE);

                        } else {
                            mMentorName.setVisibility(View.GONE);

                        }
                    }
                } else {
                    mMentorName.setVisibility(View.VISIBLE);
                    mMentorDetials.setVisibility(View.VISIBLE);
                }




            }else {


                //  get dashboard from app preferences
                DashBoard dashBoard = AppPreferences.getDashBoard();

                if(dashBoard.isWishlist()){
                    mMentorWishlist.setVisibility(VISIBLE);
                }else{
                    mMentorWishlist.setVisibility(GONE);

                }


                if (dashBoard.getHeadingOrdering() != null) {

                    if (!dashBoard.isMentorTypeLayoutVisible()) {
                        mMentorName.setVisibility(View.GONE);
                        mMentorDetials.setVisibility(View.GONE);
                    } else {

                        if (dashBoard.isShowMentorMentorTagline()) {
                            mMentorDetials.setVisibility(View.VISIBLE);
                        } else {
                            mMentorDetials.setVisibility(View.GONE);

                        }

                        if (dashBoard.isShowMentorMentorName()) {
                            mMentorName.setVisibility(View.VISIBLE);

                        } else {
                            mMentorName.setVisibility(View.GONE);

                        }
                    }
                } else {
                    mMentorDetials.setVisibility(View.VISIBLE);
                    mMentorName.setVisibility(View.VISIBLE);
                }
            }
        }





    }

    public void callWishListApi(final String mItemId ,final Mentors mItem) {
        String api = null;

        if (isWishlistSelected) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS)))
                    {

                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_WISHLIST_RELOAD));

                        if (isWishlistSelected) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();

                            mItem.setWishlist("Yes");
                        }
                        else
                        {
                            mItem.setWishlist("No");
                        }

                        try {
                            Intent intent = new Intent(Constants.BROADCAST_DETAIL_SET);
                            intent.putExtra(Keys.ARTICLE_ID, mItemId);
                            intent.putExtra(Keys.WISHLIST_VALUE, isWishlistSelected ? "Yes" : "No");

                            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);


                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }




                } catch (Exception e) {

                    if (isWishlistSelected) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                        e.printStackTrace();


                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                        e.printStackTrace();



                    }
                    isWishlistSelected=!isWishlistSelected;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isWishlistSelected) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api");
                    mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                } else {
                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mMentorWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                }
                isWishlistSelected=!isWishlistSelected;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.ITEM_ID,mItemId);
                if (mIsMentor) {
                    params.put(Keys.TYPE, Constants.TYPE_EXPERT);

                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }




}
