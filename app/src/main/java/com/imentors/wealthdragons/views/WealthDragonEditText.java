package com.imentors.wealthdragons.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import androidx.annotation.NonNull;

/**
 * Created on 7/12/17.
 */
public class WealthDragonEditText extends EditText implements ViewUtils.ViewExtension {

    public WealthDragonEditText(@NonNull Context context) {
        super(context);
        if (!isInEditMode()) {
            init(context, null);
        }
    }

    public WealthDragonEditText(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    public WealthDragonEditText(@NonNull Context context, @NonNull AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context, attrs);
        }
    }

    /**
     * init the view
     *
     * @param context context
     * @param attrs   attrs
     */
    private void init(Context context, AttributeSet attrs) {
        ViewUtils.init(this, context, attrs);
    }


    @Override
    public void onInit(Typeface typeface) {
        if (typeface != null) {
            this.setTypeface(typeface);
        }
    }
}