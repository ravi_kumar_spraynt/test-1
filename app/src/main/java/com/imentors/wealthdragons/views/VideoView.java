package com.imentors.wealthdragons.views;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardVideos;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.util.Map;
import java.util.zip.DeflaterInputStream;

public class VideoView extends LinearLayout{


    private ImageView mVideoImage,mVideoWishlist;
    private WealthDragonTextView mVideoName, mVideoDetials , mStripFreePaid;
    private Context mContext;
    private String mItemId;
    private boolean isWishlistSelected = false,mIsVideo=true;


    public VideoView(Context context) {
        super(context);
        mContext= context;
    }

    public VideoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext= context;

    }

    public VideoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext= context;

    }

    public VideoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext= context;

    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
            mVideoImage = findViewById(R.id.iv_video_image);
            mVideoName = findViewById(R.id.tv_video_name);
            mVideoDetials = findViewById(R.id.tv_video_detail);
            mStripFreePaid = findViewById(R.id.tv_strip_detail);
            mVideoWishlist=findViewById(R.id.iv_add_wishlist);
    }

    /**
     * /**
     * bind the view with article item
     *
     * @param item            article item
     */
    public void bind(final Video item , LruCache memCache , final int tabPosition) {

        String videoTitle = item.getTitle();
        String mentorName = item.getMentor_name();
        mItemId=item.getId();


        if(!TextUtils.isEmpty(item.getWishlist()) && item.getWishlist().equals("Yes")){
            mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
            isWishlistSelected = true;

        }else{
            mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart);
            isWishlistSelected = false;
        }


        mVideoWishlist.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isWishlistSelected ) {
                    mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart);
                    callWishListApi(mItemId,item);

                }else{
                    mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);
                    callWishListApi(mItemId,item);

                }
                isWishlistSelected = !isWishlistSelected;
            }
        });



        if(!TextUtils.isEmpty(videoTitle)){
            mVideoName.setText(videoTitle);
        }

        if(!TextUtils.isEmpty(mentorName)){
            mVideoDetials.setText(mentorName);
        }

        if(item.isFreePaid()){
            mStripFreePaid.setVisibility(View.INVISIBLE);
            mStripFreePaid.setText("Free Video");
        }else{
            mStripFreePaid.setVisibility(View.INVISIBLE);
        }


        mVideoImage.getLayoutParams().width = (int) Utils.getVideoImageWidth((Activity) mContext);
        mVideoImage.getLayoutParams().height = (int) Utils.getVideoImageHeight((Activity) mContext);

        double height = mVideoImage.getLayoutParams().height;
        double width = mVideoImage.getLayoutParams().width;



//        mVideoImage.setImageURI(newImageUrl);

        if(Utils.isTablet()) {

            String bigImageUrl = Utils.getImageUrl(item.getMobile_big_banner(),width,height);
            String thumbImageUrl=Utils.getImageUrl(item.getLow_qty_bigBanner(),width,height);
            Glide.with(mContext).load(item.getMobile_big_banner()).thumbnail(Glide.with(mContext).load(thumbImageUrl).dontAnimate()).error(R.drawable.no_img_video).dontAnimate().into(mVideoImage);

        }else{
            String bigImageUrl = Utils.getImageUrl(item.getMobile_small_banner(),width,height);

            String thumbImageUrl=Utils.getImageUrl(item.getLow_qty_mobile_small_banner_detail(),width,height);
            Glide.with(mContext).load(item.getMobile_small_banner()).thumbnail(Glide.with(mContext).load(thumbImageUrl).dontAnimate()).error(R.drawable.no_img_video).dontAnimate().into(mVideoImage);

        }


        if(tabPosition == Constants.VIDEO_TAB){

            //get Video DashBoard from app preferences
            DashBoardVideos dashBoard = AppPreferences.getVideoDashBoard();

            if(dashBoard.isWishlist()){
                mVideoWishlist.setVisibility(VISIBLE);
            }else{
                mVideoWishlist.setVisibility(GONE);
            }

            if (dashBoard.getAll_Videos() != null) {

                if (!dashBoard.isMentorTypeLayoutVisible()) {
                    mVideoName.setVisibility(View.GONE);
                    mVideoDetials.setVisibility(View.GONE);
                } else {

                    if (dashBoard.isShow_free_video_mentor_name()) {
                        mVideoDetials.setVisibility(View.VISIBLE);
                    } else {
                        mVideoDetials.setVisibility(View.GONE);

                    }

                    if (dashBoard.isShow_free_video_name()) {
                        mVideoName.setVisibility(View.VISIBLE);

                    } else {
                        mVideoName.setVisibility(View.GONE);

                    }
                }
            } else {
                mVideoName.setVisibility(View.VISIBLE);
                mVideoDetials.setVisibility(View.VISIBLE);
            }




        }else {


            //  get dashboard data from app preferences
            DashBoard dashBoard = AppPreferences.getDashBoard();

            if(dashBoard.isWishlist()){
                mVideoWishlist.setVisibility(VISIBLE);
            }else{
                mVideoWishlist.setVisibility(GONE);
            }

            if (dashBoard.getHeadingOrdering() != null) {

                if (!dashBoard.isVideoLayoutVisible()) {
                    mVideoName.setVisibility(View.GONE);
                    mVideoDetials.setVisibility(View.GONE);
                } else {

                    if (dashBoard.isShowFreeVideoMentorName()) {
                        mVideoDetials.setVisibility(View.VISIBLE);
                    } else {
                        mVideoDetials.setVisibility(View.GONE);

                    }

                    if (dashBoard.isShowFreeVideoName()) {
                        mVideoName.setVisibility(View.VISIBLE);

                    } else {
                        mVideoName.setVisibility(View.GONE);

                    }
                }
            } else {
                mVideoName.setVisibility(View.VISIBLE);
                mVideoDetials.setVisibility(View.VISIBLE);
            }
        }


    }

    public void callWishListApi(final String mItemId,final Video mVideo) {
        String api = null;

        if (isWishlistSelected) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS)))
                    {

                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_WISHLIST_RELOAD));

                        if (isWishlistSelected) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();

                            mVideo.setWishlist("Yes");
                        }
                        else
                        {
                            mVideo.setWishlist("No");
                        }

                        try {
                            Intent intent = new Intent(Constants.BROADCAST_DETAIL_SET);
                            intent.putExtra(Keys.ARTICLE_ID, mItemId);
                            intent.putExtra(Keys.WISHLIST_VALUE, isWishlistSelected ? "Yes" : "No");

                            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }




                } catch (Exception e) {

                    if (isWishlistSelected) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart);


                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                    }
                    isWishlistSelected=!isWishlistSelected;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isWishlistSelected) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api");
                    mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart);

                } else {
                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mVideoWishlist.setImageResource(R.drawable.ic_wishlist_heart_filled);


                }
                isWishlistSelected=!isWishlistSelected;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                params.put(Keys.ITEM_ID,mItemId);
                if (mIsVideo) {
                    params.put(Keys.TYPE, Constants.TYPE_EVENT);

                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }




}
