/*
  Copyright 2016 Google Inc. All Rights Reserved.
  <p/>
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  <p/>
  http://www.apache.org/licenses/LICENSE-2.0
  <p/>
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */

package com.imentors.wealthdragons.notifications;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.SplashActvity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import java.util.HashMap;
import java.util.Map;
import java.util.Random;



public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final Random mRnd = new Random();

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Map<String, String> map = null;
        map = remoteMessage.getData();


        if(!AppPreferences.getMaintainceMode() && !AppPreferences.getLiveChatActivityState()){
            try {
                sendNotification(map, remoteMessage);
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Send notification window
     *
     * @param map map
     */
    private void sendNotification(Map<String, String> map,RemoteMessage remoteMessage) throws PendingIntent.CanceledException {


        boolean sct = Utils.isAppRunning(WealthDragonsOnlineApplication.sharedInstance());
        boolean active = AppPreferences.getMainActivityState();
        boolean check = AppPreferences.getCustomerSupportActivityState();


        if(!Utils.isAppRunning(WealthDragonsOnlineApplication.sharedInstance()) || (!AppPreferences.getMainActivityState() && !AppPreferences.getCustomerSupportActivityState())) {

            map.put(Constants.SHOW_NOTIFICATION_DIALOG,"false");

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            @SuppressLint("IconColors") NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,getString(R.string.default_notification_channel_id))
                    .setSmallIcon(R.drawable.ic_notifcation_bold)
                    .setContentTitle(map.get(Keys.TITLE))
                    .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(map.get(Keys.BODY)))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setNumber(5)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(createPendingIntentForNotifications(map));


            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O)
            {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(getString(R.string.default_notification_channel_id), getString(R.string.app_name), importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationBuilder.setChannelId(getString(R.string.default_notification_channel_id));
                notificationManager.createNotificationChannel(notificationChannel);
            }


            if (notificationManager != null)
                notificationManager.notify(mRnd.nextInt(), notificationBuilder.build());
        }else{

            if(AppPreferences.isLoggedIn()){
                map.put(Constants.SHOW_NOTIFICATION_DIALOG,"true");
            }else{
                map.put(Constants.SHOW_NOTIFICATION_DIALOG,"false");
            }
            createPendingIntentForNotifications(map).send();
        }
    }



    private PendingIntent createPendingIntentForNotifications(Map<String, String> map) {
        Intent notificationIntent = new Intent(this, SplashActvity.class);
        notificationIntent.putExtra(Keys.NOTIFICATION, new HashMap<>(map));
        return PendingIntent.getActivity(this, mRnd.nextInt(), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        sendTokenOnServer(s);
    }

    //TODO implemented maintenance mode
    public static void sendTokenOnServer(final String token) {


        // send Fcm token


        if (AppPreferences.isLoggedIn() && !AppPreferences.isFCMTokenSendOnServer() && !TextUtils.isEmpty(token)) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.UPDATE_USER_TOKEN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    AppPreferences.markFCMTokenSendOnServer();
                }
            }, null) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {


                    Map<String, String> params = Utils.getParams(false);
                    params.put(Keys.TYPE, Keys.ANDROID);
                    params.put(Keys.TOKEN, token);

                    return params;
                }
            };

            WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
        }


    }





}