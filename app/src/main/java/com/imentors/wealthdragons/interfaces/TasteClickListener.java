package com.imentors.wealthdragons.interfaces;

import com.imentors.wealthdragons.models.Category;

/**
 * Created on 7/17/17.
 */

public interface TasteClickListener {
    void onTasteClick(String item, String taste,int groupPosition , int subPosition);
}
