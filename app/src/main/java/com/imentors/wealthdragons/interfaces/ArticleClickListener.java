package com.imentors.wealthdragons.interfaces;

import com.imentors.wealthdragons.models.DashBoardItemOdering;

/**
 * Created on 7/17/17.
 */

public interface ArticleClickListener {
    void onArticleClick(String detailType, String detailId );
}
