package com.imentors.wealthdragons.interfaces;

/**
 * Created on 7/17/17.
 */

public interface TabChangingListener {
    void onTabChanged();
}
