package com.imentors.wealthdragons.interfaces;

/**
 * Created on 7/17/17.
 */

public interface MentorClickListener {
    void onMentorClick(String detailType, String detailId);
}
