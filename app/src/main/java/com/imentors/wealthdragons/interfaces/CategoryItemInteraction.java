package com.imentors.wealthdragons.interfaces;


import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.models.MenuItem;

/**
 * Created on 7/12/17.
 */

public interface CategoryItemInteraction {

    /**
     * Handles On Menu Item Click
     *
     * @param item item
     */
    void onCategoryMenuClick(Category item);
    void onCategoryChildClick(Category.SubCategory item);

    void onCrossClick();

}
