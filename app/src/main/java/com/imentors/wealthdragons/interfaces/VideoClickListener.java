package com.imentors.wealthdragons.interfaces;

/**
 * Created on 7/17/17.
 */

public interface VideoClickListener {
    void onVideoClick(String detailType, String detailId ,String epiosdeId);
}
