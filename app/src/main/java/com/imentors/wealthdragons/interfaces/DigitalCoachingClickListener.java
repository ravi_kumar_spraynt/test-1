package com.imentors.wealthdragons.interfaces;

import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DigitalCoaching;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 7/17/17.
 */

public interface DigitalCoachingClickListener {
    void onDigitalCoachingClick(ArrayList<DigitalCoaching> digitalCoaching, int totalServerItem, int nextpage, String detailType, String detailId, String isSubscribed , String isAutoPlay, String freePaid, boolean showpayDialog, String mentorName, String mentorUrl,
                                String selectedId, String description, String banner,
                                String groupBannerImage, boolean isDownloaded, DashBoardMentorDetails dashBoardMentorDetails);
}
