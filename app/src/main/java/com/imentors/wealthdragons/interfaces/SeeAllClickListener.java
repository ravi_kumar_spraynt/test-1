package com.imentors.wealthdragons.interfaces;

import android.app.FragmentManager;

import com.imentors.wealthdragons.models.DashBoardItemOdering;

/**
 * Created on 7/17/17.
 */

public interface SeeAllClickListener {
    void onSeeAllClicked(String key, DashBoardItemOdering id );
}
