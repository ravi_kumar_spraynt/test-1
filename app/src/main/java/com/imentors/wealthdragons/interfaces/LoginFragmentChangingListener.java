package com.imentors.wealthdragons.interfaces;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created on 7/17/17.
 */

public interface LoginFragmentChangingListener{
    void onSignInClicked();
    void onSignUpClicked();
    void onForgotPasswordClicked();
}
