package com.imentors.wealthdragons.interfaces;

/**
 * Created on 7/17/17.
 */

public interface PagingListener {
    void onGetItemFromApi(final int pageNumber, final String  tasteId ,final String view_type,final String seeAllType);
}
