package com.imentors.wealthdragons.dialogFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.ServiceTypeListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ServiceTypeFragment extends DialogFragment implements ServiceTypeListAdapter.OnSericeClicked{


    private RecyclerView mRecyclerView;
    private Context mContext;
    private ServiceTypeListAdapter mCountryCodeListAdapter;
    private LinearLayout mProgressbarLayout;
    private SwipeRefreshLayout mSwipeRefershLayout;



    public static ServiceTypeFragment newInstance() {


        ServiceTypeFragment fragment = new ServiceTypeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_support_type, container, false);

        Utils.callEventLogApi("opened <b> Support Type </b> page");



        mRecyclerView = view.findViewById(R.id.listView);

        mProgressbarLayout = view.findViewById(R.id.progress_bar);

        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);




        if(AppPreferences.getSupportType().size()>0){

            //  set service type list adapter
            mCountryCodeListAdapter = new ServiceTypeListAdapter(AppPreferences.getSupportType(),this);
            mRecyclerView.setAdapter(mCountryCodeListAdapter);

        }else{

            callServiceTypeApi();

        }


        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callServiceTypeApi();
            }
        });




        return view;
    }




    private void callServiceTypeApi() {

        //  set refreshing
        mProgressbarLayout.setVisibility(View.VISIBLE);


        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressbarLayout.setVisibility(View.GONE);
        }else{
            mProgressbarLayout.setVisibility(View.VISIBLE);
        }


        //TODO implemented maintenance mode
        // calling support type api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Api.SUPPORT_TYPES_API, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {


                mProgressbarLayout.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    handleDashBoardResponse(response);
                } catch (Exception e) {
                    mProgressbarLayout.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b> Support Type </b> error in api");

                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressbarLayout.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b> Support Type </b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }



     // handling api response
    private void handleDashBoardResponse(String response) {


        try {
            List<SupportType> supportTypeList =  new Gson().fromJson(response, new TypeToken<List<SupportType>>(){}.getType());

            if(supportTypeList.size()>0){
                AppPreferences.setSupportType(supportTypeList);
                if(mCountryCodeListAdapter !=null){
                    mCountryCodeListAdapter = null;
                    mCountryCodeListAdapter = new ServiceTypeListAdapter(supportTypeList,this);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                    mRecyclerView.setLayoutManager(mLayoutManager);
                    mRecyclerView.setAdapter(mCountryCodeListAdapter);
                }
            }
        }catch (Exception e){
          e.printStackTrace();
        }



    }
    //  selected service
    @Override
    public void onServiceClicked(SupportType item) {
        Utils.callEventLogApi("selected <b>"+item.getTitle()+"</b> from"+"<b> Support Type</b>");

        Intent i = new Intent();
        i.putExtra(Constants.SERVICE_TYPE, item.getTitle());
        i.putExtra(Constants.SERVICE_TYPE_ID,item.getId());

        this.dismiss();

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,i);
    }


}
