package com.imentors.wealthdragons.dialogFragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.CountryCodeListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.fragments.BaseFragment;
import com.imentors.wealthdragons.models.CountryCode;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonEditText;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CountryCodeFragment extends BaseFragment implements CountryCodeListAdapter.OnCountryCodeClick{


    private RecyclerView mRecyclerView;
    private WealthDragonEditText mSearchKey;
    private ImageView mIvSearchCancel;
    private Context mContext;
    private CountryCodeListAdapter mCountryCodeListAdapter;
    private LinearLayout mProgressbarLayout;
    private SwipeRefreshLayout mSwipeRefershLayout;



    public static CountryCodeFragment newInstance() {


        CountryCodeFragment fragment = new CountryCodeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_fragment_country_code, container, false);
        Utils.callEventLogApi("opened <b> Country Code</b> page");



        float height = Utils.getScreenHeight(this.getActivity());
        float dialogHeight = height/6;


        view.setPadding(0,(int)dialogHeight,0,0);


        mRecyclerView = view.findViewById(R.id.listView);

        mSearchKey = view.findViewById(R.id.tv_search);

        mIvSearchCancel = view.findViewById(R.id.iv_cancel_search);

        mProgressbarLayout = view.findViewById(R.id.progress_bar);

        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);

        mSearchKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSearchKey.setCursorVisible(true);
            }
        });

        //  showed search cross
        mSearchKey.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.toString().length();
                performSearch(mSearchKey.getText().toString().trim());
                if(length==0){
                    mIvSearchCancel.setVisibility(View.INVISIBLE);
                }else{
                    mIvSearchCancel.setVisibility(View.VISIBLE);
                }
            }
        });


        mSearchKey.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideKeyboard(mContext);
                    performSearch(mSearchKey.getText().toString().trim());
                    return true;
                }

                mSearchKey.setCursorVisible(false);
                if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(mSearchKey.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                }

                return false;
            }
        });


        mIvSearchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b> Country Code</b> cancel");

                clearSearch();
            }
        });

        if(AppPreferences.getCountryCode().size()>0){

            //  set country code list adapter
            mCountryCodeListAdapter = new CountryCodeListAdapter(AppPreferences.getCountryCode(),this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setAdapter(mCountryCodeListAdapter);

        }else{

            callCountryCodeApi();

        }


        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callCountryCodeApi();
            }
        });

        return view;
    }

    private void performSearch(String trim) {

        if(mCountryCodeListAdapter!=null){
            mCountryCodeListAdapter.filter(trim);
        }
    }

    private void clearSearch(){

        if(Utils.isKeyboardVisible()){
            Utils.hideKeyboard(mContext);
            mSearchKey.setCursorVisible(false);
        }else{
            Utils.showKeyboard(mContext,mSearchKey);
            mSearchKey.setCursorVisible(true);
        }

        mSearchKey.setText("");
    }


    //  selected country code
    @Override
    public void onCountryCodeSelect(CountryCode item) {


        Intent i = new Intent();
        i.putExtra(Constants.COUNTRY_CODE, item.getPhonecode());

        Utils.callEventLogApi("selected <b> Country Code </b>"+item.getPhonecode());


        getFragmentManager().popBackStack();

        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK,i);

    }



    private void callCountryCodeApi() {

        //  set refreshing
        mProgressbarLayout.setVisibility(View.VISIBLE);

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressbarLayout.setVisibility(View.GONE);
        }else{
            mProgressbarLayout.setVisibility(View.VISIBLE);
        }


        //TODO implemented maintenance mode
        // calling country code api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Api.COUNTRY_CODES_API, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {


                mProgressbarLayout.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    handleDashBoardResponse(response);
                } catch (Exception e) {
                    mProgressbarLayout.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b> Country Code</b> error in api");

                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressbarLayout.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b> Country Code</b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }



     // handling dashboard api response
    private void handleDashBoardResponse(String response) {
        try {

        List<CountryCode> countryCodeList =  new Gson().fromJson(response, new TypeToken<List<CountryCode>>(){}.getType());

        if(countryCodeList.size()>0){
            AppPreferences.setCountryCode(countryCodeList);
            if(mCountryCodeListAdapter !=null){
                mCountryCodeListAdapter = null;
                mCountryCodeListAdapter = new CountryCodeListAdapter(countryCodeList,this);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(mCountryCodeListAdapter);
            }
        }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
