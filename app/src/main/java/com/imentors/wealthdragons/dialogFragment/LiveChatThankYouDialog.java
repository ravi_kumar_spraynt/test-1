package com.imentors.wealthdragons.dialogFragment;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Utils;


public class LiveChatThankYouDialog extends DialogFragment {

    private TextView mThankMessage;
    private Button mClose;

    private String mThankuMsg;

    private static final String THANKU_MSG = "THANKU_MSG";



    public static LiveChatThankYouDialog newInstance(String thankuMsg) {

        Bundle args = new Bundle();
        args.putString(THANKU_MSG,thankuMsg);
        LiveChatThankYouDialog fragment = new LiveChatThankYouDialog();
        fragment.setStyle(DialogFragment.STYLE_NO_FRAME,
                R.style.Chat_Comment_Dialog);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // checking empty condition


        mThankuMsg = getArguments().getString(THANKU_MSG);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        setStyle(DialogFragment.STYLE_NORMAL,
//                R.style.Chat_Comment_Dialog);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        View view=inflater.inflate(R.layout.dialog_live_chat_thank_you,container,false);

        Utils.callEventLogApi("opened <b> Subscription</b> dialog");

        mThankMessage=view.findViewById(R.id.tv_thank_message);

        if(!TextUtils.isEmpty(mThankuMsg)){
            mThankMessage.setText(mThankuMsg);

        }else{
            mThankMessage.setText(getResources().getString(R.string.chat_thanx));

        }

        mClose=view.findViewById(R.id.bt_close);

        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                getActivity().onBackPressed();

            }
        });

        return view;
    }


}
