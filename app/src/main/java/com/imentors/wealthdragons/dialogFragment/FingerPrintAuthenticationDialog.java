package com.imentors.wealthdragons.dialogFragment;

import android.app.DialogFragment;
import android.app.KeyguardManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.listeners.FingerprintHandler;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

public class FingerPrintAuthenticationDialog extends DialogFragment implements FingerprintHandler.Callback{


    private static final long ERROR_TIMEOUT_MILLIS = 1600;
    private static final long SUCCESS_DELAY_MILLIS = 1300;
    private Button mCancelButton;
    private ImageView mIcon;
    private TextView mTvError;
    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private boolean isAuthorised = false;

    private DialogDismissListener callback;

    public interface DialogDismissListener {
        void onDialogDismiss();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not create a new Fragment when the Activity is re-created such as orientation changes.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
        keyguardManager =
                (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fingerprintManager =
                    (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
        }
    }


    public void setListener(DialogDismissListener listener){
        callback = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.sign_in));
        View v = inflater.inflate(R.layout.dialog_fragment_fingerprint_authentication, container, false);
        mIcon =v.findViewById(R.id.fingerprint_icon);
        mTvError =   v.findViewById(R.id.fingerprint_status);
        mCancelButton = v.findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        setCancelable(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        try {

                generateKey();
        } catch (FingerprintException e) {
            e.printStackTrace();
        }
        if (initCipher()) {
            cryptoObject = new FingerprintManager.CryptoObject(cipher);
            FingerprintHandler helper = new FingerprintHandler(getActivity());
            helper.startAuth(fingerprintManager, cryptoObject,this);
        }
        }



        return v;
    }






    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {

            keyStore = KeyStore.getInstance("AndroidKeyStore");


            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }


    }




    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() {
        try {
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @Override
    public void onAuthenticated() {

        isAuthorised = true;
        mTvError.removeCallbacks(mResetErrorTextRunnable);
        mIcon.setImageResource(R.drawable.ic_fingerprint_success);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.success_color, null));
        }else {
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.success_color));
        }
        mTvError.setText(
                mTvError.getResources().getString(R.string.fingerprint_success));
        mIcon.postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, SUCCESS_DELAY_MILLIS);

        }




    @Override
    public void onError(CharSequence errorText, boolean isLastAttempt) {
            mIcon.setImageResource(R.drawable.ic_fingerprint_error);
            mTvError.setText(errorText);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.pinkStripColor, null));
        }else{
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.pinkStripColor));
        }
        mTvError.removeCallbacks(mResetErrorTextRunnable);
        if(!isLastAttempt){
            mTvError.postDelayed(mResetErrorTextRunnable, ERROR_TIMEOUT_MILLIS);
        }
    }

    private Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mTvError.setTextColor(
                        mTvError.getResources().getColor(R.color.hint_color, null));
            }else{
                mTvError.setTextColor(
                        mTvError.getResources().getColor(R.color.hint_color));
            }
            mTvError.setText(
                    mTvError.getResources().getString(R.string.fingerprint_hint));
            mIcon.setImageResource(R.drawable.ic_fp_40px);
        }
    };


    private class FingerprintException extends Exception {

        public FingerprintException(Exception e) {
            super(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(isAuthorised){
            callback.onDialogDismiss();
        }
    }
}
