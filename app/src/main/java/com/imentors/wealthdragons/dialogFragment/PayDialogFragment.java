package com.imentors.wealthdragons.dialogFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.material.textfield.TextInputLayout;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.User;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.CardFormCvvEditText;
import com.imentors.wealthdragons.views.CardFormEditText;
import com.imentors.wealthdragons.views.CardFormExpirationDateEditText;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.Map;

public class PayDialogFragment extends DialogFragment {

    private ProgressBar mProgressBar;
    private String mCurrency;
    private TextView mTvResponse;
    private FrameLayout mPay ,mCancel;
    private boolean mIsLoginActivity , mIsPaymentCardListDialog;
    public static final long SUCCESS_DELAY_MILLIS = 1300;
    private View view;
    private TextInputLayout mCardNumber,mExpirationDate,mCardCvv;
    private CardFormExpirationDateEditText mExpiryDateEditText;
    private CardFormEditText mCardNumberEditText;
    private CardFormCvvEditText mCvvEditText;
    private WealthDragonTextView mTvCancelBtn;
    private String mItemId;
    private String mItemType;
    private String mEclassId;
    private boolean mIsPayedInBackground = false;
    private String mCardType = null;


    public static PayDialogFragment newInstance(String currency,boolean isLoginActivity, boolean isPaymentCardListDialog,String itemId,String itemType,String eclassId) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putBoolean(Constants.IS_LOGIN_ACTIVITY,isLoginActivity);
        args.putBoolean(Constants.IS_PAYMENT_CARD_LIST_DIALOG,isPaymentCardListDialog);
        args.putString(Constants.CURRENCY, currency);
        args.putString(Constants.ITEM_ID, itemId);
        args.putString(Constants.ITEM_TYPE, itemType);
        args.putString(Constants.ECLASSES_VIDEO_ID,eclassId);
        PayDialogFragment fragment = new PayDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsLoginActivity =  getArguments().getBoolean(Constants.IS_LOGIN_ACTIVITY);
        mIsPaymentCardListDialog = getArguments().getBoolean(Constants.IS_PAYMENT_CARD_LIST_DIALOG);
        mCurrency = getArguments().getString(Constants.CURRENCY);
        mItemId = getArguments().getString(Constants.ITEM_ID);
        mItemType = getArguments().getString(Constants.ITEM_TYPE);
        mEclassId = getArguments().getString(Constants.ECLASSES_VIDEO_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        }
        else
        {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.MyPaymentFragmentStyle);

        }

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
       //getDialog().setTitle(R.string.card_detail);
        view = inflater.inflate(R.layout.dialog_paymentt, container, false);

//        getDialog().getWindow().setLayout((int) getContext().getResources().getDimension(R.dimen.error_dialog_width), LinearLayout.LayoutParams.WRAP_CONTENT);


        Utils.callEventLogApi("opened <b> Payment</b> dialog");

        mProgressBar= view.findViewById(R.id.prog_bar);
        mTvResponse= view.findViewById(R.id.payment_errer);
        mCancel=view.findViewById(R.id.fl_btn_orange_negative);
        mTvCancelBtn = view.findViewById(R.id.tv_negative_button);
        mPay= view.findViewById(R.id.fl_btn_orange_positive);
        mCardNumber=view.findViewById(R.id.cardform_card_number);
        mExpirationDate=view.findViewById(R.id.cardform_expdate);
        mExpiryDateEditText = (CardFormExpirationDateEditText) mExpirationDate.getEditText();
        mExpiryDateEditText.useDialogForExpirationDateEntry(getActivity(), true);

        mCardCvv=view.findViewById(R.id.cardform_cvv);
        mCvvEditText = (CardFormCvvEditText) mCardCvv.getEditText();
        mCardNumberEditText = (CardFormEditText) mCardNumber.getEditText();
        closeSoftKeyboard();
        openKeyboard();

        // if expiry date edit text in text changed
        mExpiryDateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mTvResponse.setVisibility(View.GONE);
                mExpirationDate.setError(null);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        // if card number edit text in text changed
        mCardNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mTvResponse.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                mCvvEditText.setText(null);
                mCvvEditText.setCardType(mCardNumberEditText.getCardType());

                if(!TextUtils.isEmpty(mCardCvv.getError())){
                    mCvvEditText.validate();
                }


            }
        });
        // if cvv edit text in text changed
        mCvvEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mTvResponse.setVisibility(View.GONE);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //  clicked pay button
        mPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                closeSoftKeyboard();


                Utils.callEventLogApi("clicked <b> Payment</b> button");

                if(TextUtils.isEmpty(mExpiryDateEditText.getText().toString())){
                    mExpiryDateEditText.setError(mExpiryDateEditText.getErrorMessage());
                }

                mCvvEditText.validate();

                    String cardnumber=mCardNumber.getEditText().getText().toString();
                    String expDate=mExpirationDate.getEditText().getText().toString();
                    String cardcvv=mCardCvv.getEditText().getText().toString();
                    Pay(cardnumber,expDate,cardcvv);

            }
        });

        if(mIsPaymentCardListDialog){
            mTvCancelBtn.setText("Back");
        }

        //  clicked cancel button
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeSoftKeyboard();
                Utils.callEventLogApi("clicked <b> Payemt Cancel</b> button");
                getDialog().cancel();

                if(mIsPaymentCardListDialog){
                    openPayCardListDialog();
                }

            }
        });



        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        if(mIsPayedInBackground){

            mIsPayedInBackground = true;

            mTvResponse.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                    if(mItemType.equals(Constants.SUBSCRIPTION)){
                        moveToDashboard();
                    }else {
                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_CLEAR_REPONSE));
                        Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
                        intent.putExtra(Constants.ECLASSES_VIDEO_ID, mEclassId);
                        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);
                    }
                }
            }, SUCCESS_DELAY_MILLIS);
        }

    }
     // calling dashboard
    private void moveToDashboard() {
        getDialog().dismiss();
        //  get user data from app preferences
        User user= AppPreferences.getUser();
        user.setDatshboard(true);
        AppPreferences.setUserData(user);
        if(mIsLoginActivity){
            Intent intent = new Intent();
            Serializable notificationHashMap = ((LoginActivity)getActivity()).getNotificationData();
            if(notificationHashMap != null) {
                intent.putExtra(Keys.NOTIFICATION, notificationHashMap);
            }
            intent.setClass(this.getActivity(),MainActivity.class);
            startActivity(intent);
            getActivity().finish();
        }else {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Utils.hideKeyboard(getContext());
        }
    }

    private void Pay(final String cardnumber, final String expDate, final String cvv)
    {

        //  check for validation
        if (TextUtils.isEmpty(cardnumber))
        {
            mCardNumber.setError(getString(com.braintreepayments.cardform.R.string.bt_card_number_required));
            mCardNumber.requestFocus();
            return;
        }else if(!mCardNumberEditText.isValid()){
            mCardNumber.setError(getString(com.braintreepayments.cardform.R.string.bt_card_number_invalid));
            mCardNumber.requestFocus();
            return;
        }
        else if(TextUtils.isEmpty(expDate))
        {
            mExpirationDate.setError(getString(com.braintreepayments.cardform.R.string.bt_expiration_required));
            mExpirationDate.requestFocus();
            return;

        }
        else if(!mExpiryDateEditText.isValid()){
            mExpirationDate.requestFocus();
            return;
        }
        else if (TextUtils.isEmpty(cvv))
        {
            mCvvEditText.validate();

//            mCardCvv.setError(getString(R.string.cvv_error));
            mCardCvv.requestFocus();
            return;
        }
        else if(!mCvvEditText.isValid()){
            mCardCvv.requestFocus();
            return;
        }

        mProgressBar.setVisibility(View.VISIBLE);
        disableView();
        mTvResponse.setVisibility(View.GONE);
        closeSoftKeyboard();


        switch (mCardNumberEditText.getCardType().getFrontResource()){
            case R.drawable.bt_ic_visa:
                mCardType = "Visa";
                break;
            case R.drawable.bt_ic_mastercard:
                mCardType = "MasterCard";
                break;
            case R.drawable.bt_ic_amex:
                mCardType ="AmericanExpress";
                break;

            default:
                mCardType ="Discover";
                break;

        }

        String api = null;

        if(mItemType.equals(Constants.TYPE_EXPERT)){

            api = Api.JOIN_DIGITAL_PAYMENT_API;


        }else if(mItemType.equals(Constants.SUBSCRIPTION)){
            api = Api.SUBSCRIPTION_PAYMENT_API;

        }else{
            api = Api.ITEM_PURCHASE_API;
        }


        //TODO implemented maintenance mode
        // calling payment api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (getContext() != null) {
                    disableView();
                    mProgressBar.setVisibility(View.GONE);
                    mTvResponse.setVisibility(View.VISIBLE);
                    try {
                        if (Utils.checkForMaintainceMode(response)) {
                            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                            return;
                        }
                        JSONObject jsonObject = new JSONObject(response);

                        if (!TextUtils.isEmpty(jsonObject.optString(Keys.ERROR))) {

                            enableView();

                            mTvResponse.setText(jsonObject.getString(Keys.ERROR));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {
                            disableView();
                            mTvResponse.setText(jsonObject.getString(Keys.SUCCESS));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.success_color));

                                mTvResponse.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mTvResponse.isShown()) {
                                            dismiss();
                                            if (mItemType.equals(Constants.SUBSCRIPTION)) {
                                                moveToDashboard();
                                            } else {
                                                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_CLEAR_REPONSE));
                                                Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
                                                intent.putExtra(Constants.ECLASSES_VIDEO_ID, mEclassId);
                                                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);
                                            }
                                        }else{
                                            mIsPayedInBackground = true;
                                        }
                                    }

                                }, SUCCESS_DELAY_MILLIS);
                        } else {
                            enableView();
                            mTvResponse.setText(getString(R.string.error_api_try_again));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        }


                    } catch (Exception e) {
                        enableView();
                        mTvResponse.setText(getString(R.string.error_api_try_again));
                        mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        Utils.callEventLogApi("getting <b> Payment</b> error in api ");
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.e("pay_error",error.toString());

                if (getContext()!=null) {
                    mProgressBar.setVisibility(View.GONE);
                    enableView();
                    mTvResponse.setText(getString(R.string.error_api_try_again));
                    mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                    mTvResponse.setVisibility(View.VISIBLE);
                }
                Utils.callEventLogApi("getting <b> Payment</b> error in api ");
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.CARD_NUMBER,cardnumber);


                if(!mItemType.equals(Constants.SUBSCRIPTION)){
                    params.put(Keys.CARD_CVV,cvv);
                    params.put(Keys.CARD_MONTH,mExpiryDateEditText.getMonth());
                    params.put(Keys.CARD_YEAR,mExpiryDateEditText.getYear());
                    params.put(Keys.CARD_TYPE,mCardType);
                    params.put(Keys.ITEM_ID,mItemId);
                    params.put(Keys.ITEM_TYPE,mItemType);
                    params.put(Keys.SAVE_CARD, getString(R.string.yes));
                }else{
                    params.put(Keys.CURRENCY,mCurrency);
                    params.put(Keys.EX_CVV,cvv);
                    params.put(Keys.EX_MONTH,mExpiryDateEditText.getMonth());
                    params.put(Keys.EX_YEAR,mExpiryDateEditText.getYear());
                }


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    //  closed payment dialog
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Payment</b> dialog");

        closeSoftKeyboard();
    }

    @Override
    public void onPause() {
        super.onPause();

        closeSoftKeyboard();
    }

    //closed soft keyboard
    private void closeSoftKeyboard() {
        if(getContext()!=null) {
            ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(view.getWindowToken(), 0);

        }
    }

    private void openKeyboard()
    {

        // opened keyboard
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        }
    }

    //  dismissed dialog
    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Utils.hideKeyboard(getContext());
        closeSoftKeyboard();
    }

    private void disableView(){
        mCancel.setEnabled(false);
        mPay.setEnabled(false);
        mCardNumber.setEnabled(false);
        mExpirationDate.setEnabled(false);
        mCardCvv.setEnabled(false);


    }

    private void enableView(){
        mCancel.setEnabled(true);
        mPay.setEnabled(true);
        mCardNumber.setEnabled(true);
        mExpirationDate.setEnabled(true);
        mCardCvv.setEnabled(true);
    }


    private void openPayCardListDialog()
    {
        PaymentCardListDialog dialogFrag= PaymentCardListDialog.newInstance(mItemId,mItemType,mEclassId);
        dialogFrag.setCancelable(false);
        dialogFrag.show(getActivity().getSupportFragmentManager(), "dialog");
    }

}




