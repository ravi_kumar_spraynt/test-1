package com.imentors.wealthdragons.dialogFragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.fragments.SubscriptionFragment;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;

public class SubscribeDialogFragment extends DialogFragment {

    private TextView mSubscribeNow;
    private TextView mRemindMeLater;


    public static SubscribeDialogFragment newInstance() {

        Bundle args = new Bundle();
        SubscribeDialogFragment fragment = new SubscribeDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }
  

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dialog_subscription,container,false);
        Utils.callEventLogApi("opened <b> Subscription</b> dialog");

        mSubscribeNow=view.findViewById(R.id.tv_subscribenow);
        mRemindMeLater=view.findViewById(R.id.remindmelater);

        mSubscribeNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b> Subscribe Now</b> button");

                FragmentManager fm = getFragmentManager();
                fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fm_main, SubscriptionFragment.newInstance(false,null));
                ft.addToBackStack(null);
                ft.commit();
                getDialog().dismiss();
            }
        });
        mRemindMeLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b> Remind Me Later </b> button");

                getDialog().dismiss();
            }
        });
        return view;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppPreferences.setSubscriptionPopUpState(false);
        AppPreferences.setShowPopupDate();
    }

    //  closed subscribe now dialog
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b> Subscribe Now</b> dialog");

    }
}
