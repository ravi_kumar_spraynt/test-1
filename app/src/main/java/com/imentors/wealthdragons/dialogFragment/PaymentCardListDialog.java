package com.imentors.wealthdragons.dialogFragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.PaymentListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PaymentCardListDialog extends DialogFragment implements PaymentListAdapter.ClickListner{


    private RecyclerView mRecyclerViewCardList;
    private PaymentListAdapter mPaymentCardListAdapter;
    private CreditCard mSelectedCreditCard;
    private List<CreditCard> mCreditCardList = new ArrayList<>();
    private CardView mAddNewCard;
    private FrameLayout mPay ,mCancel;
    private ProgressBar mProgressBar;
    private TextView mTvResponse;
    public static final long SUCCESS_DELAY_MILLIS = 1300;
    private String mCourseId;
    private String mItemType;
    private String mEclassId;
    private boolean mIsPayedInBackground = false;


    public static PaymentCardListDialog newInstance(String courseId,String itemType, String eClassId) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putString(Constants.ITEM_ID, courseId);
        args.putString(Constants.ITEM_TYPE,itemType);
        args.putString(Constants.ECLASSES_VIDEO_ID,eClassId);
        PaymentCardListDialog fragment = new PaymentCardListDialog();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCreditCardList = AppPreferences.getCreditCardList();

        mCourseId = getArguments().getString(Constants.ITEM_ID);
        mItemType = getArguments().getString(Constants.ITEM_TYPE);
        mEclassId = getArguments().getString(Constants.ECLASSES_VIDEO_ID);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        }
        else
        {
            setStyle(DialogFragment.STYLE_NORMAL, R.style.PaymentCardListStyle);

        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //getDialog().setTitle(R.string.card_detail);
        View view = inflater.inflate(R.layout.dialog_payment_card_list, container, false);

        Utils.callEventLogApi("opened <b> Payment</b> dialog");
        mProgressBar= view.findViewById(R.id.prog_bar);
        mTvResponse= view.findViewById(R.id.payment_errer);
        mRecyclerViewCardList=view.findViewById(R.id.recycler_view);
        mAddNewCard=view.findViewById(R.id.card_view);
        mPay=view.findViewById(R.id.fl_btn_orange_positive);
        mCancel=view.findViewById(R.id.fl_btn_orange_negative);
        mPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pay();
                Utils.callEventLogApi("clicked <b> Pay</b> button in payment dialog ");

            }
        });
        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                Utils.callEventLogApi("clicked <b> Cancel</b> button in payment dialog ");
            }
        });


        if(mCreditCardList.size()>=3){
            mAddNewCard.setVisibility(View.GONE);
        }else{
            mAddNewCard.setVisibility(View.VISIBLE);
        }

        mAddNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openPayDialog();
            }
        });


        // by default first card would be selected
        if(mCreditCardList.size()>0) {
            mSelectedCreditCard = mCreditCardList.get(0);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerViewCardList.setLayoutManager(mLayoutManager);
        mPaymentCardListAdapter = new PaymentListAdapter(getContext(), mCreditCardList, this);
        mRecyclerViewCardList.setAdapter(mPaymentCardListAdapter);

        return view;


    }

    private void openPayDialog()
    {
        PayDialogFragment dialogFrag= PayDialogFragment.newInstance("Ruppes",false,true, mCourseId,mItemType,mEclassId);
        dialogFrag.setCancelable(false);
        dialogFrag.show(getActivity().getSupportFragmentManager(), "dialog");
        dismiss();
    }

    @Override
    public void ClickItemListner(CreditCard creditCard, int pos) {

        mSelectedCreditCard = creditCard;
    }

    @Override
    public void onItemDelete(int pos) {
        deleteCreditCardDetails(mCreditCardList.get(pos));
        mPaymentCardListAdapter.removeItem(pos);

        if(mPaymentCardListAdapter.getItemCount()>=3){
            mAddNewCard.setVisibility(View.GONE);
        }else{
            mAddNewCard.setVisibility(View.VISIBLE);
            if(mPaymentCardListAdapter.getItemCount()==0){
                openPayDialog();
                dismiss();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mIsPayedInBackground){

            mIsPayedInBackground = true;
            mTvResponse.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_CLEAR_REPONSE));
                    // send video id in broadcast
                    Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
                    intent.putExtra(Constants.ECLASSES_VIDEO_ID, mEclassId);
                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                }
            }, SUCCESS_DELAY_MILLIS);

        }

    }

    private void deleteCreditCardDetails(final CreditCard creditCard){
        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.DELETE_CARD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new ArrayMap<>();
                params.put(Keys.CARD_ID,creditCard.getId());
                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private void pay()
    {

        String api = null;

        if(mItemType.equals(Constants.TYPE_EXPERT)){

            api = Api.JOIN_DIGITAL_PAYMENT_SAVE_CARD_API;


        }else{
            api = Api.PAYMENT_THORUGH_CARD_LIST_API;
        }


        mProgressBar.setVisibility(View.VISIBLE);
        mTvResponse.setVisibility(View.GONE);
        disableView();



        // calling payment api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if (getContext() != null) {
                    mProgressBar.setVisibility(View.GONE);
                    disableView();
                    mTvResponse.setVisibility(View.VISIBLE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        if (!TextUtils.isEmpty(jsonObject.optString(Keys.ERROR))) {
                            enableView();
                            mTvResponse.setText(jsonObject.getString(Keys.ERROR));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {
                            disableView();
                            mTvResponse.setText(jsonObject.getString(Keys.SUCCESS));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.success_color));

                                mTvResponse.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mTvResponse.isShown()) {

                                            dismiss();
                                            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_CLEAR_REPONSE));
                                            // send video id in broadcast
                                            Intent intent = new Intent(Constants.BROADCAST_PAY_FINSIH);
                                            intent.putExtra(Constants.ECLASSES_VIDEO_ID, mEclassId);
                                            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

                                        }
                                        else{
                                            mIsPayedInBackground = true;
                                        }
                                    }
                                }, SUCCESS_DELAY_MILLIS);

                        } else {
                            enableView();
                            mTvResponse.setText(getString(R.string.error_api_try_again));
                            mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        }


                    } catch (Exception e) {
                        enableView();
                        mTvResponse.setText(getString(R.string.error_api_try_again));
                        mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                        Utils.callEventLogApi("getting <b> Payment</b> error in api ");
                        e.printStackTrace();
                    }

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (getContext()!=null) {
                    mProgressBar.setVisibility(View.GONE);
                    enableView();
                    mTvResponse.setText(getString(R.string.error_api_try_again));
                    mTvResponse.setTextColor(ContextCompat.getColor(getContext(), R.color.pinkStripColor));
                    mTvResponse.setVisibility(View.VISIBLE);
                }
                Utils.callEventLogApi("getting <b> Payment</b> error in api ");
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.CARD_ID,mSelectedCreditCard.getId());
                params.put(Keys.ITEM_ID, mCourseId);
                params.put(Keys.ITEM_TYPE,mItemType);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    private void disableView(){
        mCancel.setEnabled(false);
        mPay.setEnabled(false);
        mAddNewCard.setEnabled(false);

    }

    private void enableView(){
        mCancel.setEnabled(true);
        mPay.setEnabled(true);
        mAddNewCard.setEnabled(true);

    }

}




