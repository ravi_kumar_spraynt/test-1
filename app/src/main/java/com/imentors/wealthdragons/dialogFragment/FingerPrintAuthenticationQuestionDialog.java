package com.imentors.wealthdragons.dialogFragment;

import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.imentors.wealthdragons.R;


public class FingerPrintAuthenticationQuestionDialog extends DialogFragment{


    private Button mCancelButton , mEndButton;
    private ImageView mIcon;
    private TextView mTvError , mTvHeading;


    private DialogDismissListener callback;

    public interface DialogDismissListener {
        void onDialogSuccessDismiss();
        void onDialogCancelDismiss();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Do not create a new Fragment when the Activity is re-created such as orientation changes.
        setRetainInstance(true);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }


    public void setListener(DialogDismissListener listener){
        callback = listener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.sign_in));
        View v = inflater.inflate(R.layout.dialog_fragment_fingerprint_authentication, container, false);
        setCancelable(false);
        mTvHeading = v.findViewById(R.id.fingerprint_description);
        mTvHeading.setText(R.string.fingerprint_authorization_heading);
        mIcon =v.findViewById(R.id.fingerprint_icon);
        mTvError =   v.findViewById(R.id.fingerprint_status);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.textColorblack, null));
        }else{
            mTvError.setTextColor(
                    mTvError.getResources().getColor(R.color.textColorblack));
        }
        mTvError.setText(getString(R.string.fingerprint_authorization_detail));
        mCancelButton = v.findViewById(R.id.cancel_button);
        mEndButton = v.findViewById(R.id.bt_true);
        mCancelButton.setText(getString(R.string.yes));
        mEndButton.setVisibility(View.VISIBLE);


        mCancelButton.setOnClickListener(new View.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {
                                                 callback.onDialogSuccessDismiss();
                                             }
                                         }
        );



        mEndButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onDialogCancelDismiss();
            }
        });


        getDialog().setCancelable(false);

        return v;
    }


}
