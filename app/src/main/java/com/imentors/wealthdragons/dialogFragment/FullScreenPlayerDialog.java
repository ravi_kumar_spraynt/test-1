package com.imentors.wealthdragons.dialogFragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.media.session.PlaybackStateCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FullScreenPlayerDialog extends DialogFragment {

    private List<DigitalCoaching> digitalCoachingList = new ArrayList<>();
    private List<String> videoHeadings = new ArrayList<>();

    public static final String DIGITAL_COACHING_ITEMS="DIGITAL_COACHING_ITEMS";
    public static final String AUTOPLAY="AUTOPLAY";
    public static final String POSITION="POSITION";

    public static final String VIDEOS_NAME="VIDEOS_NAME";
    private PlayerView mDialogplayerView;
    private TextView mNextVideo;
    private TextView mTvEClassName;
    private RelativeLayout mLlHeadingContainer;
    private View mVideoErrorDialog;
    private LinearLayout nextVideoFrame;
    private SimpleExoPlayer mPlayer;
    private boolean mIsAutoPlay;
    private ImageView mPosterImage;
    private TextView mVideoError;
    private ProgressBar mProgressBar;
    private int mPosition;
    private String mItemId, mVideoUrl, videoTitle;
    private Timer saveSeekTime;
    private long mResumePosition;
    private boolean isNextVideoTapped;


    public static FullScreenPlayerDialog newInstance(ArrayList<DigitalCoaching> videoUrls, boolean isAutoPlay, int pos, String itemId) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putParcelableArrayList(DIGITAL_COACHING_ITEMS, videoUrls);
        args.putBoolean(AUTOPLAY,isAutoPlay);
        args.putInt(POSITION,pos);
        args.putString(Constants.ITEM_ID,itemId);

        FullScreenPlayerDialog fragment = new FullScreenPlayerDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // On backpress, do your stuff here.
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.releaseDetailScreenPlayer();
                        dismiss();

                    }
                }, 70);

            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        digitalCoachingList = getArguments().getParcelableArrayList(DIGITAL_COACHING_ITEMS);
        videoHeadings = getArguments().getStringArrayList(VIDEOS_NAME);
        mIsAutoPlay = getArguments().getBoolean(AUTOPLAY);
        mPosition = getArguments().getInt(POSITION);
        mItemId = getArguments().getString(Constants.ITEM_ID);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callVideoUrl();
                startTimerToSendVideoSeekTime();

            }
        }, 70);


        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_video_fullscreen, container, false);


        mDialogplayerView = view.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = view.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setText("Next Video >");

        mLlHeadingContainer = view.findViewById(R.id.header_holder);
        mLlHeadingContainer.setVisibility(VISIBLE);
        mVideoErrorDialog = view.findViewById(R.id.video_error);
        nextVideoFrame = view.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(View.GONE);
        mPosterImage = view.findViewById(R.id.iv_poster_image);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mTvEClassName = view.findViewById(R.id.tv_eclasses_title);
        ImageView mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setVisibility(GONE);

        mTvEClassName.setVisibility(VISIBLE);
        videoTitle = digitalCoachingList.get(mPosition).getTitle();
        mTvEClassName.setText(videoTitle);

        Glide.with(getActivity()).load(digitalCoachingList.get(0).getBanner()).into(mPosterImage);

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if (mIsAutoPlay && mPosition<digitalCoachingList.size() && digitalCoachingList.size()>1) {
                        mNextVideo.setVisibility(VISIBLE);
                    } else {
                        mNextVideo.setVisibility(GONE);
                    }
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });

        mNextVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNextVideoTapped = true;
                playNextVideo();
            }
        });

        Utils.releaseDetailScreenPlayer();
        Utils.releaseFullScreenPlayer();

        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        return view;


    }


    @Override
    public void onPause() {
        super.onPause();
        if(mPlayer!=null){
            mPlayer.setPlayWhenReady(false);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if(mPlayer!=null) {
            mPlayer.setPlayWhenReady(true);
        }
    }

    private void showNextVideo() {
        // next video and timer according to video quantites
        if (mIsAutoPlay && mPosition<digitalCoachingList.size() && digitalCoachingList.size()>1) {
            mNextVideo.setVisibility(View.VISIBLE);

        } else {
            // case of intro video
            mNextVideo.setVisibility(View.GONE);
        }
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        Utils.releaseFullScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(getActivity(), mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching",false);

        mResumePosition = AppPreferences.getVideoResumePosition(digitalCoachingList.get(mPosition).getId());

        mPlayer = Utils.getDetailScreenConcatinatedPlayer();

        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                int stat = mPlayer.getPlaybackState();
                if(mPlayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING ) {



                }
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if(playbackState == Player.STATE_ENDED){
                    Utils.callEventLogApi("watched video of  <b>" + videoTitle+" </b>" + "from digital coaching" );

                    isNextVideoTapped = true;
                    playNextVideo();


                }

                if(playWhenReady){
                }else{
                    Utils.callEventLogApi("paused <b>"+videoTitle+"</b> digital coaching ");
                }


                if (playbackState == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + videoTitle + "</b>");


                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }

// show error screen
                mVideoError.setVisibility(VISIBLE);
                mDialogplayerView.setVisibility(View.INVISIBLE);
                mPosterImage.setVisibility(GONE);
                mProgressBar.setVisibility(GONE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDialog().onBackPressed();
                    }
                }, 30);
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch(reason){

                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + videoTitle + "</b>" );

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }
            }
        });



        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video of  <b>" + videoTitle+" </b>" + "in Digital Coaching" );

        mDialogplayerView.setPlayer(mPlayer);
        mDialogplayerView.setVisibility(View.VISIBLE);


        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        if(isNextVideoTapped){
            isNextVideoTapped = false;
            mResumePosition =0;
        }

        mPlayer.seekTo(0, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mPosterImage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);

    }


    public FullScreenPlayerDialog getInstance(){
        return this;
    }

    private void playNextVideo() {
        mPosition++;

        if(mPosition>=digitalCoachingList.size()){
            mPosition =0;
        }

        if(mPosition<digitalCoachingList.size() && mIsAutoPlay){
            videoTitle = digitalCoachingList.get(mPosition).getTitle();
            mTvEClassName.setText(videoTitle);
            Glide.with(getActivity()).load(digitalCoachingList.get(0).getBanner()).into(mPosterImage);
            callVideoUrl();
        }else{
            Utils.releaseDetailScreenPlayer();
            mDialogplayerView.setVisibility(View.INVISIBLE);
            mVideoError.setVisibility(View.INVISIBLE);
            mPosterImage.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            getDialog().onBackPressed();

        }
    }



    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Utils.releaseDetailScreenPlayer();
        if (saveSeekTime != null) {
            saveSeekTime.cancel();
        }
    }







    private void callVideoUrl(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    mVideoUrl = jsonObject.getString("video_url");

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                            if (isResumed()) {
                                initExoPlayer();
                                showNextVideo();
                            }
                        }
                    }, 400);


                }catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = digitalCoachingList.get(mPosition).getVideo_url();
                    initExoPlayer();
                    showNextVideo();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = digitalCoachingList.get(mPosition).getVideo_url();

                initExoPlayer();
                showNextVideo();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID,digitalCoachingList.get(mPosition).getId());
                params.put(Keys.ITEM_TYPE,"DigitalCoaching");
               return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    private VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                    mResumePosition = 0;

                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                    mResumePosition = mPlayer.getContentPosition();
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("DigiCoaching");

                videoDataToBeSent.setVideoId(digitalCoachingList.get(mPosition).getId());

                AppPreferences.setVideoResumePosition(digitalCoachingList.get(mPosition).getId(),mResumePosition);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }

    public void saveSeekTime() {


        //TODO implemented maintenance mode
        // calling sendSeekTime api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_VIDEO_TIME_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                VideoData mVideoData = getCurrentVideoData();

                if (mVideoData != null) {
                    params.put(Keys.VIDEOS_ID, mVideoData.getVideoId());
                    params.put(Keys.COUNTRY, mVideoData.getCountry());
                    params.put(Keys.TIME, mVideoData.getTime());
                    params.put(Keys.TYPE, mVideoData.getType());
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void startTimerToSendVideoSeekTime() {
        saveSeekTime = new Timer();

        saveSeekTime.schedule(new TimerTask() {
            @Override
            public void run() {
                saveSeekTime();
            }
        }, 100, 1000);

    }

}




