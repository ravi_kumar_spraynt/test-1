package com.imentors.wealthdragons.dialogFragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.tonyodev.fetch2.Download;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class FullScreenDownloadPlayerDialog extends DialogFragment {

    private Download mDownloadFile;

    private String address;
    private int position, mPlayingPosition;
    private List<Download> mDownloadFileList = new ArrayList<>();
    private List<String> mVideoAddress = new ArrayList<>();


    public static final String DIGITAL_COACHING_ITEMS="DIGITAL_COACHING_ITEMS";
    public static final String POSITION="POSITION";

    private PlayerView mDialogplayerView;
    private TextView mNextVideo;
    private TextView mTvEClassName;
    private RelativeLayout mLlHeadingContainer;
    private LinearLayout nextVideoFrame;
    private SimpleExoPlayer mPlayer;
    private ImageView mPosterImage;
    private TextView mVideoError;
    private ProgressBar mProgressBar;
    private String  videoTitle;
    private long mResumePosition;
    private boolean isNextVideoTapped, isNextVideoVisible;


    public static FullScreenDownloadPlayerDialog newInstance(Serializable videoUrl, int pos) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putSerializable(DIGITAL_COACHING_ITEMS, videoUrl);
        args.putInt(Constants.ITEM_POSITION, pos);

        FullScreenDownloadPlayerDialog fragment = new FullScreenDownloadPlayerDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // On backpress, do your stuff here.
                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.releaseDetailScreenPlayer();
                        dismiss();

                    }
                }, 70);

            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDownloadFileList = (List<Download>) getArguments().getSerializable(DIGITAL_COACHING_ITEMS);

        for(Download download:mDownloadFileList){
            mVideoAddress.add(download.getFile());
        }

        position =  getArguments().getInt(Constants.ITEM_POSITION);

        mPlayingPosition = position;


        isNextVideoVisible = AppPreferences.getAutoPlay().equals(Constants.AUTO_PLAY);


        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_video_fullscreen, container, false);


        mDialogplayerView = view.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = view.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setText("Next Video >");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               initExoPlayer();

            }
        }, 70);

        mLlHeadingContainer = view.findViewById(R.id.header_holder);
        mLlHeadingContainer.setVisibility(VISIBLE);
        nextVideoFrame = view.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(View.GONE);
        mPosterImage = view.findViewById(R.id.iv_poster_image);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mTvEClassName = view.findViewById(R.id.tv_eclasses_title);
        ImageView mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setVisibility(GONE);

        mTvEClassName.setVisibility(VISIBLE);
        videoTitle = mDownloadFileList.get(position).getExtras().getString(Constants.TITLE,"Course");
        mTvEClassName.setText(videoTitle);

//        Glide.with(getActivity()).load(mDownloadFile.getBanner()).into(mPosterImage);

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if(isNextVideoVisible){
                        mNextVideo.setVisibility(VISIBLE);
                    }else{
                        mNextVideo.setVisibility(GONE);
                    }

                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });

        mNextVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNextVideoTapped = true;

                mPlayingPosition++;
                if(mPlayingPosition>=mVideoAddress.size()){
                    mPlayingPosition =0;
                }
                mPlayer.seekTo(mPlayingPosition,0);
                videoTitle = mDownloadFileList.get(mPlayingPosition).getExtras().getString(Constants.TITLE,"Course");
                mTvEClassName.setText(videoTitle);
            }
        });

        Utils.releaseDetailScreenPlayer();
        Utils.releaseFullScreenPlayer();


        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        return view;


    }


    @Override
    public void onPause() {
        super.onPause();
        if(mPlayer!=null){
            mPlayer.setPlayWhenReady(false);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        if(mPlayer!=null) {
            mPlayer.setPlayWhenReady(true);
        }
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        Utils.releaseFullScreenPlayer();
        mPlayer = null;

        Utils.initilizeDetailScreenConcatinatedPlayer(getActivity(), mVideoAddress, null, false, Constants.TYPE_EXPERT, "Digital Coaching",false);

        mResumePosition = position;

        mPlayer = Utils.getDetailScreenConcatinatedPlayer();

        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                int stat = mPlayer.getPlaybackState();
                if(mPlayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING ) {



                }
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if(playbackState == Player.STATE_ENDED){
                    Utils.callEventLogApi("watched video of  <b>" + videoTitle+" </b>" + "from digital coaching" );
                }

                if(playWhenReady){
                }else{
                    Utils.callEventLogApi("paused <b>"+videoTitle+"</b> digital coaching ");
                }


                if (playbackState == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + videoTitle + "</b>");


                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }

// show error screen
                mVideoError.setVisibility(VISIBLE);
                mDialogplayerView.setVisibility(View.INVISIBLE);
                mPosterImage.setVisibility(GONE);
                mProgressBar.setVisibility(GONE);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDialog().onBackPressed();
                    }
                }, 30);
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch(reason){

                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + videoTitle + "</b>" );

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }
            }
        });



        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video of  <b>" + videoTitle+" </b>" + "in Digital Coaching" );

        mDialogplayerView.setPlayer(mPlayer);
        mDialogplayerView.setVisibility(View.VISIBLE);


        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        if(isNextVideoTapped){
            isNextVideoTapped = false;
            mResumePosition =0;
        }

        mPlayer.seekTo(mPlayingPosition, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mPosterImage.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);

    }



    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Utils.releaseDetailScreenPlayer();
    }






}




