package com.imentors.wealthdragons.digitalCoachingDetailView;

import android.app.Activity;
import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.DigitalContetnItemsAdapter;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.models.DigitalCoachingDetails;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class DigitalCoachingBelowHeading extends LinearLayout {


    private Context mContext;
    private FrameLayout mDigitalCoaching;
    private WealthDragonTextView mTvCost,mTvDigitalCoaching;
    private RecyclerView mItemListRecyclerView;
    private DigitalContetnItemsAdapter mCourseNewItemsAdapter = null;



    public DigitalCoachingBelowHeading(Context context) {
        super(context);
        mContext = context;
    }

    public DigitalCoachingBelowHeading(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public DigitalCoachingBelowHeading(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public DigitalCoachingBelowHeading(Context context, List<DigitalCoachingDetails.Content> newItemList, String itemCost , String mentorId, String btnName) {
        super(context);
        initViews(context);
        bindView(newItemList,itemCost,mentorId,btnName);
    }

    //  set data in view
    private void bindView(List<DigitalCoachingDetails.Content> newItemList, String itrmCost, final String mentorId, String btnName) {

        Utils.callEventLogApi("opened <b>Join Digital Coaching</b> page");


        if(!TextUtils.isEmpty(btnName)){
            mTvDigitalCoaching.setText(btnName);
        }

        if(newItemList.size()>0){
            mCourseNewItemsAdapter = new DigitalContetnItemsAdapter(newItemList, mContext);

            mItemListRecyclerView.setAdapter(mCourseNewItemsAdapter);
        }

        mTvCost.setText(itrmCost+" Per Month"+" | "+"Cancel at any time");
        mDigitalCoaching.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Join Digital Coaching</b> button");

                if(AppPreferences.getCreditCardList().size()>0){
                    openDialog(PaymentCardListDialog.newInstance(mentorId, Constants.TYPE_EXPERT,null));
                }else{
                    openDialog(PayDialogFragment.newInstance("Ruppee",false,false,mentorId,Constants.TYPE_EXPERT,null));
                }
            }
        });


    }

    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.digital_coaching_below_heading, this);
        mContext = context;

        mItemListRecyclerView = view.findViewById(R.id.listView);

        mTvCost=view.findViewById(R.id.tv_cost);
        mDigitalCoaching=view.findViewById(R.id.fl_digital_coaching);
        mTvDigitalCoaching=view.findViewById(R.id.tv_digital_coaching);

        mItemListRecyclerView.setLayoutManager (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mItemListRecyclerView.setNestedScrollingEnabled(false);


        // set params in tablet and phone
        if (Utils.isTablet())
        {

            double reuiredButtonWidth = Utils.getScreenWidth((Activity)mContext)/1.5;

            int marginFromStart = (int)(Utils.getScreenWidth((Activity)mContext) - reuiredButtonWidth)/2;

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMarginStart(marginFromStart-30);

            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60));
            Params.gravity = Gravity.CENTER_HORIZONTAL;
            mDigitalCoaching.setLayoutParams(Params);


        }
        else {
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
            mDigitalCoaching.setLayoutParams(Params);
            Params.setMargins(0,16,0,0);
        }


    }


    // opened dialog
    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }



}
