package com.imentors.wealthdragons.digitalCoachingDetailView;

import android.content.Context;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;


import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DigitalCoachingDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class DigitalCoachingHeadingBelowVideo extends LinearLayout {


    private WealthDragonTextView mTvNewItemHeading;
    private View mLineBelowHeading;



    public DigitalCoachingHeadingBelowVideo(Context context) {
        super(context);
    }

    public DigitalCoachingHeadingBelowVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DigitalCoachingHeadingBelowVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //  calling constructor
    public DigitalCoachingHeadingBelowVideo(Context context, List<DigitalCoachingDetails.Content> newItemList, String listTitle) {
        super(context);
        initViews(context);
        bindView(newItemList,listTitle);
    }

    //  set data in view
    private void bindView(List<DigitalCoachingDetails.Content> newItemList, String listTitle) {

        if(TextUtils.isEmpty(listTitle)){
            mTvNewItemHeading.setVisibility(GONE);
            mLineBelowHeading.setVisibility(GONE);

        }else{
            mLineBelowHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setVisibility(VISIBLE);
            mTvNewItemHeading.setText(listTitle);
        }


    }

    //  initialize course detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.digital_coaching_below_video, this);

        mTvNewItemHeading = view.findViewById(R.id.tv_digital_heading);
        mLineBelowHeading = view.findViewById(R.id.view_below_heading);

    }



}
