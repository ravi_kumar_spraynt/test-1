package com.imentors.wealthdragons.episodeDetailView;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardEpisodeDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class EpisodeDetailBelowVideo extends RelativeLayout {


    private Context mContext;
    private FrameLayout mShareBtn ;
    private WealthDragonTextView mMentorDescription, mMentorName , mRatingCount ,mDetailHeading;
    private RatingBar mRatingBar;



    public EpisodeDetailBelowVideo(Context context) {
        super(context);
        mContext = context;
    }

    public EpisodeDetailBelowVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EpisodeDetailBelowVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EpisodeDetailBelowVideo(Context context, DashBoardEpisodeDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardEpisodeDetails dashBoardDetails) {

        if(dashBoardDetails.isSocial_sharing()){
            mShareBtn.setVisibility(View.VISIBLE);
        }else{
            mShareBtn.setVisibility(View.GONE);
        }


        if(TextUtils.isEmpty(dashBoardDetails.getVideoDetail().getRating())){
            mRatingBar.setRating(0);
            mRatingCount.setText("0 Ratings");

        }else{
            mRatingBar.setRating(Float.parseFloat(dashBoardDetails.getVideoDetail().getRating()));
            mRatingCount.setText((int)Float.parseFloat(dashBoardDetails.getVideoDetail().getRating()) + " Ratings");

        }


        mMentorName.setText(dashBoardDetails.getVideoDetail().getMentor_name());



        if(!TextUtils.isEmpty(dashBoardDetails.getEpisodeDetail().getDescription())) {
            mDetailHeading.setVisibility(VISIBLE);
            mMentorDescription.setVisibility(VISIBLE);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mMentorDescription.setText(Html.fromHtml(dashBoardDetails.getEpisodeDetail().getDescription(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                mMentorDescription.setText(Html.fromHtml(dashBoardDetails.getEpisodeDetail().getDescription()));
            }
        }else{
            mDetailHeading.setVisibility(GONE);
            mMentorDescription.setVisibility(GONE);

        }



        mShareBtn.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.callEventLogApi("clicked <b>Share</b> from <b>"+dashBoardDetails.getEpisodeDetail().getName()+"</b> " + Constants.TYPE_MENTOR + " type");
                        Utils.shareArticleUrl(dashBoardDetails.getEpisodeDetail().getShare_url(),mContext,mContext.getString(R.string.share_article));
                    }
                });


    }

    //  initialize episode detail below video view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.video_detail_below_video, this);
        mContext = context;

        mShareBtn = view.findViewById(R.id.fl_btn_full_details);
        mMentorDescription = view.findViewById(R.id.tv_mentor_description);
        mMentorName = view.findViewById(R.id.tv_mentor_name);
        mRatingCount = view.findViewById(R.id.tv_set_rating_count);
        mRatingBar = findViewById(R.id.ratingBar);
        mDetailHeading =view.findViewById(R.id.tv_detail_heading);


    }



}
