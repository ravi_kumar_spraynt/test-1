package com.imentors.wealthdragons.eventDetailView;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardEventDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class EventDetailHeader extends LinearLayout {


    private Activity mActivity;
    private Context mContext;
    private WealthDragonTextView mTvEventTitle, mTvEventPunchLine, mTvEventPreferance , mTvEventMentorName;
    private ImageView mIvEventImage, mIvVideoPlay;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private SimpleExoPlayer mPlayer;
    private DashBoardEventDetails mDashBoardDetails;
    private boolean isFromActivityResult = false , isPaused = false;

    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private int mResumeWindow;
    private long mResumePosition;
    private String mVideoUrl;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen,mIsVideoError= false;
    private Dialog mFullScreenDialog;
    private boolean isLandScape,isPotrait;
    private PlayerView mDialogplayerView;
    private View mBtShare;
    private WealthDragonTextView mVideoError;
    private ImageButton mPlayImageButton;
    private Timer mTimerNextVideo = new Timer();
    final Handler mNextVideoHandler = new Handler();
    private String imageUrl;
    private FrameLayout nextVideoFrame;
    private TextView mNextVideo;
    private LinearLayout mLlHeadingContainer;


    public EventDetailHeader(Context context) {
        super(context);
        mContext = context;
    }

    public EventDetailHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EventDetailHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EventDetailHeader(Context context, DashBoardEventDetails dashBoardDetails,Activity activity) {
        super(context);
        mActivity = activity;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DashBoardEventDetails dashBoardDetails) {


        mDashBoardDetails= dashBoardDetails;


        imageUrl = Utils.getImageUrl(dashBoardDetails.getEvent().getBanner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));
        String thumbImage = Utils.getImageUrl(dashBoardDetails.getEvent().getLow_qty_banner(), Utils.getScreenWidth((Activity) mContext), Utils.getDetailScreenImageHeight((Activity) mContext));

        Glide.with(mContext).load(imageUrl).thumbnail(Glide.with(mContext).load(thumbImage).dontAnimate()).dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL).into(mIvEventImage);

        if (!TextUtils.isEmpty(dashBoardDetails.getEvent().getTitle())) {
            mTvEventTitle.setVisibility(View.VISIBLE);
            mTvEventTitle.setText(dashBoardDetails.getEvent().getTitle());
        } else {
            mTvEventTitle.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getEvent().getPunch_line())) {
            mTvEventPunchLine.setVisibility(View.VISIBLE);
            mTvEventPunchLine.setText(dashBoardDetails.getEvent().getPunch_line());
        } else {
            mTvEventPunchLine.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getEvent().getTaste_prefrence())) {
            mTvEventPreferance.setVisibility(View.VISIBLE);
            mTvEventPreferance.setText(dashBoardDetails.getEvent().getTaste_prefrence());
        } else {
            mTvEventPreferance.setVisibility(View.GONE);
        }


        mTvEventMentorName.setText(dashBoardDetails.getEvent().getMentors_name());



        if (!TextUtils.isEmpty(dashBoardDetails.getEvent().getVideo())) {
            mIvVideoPlay.setVisibility(View.VISIBLE);
            mExoPlayerView.setVisibility(View.VISIBLE);
            mVideoError.setVisibility(INVISIBLE);

            initFullscreenButton();

            mVideoUrls.add(dashBoardDetails.getEvent().getVideo());

//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList, false,Constants.TYPE_EVENT);


            initExoPlayer();


        } else {
            mIvVideoPlay.setVisibility(View.INVISIBLE);
            mExoPlayerView.setVisibility(View.INVISIBLE);
            mVideoError.setVisibility(INVISIBLE);

        }





    }

    //  initialize event detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(context instanceof OpenFullScreenVideoListener){
            mOpenFullScreenVideoListener = (OpenFullScreenVideoListener) context;
        }
        if (context instanceof Utils.DialogInteraction) {
            mDialogListener = (Utils.DialogInteraction) context;
        }

        View view = inflater.inflate(R.layout.event_detail_header, this);
        mContext = context;

        // heading
        mTvEventTitle = view.findViewById(R.id.tv_event_detail_title);
        mTvEventPunchLine = view.findViewById(R.id.tv_event_detail_punch_line);
        mTvEventPreferance = view.findViewById(R.id.tv_event_detail_taste_preferance);
        mTvEventMentorName = view.findViewById(R.id.tv_event_detail_mentor);
        mVideoError = view.findViewById(R.id.video_error);


        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);

        mBtShare = view.findViewById(R.id.fl_btn_share);

        mIvVideoPlay = view.findViewById(R.id.iv_video_play);

        mExoPlayerView =  findViewById(R.id.exoplayer);

        mIvEventImage = view.findViewById(R.id.iv_video_image);


        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                initFullscreenDialog();
                if(AppPreferences.getWifiState() && !Utils.IsWifiConnected()){
                    Utils.showTwoButtonAlertDialog(mDialogListener,mContext,mContext.getString(R.string.notification),mContext.getString(R.string.check_wifi_status),mContext.getString(R.string.okay_text),mContext.getString(R.string.go_to_setting));

                }else{

                    if(mIsVideoError){
                        mPlayImageButton.setActivated(false);
                        mIvEventImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);
                    }else{
                        mIvEventImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mExoPlayerView.setVisibility(View.VISIBLE);
                        mVideoError.setVisibility(INVISIBLE);
                    }



                    mPlayer.setPlayWhenReady(true);
                    startNextVideoTimer();
                }



            }
        });

        LocalBroadcastManager.getInstance(context).registerReceiver(mSaveVideoPosition,new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));

        LocalBroadcastManager.getInstance(context).registerReceiver(mVideoErrorListener, new IntentFilter(Constants.VIDEO_ERROR));


    }


    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);

            cancelNextVideoTimer();

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if(!isDetailOnTop){
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
            }

            if(!isFromActivityResult && !isPaused){
                mExoPlayerView.setVisibility(INVISIBLE);
                mIvEventImage.setVisibility(INVISIBLE);
                mIvVideoPlay.setVisibility(INVISIBLE);
            }

            isFromActivityResult = false;
            isPaused = false;

        }
    };

    private BroadcastReceiver mVideoErrorListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mIsVideoError = true;
            cancelNextVideoTimer();

            if(mPlayer!=null) {

                if (mPlayer.getPlayWhenReady()) {
                    try {
                        mPlayImageButton.setActivated(false);
                        mIvEventImage.setVisibility(View.INVISIBLE);
                        mIvVideoPlay.setVisibility(View.INVISIBLE);
                        mVideoError.setVisibility(VISIBLE);
                        mExoPlayerView.setVisibility(View.INVISIBLE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };


    //for activity result use
    private void initExoPlayer(int resumeWindow , long resumePosition) {


        isFromActivityResult = true;

        mResumePosition = resumePosition;
        mResumeWindow = resumeWindow;


        if (mPlayer == null) {
//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList,false,Constants.TYPE_EVENT);

            mPlayer = Utils.getDetailScreePlayer();

            mExoPlayerView.setPlayer(mPlayer);

            mPlayer.setRepeatMode(Player.REPEAT_MODE_ALL);

            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

            if (haveResumePosition) {
                mPlayer.seekTo(resumeWindow, resumePosition);
            }
            mPlayer.setPlayWhenReady(true);
        } else {
            boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

            if (haveResumePosition) {
                mPlayer.seekTo(resumeWindow, resumePosition);
            }
            mPlayer.setPlayWhenReady(true);
        }

    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelNextVideoTimer();

        if (mExoPlayerView != null && mPlayer != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mExoPlayerView.getPlayer().getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mSaveVideoPosition);
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mVideoErrorListener);

    }



    //  opened and closed full screen dialog
    private void initFullscreenButton() {

        PlayerControlView controlView = mExoPlayerView.findViewById(R.id.exo_controller);
//        controlView.findViewById(R.id.exo_player_audio).setVisibility(GONE);
//        controlView.findViewById(R.id.exo_player_video).setVisibility(VISIBLE);
        mPlayImageButton  = controlView.findViewById(R.id.exo_play);
    }

    public void openFullScreenVideo(int rotationAngle){
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }

    public boolean getPlayBackState(){
        return mPlayer.getPlayWhenReady();
    }

    public void setExoPlayer(int resumeWindow , long resumePosition){
        if (mExoPlayerView == null) {

            mExoPlayerView = findViewById(R.id.exoplayer);
            initFullscreenButton();

//            Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrls, eClassesArrayList,false,Constants.TYPE_EVENT);
        }

        initExoPlayer(resumeWindow, resumePosition);
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        if(!TextUtils.isEmpty(mDashBoardDetails.getEvent().getSeek_time())) {
            mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardDetails.getEvent().getSeek_time());
        }else{
            mResumePosition = 0;
        }
        mPlayer = Utils.getDetailScreenConcatinatedPlayer();

        mExoPlayerView.setPlayer(mPlayer);


        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            mPlayer.seekTo(0, mResumePosition);
        }

        mPlayer.setPlayWhenReady(false);
    }





    public void pauseVideo(){
        if(mPlayer!=null) {
            mPlayer.setPlayWhenReady(false);
        }

        isPaused = true;
    }


    public VideoData getCurrentEventVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition()/1000));

                videoDataToBeSent.setType(Constants.TYPE_EVENT);

                videoDataToBeSent.setVideoId(mDashBoardDetails.getEvent().getId());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }


    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>]"+ "Full Screen Video" +" Mode</b> in Player from "+ mDashBoardDetails.getEvent().getTitle());

        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer);
        mNextVideo=dialogView.findViewById(R.id.tv_nxtvideo);
        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);


        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.frame_video);
        TextView tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);
        String  videoUrl = mVideoUrls.get(mExoPlayerView.getPlayer().getCurrentWindowIndex());
        String mEClassesTitle = null;
        for(Chapters.EClasses eClass:eClassesArrayList){
            if(videoUrl.equals(eClass.getPlay_url())){
                mEClassesTitle = eClass.getTitle();
            }
        }

        tvEClassName.setText(mEClassesTitle);
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);

        Glide.with(mContext).load(imageUrl).into(dialogCourseImage);

        // on Next Click
        dialogCourseImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });
        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>]"+ "Compact Screen Video" +" Mode</b> in Player from "+ mDashBoardDetails.getEvent().getTitle());

        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        PlayerControlView playerControlViewFullScreen = mDialogplayerView.findViewById(R.id.exo_controller);
        playerControlViewFullScreen.setVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility==View.VISIBLE){
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if(mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                        mNextVideo.setVisibility(VISIBLE);
                    }else{
                        mNextVideo.setVisibility(GONE);
                    }
                }else{
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });


    }


    private void startNextVideoTimer(){
        mTimerNextVideo.schedule(new TimerTask() {
            @Override
            public void run() {
                mNextVideoHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mPlayer != null) {
                            // next video option will be shown before 45 seconds from end
                            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && mPlayer.getNextWindowIndex() != -1 && mPlayer.getRepeatMode() == Player.REPEAT_MODE_ALL) {
                                showNextVideoLabel();
                            } else {
                                disableNextVideoLabel();
                            }
                        }

                    }
                });
            }
        }, 0, 500);
    }


    private void cancelNextVideoTimer(){
        mTimerNextVideo.cancel();
        disableNextVideoLabel();
    }


    private void showNextVideoLabel(){
        // if full screen
        if(mExoPlayerFullscreen) {

            if (nextVideoFrame.getVisibility() == GONE) {
                nextVideoFrame.setVisibility(VISIBLE);
            }
        }

    }

    private void disableNextVideoLabel(){
        // if full screen
        if(mExoPlayerFullscreen) {

            if (nextVideoFrame.getVisibility() == VISIBLE) {
                nextVideoFrame.setVisibility(GONE);
            }
        }
    }

}
