package com.imentors.wealthdragons.eventDetailView;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.courseDetailView.CourseDetailBelowVideo;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardEventDetails;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class EventDetailBelowVideo extends RelativeLayout {


    private Context mContext;
    private FrameLayout mGetInstantAccess, mAddToWishList , mTakeThisEvent;
    private WealthDragonTextView mTvAddToWishList , mTvRemoveWishList , mTvProcessing , mTvEventPrice;
    private String mItemId;
    private boolean mIsItemAdded = false;
    private CourseDetailBelowVideo.OnItemClickListener mOnItemClickListener = null;



    public EventDetailBelowVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EventDetailBelowVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EventDetailBelowVideo(Context context, DashBoardEventDetails dashBoardDetails, CourseDetailBelowVideo.OnItemClickListener onItemClickListener) {
        super(context);
        this.mOnItemClickListener = onItemClickListener;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DashBoardEventDetails dashBoardDetails) {

        mItemId = dashBoardDetails.getEvent().getId();

        if(dashBoardDetails.getIs_subscribed()){
            mTakeThisEvent.setVisibility(View.VISIBLE);
            mTvEventPrice.setVisibility(View.VISIBLE);

        }else{
            mTakeThisEvent.setVisibility(View.GONE);
            mTvEventPrice.setVisibility(View.GONE);

        }

        if(dashBoardDetails.isWishlist()){
            mAddToWishList.setVisibility(View.VISIBLE);

        }else{
            mAddToWishList.setVisibility(View.GONE);

        }


        if(dashBoardDetails.getEvent().getWishlist().equals(mContext.getString(R.string.no))){

            mTvRemoveWishList.setVisibility(GONE);
            mTvAddToWishList.setVisibility(VISIBLE);
            mIsItemAdded = false;

        }else{

            mTvRemoveWishList.setVisibility(VISIBLE);
            mTvAddToWishList.setVisibility(GONE);
            mIsItemAdded = true;

        }

        mAddToWishList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddToWishList.setVisibility(View.GONE);
                mTvProcessing.setVisibility(VISIBLE);
                callWishListApi();
            }
        });

        mTakeThisEvent.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               mOnItemClickListener.onSubScriptionButtonClick(false);
            }
        });

        if(!TextUtils.isEmpty(dashBoardDetails.getEvent().getCost())){
            mTvEventPrice.setText("£"+dashBoardDetails.getEvent().getCost());
        }else{
            mTvEventPrice.setText("£ 0.00");
        }



    }

    // initialize mentor detail header view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_below_video, this);
        mContext = context;

        mGetInstantAccess = view.findViewById(R.id.fl_btn_instant_access);
        mGetInstantAccess.setVisibility(GONE);
        mAddToWishList = view.findViewById(R.id.fl_btn_wishlist);
        mTvAddToWishList = view.findViewById(R.id.tv_add_to_wishlist);
        mTvRemoveWishList = view.findViewById(R.id.tv_remove_wishlist);
        mTvProcessing = view.findViewById(R.id.tv_processing);
        mTakeThisEvent = view.findViewById(R.id.fl_take_this_event);
        mTakeThisEvent.setVisibility(VISIBLE);
        mTvEventPrice = view.findViewById(R.id.tv_event_price);
        mTvEventPrice.setVisibility(VISIBLE);
    }


    public void callWishListApi( ) {
        String api = null;

        if(mIsItemAdded){
            //  calling remove wishlist api
            api= Api.REMOVE_WISHLIST_API;
        }else{
            //  calling add wishlist api
            api=Api.ADD_WISHLIST_API;
        }

        //  calling api and maintaining response


        //TODO implemented maintenance mode
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    mTvProcessing.setVisibility(GONE);
                    mAddToWishList.setVisibility(View.VISIBLE);

                    mIsItemAdded = !mIsItemAdded;

                    if(mIsItemAdded){
                        mTvRemoveWishList.setVisibility(VISIBLE);
                        mTvAddToWishList.setVisibility(GONE);
                    }else{
                        mTvRemoveWishList.setVisibility(GONE);
                        mTvAddToWishList.setVisibility(VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                    params.put(Keys.ITEM_ID, mItemId);
                    params.put(Keys.TYPE, "Event");



                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }






}
