package com.imentors.wealthdragons.eventDetailView;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardEventDetails;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class EventDetailDescription extends LinearLayout {


    private Context mContext;
    private FrameLayout mBtShare, mBtFullDetails;
    private WealthDragonTextView mTvCourseShortDescription, mTvCourseFullDescription , mTvFullDetailBtn;
    private boolean isFullVisible = false;




    public EventDetailDescription(Context context) {
        super(context);
        mContext = context;
    }

    public EventDetailDescription(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public EventDetailDescription(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public EventDetailDescription(Context context, DashBoardEventDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardEventDetails dashBoardDetails) {

      if(!TextUtils.isEmpty(dashBoardDetails.getEvent().getShare_url())){
          mBtShare.setVisibility(View.VISIBLE);
      }else{
          mBtShare.setVisibility(View.GONE);
      }

        if(!TextUtils.isEmpty(dashBoardDetails.getEvent().getDescription())){
            mTvCourseShortDescription.setVisibility(View.VISIBLE);
            mTvCourseShortDescription.setText(dashBoardDetails.getEvent().getDescription());
        }else{
            mTvCourseShortDescription.setVisibility(View.GONE);
        }


        mBtFullDetails.setVisibility(View.GONE);
        mBtFullDetails.setVisibility(View.GONE);


      if(dashBoardDetails.isSocial_sharing()){
          mBtShare.setVisibility(View.VISIBLE);

      }else {
          mBtShare.setVisibility(View.GONE);
      }


      mBtShare.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
              mBtShare.setEnabled(false);
              Utils.callEventLogApi("clicked <b>Share</b> from <b>"+dashBoardDetails.getEvent().getTitle()+"</b> " + Constants.TYPE_EVENT + " type");
              Utils.shareArticleUrl(dashBoardDetails.getEvent().getShare_url(),mContext,mContext.getString(R.string.share_article));
          }
      });

    }


    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if(hasWindowFocus){
            mBtShare.setEnabled(true);
        }
    }

    //  initialize event detail description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_description, this);
        mContext = context;

        mBtShare = view.findViewById(R.id.fl_btn_share);
        mBtFullDetails = view.findViewById(R.id.fl_btn_full_details);

        TextView detailTitle =  view.findViewById(R.id.tv_detail_title);
        detailTitle.setText(mContext.getString(R.string.event_description));

        mTvCourseFullDescription = view.findViewById(R.id.tv_course_full_description);
        mTvCourseShortDescription = view.findViewById(R.id.tv_course_small_description);

        mTvFullDetailBtn = view.findViewById(R.id.tv_full_detail_btn);


    }



}
