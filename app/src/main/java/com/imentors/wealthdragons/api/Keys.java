package com.imentors.wealthdragons.api;

/**
 * Created on 7/4/17.
 */

public class Keys {

    private Keys() {
        /* cannot be instantiated */
    }

    public static final String AUTHORIZATION = "Authorization";
    public static final String RESET_TOKEN = "reset_token";
    public static final String TOKEN = "token";
    public static final String USER_TOKEN = "user_token";
    public static final String LOG_MESSAGE = "log_message";
    public static final String USER_ID = "user_id";
    public static final String TASTE_ID = "taste_id";
    public static final String PARENT_ID = "parent_cat_id";
    public static final String PAGE_NO = "page_no";
    public static final String USERID = "userid";
    public static final String ORDER_ID = "order_id";




    public static final String ANDROID = "Android";
    public static final String TABLET = "Tablet";

    public static final String DEVICE = "device";
    public static final String PAYMENT_METHOD = "payment_method";
    public static final String CC = "CC";
    public static final String DEVICE_ID = "device_id";
    public static final String SUCCESS  = "success";
    public static final String _ERROR = "_error";
    public static final String _MAINTAINANCE = "_maintenance";
    public static final String _CATEGORY = "_category";

    public static final String ERROR = "error";
    public static final String _INACTIVE = "_inactive";
    public static final String INACTIVE = "inactive";
    public static final String DEVICE_NAME = "device_name";
    public static final String URL = "url";
    public static final String LOGIN_WITH = "login_with";




    //public static final String STATUS = "status";
    //public static final String RESULT = "result";


    // Register Screen
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String IMAGE = "image";



    // Signing Screen

    public static final String USER_PASSWORD = "data[User][password]";
    public static final String USER_EMAIL = "data[User][email]";


    // support screen

    public static final String SUBJECT = "subject";
    public static final String MESSAGE = "message";

    // search screen
    public static final String SEARCH_KEY ="search_key";
    public static final String KEYWORDS_KEY = "key_words";



    // detail screen

    public static final String COURSE_ID = "course_id";
    public static final String MENTOR_ID = "mentor_id";
    public static final String VIDEOS_ID = "video_id";
    public static final String CATEGORIES = "categories";
    public static final String PROGRAMME_ID = "programme_id";

    public static final String CURRENT_COURSE_ID = "current_course_id";
    public static final String RELATED_COURSE = "Related_Courses";
    public static final String EVENT_ID = "event_id";
    public static final String PROGRAMMES_ID = "programme_id";
    public static final String EPISODE_ID = "episode_id";
    public static final String VIDEO_ID = "video_id";
    public static final String COUNTRY = "country";
    public static final String TIME = "time";



    //quiz
    public static final String QUIZ_LIST_ID = "quiz_list_id";
    public static final String QUESTION_ID = "question_id";
    public static final String ANSWER = "answer";
    public static final String FINISH = "finish";

    //wishlist
    public static final String ITEM_ID = "item_id";
    public static final String TYPE = "type";
    public static final String STATUS = "status";



    //Message
    public static final String MESSAGE_ID = "message_id";
    public static final String ID = "id";
    public static final String CARD_ID = "card_id";
    public static final String ITEM_TYPE = "item_type";



    public static final String REQUEST_ID = "request_id";


    // live chat
    public static final String STREAMING_URL = "StreamingURL";
    public static final String CHAT_TOKEN = "chatToken";
    public static final String CHAT_USER_ID = "UserId";
    public static final String USER_TYPE = "UserType";
    public static final String THANK_YOU_MSG = "ThankYouMsg";
    public static final String CANCEL_MESSAGE_TITLE = "CancelMsg";



    //customer support

    public static final String SUPPORT_FIRST_NAME = "support_first_name";
    public static final String SUPPORT_LAST_NAME = "support_last_name";
    public static final String SUPPORT_EMAIL = "contact_email";
    public static final String SUPPORT_MOBILE = "contact_mobile";
    public static final String SUPPORT_SERVICE_CODE = "service_code";
    public static final String SUPPORT_SERVICE_TYPE_ID = "support_type_id";
    public static final String SUPPORT_CONTACT_MESSAGE = "contact_message";
    public static final String SUPPORT_COUNTRY_CODE = "country_code";



    //taste preferences

    public static final String TASTE = "taste";
    public static final String CAT_ID = "cat_id";


    // settings
    public static final String ENABLE = "enable";
    public static final String DATA_USAGE = "data_usage";
    public static final String NEXT_EPISODE = "next_episode";


    // notification
    public static final String TITLE = "title";
    public static final String BODY = "body";
    public static final String NOTIFICATION = "notification";
    public static final String LAYOUT = "layout";
    public static final String EXPERT_ID = "expert_id";
    public static final String EXPERT_ID_CHAT = "ExpertID";




    // Log Message
    public static final String MESSAGE_LOG = "log_message";
    public static final String VERSION = "version";

    public static final String CURRENCY="currency";
    public static final String CARD_NUMBER="card_no";
    public static final String CARD_MONTH="ex_month";
    public static final String CARD_YEAR="ex_year";
    public static final String CARD_CVV="cvv";
    public static final String CARD_TYPE="card_type";
    public static final String SAVE_CARD = "save_card";
    public static final String EX_MONTH="card_month";
    public static final String EX_YEAR="card_year";
    public static final String EX_CVV="card_cvv";

    //card_month,card_year,card_cvv

    public static final String ON = "ON";

    public static final String ECLASS_ID = "eclass_id";


    public static final String ARTICLE_ID = "ARTICLE_ID";
    public static final String WISHLIST_VALUE = "WISHLIST_VALUE";
    public static final String RELOAD_PAGE = "RELOAD_PAGE";

    public static final String LIVE_DIGI_COACHING_ID = "live_digi_coaching_id";
    public static final String LIVE_AUDIENCE = "LiveAudience";
    public static final String LIVE_SESSION_ROW_ID = "live_session_row_id";

}
