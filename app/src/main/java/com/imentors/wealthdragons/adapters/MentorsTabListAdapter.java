package com.imentors.wealthdragons.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.FooterViewHolder;
import com.imentors.wealthdragons.views.MentorView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 02-02-2016.
 */
public class MentorsTabListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {


    //article list view
    private static final int TYPE_MENTOR = 0;

    //Type footer and empty
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_EMPTY = 2;



    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener;


    //ref to the Mentors items
    private List<Mentors> mItemsMentors;



    private String mTasteId;

    private Context mContext;

    private int mTabPosition ,mItemsCountOnServer, mNextPage;

    private boolean isTablet, mServerError, mIsSubCategory = false;

    private PagingListener mPagingListener ;




    public MentorsTabListAdapter(OnItemClickListener onItemClickListener, PagingListener pagingListener, Context context ) {
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        mPagingListener = pagingListener;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

    }


    /**
     * set the article items
     *
     * @param itemsMentors             mentors items

     */
    public void setItems(final List<Mentors> itemsMentors ,  int tabPosition, final int nextPage, final int itemsCountOnServer  , final String tasteId) {


        if(itemsMentors != null){
            mItemsMentors = new ArrayList<>();
            mItemsMentors.addAll(itemsMentors);
        }


        mTabPosition = tabPosition;

        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;

        notifyDataSetChanged();
    }


    public void addItems(final List<Mentors> itemsMentors , final int nextPage, final int itemsCountOnServer  , final String tasteId){
        if(mItemsMentors!=null){
                mItemsMentors.addAll(itemsMentors);
        }

        mIsSubCategory = false;

        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;

        notifyDataSetChanged();

    }


    public void setServerError(){
        mServerError = true;
        notifyDataSetChanged();
    }

    public void clearData(int position){

        if(position == Constants.MENTOR_TAB) {

            if (mItemsMentors != null) {

                mItemsMentors.clear();

            }
        }


        notifyDataSetChanged();

    }

    public void isSubCategoryFilterData(){
        mIsSubCategory = true;
    }




    /**
     * get the article items
     *
     * @return article items
     */
    public List<Mentors> getMentorsItems() {

        if(mItemsMentors!=null){
            return mItemsMentors;
        }

        return  null;
    }





    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {

        View view;
        switch (viewType) {
            case TYPE_MENTOR:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.mentor_list_item, parent, false);
                return new MentorViewHolder(view, mOnItemClickListener);
            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_footer, parent, false);
                return new FooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_empty, parent, false);
                return new EmptyViewHolder(view);
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof MentorsTabListAdapter.MentorViewHolder) {
            Mentors item = mItemsMentors.get(position);
            //cast holder to ItemViewHolder and set data for header.
            MentorViewHolder viewHolder = (MentorViewHolder) holder;
            viewHolder.bind(item, position,mItemsMentors.size(),mContext);

        }else if (holder instanceof FooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        }



    }

    @Override
    public int getItemViewType(int position) {
        if (position >= mItemsMentors.size()) {
            final int size = mItemsMentors.size();
            if (size == 0) {
                if(mIsSubCategory){
                    return TYPE_FOOTER;
                }
                return TYPE_EMPTY;
            } else {
                return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
            }
        } else {
            return TYPE_MENTOR;
        }
    }

    @Override
    public int getItemCount() {

        if(mItemsMentors!=null){

            // plus one for footer
          return   mItemsMentors.size()+1;
        }

        return 0;

    }


    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onMentorClick(Mentors item, int position);
        void setHeader(String title, String imageHeader, int listSize, int tabPosition);
    }



    /**
     * View holder class for items fo recycler view
     */
    private static class MentorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final OnItemClickListener mOnItemClickListener;
        private final MentorView mMentorBannerLayout;
        // current bind to view holder
        private Mentors mCurrentItem;
        //position
        private int mPosition;


        MentorViewHolder(@NonNull View view, final OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mMentorBannerLayout =  view.findViewById(R.id.dashboard_mentor_banner);

        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onMentorClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Mentors item, final int position , int totalItems , Context mContext ) {
            mCurrentItem = item;
            mPosition = position;


            if(totalItems-1 == position && totalItems>1){
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int)mContext.getResources().getDimension(R.dimen.margin_default),0,(int)mContext.getResources().getDimension(R.dimen.margin_default),0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mMentorBannerLayout.bind(item, null,Constants.MENTOR_TAB,false);
        }
    }


    private void showFooterViewHolder(int position, FooterViewHolder viewHolder) {
        if(mItemsMentors !=null){
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mItemsMentors.size() && mItemsMentors.size() < mItemsCountOnServer && !mIsSubCategory) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError(0);

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                }
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading(0);
                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_MENTOR,null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError(0);

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                            }
                        }
                    });
                }


            }else if(mPagingListener != null && mIsSubCategory){
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError((int)Utils.getMentorImageHeight((Activity)mContext));

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_MENTOR,null);
                                    notifyDataSetChanged();

                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading((int)Utils.getMentorImageHeight((Activity)mContext));
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError((int)Utils.getMentorImageHeight((Activity)mContext));

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_MENTOR,null);
                                notifyDataSetChanged();
                            }
                            Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                        }
                    });
                }
            }
        }
    }


}
