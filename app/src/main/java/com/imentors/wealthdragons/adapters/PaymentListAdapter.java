package com.imentors.wealthdragons.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;

import java.util.List;

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentViewHolder> {

    private List<CreditCard> creditCardList;
    private Context mContext;
    private ClickListner mClickListner;
    private int selectionPostion=0;


    public PaymentListAdapter(Context context, List<CreditCard> mCreditCardList, ClickListner listner  ) {

        creditCardList = mCreditCardList;
        mContext=context;
        mClickListner=listner;
    }

    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_list_item, parent, false);

        return new PaymentViewHolder(view,mClickListner);
    }

   public interface ClickListner{

       void ClickItemListner(CreditCard creditCard, int item);
       void onItemDelete(int pos);
   }



    @Override
    public void onBindViewHolder(@NonNull PaymentViewHolder holder, int position) {

        holder.bind(creditCardList.get(position),position);


    }

    public void removeItem(int position){
        creditCardList.remove(position);
        AppPreferences.setCreditCardList(creditCardList);
        notifyItemRemoved(position);
    }

    public CreditCard getItem(int pos){
        return creditCardList.get(pos);
    }


    @Override
    public int getItemCount() {
        return creditCardList.size();
    }

    public class PaymentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mAccountNumber;
        private RecyclerView mRecycleview;
        private ClickListner clickListner;
        private CreditCard mCreditCard;
        private int pos;
        private RadioButton mCreditCardSelectionButton;
        private ImageView mDelete;
        private TextView mTvCardType,mTvExpMonth;

        public PaymentViewHolder(View itemView, final ClickListner mclickListner) {
            super(itemView);
            mAccountNumber=itemView.findViewById(R.id.tv_card_number);
            mDelete=itemView.findViewById(R.id.delete_icon);
            mCreditCardSelectionButton = itemView.findViewById(R.id.rb_selection);
            mTvCardType=itemView.findViewById(R.id.tv_card_type);
            mTvExpMonth=itemView.findViewById(R.id.tv_expires_month);
            clickListner=mclickListner;
            itemView.setOnClickListener(this);

//            mDelete.setOnClickListener(this);
//            mDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                   mclickListner.onItemDelete(getAdapterPosition());
//                }
//            });


        }

        public void bind(CreditCard creditCard, int position) {
            pos = position;
            mCreditCard = creditCard;
            mAccountNumber.setText(creditCard.getCard_no());

            if (!TextUtils.isEmpty(creditCard.getEx_month()))
            {
                mTvExpMonth.setVisibility(View.VISIBLE);
                mTvExpMonth.setText("Expires "+creditCard.getEx_month()+"/"+creditCard.getEx_year());
            }
            else
            {
                mTvExpMonth.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(creditCard.getCard_type()))
            {
//                mTvCardType.setVisibility(View.VISIBLE);
                mTvCardType.setText(creditCard.getCard_type());
            }else{
                mTvCardType.setVisibility(View.GONE);
            }

            if(position == selectionPostion){
                mCreditCardSelectionButton.setChecked(true);
                Utils.callEventLogApi("selected <b>"+creditCard.getCard_no()+"</b> from <b>"+creditCard.getCard_type()+"</b> in payment card list");
            }else{
                mCreditCardSelectionButton.setChecked(false);
            }

            switch (creditCard.getCard_type()){
                case "Visa":
                    mDelete.setImageResource(R.drawable.bt_ic_visa);
                    break;
                case "MasterCard":
                    mDelete.setImageResource(R.drawable.bt_ic_mastercard);
                    break;
                case "AmericanExpress":
                    mDelete.setImageResource(R.drawable.bt_ic_amex);

                default:
                    mDelete.setImageResource(R.drawable.bt_ic_discover);
                    break;

            }
        }

        @Override
        public void onClick(View v) {
            selectionPostion = pos;
            mClickListner.ClickItemListner(mCreditCard,pos);
            notifyDataSetChanged();

        }
    }
}
