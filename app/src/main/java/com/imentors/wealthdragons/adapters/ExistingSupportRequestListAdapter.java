package com.imentors.wealthdragons.adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Utils;

import java.util.List;

public class ExistingSupportRequestListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<SupportType.ExistingSupportMessage> mSupportTypeList;
    private OnViewDetailClick mOnItemClickListener;

    public ExistingSupportRequestListAdapter(List<SupportType.ExistingSupportMessage> supportTypeList, OnViewDetailClick onItemClickListener) {
        this.mSupportTypeList = supportTypeList;
        mOnItemClickListener = onItemClickListener;
    }



    /**
     * different click handled by onItemClick
     */
    public interface OnViewDetailClick {
        void onViewDetailClicked(SupportType.ExistingSupportMessage item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.existing_support_request_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mSupportTypeList.get(position));

        viewHolder.fmViewDetailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Ticket Detail</b> of  <b>" +mSupportTypeList.get(position).getTicket_no()+"</b> ticket number");
                mOnItemClickListener.onViewDetailClicked(mSupportTypeList.get(position));
            }
        });

    }


    @Override
    public int getItemCount() {
        return mSupportTypeList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tvTicketNumber , tvStatus;
        public FrameLayout fmViewDetailButton;


        //  initialize view
        public MyViewHolder(View view ) {
            super(view);
            tvTicketNumber =  view.findViewById(R.id.tv_ticket_number);
            tvStatus = view.findViewById(R.id.tv_ticket_status);
            fmViewDetailButton = view.findViewById(R.id.fl_view_details);
        }



        // Set values to the list items
        void bind(final SupportType.ExistingSupportMessage item){
            tvStatus.setText(item.getMessage_status());
            tvTicketNumber.setText(item.getTicket_no());
        }


    }
}
