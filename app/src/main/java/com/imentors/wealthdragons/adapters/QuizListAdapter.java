package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class QuizListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<String> mAnswerList;
    private Context mContext;
    private String mLayoutType;
    private String mQuestion;
    private List<Integer> mSelectedListPosition = new ArrayList<>();
    private int mSelectedPosition=-1;

    public QuizListAdapter(Context context) {
        mContext = context;
    }


    //  set new items
    public void setNewItems(List<String> answerList , String layoutType , String question){
        if(mAnswerList!=null) {
            mAnswerList.clear();
        }
        mQuestion=question;
        mAnswerList = answerList;
        mLayoutType = layoutType;
        mSelectedListPosition.clear();
        mSelectedPosition=-1;
        notifyDataSetChanged();
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.quiz_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    //  set data in view
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;

        viewHolder.bind(mAnswerList.get(position),position,mContext,mLayoutType);


        viewHolder.mLlQuizListItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("selected <b>"+mAnswerList.get(position)+"</b> from <b>'"+mQuestion+"'</b> question");
                switch(mLayoutType){
                    case Constants.MULTIPLE_CHOICE_CHECKBOX:
                         int mSelectedPositionInList = -1;
                        if(mSelectedListPosition.size()>0) {

                            for (int i=0;i<mSelectedListPosition.size();i++) {

                                if (mSelectedListPosition.get(i) == position){
                                    mSelectedPositionInList = i;
                                    break;
                                }
                            }

                            if(mSelectedPositionInList>=0){
                                mSelectedListPosition.remove(mSelectedPositionInList);
                            }else{
                                mSelectedListPosition.add(position);
                            }

                        }else {
                            mSelectedListPosition.add(position);
                        }
                        notifyDataSetChanged();
                        break;
                    case Constants.TRUE_FALSE:
                        mSelectedPosition = position;
                        notifyDataSetChanged();
                        break;
                    case Constants.MULTIPLE_CHOICE_RADIO:
                        mSelectedPosition = position;
                        notifyDataSetChanged();
                        break;
                }
            }
        });


        switch(mLayoutType){
            case Constants.MULTIPLE_CHOICE_CHECKBOX:

                if(mSelectedListPosition.size()>0){

                    for(int select: mSelectedListPosition){
                        if(select == position){
                            viewHolder.mIvMark.setImageResource(R.drawable.multi_check_active);
                            return;
                        }
                    }
                    viewHolder.mIvMark.setImageResource(R.drawable.multi_check_inactive);

                }else{
                    viewHolder.mIvMark.setImageResource(R.drawable.multi_check_inactive);
                }

                break;
            case Constants.TRUE_FALSE:

                if(position == mSelectedPosition){
                    viewHolder.mIvMark.setImageResource(R.drawable.radio_check_active);

                }else{
                    viewHolder.mIvMark.setImageResource(R.drawable.radio_check_inactive);

                }

                break;
            case Constants.MULTIPLE_CHOICE_RADIO:
                if(position == mSelectedPosition) {
                    viewHolder.mIvMark.setImageResource(R.drawable.radio_check_active);

                }else{
                    viewHolder.mIvMark.setImageResource(R.drawable.radio_check_inactive);

                }

                break;
        }


    }

    public int getSelecetedItem(){
       return mSelectedPosition;
    }

    public List<Integer> getSelectedItemList(){
        return mSelectedListPosition;
    }


    @Override
    public int getItemCount() {
        return mAnswerList.size();
    }


    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView title;
        private int mPosition;
        private ImageView mIvMark;
        private LinearLayout mLlQuizListItem;

        //  initialize view
        public MyViewHolder(View view) {
            super(view);
            mIvMark = view.findViewById(R.id.iv_selected_drawable);
            title =  view.findViewById(R.id.tv_quiz_answer);
            mLlQuizListItem = view.findViewById(R.id.ll_quiz_list_item);
        }


        // Set values to the list items
        void bind(final String item, final int position , Context context , String layoutType){
            mPosition = position;

            if(mPosition%2==0){
                mLlQuizListItem.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundGray));
            }else{
                mLlQuizListItem.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
            }

            title.setText(item);
        }


    }
}
