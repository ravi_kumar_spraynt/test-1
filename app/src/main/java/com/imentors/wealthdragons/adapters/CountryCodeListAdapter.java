package com.imentors.wealthdragons.adapters;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.CountryCode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CountryCodeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnCountryCodeClick mOnItemClickListener;
    private List<CountryCode> mFilterCountryCodeList = null;
    private ArrayList<CountryCode> mCountryCodeList = new ArrayList<>();

    //  calling constructor
    public CountryCodeListAdapter(List<CountryCode> countryCode, OnCountryCodeClick onItemClickListener) {
        mCountryCodeList.clear();
        if (mFilterCountryCodeList != null) {
            mFilterCountryCodeList.clear();
        }
        mFilterCountryCodeList = countryCode;
        mOnItemClickListener = onItemClickListener;
        mCountryCodeList.addAll(countryCode);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_fragment_country_code_list_item, parent, false);

        return new MyViewHolder(itemView, mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mFilterCountryCodeList.get(position));
    }

    @Override
    public int getItemCount() {
        return mFilterCountryCodeList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mFilterCountryCodeList.clear();
        if (charText.length() == 0) {
            mFilterCountryCodeList.addAll(mCountryCodeList);
        } else {
            for (CountryCode wp : mCountryCodeList) {
                if (wp.getNicename().toLowerCase(Locale.getDefault()).contains(charText)) {
                    mFilterCountryCodeList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnCountryCodeClick {
        void onCountryCodeSelect(CountryCode item);
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private OnCountryCodeClick onItemClickListener;
        private CountryCode mItem;


        public MyViewHolder(View view, OnCountryCodeClick listener) {
            super(view);
            title = view.findViewById(R.id.txt_name);
            view.setOnClickListener(this);
            onItemClickListener = listener;
        }


        // Set values to the list items
        void bind(final CountryCode item) {
            mItem = item;
            title.setText(item.getNicename() + "(" + item.getPhonecode() + ")");
        }


        @Override
        public void onClick(View v) {
            if (onItemClickListener != null) {
                onItemClickListener.onCountryCodeSelect(mItem);
            }
        }
    }
}
