package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facebook.keyframes.KeyframesDrawable;
import com.facebook.keyframes.KeyframesDrawableBuilder;
import com.facebook.keyframes.deserializers.KFImageDeserializer;
import com.facebook.keyframes.model.KFImage;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.ChatMessage;
import com.imentors.wealthdragons.utils.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import de.hdodenhof.circleimageview.CircleImageView;


public class ChatMessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_CHAT_MESSAGE = 0;
    private static final int TYPE_CHAT_EMOJI = 1;
    private static final int TYPE_CHAT_EMPTY = 3;
    private List<ChatMessage> mMessageList;
    private Context mContext;


    public ChatMessageListAdapter(List<ChatMessage> supportTypeList, Context context) {
        this.mMessageList = supportTypeList;
        mContext = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {

            case TYPE_CHAT_MESSAGE:

                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_list_item, parent, false);
                return new MyChatViewHolder(view, mContext);

            case TYPE_CHAT_EMPTY:

                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_footer_list_item_empty, parent, false);
                return new EmptyFooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_list_emoji_item, parent, false);
                return new MyEmojiViewHolder(view, mContext);
        }


    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof MyChatViewHolder) {
            MyChatViewHolder viewHolder = (MyChatViewHolder) holder;
            viewHolder.bind(mMessageList.get(position));
        } else if (holder instanceof MyEmojiViewHolder) {
            MyEmojiViewHolder viewHolder = (MyEmojiViewHolder) holder;
            viewHolder.bind(mMessageList.get(position));
        }
    }


    //  add new items
    public void addNewItem(ChatMessage item) {
        if (item != null) {
            mMessageList.add(item);
            notifyItemInserted(getItemCount() - 1);
        }
    }


    @Override
    public int getItemViewType(int position) {

        if (!mMessageList.isEmpty()) {
            ChatMessage chatMessage = mMessageList.get(position);

            if (!TextUtils.isEmpty(chatMessage.getMessage())) {
                if (!TextUtils.isEmpty(chatMessage.getMessage())) {

                    switch (chatMessage.getMessage()) {
                        case Constants.ANGRY:
                            return TYPE_CHAT_EMOJI;

                        case Constants.SAD:
                            return TYPE_CHAT_EMOJI;

                        case Constants.HAHA:
                            return TYPE_CHAT_EMOJI;

                        case Constants.LOVE:
                            return TYPE_CHAT_EMOJI;

                        case Constants.LIKE:
                            return TYPE_CHAT_EMOJI;

                        case Constants.WOW:
                            return TYPE_CHAT_EMOJI;
                        default:
                            return TYPE_CHAT_MESSAGE;

                    }

                } else {
                    return TYPE_CHAT_MESSAGE;

                }
            } else {
                return TYPE_CHAT_EMPTY;
            }

        }

        return super.getItemViewType(position);


    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyChatViewHolder extends RecyclerView.ViewHolder {
        private TextView tvUserName;
        private CircleImageView ivUserImage;
        private Context mContext;

        public MyChatViewHolder(View view, Context context) {
            super(view);
            mContext = context;
            tvUserName = view.findViewById(R.id.text_message_body);
            ivUserImage = view.findViewById(R.id.image_message_profile);

        }

        // Set values to the list items
        void bind(final ChatMessage item) {

            tvUserName.setText(item.getMessage());
            Glide.with(mContext).load(item.getImage()).dontAnimate().into(ivUserImage);

        }


    }

    //   * View holder class for items fo recycler view
    private static class MyEmojiViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivUserImage;
        private ImageView imEmoji;
        private Context mContext;

        public MyEmojiViewHolder(View view, Context context) {
            super(view);
            mContext = context;
            ivUserImage = view.findViewById(R.id.image_message_profile);
            imEmoji = view.findViewById(R.id.im_emoji_icon);


        }

        // Set values to the list items
        void bind(final ChatMessage item) {


            switch (item.getMessage()) {
                case Constants.ANGRY:


                    setGifImage("Anger.json");


                    break;
                case Constants.SAD:


                    setGifImage("Sorry.json");


                    break;
                case Constants.HAHA:

                    setGifImage("Haha.json");


                    break;
                case Constants.LOVE:

                    setGifImage("Love.json");

                    break;
                case Constants.LIKE:

                    setGifImage("Like.json");

                    break;
                default:

                    setGifImage("Wow.json");

                    break;
            }


            Glide.with(mContext).load(item.getImage()).into(ivUserImage);


        }

        private KFImage getKFImage(String fileName) {
            AssetManager assetManager = mContext.getAssets();

            InputStream stream;
            KFImage kfImage = null;

            try {
                stream = assetManager.open(fileName);
                kfImage = KFImageDeserializer.deserialize(stream);
            } catch (IOException e) {
                Logger.getLogger(Constants.LOG, e.toString());
            }

            return kfImage;
        }


        private void setGifImage(String resource) {
            KeyframesDrawable imageDrawable = new KeyframesDrawableBuilder().withImage(getKFImage(resource)).build();
            imageDrawable.startAnimation();

            imEmoji.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            imEmoji.setImageDrawable(imageDrawable);
            imEmoji.setImageAlpha(0);
        }


    }


    public class EmptyFooterViewHolder extends RecyclerView.ViewHolder {


        public EmptyFooterViewHolder(@NonNull View view) {
            super(view);
        }


    }

}
