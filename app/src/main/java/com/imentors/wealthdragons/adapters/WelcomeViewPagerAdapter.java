package com.imentors.wealthdragons.adapters;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.imentors.wealthdragons.fragments.ForgotPasswordFragment;
import com.imentors.wealthdragons.fragments.RegisterFragment;
import com.imentors.wealthdragons.fragments.SignInFragment;
import com.imentors.wealthdragons.fragments.WelcomeFragment;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.Constants;

import java.io.Serializable;
import java.util.logging.Logger;

public class WelcomeViewPagerAdapter extends FragmentStatePagerAdapter {

    private int mNumberOfPagesInSlider;
    private WelcomeModel mWelcomeModelData;
    private boolean mIsNewPager= false;
    private Serializable mNotificationData= null;


    public WelcomeViewPagerAdapter(FragmentManager fm, WelcomeModel welcomeModel , boolean isNewPager, Serializable notificationData) {
        super(fm);
        mWelcomeModelData = welcomeModel;
        // add static pages count too when created
        mNumberOfPagesInSlider = mWelcomeModelData.getNumberOfFragmentsInSlider();
        mIsNewPager = isNewPager;
        mNotificationData = notificationData;

    }

    @Override
    public Fragment getItem(int position) {

        if(mIsNewPager){
            if (mWelcomeModelData.getUserGroup().isSignUpButtonVisible() && position == 0) {
                return RegisterFragment.newInstance(mWelcomeModelData, position,mNotificationData);

            } else if (mWelcomeModelData.getUserGroup().isSignInVisible()) {

                if (!mWelcomeModelData.getUserGroup().isSignUpButtonVisible() && (position == 0 || position == 1 )) {

                    return SignInFragment.newInstance(mWelcomeModelData, position,mNotificationData);

                }

            }

            return ForgotPasswordFragment.newInstance(mWelcomeModelData, position);

        }  else {

            if (position < mNumberOfPagesInSlider) {
                return WelcomeFragment.newInstance(mWelcomeModelData, position,mNotificationData);

            } else if (mWelcomeModelData.getUserGroup().isSignUpButtonVisible() && position == mNumberOfPagesInSlider) {

                return RegisterFragment.newInstance(mWelcomeModelData, position,mNotificationData);

            } else if (mWelcomeModelData.getUserGroup().isSignInVisible()) {

                if (!mWelcomeModelData.getUserGroup().isSignUpButtonVisible() && position == mNumberOfPagesInSlider) {

                    return SignInFragment.newInstance(mWelcomeModelData, position,mNotificationData);

                } else if (mWelcomeModelData.getUserGroup().isSignUpButtonVisible() && position == mNumberOfPagesInSlider + 1) {

                    try {
                        return SignInFragment.newInstance(mWelcomeModelData, position,mNotificationData);

                    }catch (Exception e){
                        Logger.getLogger(Constants.LOG, e.toString());
                    }


                }


            }

            return ForgotPasswordFragment.newInstance(mWelcomeModelData, position);

        }

    }

    //  counting total number of fragment
    @Override
    public int getCount() {
        if(mIsNewPager){
            return mWelcomeModelData.getTotalNumberOfFragments()-mWelcomeModelData.getNumberOfFragmentsInSlider();

        }else {
            return mWelcomeModelData.getTotalNumberOfFragments();

        }
    }

}
