package com.imentors.wealthdragons.adapters;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.PaymentHistory;
import com.imentors.wealthdragons.utils.Constants;

import java.util.List;

public class PaymentHistoryListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<PaymentHistory.OrderData> mPaymentOrderList;
    private Context mContext;
    private OnListItemClicked mOnItemClicked;

    public PaymentHistoryListAdapter(Context context , OnListItemClicked onListItemClicked) {
        mContext = context;
        mOnItemClicked = onListItemClicked;
    }


    //  set new items
    public void setNewItems(List<PaymentHistory.OrderData> paymentOrderList ){
        if(mPaymentOrderList!=null) {
            mPaymentOrderList.clear();
        }
        mPaymentOrderList = paymentOrderList;
        notifyDataSetChanged();
    }



    /**
     * different click handled by onItemClick
     */
    public interface OnListItemClicked {
        void onItemClicked(PaymentHistory.OrderData item);
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_history_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;

        viewHolder.bind(mPaymentOrderList.get(position),position,mContext);

        viewHolder.mLayoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClicked.onItemClicked(mPaymentOrderList.get(position));
            }
        });

    }



    @Override
    public int getItemCount() {
        return mPaymentOrderList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView mTitle , mPrice ,mDate ,mMemberType;
        private int mPosition;
        public LinearLayout mLayoutContainer;


        //  initialize view
        public MyViewHolder(View view) {
            super(view);
            mTitle =  view.findViewById(R.id.tv_member_title);
            mPrice = view.findViewById(R.id.tv_price);
            mDate = view.findViewById(R.id.tv_payment_date);
            mMemberType = view.findViewById(R.id.tv_member_type);
            mLayoutContainer = view.findViewById(R.id.ll_payment_contatiner);
        }


        // Set values to the list items
        void bind(final PaymentHistory.OrderData item, final int position , Context context){
            mPosition = position;
            mTitle.setText(item.getTitle());
            mPrice.setText(item.getCurrencyIcon()+item.getPrice());
            if(item.getType().equals(Constants.TYPE_EXPERT)){
                mMemberType.setText(Constants.TYPE_MENTOR);
            }else{
                mMemberType.setText(item.getType());
            }
            mDate.setText(item.getModified());

        }


    }
}
