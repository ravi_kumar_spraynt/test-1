package com.imentors.wealthdragons.adapters;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.ReviewFooterViewHolder;


import java.util.List;

public class SearchTopListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private int mItemsCountOnServer;
    private List<String> mentorList;
    private OnTopSearchesClick mOnItemClickListener;
    private static final int TYPE_FOOTER = 1;

    private static final int TYPE_EMPTY = 2;

    private static final int TYPE_VIEW = 3;
    private int mNextPage;
    private int mCurrentSize;
    private boolean mServerError;
    private PagingListener mPagingListener;


    public SearchTopListAdapter(List<String> mentorList, PagingListener pagingListener,OnTopSearchesClick onItemClickListener,int itemsCountOnServer,int nextPage) {
        this.mentorList = mentorList;
        mItemsCountOnServer = itemsCountOnServer;
        mCurrentSize = mentorList.size();
        mOnItemClickListener = onItemClickListener;
        mNextPage = nextPage;
        mPagingListener = pagingListener;
    }

    //  set server error
    public void setServerError() {
        mServerError = true;
        notifyDataSetChanged();
    }

    /**
     * different click handled by onItemClick
     */
    public interface OnTopSearchesClick {
        void onTopSearchItemClick(String item);
    }

    //  add items
    public void addItems(List<String> mentorList,int itemsCountOnServer,int nextPage) {
        if (mentorList != null) {
            this.mentorList.addAll(mentorList);
            mCurrentSize = this.mentorList.size();
            mNextPage = nextPage;
            mItemsCountOnServer = itemsCountOnServer;
            notifyDataSetChanged();
        }

    }

        @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view;
            switch (viewType) {

                case TYPE_FOOTER:
                    view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.review_footer_item, parent, false);
                    return new ReviewFooterViewHolder(view);

                case TYPE_EMPTY:
                    view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.review_footer_list_item_empty, parent, false);
                    return new EmptyViewHolder(view);

                default:
                    view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.fragment_search_top_list_item, parent, false);

                    return new MyViewHolder(view,mOnItemClickListener);
            }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReviewFooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            ReviewFooterViewHolder viewHolder = (ReviewFooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        } else if (holder instanceof MyViewHolder) {

            MyViewHolder viewHolder = (MyViewHolder) holder;
            viewHolder.bind(mentorList.get(position),position,mentorList.size());
        }

    }

    @Override
    public int getItemViewType(int position) {

        if (mentorList != null) {
            final int size = mentorList.size();

            if (position >= size) {
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_VIEW;
            }


        }


        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mentorList.size()+1;
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private View mDivider;
        private TextView title;
        private OnTopSearchesClick onItemClickListener;
        private String mItem;
        private int mPosition;



        //  initialize view
        public MyViewHolder(View view , OnTopSearchesClick listener) {
            super(view);
            title =  view.findViewById(R.id.txt_name);
            mDivider=view.findViewById(R.id.divider);
            view.setOnClickListener(this);
            onItemClickListener = listener;
        }



        // Set values to the list items
        void bind(final String item, final int position,int length){
            mItem= item;
            mPosition = position;
            title.setText(item);
            if (mPosition==length-1)
            {
                mDivider.setVisibility(View.GONE);

            }else{
                mDivider.setVisibility(View.VISIBLE);
            }
        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                onItemClickListener.onTopSearchItemClick(mItem);
            }
        }
    }


    private void showFooterViewHolder(int position, ReviewFooterViewHolder viewHolder) {
        if (mentorList != null) {
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mentorList.size() && mentorList.size() < mItemsCountOnServer) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError( );

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                                }
                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading();

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, null, Constants.TYPE_SEARCH,null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError();

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }


            }
        }


    }




}
