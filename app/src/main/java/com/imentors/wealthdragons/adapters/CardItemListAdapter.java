package com.imentors.wealthdragons.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;

import java.util.List;

public class CardItemListAdapter extends RecyclerView.Adapter<CardItemListAdapter.MyViewHolder> {
    private List<CreditCard> mCreditCard;
    private Context mContext;
    private OnListItemClicked mOnItemClicked;
    private int currentPosition;

    //  calling constructor
    public CardItemListAdapter(Context context, List<CreditCard> creditCards, OnListItemClicked onListItemClicked) {
        mContext = context;
        mOnItemClicked = onListItemClicked;
        mCreditCard = creditCards;
    }

    public void removeItem(int position) {
        mCreditCard.remove(position);
        AppPreferences.setCreditCardList(mCreditCard);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.bind(mCreditCard.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        return mCreditCard.size();
    }

    public interface OnListItemClicked {
        void onItemDelete(int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, Utils.DialogInteraction {
        private TextView mCardNumber;
        private TextView mExpMonthYear;
        private ImageView mDeleteIcon;

        public MyViewHolder(View view) {
            super(view);
            mCardNumber = view.findViewById(R.id.tv_card_number);
            mExpMonthYear = view.findViewById(R.id.tv_expires_months);
            mDeleteIcon = view.findViewById(R.id.delete_icon);
            itemView.setOnClickListener(this);
            mDeleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentPosition = getAdapterPosition();
                    showSignOutDialogBox();
                }
            });
        }

        public void bind(final CreditCard item, Context context) {
            CreditCard creditCard = item;
            mContext = context;
            mCardNumber.setText(item.getCard_no());
            mExpMonthYear.setText("Expires " + creditCard.getEx_month() + "/" + creditCard.getEx_year());
        }

        @Override
        public void onClick(View v) {
            // no use
        }

        private void showSignOutDialogBox() {
            Utils.showTwoButtonAlertDialog(this, mContext, mContext.getString(R.string.dialog_card_title), mContext.getString(R.string.dialog_card_delete), null, null);
        }

        @Override
        public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {
            mOnItemClicked.onItemDelete(currentPosition);
            dialog.dismiss();
        }

        @Override
        public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
            dialog.dismiss();
        }
    }


}

