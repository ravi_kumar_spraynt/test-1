package com.imentors.wealthdragons.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Feeds;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.ReviewFooterViewHolder;

import java.util.List;

public class FeedDetailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private int mItemsCountOnServer;
    private List<Feeds.Feed> mFeeds;
    private Context mContext;
    private OnListItemClicked mOnItemClicked;
    private PagingListener mPagingListener;
    private boolean mServerError;
    private static final int TYPE_FOOTER = 1;
    private int mNextPage;
    private static final int TYPE_EMPTY = 2;
    private static final int TYPE_VIEW = 3;
    private int mCurrentSize;


    public FeedDetailListAdapter(Context context, PagingListener pagingListener, List<Feeds.Feed> feeds, OnListItemClicked onListItemClicked,int itemsCountOnServer,int nextPage) {

        mContext = context;
        mOnItemClicked = onListItemClicked;
        mFeeds = feeds;
        mPagingListener = pagingListener;
        mItemsCountOnServer = itemsCountOnServer;
        mNextPage = nextPage;

    }

    //  set server error
    public void setServerError() {
        mServerError = true;
        notifyDataSetChanged();
    }


    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnListItemClicked {
        void onItemClicked(Feeds.Feed item, int postion);
    }

    //  add items
    public void addItems(List<Feeds.Feed> mentorList,int itemsCountOnServer,int nextPage) {
        if (mentorList != null) {
            this.mFeeds.addAll(mentorList);
            mCurrentSize = this.mFeeds.size();
            mNextPage = nextPage;
            mItemsCountOnServer = itemsCountOnServer;
            notifyDataSetChanged();
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view;
        switch (viewType) {

            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_item, parent, false);
                return new ReviewFooterViewHolder(view);

            case TYPE_EMPTY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_list_item_empty, parent, false);
                return new EmptyViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.feed_list_item, parent, false);

                return new MyViewHolder(view,mContext);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {



        if (holder instanceof ReviewFooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            ReviewFooterViewHolder viewHolder = (ReviewFooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        } else if (holder instanceof MyViewHolder) {

            MyViewHolder viewHolder = (MyViewHolder) holder;

            viewHolder.bind(mFeeds.get(position),position,mContext);

            viewHolder.mIvBannerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClicked.onItemClicked(mFeeds.get(position),position);
                }
            });
        }

    }



    @Override
    public int getItemCount() {
        return mFeeds.size()+1;
    }

    //   * View holder class for items fo recycler view

    private static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView mTitle,mDate;
        private ImageView mIvBannerImage;
        private int mPosition;


        //  initialize data
        public MyViewHolder(View view,Context context) {
            super(view);
            mTitle =  view.findViewById(R.id.tv_title);
            mDate=view.findViewById(R.id.tv_date);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mIvBannerImage.getLayoutParams().height = (int) Utils.getFeedsImageHeight((Activity) context);

        }


        // Set values to the list items
        void bind(final Feeds.Feed item, final int position , Context context){
            mPosition = position;
            Glide.with(context).load(item.getBanner()).into(mIvBannerImage);
            mTitle.setText(item.getTitle());
            mDate.setText(item.getCreated());

        }

    }



    private void showFooterViewHolder(int position, ReviewFooterViewHolder viewHolder) {
        if (mFeeds != null) {
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mFeeds.size() && mFeeds.size() < mItemsCountOnServer) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError( );

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                                }
                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading();

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, null, Constants.TYPE_SEARCH,null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError();

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }


            }
        }


    }


    @Override
    public int getItemViewType(int position) {

        if (mFeeds != null) {
            final int size = mFeeds.size();

            if (position >= size) {
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_VIEW;
            }


        }


        return super.getItemViewType(position);
    }




}
