package com.imentors.wealthdragons.adapters;


import android.graphics.Typeface;

import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.CourseItemListModel;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;


import java.util.List;

public class CourseNewItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<CourseItemListModel> mNewItemList;
    private String mListType;

    public CourseNewItemsAdapter(List<CourseItemListModel> newItemList, String listTitle) {
        this.mNewItemList = newItemList;
        mListType = listTitle;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_course_detail, parent, false);
        return new MyViewHolder(view, mListType);


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mNewItemList.get(position), position);

    }


    @Override
    public int getItemCount() {
        return mNewItemList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mListHeading;
        private TextView mDescription;
        private String mListTitle;
        private ImageView mListIcon;


        //  initialize view
        public MyViewHolder(View view, String listTitle) {
            super(view);
            mListHeading = view.findViewById(R.id.tv_course_item_heading);
            mDescription = view.findViewById(R.id.tv_course_item_description);
            mListIcon = view.findViewById(R.id.tv_course_item_list_icon);
            mListTitle = listTitle;
        }


        // Set values to the list items
        void bind(CourseItemListModel item, int position) {


            switch (mListTitle) {

                case Constants.COURSE_REGION:
                    mListIcon.setVisibility(View.GONE);

                    if (TextUtils.isEmpty(item.getHeading())) {
                        mListHeading.setVisibility(View.GONE);
                    } else {
                        mListHeading.setVisibility(View.VISIBLE);

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mListHeading.setText(Html.fromHtml(item.getHeading(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            mListHeading.setText(Html.fromHtml(item.getHeading()));
                        }

                        if (position != 0) {

                            mListHeading.setPadding(0, Utils.dpToPx(16), Utils.dpToPx(4), 0);
                        }
                        mListHeading.setTypeface(mListHeading.getTypeface(), Typeface.BOLD);


                    }
                    mListHeading.setMovementMethod(LinkMovementMethod.getInstance());


                    if (TextUtils.isEmpty(item.getText())) {
                        mDescription.setVisibility(View.GONE);
                    } else {
                        mDescription.setVisibility(View.VISIBLE);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mDescription.setText(Html.fromHtml(item.getText(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            mDescription.setText(Html.fromHtml(item.getText()));
                        }
                    }
                    mDescription.setMovementMethod(LinkMovementMethod.getInstance());


                    break;

                case Constants.COURSE_FAQS:
                    mListIcon.setVisibility(View.GONE);

                    if (TextUtils.isEmpty(item.getQuestion())) {
                        mListHeading.setVisibility(View.GONE);
                    } else {
                        mListHeading.setVisibility(View.VISIBLE);

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mListHeading.setText("Q: " + Html.fromHtml(item.getQuestion(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            mListHeading.setText("Q: " + Html.fromHtml(item.getQuestion()));
                        }

                        mListHeading.setTypeface(mListHeading.getTypeface(), Typeface.BOLD);

                    }
                    mListHeading.setMovementMethod(LinkMovementMethod.getInstance());


                    if (TextUtils.isEmpty(item.getAnswer())) {
                        mDescription.setVisibility(View.GONE);
                    } else {
                        mDescription.setVisibility(View.VISIBLE);


                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mDescription.setText("A: " + Html.fromHtml(item.getAnswer(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            mDescription.setText("A: " + Html.fromHtml(item.getAnswer()));
                        }
                    }
                    mDescription.setMovementMethod(LinkMovementMethod.getInstance());

                    break;


                case Constants.MENTOR_SKILLS:
                    manageHonorAndSkills(item);


                    break;

                case Constants.MENTOR_HONORS:
                    manageHonorAndSkills(item);

                    break;
                default:

                    mListHeading.setVisibility(View.GONE);
                    mListIcon.setVisibility(View.VISIBLE);
                    if (TextUtils.isEmpty(item.getText())) {
                        mDescription.setVisibility(View.GONE);
                    } else {
                        mDescription.setVisibility(View.VISIBLE);

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            mDescription.setText(Html.fromHtml(item.getText(), Html.FROM_HTML_MODE_LEGACY));
                        } else {
                            mDescription.setText(Html.fromHtml(item.getText()));
                        }

                    }
                    mDescription.setMovementMethod(LinkMovementMethod.getInstance());

                    break;


            }


        }

        private void manageHonorAndSkills(CourseItemListModel item) {
            mListIcon.setVisibility(View.VISIBLE);

            if (TextUtils.isEmpty(item.getHeading())) {
                mListHeading.setVisibility(View.GONE);
            } else {
                mListHeading.setVisibility(View.VISIBLE);

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    mListHeading.setText(Html.fromHtml(item.getHeading(), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    mListHeading.setText(Html.fromHtml(item.getHeading()));
                }
            }
            mListHeading.setMovementMethod(LinkMovementMethod.getInstance());


            if (TextUtils.isEmpty(item.getDescription())) {
                mDescription.setVisibility(View.GONE);
            } else {
                mDescription.setVisibility(View.VISIBLE);


                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    mDescription.setText(Html.fromHtml(item.getDescription(), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    mDescription.setText(Html.fromHtml(item.getDescription()));
                }
            }
            mDescription.setMovementMethod(LinkMovementMethod.getInstance());
        }


    }


}
