package com.imentors.wealthdragons.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 7/10/17.
 */
public class MenuExpandableListAdapter extends BaseExpandableListAdapter {

    @SuppressWarnings("CanBeFinal")
    private List<Category> mItems;
    @SuppressWarnings("CanBeFinal")
    private LayoutInflater mInflater;
    private Context mContext = null;
    private boolean isStaticData;
    private int mSizeOfOnlineData;
    private int mSizeOfStaticData;
    private static  String selectedItemName;
    private static  String selectedItemId;

    private static int mSelecetdGroupCount;
    private int mMessageCount;
    // required to make group expanded even in case of notify
    private List<Integer> mSelectedGroupPosition = new ArrayList<>();


    public MenuExpandableListAdapter(Context context) {
        mContext= context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        selectedItemName = null;
        mSelecetdGroupCount=-1;
    }




    //  set items
    public void setItems(List<Category> items , boolean is_static_data , int sizeOfStaticData , int messageCount){
        mItems = items;
        isStaticData = is_static_data;
        mSizeOfOnlineData = AppPreferences.getOnlineCategoryCount();
        mSizeOfStaticData = sizeOfStaticData;
        mMessageCount = messageCount;
    }





    //  clear data
    public void clearData(){
        if(mItems!=null) {
            mItems.clear();
            notifyDataSetChanged();
        }
    }

    public void selctedItemPosition(String itemName ,String itemId, int groupSelectedPosition){
        selectedItemName = itemName;
        selectedItemId=itemId;
        mSelecetdGroupCount = groupSelectedPosition;
        notifyDataSetChanged();
    }




    //  return count
    @Override
    public int getGroupCount() {
        if(mItems!=null){
            return mItems.size();
        }
        return 0;
    }

    //  return children count
    @Override
    public int getChildrenCount(int groupPosition) {

        if( mItems.get(groupPosition).getSub()!=null){
            return mItems.get(groupPosition).getSub().size();
        }

        return 0;
    }

    //  return get position
    @Override
    public Object getGroup(int groupPosition) {

        return mItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if( mItems.get(groupPosition).getSub()!=null){
            return mItems.get(groupPosition).getSub().get(childPosition);
        }

        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition,final boolean isExpanded, View convertView,final ViewGroup parent) {


        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_menu_item, parent, false);
            viewHolder = new ViewHolder();

            // Initialize different views
            viewHolder.nameView = convertView.findViewById(R.id.txt_name);
            viewHolder.nameView.setAllCaps(true);
            viewHolder.imageView = convertView.findViewById(R.id.imageView);
            viewHolder.ivExpandableImage = convertView.findViewById(R.id.iv_expand);
            viewHolder.mLlGroupLayout = convertView.findViewById(R.id.ll_menu_category);
            viewHolder.messageCount = convertView.findViewById(R.id.tv_message_count);

            // Set tag
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }





        // Set values to the list items
        final Category item = mItems.get(groupPosition);

        // static data is added in the list in string form thus converting back to int
        if(isStaticData) {
            //check if there is any item in static data
                viewHolder.nameView.setText(mContext.getString(Integer.parseInt(item.getTitle())));
                Glide.with(mContext).load(Integer.parseInt(item.getBanner())).into(viewHolder.imageView);

            if (mContext.getString(Integer.parseInt(item.getTitle())).equals(mContext.getString(R.string.message))) {
                if (mMessageCount > 0) {
                    viewHolder.messageCount.setVisibility(View.VISIBLE);
                    viewHolder.messageCount.setText(mMessageCount);
                } else {
                    viewHolder.messageCount.setVisibility(View.GONE);
                }
            } else {
                viewHolder.messageCount.setVisibility(View.GONE);
            }

        }else{

            if(groupPosition>=mSizeOfOnlineData){
                viewHolder.nameView.setText(mContext.getString(Integer.parseInt(item.getTitle())));
                Glide.with(mContext).load(Integer.parseInt(item.getBanner())).into(viewHolder.imageView);
                viewHolder.ivExpandableImage.setVisibility(View.INVISIBLE);

                if (mContext.getString(Integer.parseInt(item.getTitle())).equals(mContext.getString(R.string.messages))) {
                    if (mMessageCount > 0) {
                        viewHolder.messageCount.setVisibility(View.VISIBLE);
                        viewHolder.messageCount.setText(Integer.toString(mMessageCount));
                    } else {
                        viewHolder.messageCount.setVisibility(View.GONE);
                    }
                } else {
                    viewHolder.messageCount.setVisibility(View.GONE);
                }

            }else{
                viewHolder.nameView.setText(item.getTitle());
                Glide.with(mContext).load(item.getBanner()).into(viewHolder.imageView);
                viewHolder.messageCount.setVisibility(View.GONE);

            }

        }



        // check if group is expanded or not
        boolean isGroupExpanded = false;
        for(int i:mSelectedGroupPosition){
            if(groupPosition == i){
                isGroupExpanded=true;
            }
        }




        // show this options if it is not a static data
        if(!isStaticData) {
            if (item.getSub() != null) {
                if (item.getSub().size() > 0) {
                    viewHolder.ivExpandableImage.setVisibility(View.VISIBLE);
                    if (isGroupExpanded) {
                        Glide.with(mContext).load(R.drawable.ic_down_arrow).into(viewHolder.ivExpandableImage);
                    } else {
                        Glide.with(mContext).load(R.drawable.ic_right_arrow_menu).into(viewHolder.ivExpandableImage);
                    }
                } else {
                    viewHolder.ivExpandableImage.setVisibility(View.INVISIBLE);
                }
            } else {
                viewHolder.ivExpandableImage.setVisibility(View.INVISIBLE);
            }
        }

        if(!isGroupExpanded)
        {
            ((ExpandableListView) parent).collapseGroup(groupPosition);

        }
        else{
            ((ExpandableListView) parent).expandGroup(groupPosition, true);


        }

        final boolean finalIsGroupExpanded = isGroupExpanded;
        viewHolder.ivExpandableImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked<b> Expandable Icon</b> from " +mItems.get(groupPosition).getTitle());
                if(finalIsGroupExpanded)
                {
                    ((ExpandableListView) parent).collapseGroup(groupPosition);

                    int positionInList = -1;

                    for(int i=0; i<mSelectedGroupPosition.size(); i++){
                        if(mSelectedGroupPosition.get(i) == groupPosition){
                            positionInList = i;
                        }
                    }

                    if(positionInList > -1) {
                        mSelectedGroupPosition.remove(positionInList);
                    }

                }
                else{
                    ((ExpandableListView) parent).expandGroup(groupPosition, true);

                    boolean isPresent = false;

                    for(int i:mSelectedGroupPosition){
                        if(groupPosition == i){
                            isPresent=true;
                        }
                    }

                    if(!isPresent) {
                        mSelectedGroupPosition.add(groupPosition);
                    }
                }

            }
        });




        // to show selected item in list after refresh
        if(!TextUtils.isEmpty(item.getId())) {
            if (item.getId().equals(selectedItemId)) {
                viewHolder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.ivExpandableImage.setColorFilter(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.mLlGroupLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.btnBGcolorBlue));
            } else {
                viewHolder.nameView.setTextColor(ContextCompat.getColor(mContext, R.color.textColorblack));
                viewHolder.imageView.setColorFilter(ContextCompat.getColor(mContext, R.color.textColorblack));
                viewHolder.ivExpandableImage.setColorFilter(ContextCompat.getColor(mContext, R.color.textColorblack));
                viewHolder.mLlGroupLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            }
        }
            else
            {
                if(item.getTitle().equals(selectedItemName)){
                    viewHolder.nameView.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));
                    viewHolder.imageView.setColorFilter(ContextCompat.getColor(mContext,R.color.colorWhite));
                    viewHolder.ivExpandableImage.setColorFilter(ContextCompat.getColor(mContext,R.color.colorWhite));
                    viewHolder.mLlGroupLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.btnBGcolorBlue));
                }else{
                    viewHolder.nameView.setTextColor(ContextCompat.getColor(mContext,R.color.textColorblack));
                    viewHolder.imageView.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblack));
                    viewHolder.ivExpandableImage.setColorFilter(ContextCompat.getColor(mContext,R.color.textColorblack));
                    viewHolder.mLlGroupLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.colorWhite));
                }
            }


            if(groupPosition>=mSizeOfOnlineData){
            if(mContext.getString(Integer.parseInt(item.getTitle())).equals(mContext.getString(R.string.customer_support))){
                selectedItemName = null;
            }
            }

        return convertView;

        }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ViewHolderSubItem viewHolder;

        final Category.SubCategory childItem = (Category.SubCategory) getChild(groupPosition,childPosition);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.fragment_menu_sub_item, parent, false);
            viewHolder = new ViewHolderSubItem();
            viewHolder.mLlSubLayout = convertView.findViewById(R.id.ll_sub_item);
            viewHolder.tvSubItemName = convertView.findViewById(R.id.txt_sub_item_name);
            // Set tag
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderSubItem) convertView.getTag();
        }

        // Set values to the list items
        viewHolder.tvSubItemName.setText(childItem.getTitle());


//        if(!TextUtils.isEmpty(selectedItemName)){
//            if(viewHolder.tvSubItemName.getText().toString().equals(selectedItemName)){
//                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.textColorblue));
//                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));
//            }else{
//                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.backgroundGray));
//                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext,R.color.textColorblack));
//            }
//        }

        if(!TextUtils.isEmpty(childItem.getId())) {
            if (childItem.getId().equals(selectedItemId)) {
                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext, R.color.colorWhite));
                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.btnBGcolorBlue));
            } else {
                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext, R.color.textColorblack));
                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorWhite));
            }
        }
        else
        {
            if(viewHolder.tvSubItemName.getText().toString().equals(selectedItemName)){
                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.textColorblue));
                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext,R.color.colorWhite));
            }else{
                viewHolder.mLlSubLayout.setBackgroundColor(ContextCompat.getColor(mContext,R.color.backgroundGray));
                viewHolder.tvSubItemName.setTextColor(ContextCompat.getColor(mContext,R.color.textColorblack));
            }
        }

        return convertView;

    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    /**
     * View Holder class
     */
    private static class ViewHolder {
        TextView nameView;
        ImageView imageView;
        ImageView ivExpandableImage;
        LinearLayout mLlGroupLayout;
        TextView messageCount;

    }

    private static class ViewHolderSubItem {
        TextView tvSubItemName;
        LinearLayout mLlSubLayout;
    }


}
