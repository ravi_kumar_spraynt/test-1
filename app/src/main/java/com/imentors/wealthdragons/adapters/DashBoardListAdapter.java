package com.imentors.wealthdragons.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.ArticleView;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.EventView;
import com.imentors.wealthdragons.views.FooterViewHolder;
import com.imentors.wealthdragons.views.MentorView;
import com.imentors.wealthdragons.views.VideoView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 02-02-2016.
 */
public class DashBoardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //article list view
    private static final int TYPE_ARTICLE = 0;

    //mentor list view
    private static final int TYPE_MENTOR = 1;

    //video list view
    private static final int TYPE_VIDEO = 2;

    //event list view
    private static final int TYPE_EVENT = 3;


    //show the empty view if article type is event and event date is passed
    private static final int TYPE_FOOTER = 4;

    private static final int TYPE_EMPTY = 5;


    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener;

    //ref to the article items
    private List<Article> mItemsArticle;

    //ref to the Video items
    private List<Video> mItemsVideo;

    //ref to the Mentors items
    private List<Mentors> mItemsMentors;

    //ref to the Event items
    private List<Event> mItemsEvents;


    private String mTitle;
    private String  mHeaderImage;
    private String  mId;
    private String  mViewType;
    private String  mHeading;

    private Context mContext;

    private int mLimitForViewAll;

    private boolean mServerError;

    private int mItemsCountOnServer;
    private int  mNextPage ;
    private int  mCurrentSize;
    private int   mTotalItemsInList;


    private PagingListener mPagingListener;
    private boolean mIsProgrammeType;


    public DashBoardListAdapter(OnItemClickListener onItemClickListener, PagingListener pagingListener, Context context) {
        mOnItemClickListener = onItemClickListener;
        mPagingListener = pagingListener;
        mContext = context;

    }


    /**
     * set the article items
     *
     * @param itemsArticle article items
     * @param itemsMentors mentors items
     * @param itemsVideo   video items
     */
    public void setItems(final List<Article> itemsArticle, final List<Mentors> itemsMentors, final List<Video> itemsVideo, final List<Event> itemsEvent, final String ttile,final String heading, final String headerImage, int limitForViewAll, final int nextPage, final int itemsCountOnServer, final String id, final String viewType) {


        mNextPage = nextPage;
        mItemsCountOnServer = itemsCountOnServer;
        mId = id;
        mViewType = viewType;

        mIsProgrammeType = mViewType.equals(Constants.TYPE_PROGRAMMES);

        if (itemsArticle != null) {
            mItemsArticle = new ArrayList<>();
            mItemsArticle.addAll(itemsArticle);
            mCurrentSize = mItemsArticle.size();
            mTotalItemsInList = itemsArticle.size();

        }

        if (itemsMentors != null) {
            mItemsMentors = new ArrayList<>();
            mItemsMentors.addAll(itemsMentors);
            mCurrentSize = mItemsMentors.size();
            mTotalItemsInList = itemsMentors.size();

        }

        if (itemsVideo != null) {
            mItemsVideo = new ArrayList<>();
            mItemsVideo.addAll(itemsVideo);
            mCurrentSize = mItemsVideo.size();
            mTotalItemsInList = itemsVideo.size();

        }

        if (itemsEvent != null) {
            mItemsEvents = new ArrayList<>();
            mItemsEvents.addAll(itemsEvent);
            mCurrentSize = mItemsEvents.size();
            mTotalItemsInList = itemsEvent.size();

        }

        mTitle = ttile;
        mHeaderImage = headerImage;
        mHeading=heading;
        mLimitForViewAll = limitForViewAll;

        notifyDataSetChanged();
    }


    //  add items
    public void addItems(final List<Article> itemsArticle, final List<Mentors> itemsMentors, final List<Video> itemsVideo, final List<Event> itemsEvent, final int nextPage, final int itemsCountOnServer) {
        if (mItemsArticle != null) {
            mItemsArticle.addAll(itemsArticle);
            mCurrentSize = mItemsArticle.size();
            mTotalItemsInList+=itemsArticle.size();
        }


        if (mItemsMentors != null) {
            mItemsMentors.addAll(itemsMentors);
            mCurrentSize = mItemsMentors.size();
            mTotalItemsInList+=itemsMentors.size();

        }

        if (mItemsVideo != null) {
            mItemsVideo.addAll(itemsVideo);
            mCurrentSize = mItemsVideo.size();
            mTotalItemsInList+=itemsVideo.size();

        }

        if (mItemsEvents != null) {
            mItemsEvents.addAll(itemsEvent);
            mCurrentSize = mItemsEvents.size();
            mTotalItemsInList+=itemsEvent.size();

        }


        mNextPage = nextPage;
        mItemsCountOnServer = itemsCountOnServer;

        notifyDataSetChanged();

    }


    //  clear item
    public void clearData() {

        if (mItemsArticle != null) {
            mItemsArticle.clear();

        }

        if (mItemsMentors != null) {
            mItemsMentors.clear();

        }

        if (mItemsVideo != null) {
            mItemsVideo.clear();

        }

        if (mItemsEvents != null) {
            mItemsEvents.clear();


        }

        notifyDataSetChanged();

    }


    /**
     * get the article items
     *
     * @return article items
     */
    public List<Article> getArticleItems() {

        if (mItemsArticle != null) {
            return mItemsArticle;
        }

        return new ArrayList<>();
    }


    /**
     * get the article items
     *
     * @return article items
     */
    public List<Mentors> getMentorsItems() {

        if (mItemsMentors != null) {
            return mItemsMentors;
        }

        return new ArrayList<>();
    }


    /**
     * get the video items
     *
     * @return video items
     */
    public List<Video> getVideoItems() {

        if (mItemsVideo != null) {
            return mItemsVideo;
        }
        return new ArrayList<>();
    }


    /**
     * get the event items
     *
     * @return event items
     */
    public List<Event> getEventItems() {

        if (mItemsEvents != null) {
            return mItemsEvents;
        }
        return new ArrayList<>();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {

        View view;
        switch (viewType) {
            case TYPE_ARTICLE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_banner_layout, parent, false);
                return new ArticleViewHolder(view, mOnItemClickListener);

            case TYPE_VIDEO:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.video_list_item, parent, false);
                return new VideoViewHolder(view, mOnItemClickListener);


            case TYPE_EVENT:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.event_banner_layout, parent, false);
                return new EventViewHolder(view, mOnItemClickListener);

            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_footer, parent, false);
                return new FooterViewHolder(view);

            case TYPE_EMPTY:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_empty, parent, false);
                return new EmptyViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.mentor_list_item, parent, false);
                return new MentorViewHolder(view, mOnItemClickListener);
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ArticleViewHolder) {

            Article item = mItemsArticle.get(position);
            //cast holder to ItemViewHolder and set data for header.
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            viewHolder.bind(item, position, mItemsArticle.size(), mContext,mIsProgrammeType);

        } else if (holder instanceof VideoViewHolder) {

            Video item = mItemsVideo.get(position);
            //cast holder to ItemViewHolder and set data for header.
            VideoViewHolder viewHolder = (VideoViewHolder) holder;
            viewHolder.bind(item, position, mItemsVideo.size(), mContext);

        } else if (holder instanceof EventViewHolder) {
            Event item = mItemsEvents.get(position);
            //cast holder to ItemViewHolder and set data for header.
            EventViewHolder viewHolder = (EventViewHolder) holder;
            viewHolder.bind(item, position, mItemsEvents.size(), mContext);

        } else if (holder instanceof FooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        } else if (holder instanceof MentorViewHolder) {

            Mentors item = mItemsMentors.get(position);
            //cast holder to ItemViewHolder and set data for header.
            MentorViewHolder viewHolder = (MentorViewHolder) holder;
            viewHolder.bind(item, position, mItemsMentors.size(), mContext);


        }


    }

    @Override
    public int getItemViewType(final int position) {


        if (mItemsMentors != null) {

            if (position >= mItemsMentors.size()) {
                final int size = mItemsMentors.size();
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_MENTOR;
            }


        } else if (mItemsVideo != null) {

            if (position >= mItemsVideo.size()) {
                final int size = mItemsVideo.size();
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_VIDEO;
            }

        } else if (mItemsEvents != null) {
            if (position >= mItemsEvents.size()) {
                final int size = mItemsEvents.size();
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_EVENT;
            }

        } else {
            if (position >= mItemsArticle.size()) {
                final int size = mItemsArticle.size();
                if (size == 0) {
                    return TYPE_EMPTY;
                } else {
                    return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
                }
            } else {
                return TYPE_ARTICLE;
            }

        }


    }

    @Override
    public int getItemCount() {

        if (mItemsMentors != null) {

            if (!mItemsMentors.isEmpty()) {
                mOnItemClickListener.setHeader(mTitle,mHeading, mHeaderImage, mTotalItemsInList, mLimitForViewAll);
            } else {
                mOnItemClickListener.setHeader(null,null, null, mTotalItemsInList, mLimitForViewAll);

            }

            return mItemsMentors.size() + 1;
        }

        if (mItemsArticle != null) {

            if (!mItemsArticle.isEmpty()) {
                mOnItemClickListener.setHeader(mTitle,mHeading, mHeaderImage, mTotalItemsInList, mLimitForViewAll);
            } else {
                mOnItemClickListener.setHeader(null,null, null, mTotalItemsInList, mLimitForViewAll);
            }


            return mItemsArticle.size() + 1;

        }

        if (mItemsVideo != null) {

            if (!mItemsVideo.isEmpty()) {
                mOnItemClickListener.setHeader(mTitle,mHeading, mHeaderImage, mTotalItemsInList, mLimitForViewAll);
            } else {
                mOnItemClickListener.setHeader(null,null, null, mTotalItemsInList, mLimitForViewAll);
            }

            return mItemsVideo.size() + 1;
        }

        if (mItemsEvents != null) {

            if (!mItemsEvents.isEmpty()) {
                mOnItemClickListener.setHeader(mTitle,mHeading, mHeaderImage, mTotalItemsInList, mLimitForViewAll);
            } else {
                mOnItemClickListener.setHeader(null, null,null, mTotalItemsInList, mLimitForViewAll);
            }

            return mItemsEvents.size() + 1;
        }

        return 0;


    }

    public void setServerError() {

        mServerError = true;
        notifyDataSetChanged();
    }


    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onArticleClick(Article item, int position);

        void onMentorClick(Mentors item, int position);

        void onVideoClick(Video item, int position);

        void onEventClick(Event item, int position);

        void setHeader(String title,String heading, String imageHeader, int listSize, int viewAllVisibilityLimit);
    }

    /**
     * View holder class for items fo recycler view
     */
    private static class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final OnItemClickListener mOnItemClickListener;
        private final ArticleView mArticleBannerLayout;
        // current bind to view holder
        private Article mCurrentItem;
        //position
        private int mPosition;


        ArticleViewHolder(@NonNull View view, final OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);
            mArticleBannerLayout = view.findViewById(R.id.dashboard_article_banner);
        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onArticleClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Article item, final int position, int totalItems, Context mContext,boolean mViewType) {
            mCurrentItem = item;
            mPosition = position;

            if (totalItems - 1 == position && totalItems > 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int) mContext.getResources().getDimension(R.dimen.margin_default), 0, (int) mContext.getResources().getDimension(R.dimen.margin_default), 0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mArticleBannerLayout.bind(item, null, 0,mViewType,false);

        }
    }


    /**
     * View holder class for items fo recycler view
     */
    private static class MentorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final OnItemClickListener mOnItemClickListener;
        private final MentorView mMentorBannerLayout;
        // current bind to view holder
        private Mentors mCurrentItem;
        //position
        private int mPosition;


        MentorViewHolder(@NonNull View view, final OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mMentorBannerLayout = view.findViewById(R.id.dashboard_mentor_banner);

        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onMentorClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Mentors item, final int position, int totalItems, Context mContext) {
            mCurrentItem = item;
            mPosition = position;


            if (totalItems - 1 == position && totalItems > 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int) mContext.getResources().getDimension(R.dimen.margin_default), 0, (int) mContext.getResources().getDimension(R.dimen.margin_default), 0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mMentorBannerLayout.bind(item, null, 0,false);
        }
    }


    /**
     * View holder class for items fo recycler view
     */
    private static class VideoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final OnItemClickListener mOnItemClickListener;
        private final VideoView mVideoBannerLayout;
        // current bind to view holder
        private Video mCurrentItem;
        //position
        private int mPosition;


        VideoViewHolder(@NonNull View view, final OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mVideoBannerLayout = view.findViewById(R.id.dashboard_video_banner);

        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onVideoClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Video item, final int position, int totalItems, Context mContext) {
            mCurrentItem = item;
            mPosition = position;

            if (totalItems - 1 == position && totalItems > 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int) mContext.getResources().getDimension(R.dimen.margin_default), 0, (int) mContext.getResources().getDimension(R.dimen.margin_default), 0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }
            mVideoBannerLayout.bind(item, null, 0);
        }
    }

    /**
     * View holder class for items fo recycler view
     */
    private static class EventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final OnItemClickListener mOnItemClickListener;
        private final EventView mEventBannerLayout;
        // current bind to view holder
        private Event mCurrentItem;
        //position
        private int mPosition;


        EventViewHolder(@NonNull View view, final OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);

            mEventBannerLayout = view.findViewById(R.id.dashboard_event_banner);

        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onEventClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Event item, final int position, int totalItems, Context mContext) {
            mCurrentItem = item;
            mPosition = position;

            if (totalItems - 1 == position && totalItems > 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int) mContext.getResources().getDimension(R.dimen.margin_default), 0, (int) mContext.getResources().getDimension(R.dimen.margin_default), 0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mEventBannerLayout.bind(item, null);
        }
    }


    private void showFooterViewHolder(int position, FooterViewHolder viewHolder) {
            //check whether to need to call next page from the server
            if (mCurrentSize != 0 && mPagingListener != null && mNextPage != 0 && position >= mCurrentSize && mCurrentSize < mItemsCountOnServer) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError(0);

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");
                                }

                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading(0);

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, mId, mViewType, null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError(0);

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }
        }


    }


}
