package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DigitalCoachingDetails;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;

public class DigitalContetnItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<DigitalCoachingDetails.Content> mNewItemList;
    private Context mContext;


    public DigitalContetnItemsAdapter(List<DigitalCoachingDetails.Content> newItemList, Context context) {
        this.mNewItemList = newItemList;
        mContext = context;
    }






        @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view;

            view = LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_digital_coaching, parent, false);
            return new MyViewHolder(view);


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;
            viewHolder.bind(mNewItemList.get(position),mContext);

    }




    @Override
    public int getItemCount() {
        return mNewItemList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{

        private WealthDragonTextView mTvHeading;
        private ImageView mHeaderImage;
        private WealthDragonTextView mTvDiscription;


        //  initialize view
        public MyViewHolder(View view ) {
            super(view);
            mTvHeading =  view.findViewById(R.id.tv_heading);
            mTvDiscription =  view.findViewById(R.id.tv_description);
            mHeaderImage = view.findViewById(R.id.iv_image_header);


        }



        // Set values to the list items
        void bind(DigitalCoachingDetails.Content item, Context context){
            Glide.with(context).load(item.getIcon()).dontAnimate().error(R.drawable.no_img_mob).into(mHeaderImage);
            mTvHeading.setText(item.getHeading());
            mTvDiscription.setText(item.getDescription());
            mHeaderImage.setColorFilter(ContextCompat.getColor(context, R.color.btnBGcolorOrange), android.graphics.PorterDuff.Mode.MULTIPLY);


        }


    }


}
