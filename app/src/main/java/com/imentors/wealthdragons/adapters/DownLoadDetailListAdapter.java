package com.imentors.wealthdragons.adapters;


import android.app.Activity;
import android.content.Context;
import android.net.Uri;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.listeners.DownloadActionListener;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;

import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Status;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownLoadDetailListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //    private List<Feeds.Feed> mFeeds;
    private Context mContext;
    private List<Download> mListItem = new ArrayList<>();
    private DownloadActionListener mOnItemClicked;



    public DownLoadDetailListAdapter(Context context, List<Download> listData, DownloadActionListener onListItemClicked) {

        mContext = context;
        mOnItemClicked = onListItemClicked;
        mListItem = listData;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.download_list_item, parent, false);

        return new MyViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final MyViewHolder viewHolder = (MyViewHolder) holder;

        viewHolder.bind(mListItem.get(position), position, mContext);

        viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position<mListItem.size()) {
                    mOnItemClicked.onDownloadItemClicked(mListItem,position);
                }
            }
        });

        viewHolder.mStatusDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position<mListItem.size()) {
                    if(viewHolder.mIvRetry.getVisibility()== View.VISIBLE){
                        mOnItemClicked.onRetryDownload(mListItem.get(position).getId());
                    }
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return mListItem.size();
    }

    //   * View holder class for items fo recycler view

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle, mMentorName, mTvPending, mTvProgress,mTvDuration;
        private ImageView mIvBannerImage, mIvRetry;
        private int mPosition;
        private ProgressBar mProgressBar;
        private CardView mCardView;
        public LinearLayout mViewForeground,mStatusDownload;
        public RelativeLayout mViewBackground;


        //  initialize data
        public MyViewHolder(View view, Context context) {
            super(view);
            mCardView = view.findViewById(R.id.card_view);
            mTitle = view.findViewById(R.id.tv_title);
            mMentorName = view.findViewById(R.id.tv_mentor_name);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mIvBannerImage.getLayoutParams().width = (int) Utils.getDownloadImageWidth((Activity) context);
            mIvBannerImage.getLayoutParams().height = (int) Utils.getDownloadImageHeight((Activity) context);
            mProgressBar = view.findViewById(R.id.progress_bar);
            mTvPending = view.findViewById(R.id.tv_pending);
            mIvRetry = view.findViewById(R.id.ic_reload);
            mViewBackground = view.findViewById(R.id.view_background);
            mViewForeground = view.findViewById(R.id.ll_payment_contatiner);
            mTvProgress = view.findViewById(R.id.tv_progress);
            mTvDuration=view.findViewById(R.id.tv_video_duration);
            mStatusDownload = view.findViewById(R.id.ll_downlod_status);

        }


        // Set values to the list items
        void bind(final Download item, final int position, Context context) {
            mPosition = position;


            Uri uri = Uri.fromFile(new File(item.getExtras().getString(Constants.BANNER_PATH, "image")));
            String duration = item.getExtras().getString(Constants.VIDEO_DURATION,"0");


            Glide.with(context).load(uri).asBitmap().centerCrop().dontAnimate().error(R.drawable.blackwings).into(mIvBannerImage);
            String title = item.getExtras().getString(Constants.TITLE,"Course");
            String id = item.getExtras().getString(Constants.ID,"0");

            if(title.equals("Course")){
                mTitle.setText(item.getExtras().getString(Constants.MENTOR_NAME, "Course"));
                if(id.contains("Digital")){

                    mMentorName.setVisibility(View.VISIBLE);
                    mMentorName.setText("Digital Intro Video");

                }else{
                    mMentorName.setVisibility(View.GONE);

                }
            }else{
                mTitle.setText(title);
                mMentorName.setVisibility(View.VISIBLE);
                mMentorName.setText(item.getExtras().getString(Constants.MENTOR_NAME, "Mentor"));

            }



            if (!duration.equals("0"))
            {
                mTvDuration.setVisibility(View.VISIBLE);
                mTvDuration.setText(item.getExtras().getString(Constants.VIDEO_DURATION,"0"));

            }
            else
            {
                mTvDuration.setVisibility(View.GONE);

            }



            if (item.getStatus() == Status.QUEUED) {
                mTvPending.setVisibility(View.VISIBLE);
                mTvPending.setText("Waiting for download");
                mProgressBar.setVisibility(View.GONE);
                mTvProgress.setVisibility(View.GONE);

                mIvRetry.setVisibility(View.GONE);

            } else if (item.getStatus() == Status.FAILED) {
                mTvPending.setVisibility(View.VISIBLE);
                mTvPending.setText("Restart Download");
                mProgressBar.setVisibility(View.GONE);
                mTvProgress.setVisibility(View.GONE);

                mIvRetry.setVisibility(View.VISIBLE);


            } else if (item.getStatus() == Status.COMPLETED) {
                mTvPending.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);
                mTvProgress.setVisibility(View.GONE);

                mIvRetry.setVisibility(View.GONE);


            } else {


                mProgressBar.setVisibility(View.VISIBLE);
                mTvProgress.setVisibility(View.VISIBLE);

                mProgressBar.setProgress(item.getProgress());
                mTvProgress.setText(item.getProgress() +"%");
                mTvPending.setVisibility(View.GONE);
                mIvRetry.setVisibility(View.GONE);

            }


//            mDate.setText(item.getCreated());

        }

    }


    public void removeSwippedItem(int position){

        mOnItemClicked.onRemoveDownload(mListItem.get(position).getId());

    }


    public void updateProgress(Download progress) {

        for (int i = 0; i < mListItem.size(); i++) {

            Download download = mListItem.get(i);
            if (progress.getFile().equals(download.getFile())) {

                switch (progress.getStatus()) {
                    case REMOVED:
                        mListItem.remove(i);
                        notifyItemRemoved(i);
                        if(mListItem.size()<=0){
                            mOnItemClicked.allItemsRemoved();
                        }
                        break;
                    case DELETED: {
                        mListItem.remove(i);
                        notifyItemRemoved(i);
                        if(mListItem.size()<=0){
                            mOnItemClicked.allItemsRemoved();
                        }
                        break;
                    }
                    default: {

                        mListItem.set(i, progress);
                        notifyItemChanged(i);
                    }
                }
                return;

            }

        }


    }


}
