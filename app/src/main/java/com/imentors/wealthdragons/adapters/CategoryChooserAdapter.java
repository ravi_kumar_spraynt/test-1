package com.imentors.wealthdragons.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.UserCategory;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.HashMap;
import java.util.List;

public class CategoryChooserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UserCategory.category> mUserCategory;
    private Context mContext;
    private OnListItemClicked mOnItemClicked;
    private int mTotalItem = 6;
    private HashMap<String, Boolean> map;


    public CategoryChooserAdapter(Context context, List<UserCategory.category> userCategory, OnListItemClicked onListItemClicked, int selectedItemCount) {

        mContext = context;
        mOnItemClicked = onListItemClicked;
        mUserCategory = userCategory;
        map = new HashMap<>();
        mTotalItem = selectedItemCount;

        for (int i = 0; i < mUserCategory.size(); i++) {
            map.put(mUserCategory.get(i).getId(), true);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_item_chooser, parent, false);

        return new MyViewHolder(itemView, mContext);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        final MyViewHolder viewHolder = (MyViewHolder) holder;
//
        viewHolder.bind(mUserCategory.get(position), mContext);


        viewHolder.mBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserCategory.category userCategory = mUserCategory.get(position);
                String id = userCategory.getId();

                Boolean b = map.get(id);

                if (Boolean.TRUE.equals(b)) {
                    --mTotalItem;
                    viewHolder.mivHoverImage.setVisibility(View.VISIBLE);
                    viewHolder.mIvBannerImage.setVisibility(View.VISIBLE);
                    viewHolder.mHoverTitle.setVisibility(View.VISIBLE);
                    viewHolder.mTitle.setVisibility(View.GONE);
                    Utils.callEventLogApi("selected " + "<b>" + userCategory.getTitle() + " </b>" + "category from category screen");
                    map.put(id, false);

                } else {
                    ++mTotalItem;
                    viewHolder.mivHoverImage.setVisibility(View.INVISIBLE);
                    viewHolder.mIvBannerImage.setVisibility(View.VISIBLE);
                    viewHolder.mHoverTitle.setVisibility(View.INVISIBLE);
                    viewHolder.mTitle.setVisibility(View.VISIBLE);
                    map.put(id, true);
                    Utils.callEventLogApi("unselected " + "<b>" + userCategory.getTitle() + " </b>" + "category from category screen");


                }
                mOnItemClicked.onItemClicked(userCategory, position, mTotalItem, map);


            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserCategory.size();
    }

    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnListItemClicked {
        void onItemClicked(UserCategory.category item, int postion, int totalItem, HashMap<String, Boolean> selectedItemList);
    }

    //   * View holder class for items fo recycler view

    private static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView mIvBannerImage;
        private ImageView mivHoverImage;
        private WealthDragonTextView mTitle;
        private WealthDragonTextView mHoverTitle;

        private FrameLayout mBannerImage;


        //  initialize data
        public MyViewHolder(View view, Context context) {
            super(view);
            mTitle = view.findViewById(R.id.tv_category_main_heading);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mivHoverImage = view.findViewById(R.id.iv_hover_image);
            mBannerImage = view.findViewById(R.id.ll_bannerImage);
            mHoverTitle = view.findViewById(R.id.tv_hover_text);
            mIvBannerImage.getLayoutParams().height = (int) Utils.getFeedsImageHeight((Activity) context);

        }


        // Set values to the list items
        void bind(final UserCategory.category item, Context context) {
            Glide.with(context).load(item.getBanner_three()).into(mIvBannerImage);
            mTitle.setText(item.getTitle());
            mHoverTitle.setText(item.getTitle());


        }

    }


}
