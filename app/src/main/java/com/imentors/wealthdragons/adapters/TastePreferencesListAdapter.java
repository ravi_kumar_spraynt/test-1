package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.TasteClickListener;
import com.imentors.wealthdragons.models.Category;

import java.util.List;

public class TastePreferencesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<Category.SubCategory> mSubCategoryList;
    private Context mContext;
    public static final String NO_TASTE = "NoSet";
    public static final String TASTE_OFTEN = "often";
    public static final String TASTE_NEVER = "never";
    private TasteClickListener mOnTasteClickListener;
    private int mGroupPosition;

    //  constructor
    public TastePreferencesListAdapter(List<Category.SubCategory> categoryList , Context context , TasteClickListener onTasteClickListener, int groupPosition) {
        this.mSubCategoryList = categoryList;
        this.mContext = context;
        this.mOnTasteClickListener = onTasteClickListener;
        mGroupPosition = groupPosition;
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.taste_preferences_sub_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    //  set data in view
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final Category.SubCategory item = mSubCategoryList.get(position);
        final MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(item,mContext,position);



        viewHolder.ivNever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnTasteClickListener.onTasteClick(item.getId(),TASTE_NEVER,mGroupPosition,position);
                viewHolder.ivOften.setImageResource(R.drawable.radio_check_inactive);
                viewHolder.ivNever.setImageResource(R.drawable.radio_check_active);

            }
        });

        viewHolder.ivOften.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnTasteClickListener.onTasteClick(item.getId(),TASTE_OFTEN,mGroupPosition,position);
                viewHolder.ivNever.setImageResource(R.drawable.radio_check_inactive);
                viewHolder.ivOften.setImageResource(R.drawable.radio_check_active);
            }
        });


    }


    //  counting item size
    @Override
    public int getItemCount() {
        return mSubCategoryList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
         TextView tvTasteName ;
         ImageView ivNever , ivOften;
         LinearLayout mLinearLayoutSub;


         //  initialize view
        public MyViewHolder(View view ) {
            super(view);
            tvTasteName =  view.findViewById(R.id.tv_taste);
            ivNever =  view.findViewById(R.id.iv_taste_never);
            ivOften =  view.findViewById(R.id.iv_taste_often);
            mLinearLayoutSub = view.findViewById(R.id.ll_date_container);
        }



        // Set values to the list items
        void bind(final Category.SubCategory item, Context context ,int position){
            tvTasteName.setText("-- "+item.getTitle());
            switch (item.getTaste()){
                case NO_TASTE:
                    ivNever.setImageResource(R.drawable.radio_check_active);
                    ivOften.setImageResource(R.drawable.radio_check_inactive);
                    break;
                case TASTE_OFTEN:
                    ivNever.setImageResource(R.drawable.radio_check_inactive);
                    ivOften.setImageResource(R.drawable.radio_check_active);
                    break;
                default:
                    ivNever.setImageResource(R.drawable.radio_check_active);
                    ivOften.setImageResource(R.drawable.radio_check_inactive);
                    break;

            }

            if(position%2==0){
                mLinearLayoutSub.setBackgroundColor(ContextCompat.getColor(context,R.color.backgroundGray));
            }else{
                mLinearLayoutSub.setBackgroundColor(ContextCompat.getColor(context,R.color.colorWhite));
            }

        }

    }
}
