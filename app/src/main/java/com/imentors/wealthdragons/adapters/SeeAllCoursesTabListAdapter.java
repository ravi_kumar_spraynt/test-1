package com.imentors.wealthdragons.adapters;

import android.app.Activity;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.ArticleView;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.FooterViewHolder;

import java.util.ArrayList;
import java.util.List;


/**
 * Created on 02-02-2016.
 */
public class SeeAllCoursesTabListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //article list view
    private static final int TYPE_ARTICLE = 0;

    //Type footer and empty
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_EMPTY = 2;



    //ref to the item click listener
    private final OnItemClickListener mOnItemClickListener;


    //ref to the Mentors items
    private List<Article> mItemsSeeAllCourses;


    private String mTasteId , mSeeAllApiKey;

    private Context mContext;

    private int mTabPosition,mItemsCountOnServer, mNextPage;

    private boolean isTablet , mServerError ,mIsSubCategory = false;
    private PagingListener mPagingListener;
    private boolean mIsProgrammeType;


    public SeeAllCoursesTabListAdapter(OnItemClickListener onItemClickListener, PagingListener pagingListener, Context context ) {
        mOnItemClickListener = onItemClickListener;
        mContext = context;
        mPagingListener = pagingListener;
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

    }


    /**
     * set the article items
     *
     * @param itemsArticle               article items

     */
    public void setItems(final List<Article> itemsArticle ,  int tabPosition, final int nextPage, final int itemsCountOnServer  , final String tasteId , final String seeAllApiKey, boolean isProgrammeType) {


        if(itemsArticle != null){
            mItemsSeeAllCourses = new ArrayList<>();
            mItemsSeeAllCourses.addAll(itemsArticle);
        }



        mTabPosition = tabPosition;
        mIsProgrammeType = isProgrammeType;
        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;
        mSeeAllApiKey = seeAllApiKey;
        notifyDataSetChanged();
    }



    //  add items
    public void addItems(final List<Article> itemsArticle , final int nextPage, final int itemsCountOnServer  , final String tasteId, final String seeAllApiKey){
        if(mItemsSeeAllCourses!=null){
            mItemsSeeAllCourses.addAll(itemsArticle);
        }

        mIsSubCategory = false;

        mNextPage = nextPage;
        mTasteId = tasteId;
        mItemsCountOnServer = itemsCountOnServer;
        mSeeAllApiKey = seeAllApiKey;

        notifyDataSetChanged();

    }


    //  set server error
    public void setServerError(){
        mServerError = true;
        notifyDataSetChanged();
    }


    //  clear data
    public void clearData(int position){

        if(position == Constants.SEE_ALL_COURSES_TAB) {

            if (mItemsSeeAllCourses != null) {
                mItemsSeeAllCourses.clear();
            }
        }


        notifyDataSetChanged();

    }


    public void isSubCategoryFilterData(){
        mIsSubCategory = true;
    }

    /**
     * get the article items
     *
     * @return article items
     */
    public List<Article> getArticleItems() {

        if(mItemsSeeAllCourses !=null){
            return mItemsSeeAllCourses;
        }

        return  null;
    }





    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, final int viewType) {



        View view;
        switch (viewType) {
            case TYPE_ARTICLE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.article_banner_layout, parent, false);
                return new ArticleViewHolder(view, mOnItemClickListener);
            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_footer, parent, false);
                return new FooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.fragment_article_list_item_empty, parent, false);
                return new EmptyViewHolder(view);
        }




    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        if (holder instanceof SeeAllCoursesTabListAdapter.ArticleViewHolder) {
            Article item = mItemsSeeAllCourses.get(position);
            //cast holder to ItemViewHolder and set data for header.
            ArticleViewHolder viewHolder = (ArticleViewHolder) holder;
            viewHolder.bind(item, position, mItemsSeeAllCourses.size(),mContext,mIsProgrammeType);

        }else if (holder instanceof FooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        }


    }


    @Override
    public int getItemViewType(int position) {
        if (position >= mItemsSeeAllCourses.size()) {
            final int size = mItemsSeeAllCourses.size();
            if (size == 0) {
                if(mIsSubCategory){
                    return TYPE_FOOTER;

                }
                return TYPE_EMPTY;
            } else {
                return (size >= mItemsCountOnServer) ? TYPE_EMPTY : TYPE_FOOTER;
            }
        } else {
            return TYPE_ARTICLE;
        }
    }

    @Override
    public int getItemCount() {

        if(mItemsSeeAllCourses !=null){

            // plus one for footer
          return   mItemsSeeAllCourses.size()+1;
        }

        return 0;

    }


    /**
     * different click handled by onItemClick , onNetworkClick and onBookmarkClick
     */
    public interface OnItemClickListener {
        void onArticleClick(Article item, int position);
        void setHeader(String title, String imageHeader, int listSize, int tabPosition);

    }



    /**
     * View holder class for items fo recycler view
     */
    private static class ArticleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        //view on click listener need to forward click events
        private final SeeAllCoursesTabListAdapter.OnItemClickListener mOnItemClickListener;
        private final ArticleView mArticleBannerLayout;
        // current bind to view holder
        private Article mCurrentItem;
        //position
        private int mPosition;


        ArticleViewHolder(@NonNull View view, final SeeAllCoursesTabListAdapter.OnItemClickListener listener) {
            super(view);
            mOnItemClickListener = listener;
            view.setOnClickListener(this);
            mArticleBannerLayout = view.findViewById(R.id.dashboard_article_banner);
        }

        @Override
        public void onClick(View view) {
            if (mCurrentItem != null && mOnItemClickListener != null) {
                mOnItemClickListener.onArticleClick(mCurrentItem, mPosition);
            }
        }

        /**
         * Bind the the values of the view holder
         *
         * @param item     article item
         * @param position position
         */
        void bind(final Article item, final int position , int totalItems , Context mContext,boolean isProgrammeType ) {
            mCurrentItem = item;
            mPosition = position;

            if(totalItems-1 == position && totalItems>1){
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins((int)mContext.getResources().getDimension(R.dimen.margin_default),0,(int)mContext.getResources().getDimension(R.dimen.margin_default),0);
                itemView.findViewById(R.id.card_view).setLayoutParams(params);

            }

            mArticleBannerLayout.bind(item, null,Constants.COURSE_TAB,isProgrammeType,false);

        }
    }




    private void showFooterViewHolder(int position, FooterViewHolder viewHolder) {
      if(mItemsSeeAllCourses !=null){
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mItemsSeeAllCourses.size() && mItemsSeeAllCourses.size() < mItemsCountOnServer && !mIsSubCategory) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError(0);

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagination Retry</b>");

                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading(0);

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_COURSES,mSeeAllApiKey);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError(0);

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagination Retry</b>");

                            }
                        }
                    });
                }

            }else if(mPagingListener != null && mIsSubCategory){
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if(mServerError){

                        viewHolder.showServerError((int)Utils.getCourseImageHeight((Activity)mContext));

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_COURSES,mSeeAllApiKey);
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagination Retry</b>");


                                }
                            }
                        });

                    }else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading((int)Utils.getCourseImageHeight((Activity)mContext));
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError((int)Utils.getCourseImageHeight((Activity)mContext));

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                mPagingListener.onGetItemFromApi(mNextPage, mTasteId, Constants.TYPE_SEE_ALL_COURSES,mSeeAllApiKey);
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagination Retry</b>");

                            }
                        }
                    });
                }
            }
        }
    }



}
