package com.imentors.wealthdragons.adapters;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.listeners.DownloadActionListener;
import com.imentors.wealthdragons.models.GroupDownloadFile;
import com.imentors.wealthdragons.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DownLoadDetailGroupListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //    private List<Feeds.Feed> mFeeds;
    private Context mContext;
    private List<GroupDownloadFile> mListItem = new ArrayList<>();
    private DownloadActionListener mOnItemClicked;


    public DownLoadDetailGroupListAdapter(Context context, List<GroupDownloadFile> listData, DownloadActionListener onListItemClicked) {

        mContext = context;
        mOnItemClicked = onListItemClicked;
        mListItem = listData;
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grop_download_list_item, parent, false);

        return new MyViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        MyViewHolder viewHolder = (MyViewHolder) holder;

        viewHolder.bind(mListItem.get(position), position, mContext);

        viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position<mListItem.size()) {
                    mOnItemClicked.onGroupItemClicked(mListItem.get(position));
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return mListItem.size();
    }

    //   * View holder class for items fo recycler view

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle, mMentorName, mTvPending, mTvProgress,mTvDuration;
        private ImageView mIvBannerImage, mIvRetry;
        private int mPosition;
        private ProgressBar mProgressBar;
        private CardView mCardView;
        public LinearLayout mViewForeground;
        public RelativeLayout mViewBackground;


        //  initialize data
        public MyViewHolder(View view, Context context) {
            super(view);
            mCardView = view.findViewById(R.id.card_view);
            mTitle = view.findViewById(R.id.tv_title);
            mMentorName = view.findViewById(R.id.tv_mentor_name);
            mIvBannerImage = view.findViewById(R.id.iv_banner_image);
            mIvBannerImage.getLayoutParams().width = (int) Utils.getDownloadImageWidth((Activity) context);
            mIvBannerImage.getLayoutParams().height = (int) Utils.getDownloadImageHeight((Activity) context);
            mProgressBar = view.findViewById(R.id.progress_bar);
            mTvPending = view.findViewById(R.id.tv_pending);
            mIvRetry = view.findViewById(R.id.ic_reload);
            mViewBackground = view.findViewById(R.id.view_background);
            mViewForeground = view.findViewById(R.id.ll_payment_contatiner);
            mTvProgress = view.findViewById(R.id.tv_progress);
            mTvDuration=view.findViewById(R.id.tv_video_duration);

        }


        // Set values to the list items
        void bind(final GroupDownloadFile item, final int position, Context context) {
            mPosition = position;


            Uri uri = Uri.fromFile(new File(item.getmGroupBannerAddress()));

            Glide.with(context)
                    .load(uri)
                    .asBitmap()
                    .centerCrop()
                    .dontAnimate()
                    .error(R.drawable.blackwings)
                    .listener(new RequestListener<Uri, Bitmap>() {
                        @Override
                        public boolean onException(Exception e, Uri model, Target<Bitmap> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Uri model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(mIvBannerImage)
            ;
            mTitle.setText(item.getmGroupTitle());
            if(item.getmTotalItem()>1){
                mMentorName.setText(item.getmTotalItem() +" Videos");

            }else{
                mMentorName.setText(item.getmTotalItem() +" Video");

            }
        }

    }


    public void removeSwippedItem(int position){

        mOnItemClicked.onRemoveDownload(Integer.valueOf(mListItem.get(position).getmId()));
        mListItem.remove(position);
        notifyItemRemoved(position);
        if(mListItem.size()<=0){
            mOnItemClicked.allItemsRemoved();
        }
    }




}
