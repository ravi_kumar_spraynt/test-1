package com.imentors.wealthdragons.adapters;


import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Utils;

import java.util.List;

public class ExistingSupportReplyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<SupportType.ExistingSupportMessage> mSupportTypeList;
    private OnListClicked mOnListClicked;

    public ExistingSupportReplyListAdapter(List<SupportType.ExistingSupportMessage> supportTypeList,OnListClicked OnListClicked) {
        this.mSupportTypeList = supportTypeList;
        this.mOnListClicked=OnListClicked;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.existing_support_reply_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mSupportTypeList.get(position));
        viewHolder.tvAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>View Attachment</b> of  <b>" +mSupportTypeList.get(position).getContact_message()+"</b> message");
                mOnListClicked.OnListClicked(mSupportTypeList.get(position));
            }
        });

    }

    /**
     * different click handled by onItemClick
     */    //  interface
    public interface OnListClicked{

        void OnListClicked(SupportType.ExistingSupportMessage item);

    }


    //  add new items
    public void addNewItem(SupportType.ExistingSupportMessage item){
        mSupportTypeList.add(item);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mSupportTypeList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView tvUserName ;
        public TextView  tvMessage;
        public TextView tvDate;

        public FrameLayout tvAttachment;

        public MyViewHolder(View view ) {
            super(view);
            tvUserName =  view.findViewById(R.id.tv_user);
            tvMessage = view.findViewById(R.id.tv_message);
            tvAttachment=view.findViewById(R.id.view_attach);
            tvDate = view.findViewById(R.id.tv_date);

        }



        // Set values to the list items
        void bind(final SupportType.ExistingSupportMessage item){
            tvUserName.setText(item.getMsg_by());
            tvMessage.setText(item.getContact_message());
            if(TextUtils.isEmpty(item.getDate())){
                tvDate.setVisibility(View.GONE);
            }else{
                tvDate.setVisibility(View.VISIBLE);
                tvDate.setText(item.getDate());
            }

            if (!TextUtils.isEmpty(item.getAttachments()))
            {
                tvAttachment.setVisibility(View.VISIBLE);

            }
            else
            {
                tvAttachment.setVisibility(View.GONE);
            }


        }


    }
}
