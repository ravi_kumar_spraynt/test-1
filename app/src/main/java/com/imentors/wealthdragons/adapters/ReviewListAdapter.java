package com.imentors.wealthdragons.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Review;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.EmptyViewHolder;
import com.imentors.wealthdragons.views.ReviewFooterViewHolder;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List<Review> mReviewList;
    private Context mContext;
    private int mNextPage;
    private int mTotalItems;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_REVIEW = 0;
    private static final int TYPE_EMPTY = 2;

    private PagingListener mPagingListener;
    private boolean  mServerError;


    public ReviewListAdapter( PagingListener pagingListener, Context context) {
        mContext = context;
        mPagingListener = pagingListener;
    }

    //  set items
    public void setItems(List<Review> reviewList , int nextPage, int totalItems) {
        this.mReviewList = reviewList;
        mNextPage = nextPage;
        mTotalItems=totalItems;
        notifyDataSetChanged();
    }

    public void addItems(List<Review> reviewList , int nextPage , int totalItems){

        mReviewList.addAll(reviewList);
        mNextPage = nextPage;
        mTotalItems=totalItems;
        notifyDataSetChanged();

    }

    //  set server error
    public void setServerError() {
        mServerError = true;
        notifyDataSetChanged();
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {



        View view;
        switch (viewType) {
            case TYPE_REVIEW:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.reviews_list_item, parent, false);
                return new MyViewHolder(view,mContext);
            case TYPE_FOOTER:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_item, parent, false);
                return new ReviewFooterViewHolder(view);

            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.review_footer_list_item_empty, parent, false);
                return new EmptyViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ReviewListAdapter.MyViewHolder) {
            Review item = mReviewList.get(position);
            //cast holder to ItemViewHolder and set data for header.
            ReviewListAdapter.MyViewHolder viewHolder = (ReviewListAdapter.MyViewHolder) holder;
            viewHolder.bind(item, position);

            if(position == mTotalItems-1){
                viewHolder.mUnderLineView.setVisibility(View.GONE);
            }

        } else if (holder instanceof ReviewFooterViewHolder) {
            //cast holder to FooterViewHolder and set data for header.
            ReviewFooterViewHolder viewHolder = (ReviewFooterViewHolder) holder;
            showFooterViewHolder(position, viewHolder);

        }




    }


    @Override
    public int getItemViewType(int position) {
        if (position >= mReviewList.size()) {
            final int size = mReviewList.size();
            if (size == 0) {
                return TYPE_EMPTY;
            } else {
                return (size >= mTotalItems) ? TYPE_EMPTY : TYPE_FOOTER;
            }
        } else {
            return TYPE_REVIEW;
        }
    }



    @Override
    public int getItemCount() {
        return mReviewList.size()+1;
    }


    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle , tvReviewerAgo , tvReview , tvReviewerName;
        private Context mContext;
        private CircleImageView mReviewerImage;
        private RatingBar mReviewRating;
        public View mUnderLineView;

        //  initialize view
        public MyViewHolder(View view  , Context context) {
            super(view);
            tvTitle =  view.findViewById(R.id.tv_reviewer_heading);
            tvReviewerAgo = view.findViewById(R.id.tv_review_ago);
            tvReviewerName = view.findViewById(R.id.tv_reviewer_name);
            tvReview = view.findViewById(R.id.tv_reviewer_review);
            mReviewerImage = view.findViewById(R.id.iv_reviewer_image);
            mReviewRating = view.findViewById(R.id.reviews_rating);
            mUnderLineView = view.findViewById(R.id.view_underline);
            mContext = context;
        }

        // Set values to the list items
        void bind(final Review item, final int position){
            tvTitle.setText(item.getHeading());
            tvReviewerAgo.setText(item.getAgo());
            tvReview.setText(item.getReview());
            tvReviewerName.setText("by " + item.getUser_name());
            Glide.with(mContext).load(item.getUser_banner()).into(mReviewerImage);
            mReviewRating.setRating(Float.parseFloat(item.getRating()));


        }


    }


    private void showFooterViewHolder(int position, ReviewFooterViewHolder viewHolder) {
        if (mReviewList != null) {
            //check whether to need to call next page from the server
            if (mPagingListener != null && mNextPage != 0 && position >= mReviewList.size() && mReviewList.size() < mTotalItems) {
                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {

                    // if there is server error
                    if (mServerError) {

                        viewHolder.showServerError( );

                        //set the click listener to reload adapter on user click
                        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                    // set it to false again
                                    mServerError = false;
                                    notifyDataSetChanged();
                                    Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                                }
                            }
                        });

                    } else {

                        //remove the click event if loading items
                        viewHolder.itemView.setOnClickListener(null);
                        //show loading indicator
                        viewHolder.showLoading();

                        //fetch the new page from the server
                        mPagingListener.onGetItemFromApi(mNextPage, null, Constants.TYPE_REVIEW,null);
                    }
                } else {
                    //show the no internet error
                    viewHolder.showNoInternetError();

                    //set the click listener to reload adapter on user click
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isNetworkAvailable(WealthDragonsOnlineApplication.sharedInstance())) {
                                notifyDataSetChanged();
                                Utils.callEventLogApi("clicked<b> Pagentaion Retry</b>");

                            }
                        }
                    });
                }


            }
        }


    }



}
