package com.imentors.wealthdragons.adapters;



import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.SupportType;

import java.util.ArrayList;
import java.util.List;

public class ServiceTypeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private OnSericeClicked mOnItemClickListener;
    private ArrayList<SupportType> mServiceTypeList = new ArrayList<>();

    public ServiceTypeListAdapter(List<SupportType> serviceTypeList, OnSericeClicked onItemClickListener) {
        mServiceTypeList.clear();
        mOnItemClickListener = onItemClickListener;
        mServiceTypeList.addAll(serviceTypeList);
    }



    /**
     * different click handled by onItemClick
     */
    public interface OnSericeClicked {
        void onServiceClicked(SupportType item);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.dialog_fragment_country_code_list_item, parent, false);

        return new MyViewHolder(itemView,mOnItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mServiceTypeList.get(position),position);
    }


    @Override
    public int getItemCount() {
        return mServiceTypeList.size();
    }

    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView title;
        private OnSericeClicked onItemClickListener;
        private SupportType mItem;


        //  initialize view
        public MyViewHolder(View view , OnSericeClicked listener) {
            super(view);
            title =  view.findViewById(R.id.txt_name);
            view.setOnClickListener(this);
            onItemClickListener = listener;
        }



        // Set values to the list items
        void bind(final SupportType item, final int position){
            mItem= item;
            title.setText(item.getTitle());
        }


        @Override
        public void onClick(View v) {
            if(onItemClickListener != null){
                onItemClickListener.onServiceClicked(mItem);
            }
        }
    }


}
