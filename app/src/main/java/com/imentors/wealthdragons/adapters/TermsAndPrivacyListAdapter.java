package com.imentors.wealthdragons.adapters;

import android.content.Context;

import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import androidx.fragment.app.Fragment;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.fragments.TermsAndConditionFragment;
import com.imentors.wealthdragons.models.TermsAndPrivacy;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;



/**
 * Created on 02-02-2016.
 */
public class TermsAndPrivacyListAdapter extends BaseAdapter {


    @SuppressWarnings("CanBeFinal")
    private LayoutInflater mInflater;
    private List<TermsAndPrivacy.Content> mItems;
    private Context mContext;
    private boolean mIsPrivacyPolicy;
    private boolean mIsMainActivity;


    /**
     * different click handled by onItemClick
     */
    public interface OnSpanClickListener {
        void onSpanClicked(Fragment frag , boolean addToBackStack);
    }

    private OnSpanClickListener mOnSpanClickListener ;

    public TermsAndPrivacyListAdapter(Context context , OnSpanClickListener onSpanClickListener , boolean isPrivacyPloicy, boolean isMainActivity) {

        mContext = context;
        mOnSpanClickListener = onSpanClickListener;
        mIsPrivacyPolicy = isPrivacyPloicy;
        mIsMainActivity = isMainActivity;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    /**
     * set the article items
     * @param items             article items
     */
    public void setItems(final List<TermsAndPrivacy.Content> items) {
        mItems = items;
        notifyDataSetChanged();
    }


    //  counting item size
    @Override
    public int getCount() {
        if (mItems == null) {
            return 0;
        }
        return mItems.size();
    }

    public void clearData(){
        if(mItems!=null) {
            mItems.clear();
            notifyDataSetChanged();
        }
    }


    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.fragment_privacy_terms_list_item, parent, false);

        WealthDragonTextView tvHeading = rowView.findViewById(R.id.tv_list_item_heading);
        WealthDragonTextView tvDescription = rowView.findViewById(R.id.tv_list_item_description);
        View lineView = rowView.findViewById(R.id.view_underline);

        TermsAndPrivacy.Content content = mItems.get(position);


        // Set values to the list items
        if(!TextUtils.isEmpty(content.getHeading())){
            tvHeading.setVisibility(View.VISIBLE);
            lineView.setVisibility(View.VISIBLE);
            tvHeading.setText(content.getHeading());
        }else{
            tvHeading.setVisibility(View.GONE);
            lineView.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(content.getDescription())){
            tvDescription.setVisibility(View.VISIBLE);
            tvDescription.setText(content.getDescription());
            if(!mIsPrivacyPolicy) {
                String[] clikableLinks = {mContext.getString(R.string.privacy_policy)};
                ClickableSpan[] clickableSpan = {privacyPolicySpan};
                Utils.makeLinks(tvDescription, clikableLinks, clickableSpan);
            }
        }else {
            tvDescription.setVisibility(View.GONE);
        }

        return rowView;
    }



    // click of privacy policy
    ClickableSpan privacyPolicySpan = new ClickableSpan() {
        @Override
        public void onClick(View widget) {

            TermsAndConditionFragment termFragment  = TermsAndConditionFragment.newInstance(true,mIsMainActivity);
            mOnSpanClickListener.onSpanClicked(termFragment,true);

        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };




}
