package com.imentors.wealthdragons.adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.imentors.wealthdragons.fragments.DashBoardFragment;
import com.imentors.wealthdragons.fragments.MyPurchaseFragment;
import com.imentors.wealthdragons.fragments.OtherTabFragment;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;

public class DashBoardViewPagerAdapter extends FragmentPagerAdapter {

    private Fragment mCurrentFragment;
    private int numberOfItems=1;
    private DashBoard mDashBoard = AppPreferences.getDashBoard();

    public DashBoardViewPagerAdapter(FragmentManager fm) {
        super(fm);
        if(mDashBoard.getSlider()!=null){
            if(mDashBoard.isMentorTabVisible()){
                numberOfItems++;
            }
            if(mDashBoard.isVideoTabVisible()){
                numberOfItems++;
            }
            if(mDashBoard.isCourseTabVisible()){
                numberOfItems++;
            }
            if(mDashBoard.isPurchaseTabVisible()){
                numberOfItems++;
            }

        }
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return DashBoardFragment.newInstance();
            case 1:
                if(mDashBoard.isMentorTabVisible()){
                    return   OtherTabFragment.newInstance(Constants.MENTOR_TAB,null);
                } else if(mDashBoard.isVideoTabVisible()){
                    return   OtherTabFragment.newInstance(Constants.VIDEO_TAB,null);
                }else if(mDashBoard.isCourseTabVisible()){
                    return   OtherTabFragment.newInstance(Constants.COURSE_TAB,null);
                } else {
                    return   MyPurchaseFragment.newInstance();
                }
            case 2:

                if(mDashBoard.isMentorTabVisible() && mDashBoard.isVideoTabVisible() ){
                    return   OtherTabFragment.newInstance(Constants.VIDEO_TAB,null);
                } else if((mDashBoard.isMentorTabVisible() || mDashBoard.isVideoTabVisible() ) && mDashBoard.isCourseTabVisible() ){
                    return   OtherTabFragment.newInstance(Constants.COURSE_TAB,null);
                }else{
                    return   MyPurchaseFragment.newInstance();
                }


            case 3:
                if(mDashBoard.isVideoTabVisible() && mDashBoard.isCourseTabVisible() && mDashBoard.isMentorTabVisible()){
                    return   OtherTabFragment.newInstance(Constants.COURSE_TAB,null);
                } else{
                    return   MyPurchaseFragment.newInstance();
                }


            default:
                return  MyPurchaseFragment.newInstance();


        }

    }

    @Override
    public int getCount() {
       return numberOfItems;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if(object instanceof DashBoardFragment){
            return super.getItemPosition(object);
        }else {
            return PagerAdapter.POSITION_NONE;
        }
    }

    @Override
    public long getItemId(int position) {

        switch (position){
            case 0:
                return super.getItemId(position);
            case 1:
                if(mDashBoard.isMentorTabVisible()){
                    return Constants.MENTOR_TAB;
                } else if(mDashBoard.isVideoTabVisible()){
                    return  Constants.VIDEO_TAB;
                }else if(mDashBoard.isCourseTabVisible()){
                    return  Constants.COURSE_TAB;
                } else{
                    return  Constants.PURCHASE_TAB;
                }
            case 2:
                if(mDashBoard.isMentorTabVisible() && mDashBoard.isVideoTabVisible()){
                    return  Constants.VIDEO_TAB;
                }
                else if((mDashBoard.isMentorTabVisible() || mDashBoard.isVideoTabVisible() ) && mDashBoard.isCourseTabVisible() ){
                    return  Constants.COURSE_TAB;
                }else{
                    return  Constants.PURCHASE_TAB;
                }

            case 3:
                if(mDashBoard.isMentorTabVisible() && mDashBoard.isVideoTabVisible() && mDashBoard.isCourseTabVisible()){
                    return  Constants.COURSE_TAB;
                } else{
                    return  Constants.PURCHASE_TAB;
                }

            default:
                return  Constants.PURCHASE_TAB;

        }

    }

    public Fragment getCurrentFragment(){
        return mCurrentFragment;
    }

    public void setCount(int count , DashBoard dashBoard){
        numberOfItems = count;
        mDashBoard = dashBoard;
        notifyDataSetChanged();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if(getCurrentFragment()!=object){
            mCurrentFragment = (Fragment) object;
        }
        super.setPrimaryItem(container, position, object);
    }


}
