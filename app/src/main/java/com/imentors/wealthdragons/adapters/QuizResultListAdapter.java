package com.imentors.wealthdragons.adapters;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.QuizResult;

import java.util.List;

public class QuizResultListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {

    private List< QuizResult.Question> mQusetionList;
    private Context mContext;

    public QuizResultListAdapter(List< QuizResult.Question> qusetionList , Context context) {
        this.mQusetionList = qusetionList;
        this.mContext = context;
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.quiz_result_list_subcategory_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyViewHolder viewHolder = (MyViewHolder) holder;
        viewHolder.bind(mQusetionList.get(position),mContext);
    }


    @Override
    public int getItemCount() {
        return mQusetionList.size();
    }


    //   * View holder class for items fo recycler view
    private static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvQuestionName , tvYourAnswer, tvCorrectAnswer;


        //  initialize view
        public MyViewHolder(View view ) {
            super(view);
            tvQuestionName =  view.findViewById(R.id.tv_quiz_ques);
            tvCorrectAnswer =  view.findViewById(R.id.tv_quiz_correct_answer);
            tvYourAnswer =  view.findViewById(R.id.tv_quiz_your_answer);
        }



        // Set values to the list items
        void bind(final  QuizResult.Question item, Context context){
            tvQuestionName.setText(item.getQuestion());
            tvYourAnswer.setText(context.getString(R.string.your_answer)+" "+item.getAnswer());
            tvCorrectAnswer.setText(context.getString(R.string.correct_answer)+" "+item.getCorrect_answer());
        }

    }
}
