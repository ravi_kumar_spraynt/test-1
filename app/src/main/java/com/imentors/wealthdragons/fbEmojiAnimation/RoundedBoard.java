package com.imentors.wealthdragons.fbEmojiAnimation;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


/**
 * Created by KenZira on 3/10/17.
 */

public class RoundedBoard {

  static final int WIDTH = 6 * Emotion.MEDIUM_SIZE + 7 * Constants.HORIZONTAL_SPACING;

  static final int HEIGHT = ( WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet))?Utils.dpToPx(52):Utils.dpToPx(46);

  static final float SCALED_DOWN_HEIGHT = ( WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet))?Utils.dpToPx(52):Utils.dpToPx(46);

  static final float LEFT = ( WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet))?Utils.pxWidth/2+Utils.dpToPx(10):Utils.pxWidth/4;

  static final float BOTTOM = (Utils.pxHeight/2) - (Utils.pxHeight/20);

  static final float TOP = BOTTOM - HEIGHT;

  static final float BASE_LINE = TOP + Emotion.MEDIUM_SIZE + Constants.VERTICAL_SPACING;

  float height = HEIGHT;
  float y;

  private float radius = height / 2;

  float startAnimatedHeight;
  float endAnimatedHeight;

  float startAnimatedY;
  float endAnimatedY;

  private Paint boardPaint;
  private RectF rect;

  RoundedBoard() {
    initPaint();
    rect = new RectF();
  }

  private void initPaint() {
    boardPaint = new Paint();
    boardPaint.setAntiAlias(true);
    boardPaint.setStyle(Paint.Style.FILL);
    boardPaint.setColor(Color.WHITE);
//    boardPaint.setShadowLayer(5.0f, 0.0f, 2.0f, 0xFF000000);
  }

  void setCurrentHeight(float newHeight) {
    height = newHeight;
    y = BOTTOM - height;
  }

  void draw(Canvas canvas) {
    rect.set(LEFT, y, LEFT + WIDTH, y+HEIGHT);
    canvas.drawRoundRect(rect, radius, radius, boardPaint);
  }
}
