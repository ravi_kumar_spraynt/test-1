package com.imentors.wealthdragons.listeners;

import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.GroupDownloadFile;
import com.tonyodev.fetch2.Download;

import java.util.List;

public interface DownloadActionListener {

    void onRemoveDownload(int id);

    void onRetryDownload(int id);

    void onItemClicked(Download download);
    void onGroupItemClicked(GroupDownloadFile download);

    void onDownloadItemClicked(List<Download> download, int position);

    void allItemsRemoved();
}
