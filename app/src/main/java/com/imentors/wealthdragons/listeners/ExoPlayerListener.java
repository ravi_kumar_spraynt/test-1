package com.imentors.wealthdragons.listeners;



import android.content.Intent;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.ArrayList;
import java.util.List;


public class ExoPlayerListener implements Player.EventListener {

    private SimpleExoPlayer mExoplayer;
    private List<Chapters.EClasses>  mEclasses = new ArrayList<>();
    private int pos = -1;
    private boolean mIsAutoPlay = false;
    private String mType ;
    private boolean isVideoPlaying = false;
    private String mVideoName;
    private boolean isVideoLoaded =  false;
    private boolean mIsIntroVideo = false;

    public ExoPlayerListener(SimpleExoPlayer exoPlayer, List<Chapters.EClasses> eClassesList, boolean isAutoPlay , String type, String videoName,boolean isIntroVideo){
        mExoplayer = exoPlayer;
        mEclasses = eClassesList;
        mIsAutoPlay = isAutoPlay;
        mType = type;
        mVideoName = videoName;
        mIsIntroVideo = isIntroVideo;

    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {



        trackSelections.get(0);
        if(!mIsAutoPlay) {
            if (mExoplayer.getCurrentWindowIndex() == mExoplayer.getNextWindowIndex() && mExoplayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING) {
                if(mExoplayer.getPlayWhenReady()) {
                    mExoplayer.setPlayWhenReady(false);
                }
            }
        }

        if(mExoplayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING && mEclasses!= null) {

            if (mEclasses.size() > 0) {
                Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
                if (!mIsAutoPlay) {
                    String startTime = TextUtils.isEmpty(eclass.getSeek_time()) ? "0" : eclass.getSeek_time();
                    Utils.callEventLogApi("watched "+"<b>" + eclass.getTitle() + "</b> " + mType + " from <b> " + Utils.getMinHourSecFromSeconds(Long.parseLong(startTime)) + "</b> to <b>" + eclass.getDuration() + "</b>");
                } else if (mExoplayer.getCurrentWindowIndex() == mExoplayer.getNextWindowIndex()) {
                    String startTime = TextUtils.isEmpty(eclass.getSeek_time()) ? "0" : eclass.getSeek_time();
                    Utils.callEventLogApi("clicked <b>Replay</b> for <b> " +Utils.getMinHourSecFromSeconds(Long.parseLong(startTime)) + "</b> of <b> " + mType + "</b> ");

                }
            }
        }

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {


        if(playWhenReady && playbackState!=Player.STATE_READY)
            isVideoPlaying= false;

        if(isVideoPlaying && playbackState == Player.STATE_READY){

            String log=null;
            if(mEclasses!=null && mEclasses.size()>0) {
                Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
                log = eclass.getTitle();
            }else if(!TextUtils.isEmpty(mVideoName)){
                if(mType == Constants.TYPE_DETAIL_VIDEO)
                    log = mVideoName;
                else
                    log = mVideoName;

            }


            if(playWhenReady){
                Utils.callEventLogApi("played <b>"+log+"</b> video ");
            }else{
                Utils.callEventLogApi("paused <b>"+log+"</b> video ");
            }
        }


        if(playWhenReady && playbackState==Player.STATE_READY)
        isVideoPlaying= true;


        if(playbackState == Player.STATE_ENDED){
            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_ENDED));
        }



        if (playbackState == Player.STATE_BUFFERING){
            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_BUFFERING));
        }else{
            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_BUFFERING_STOP));
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {
        isVideoLoaded = true;

        if(mEclasses!=null)
        if(mEclasses.size()>0) {
            Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
            String startTime = TextUtils.isEmpty(eclass.getSeek_time()) ? "0" : eclass.getSeek_time();
            Utils.callEventLogApi("watching  <b>" + eclass.getTitle()+" </b> " + mType + " from <b>" + Utils.getMinHourSecFromSeconds(Long.parseLong(startTime)) + "</b> to <b>" + eclass.getDuration() + "</b>");
        }else if(!TextUtils.isEmpty(mVideoName)){
            if(mType==Constants.TYPE_DETAIL_VIDEO)
                Utils.callEventLogApi("watching <b>"+mVideoName+"</b>");
            else
                Utils.callEventLogApi("watching <b> intro video </b> of <b>"+mVideoName+"</b>");

        }

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        if (error != null) {

            try {

                Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoName + "</b>");

                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.VIDEO_ERROR));

                switch (error.type) {
                    case ExoPlaybackException.TYPE_SOURCE:
                        Log.e("playerror", "TYPE_SOURCE: " + error.getSourceException().getMessage());
                        break;

                    case ExoPlaybackException.TYPE_RENDERER:
                        Log.e("playerror", "TYPE_RENDERER: " + error.getRendererException().getMessage());
                        break;

                    case ExoPlaybackException.TYPE_UNEXPECTED:
                        Log.e("playerror", "TYPE_UNEXPECTED: " + error.getUnexpectedException().getMessage());
                        break;
                }
            }
            catch (IllegalStateException illegalException)
            {
                illegalException.printStackTrace();
                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.VIDEO_ERROR));

            }
        }

    }

    @Override
    public void onPositionDiscontinuity(int reason) {


        // for handling log
        switch(reason){
            case Player.DISCONTINUITY_REASON_PERIOD_TRANSITION:
                if(mEclasses!=null)
                    if(mEclasses.size()>0) {
                        Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
                        String startTime = TextUtils.isEmpty(eclass.getSeek_time()) ? "0" : eclass.getSeek_time();
                        Utils.callEventLogApi("started new video  " + eclass.getTitle()+" " + mType + " from <b> " + Utils.getMinHourSecFromSeconds(Long.parseLong(startTime))  + " </b> to <b>" + eclass.getDuration() + "</b>");
                    }else{
                        Utils.callEventLogApi("started new video  " + mVideoName);
                    }
                break;
            case Player.DISCONTINUITY_REASON_SEEK:
                if(mExoplayer.getPlaybackState() == Player.STATE_READY && isVideoLoaded ) {
                    if(mEclasses!=null && mEclasses.size()>0) {
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mExoplayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mEclasses.get(mExoplayer.getCurrentWindowIndex()).getTitle() + "</b>");
                    }else{
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mExoplayer.getContentPosition() / 1000) + "</b>" + " from" + "<b> " + mVideoName + "</b>");

                    }
                }break;
        }


        if(pos != mExoplayer.getCurrentWindowIndex()) {

            pos = mExoplayer.getCurrentWindowIndex();

                // condition for episodes
                if (mEclasses!=null && mEclasses.size() > 0) {


                    Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
                    String videoId = eclass.getId();

                    // send video id in broadcast
                    Intent intent = new Intent(Constants.BROADCAST_MARK_RUNNING_TRACK);
                    intent.putExtra(Constants.ECLASSES_VIDEO_ID, videoId);

                    // send boolean is played in broadcast
                    intent.putExtra(Constants.IS_VIDEO_PLAYED, false);


                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);
                }else {
                    LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_MARK_RUNNING_TRACK));

                }
        }

        if(mEclasses!=null && mEclasses.size()>0) {

            if(mIsIntroVideo){
                if(mExoplayer.getCurrentWindowIndex()>0){
                    checkFreePaid();
                }
            }else{
                checkFreePaid();
            }
        }


    }

    private void checkFreePaid() {
        Chapters.EClasses eclass = mEclasses.get(mExoplayer.getCurrentWindowIndex());
        if(!eclass.getFree_paid().equals("Free") && mExoplayer.getPlayWhenReady() == true){
            String videoId = eclass.getId();

            // send video id in broadcast
            Intent intent = new Intent(Constants.BROADCAST_CHECK_FREE_PAID_TRACK);
            intent.putExtra(Constants.ECLASSES_VIDEO_ID, videoId);


            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);
        }
    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {
        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_POSITION_CHANGED));

        if (mExoplayer.getPlaybackState() == Player.STATE_BUFFERING){
            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_BUFFERING));
        }else{
            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_VIDEO_BUFFERING_STOP));
        }

    }







}
