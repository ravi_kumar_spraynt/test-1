package com.imentors.wealthdragons.utils;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class FileDownloader {


    private static final int  MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory, Context context){
        try {

            URL url = new URL(fileUrl);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            //urlConnection.setRequestMethod("GET");
            //urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(directory);
            int totalSize = urlConnection.getContentLength();

            byte[] buffer = new byte[MEGABYTE];
            int bufferLength = 0;
            while((bufferLength = inputStream.read(buffer))>0 ){
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();

            openFile(directory,context);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(context, "Downloading issue", Toast.LENGTH_SHORT).show();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Toast.makeText(context, "Downloading issue", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(context, "Downloading issue", Toast.LENGTH_SHORT).show();

        }
    }

    private static void openFile(File pdfFile,Context context) {
        if(pdfFile.exists()){
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            Uri uri = null;
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                uri = FileProvider.getUriForFile(
                        context,
                        context.getApplicationContext()
                                .getPackageName() + ".provider", pdfFile);


                pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }else{
                uri = Uri.fromFile(pdfFile);

            }


            pdfIntent.setDataAndType(uri, "application/pdf");
        try{
            context.startActivity(pdfIntent);
        }catch(Exception e){
            Toast.makeText(context, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
        }else{
            Toast.makeText(context, "No File available to view ", Toast.LENGTH_SHORT).show();
        }


//        Utils.openDocument(fileUrl, context);


//        try {
//            context.startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));
//        }catch (Exception e){
//            e.printStackTrace();
//        }

//        File extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
//
//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//        Uri uri = Uri.parse(extStorageDirectory.getAbsolutePath());
//        intent.setDataAndType(uri, "file/*");
//        context.startActivity(Intent.createChooser(intent, "Open Download Folder"));





    }
}
