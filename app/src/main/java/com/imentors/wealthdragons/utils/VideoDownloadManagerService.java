package com.imentors.wealthdragons.utils;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.os.ResultReceiver;
import android.util.Log;


import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.imentors.wealthdragons.models.DownloadFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class VideoDownloadManagerService extends Service {

    public static final String TAG = "Download Task";
    private int result = Activity.RESULT_CANCELED;
    private DownloadFile mDownLoadFile;
    private File outputFile;
    public static final String NOTIFICATION = "service receiver";
    private ResultReceiver receiver;
    public static final int UPDATE_PROGRESS = 8344;
    Bundle resultData = new Bundle();
    public static final String PROGRESS_BAR = "progress bar";
    private int mIntentId = -1;


    @Override
    public void onCreate() {
        super.onCreate();


    }


    private void startDownload() {

        if (getApplicationContext() != null) {
            try {

                URL url = new URL(mDownLoadFile.getVideoUrl());//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                int fileLength = c.getContentLength();

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("Downlaod", "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                outputFile = new File(getApplicationContext().getFilesDir(), mDownLoadFile.getTitle());//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e("Downlaod", "File Created");
                }

                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppPreferences.removeDownloadListItem(mDownLoadFile);
                    }
                });


                mDownLoadFile.setVideoAddress(outputFile.getPath());



                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppPreferences.addDownloadListItem(mDownLoadFile);

                    }
                });


                FileOutputStream fos = getApplicationContext().openFileOutput(mDownLoadFile.getTitle(), Context.MODE_PRIVATE);//Get OutputStream for NewFile Location


                InputStream is = c.getInputStream();//Get InputStream for connection
                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;
                    int progress = (int) (total * 100 / fileLength);
                    Log.e("Downlaod", "Download Progress" + progress);

                    // publishing the progress....
                    if(progress>0) {
                        Intent intent = new Intent(PROGRESS_BAR);
                        intent.putExtra("Progress", progress);

                        publishProgress(intent);
                    }

                    fos.write(buffer, 0, len1);//Write new file

                }

                //Close all connection after doing task
                fos.close();
                is.close();
                result = Activity.RESULT_OK;

            } catch (Exception e) {

                //Read exception if something went wrong
                result = Activity.RESULT_CANCELED;
                e.printStackTrace();
                outputFile = null;
                Log.e("Downlaod", "Download Error Exception " + e.getMessage());
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        AppPreferences.removeDownloadListItem(mDownLoadFile);


                        AppPreferences.addDownloadListItem(mDownLoadFile);
                    }
                });

            }

            publishResults();
        }

    }




    private void publishProgress(Intent intent) {

        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(intent);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("Downlaod", "Download Destroy");

    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }





    @Override
    public int onStartCommand( Intent intent, int flags, int startId) {
        mDownLoadFile = (DownloadFile) intent.getSerializableExtra(TAG);
        receiver = intent.getParcelableExtra("receiver");
        startDownload();
        return START_STICKY;

    }

    private void publishResults() {

        Intent intent = new Intent(NOTIFICATION);
//        intent.putExtra(Constants.ITEM_ID, mDownLoadFile.getId());
        intent.putExtra(Constants.RESULT, result);

        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(intent);

    }
}
