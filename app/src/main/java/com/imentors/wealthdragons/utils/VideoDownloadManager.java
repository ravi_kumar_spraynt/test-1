package com.imentors.wealthdragons.utils;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.imentors.wealthdragons.models.DownloadFile;

import java.io.File;


public class VideoDownloadManager {

    private static final String TAG = "Download Task";
    private Context context;
    private String downloadUrl = "", downloadFileName = "";
    private DownloadFile mDownLoadFile;
    private long mDownloadID;

    public VideoDownloadManager(Context context, DownloadFile downloadFile) {
        this.context = context;
        this.downloadUrl = downloadFile.getVideoUrl();
        mDownLoadFile = downloadFile;
        AppPreferences.addDownloadListItem(mDownLoadFile);

        downloadFileName = downloadFile.getTitle();//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);

        startDownload();

    }

    private void startDownload() {

        try {
            File file = new File(context.getFilesDir(), downloadFileName);
            //Create New File if not present
            if (!file.exists()) {
                file.createNewFile();
                Log.e("Downlaod", "File Created");
            }
        /*
        Create a DownloadManager.Request with all the information necessary to start the download

         */

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(mDownLoadFile.getVideoUrl()))
                    .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI |
                            DownloadManager.Request.NETWORK_MOBILE)
                    .setTitle(mDownLoadFile.getTitle())// Title of the Download Notification
                    .setDescription("Downloading")// Description of the Download Notification
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_ONLY_COMPLETION)// Visibility of the download Notification
                    .setDestinationUri(Uri.fromFile(file))// Uri of the destination file
                    .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                    .setAllowedOverRoaming(true);// Set if download is allowed on roaming network


            DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            mDownloadID = downloadManager.enqueue(request);// enqueue puts the download request in the queue.

        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
