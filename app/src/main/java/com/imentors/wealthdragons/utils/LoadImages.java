package com.imentors.wealthdragons.utils;


import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.LruCache;


import java.util.HashMap;


public class LoadImages extends AsyncTask<HashMap<String,String>,String,LruCache<String,Bitmap>> {


    private static  final String DIRECTORY_NAME = "WEALTH_DRAGON_IMAGES";


    private String mKey;
    private LruCache<String, Bitmap> mLruCache;

    public LoadImages(LoadImagesResponse resultResponse ,  LruCache<String, Bitmap> lruCache){
        this.response = resultResponse;
        this.mLruCache = lruCache;
    }

    @Override
    protected LruCache<String, Bitmap> doInBackground(HashMap<String,String>... paths) {

        HashMap<String,String> pathMap = paths[0];

        try {
//            if (pathMap != null) {
//
//
//                for (Map.Entry me : pathMap.entrySet()) {
//
//                    Log.e(me.getKey().toString(), me.getValue().toString());
//
//                    Bitmap bmp = Utils.loadImageFromStorage(me.getValue().toString(), me.getKey().toString());
//                    Bitmap emptyBitmap = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());
//                    if (!TextUtils.isEmpty(me.getKey().toString())) {
//                        if (!bmp.sameAs(emptyBitmap)) {
//                            mLruCache.put(me.getKey().toString(), bmp);
//                        }
//                    }
//                }
//            }
            return mLruCache;
        }catch (Exception e){
            return mLruCache;
        }

    }

    // you may separate this or combined to caller class.
    public interface LoadImagesResponse {
        void processFinish(LruCache<String, Bitmap> output);
    }

    public LoadImagesResponse response = null;


    @Override
    protected void onPostExecute(LruCache<String, Bitmap> stringBitmapLruCache) {
        if(stringBitmapLruCache!=null) {
            response.processFinish(stringBitmapLruCache);
        }
    }
}
