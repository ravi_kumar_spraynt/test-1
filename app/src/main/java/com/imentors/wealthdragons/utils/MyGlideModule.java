package com.imentors.wealthdragons.utils;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.module.GlideModule;


/**
 * Created on 7/03/17.
 */
public class MyGlideModule implements GlideModule {

    @Override
    public void registerComponents(Context context, Glide glide) {

    }


    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        int cacheSize20MegaBytes = 20*1024*1024;
        builder.setDiskCache(
                new InternalCacheDiskCacheFactory(context, cacheSize20MegaBytes)
        );

    }
}