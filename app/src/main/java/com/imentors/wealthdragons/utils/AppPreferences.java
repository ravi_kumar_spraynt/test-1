package com.imentors.wealthdragons.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.models.Categories;
import com.imentors.wealthdragons.models.CountryCode;
import com.imentors.wealthdragons.models.CreditCard;
import com.imentors.wealthdragons.models.Customer;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardCourses;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardEpisodeDetails;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DashBoardMentors;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.DashBoardVideos;
import com.imentors.wealthdragons.models.DashBoardWishList;
import com.imentors.wealthdragons.models.DeepLink;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.SearchKeywords;
import com.imentors.wealthdragons.models.Session;
import com.imentors.wealthdragons.models.Settings;
import com.imentors.wealthdragons.models.Subscription;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.models.TermsAndPrivacy;
import com.imentors.wealthdragons.models.User;
import com.imentors.wealthdragons.models.UserCredentials;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AppPreferences {

    private static final String PREFS_NAME = "MyPrefsFile";
    private static final String SESSION = "session";
    private static final String IS_PREFERENCE_ADDED = "is_preference_added";

    private static final String SELECTED_HOME_SCREEN = "selected_home_screen";

    private static final String IS_FCM_TOKEN_SEND = "IS_FCM_TOKEN_SEND";

    private static final String IS_TABLAYOUT_RESIZED = "IS_TABLAYOUT_RESIZED";

    private static final String IS_FULL_SCREEN_ACTIVE = "IS_FULL_SCREEN_ACTIVE";

    private static final String IS_CUSTOMER_SUPPORT_ACTIVE = "IS_CUSTOMER_SUPPORT_ACTIVE";

    private static final String IS_LIVE_CHAT_ACTIVE = "IS_LIVE_CHAT_ACTIVE";


    private static final String IS_TAB_CHANGED = "IS_TAB_CHANGED";


    private static final String IS_DATA_LOADED = "IS_DATA_LOADED";


    private static final String SUBSCRIPTION = "subscription";

    private static final String TERMS_AND_CONDITION = "terms_and_condition";

    private static final String PRIVACY_POLICY = "privacy_policy";

    private static final String USER = "user";

    private static final String SERVICE_CODE = "service_code";

    private static final String USER_CREDENTIALS = "user_credentails";

    private static final String CATEGORIES = "categories";

    private static final String CATEGORIES_JSON = "categories_json";
    private static final String UPDATE_VERSION_NUMBER = "UPDATE_VERSION_NUMBER";

    private static final String KEY_IS_UPDATE_MESSAGE_REQUIRED = "KEY_IS_UPDATE_MESSAGE_REQUIRED";

    private static final String DASHBOARD = "dashboard";

    private static final String DASHBOARD_MENTORS = "dashboard_mentors";

    private static final String DASHBOARD_VIDEOS = "dashboard_videos";

    private static final String DASHBOARD_COURSES = "dashboard_courses";

    private static final String ONLINE_CATEGORY_COUNT = "online_category_count";

    private static final String IMAGES_SET = "images_set";

    private static final String VIDEO_RESUME_WINDOW = "video_resume_window";

    private static final String VIDEO_RESUME_POSITION = "video_resume_position";


    private static final String SEARCH_KEYWORD = "search_keyword";

    private static final String DASHBOARD_COURSE_DETAIL_DATA = "dashboard_course_detail_data";

    private static final String DASHBOARD_MENTOR_DETAIL_DATA = "dashboard_mentor_detail_data";

    private static final String DASHBOARD_VIDEO_DETAIL_DATA = "dashboard_video_detail_data";

    private static final String DASHBOARD_EPISODE_DETAIL_DATA = "dashboard_episode_detail_data";

    private static final String DASHBOARD_WISH_LIST_DATA = "dashboard_wishlist_data";

    private static final String CUSTOMER_DATA = "CUSTOMER_DATA";

    private static final String COUNTRY_CODE = "COUNTRY_CODE";

    private static final String SUPPORT_TYPE = "SUPPORT_TYPE";

    private static final String WIFI = "WIFI";

    private static final String NOTIFICATION = "NOTIFICATION";

    private static final String MAIN_ACTIVITY_STATE = "MAIN_ACTIVITY_STATE";

    private static final String LOGIN_ACTIVITY_STATE = "LOGIN_ACTIVITY_STATE";

    private static final String DEEP_LINK = "DEEP_LINK";

    private static final String SHOW_POPUP = "SHOW_POPUP";

    private static final String SHOW_POPUP_DATE = "SHOW_POPUP_DATE";

    private static String FULL_SCREEN_STATUS = "FULL_SCREEN_STATUS";

    private static String CARD_LIST = "CARD_LIST";

    private static final String MAINTAINCE_MODE_CROSS_CLICK = "MAINTAINCE_MODE_CROSS_CLICK";

    private static final String MAINTAINCE_MODE = "MAINTAINCE_MODE";

    private static final String MAINTAINCE_MODE_UPDATE = "MAINTAINCE_MODE_UPDATE";


    private static final String OPEN_CONTACT_SUPPORT = "OPEN_CONTACT_SUPPORT";
    private static final String OPEN_FINGERPRINT_DIALOG = "OPEN_FINGERPRINT_DIALOG";
    private static final String VIDEO_DOWNLOAD_LIST = "VIDEO_DOWNLOAD_LIST";
    private static final String SETTINGS = "SETTINGS";


    private static SharedPreferences mSharedPreferences;
    //ref to the last updated session
    private static Session mSession;
    private static Subscription mSubscription;
    private static User mUser;
    private static String mServiceCode;
    private static UserCredentials mUserCredentials;
    private static TermsAndPrivacy mTermsAndCondition;
    private static TermsAndPrivacy mPrivacyPolicy;
    private static Categories mCategories;
    private static String categoriesJson;
    private static DashBoard mDashboard;
    private static DashBoardMentors mMentorDashBoard;
    private static DashBoardVideos mDashBoardVideos;
    private static DashBoardCourses mDashBoardCourses;
    private static DashBoardCourseDetails mDashBoardCourseDetails;
    private static DashBoardMentorDetails mDashBoardMentorDetails;
    private static DashBoardVideoDetails mDashBoardVideoDetails;
    private static DashBoardEpisodeDetails mDashBoardEpisodeDetails;

    private static DashBoardWishList mDashBoardWishList;
    private static Customer mCustomerData;
    private static List<CountryCode> mListCountryCode;
    private static List<SupportType> mListSupportType;
    private static List<CreditCard> mListCreditCards;
    private static DeepLink mDeepLink;

    private static SearchKeywords mSearchKeywords;
    private static HashMap<String, String> mImagesSet = new HashMap<>();
    private static HashMap<String, Integer> mVideoResumeWindowSet = new HashMap<>();
    private static HashMap<String, Long> mVideoResumePositionSet = new HashMap<>();
    private static Serializable mNotificationData;
    private static List<DownloadFile> mDownloadFileList = new ArrayList<>();
    private static Settings mSettings;


    private AppPreferences() {
        //private constructor to enforce Singleton pattern
    }

    /**
     * initialize the reference to shared preferences, this need to be initialize before any function call to this class
     *
     * @param context application context
     */
    static void init(@NonNull Context context) {
        mSharedPreferences = context.getSharedPreferences(PREFS_NAME, 0);
    }

    /**
     * put boolean value with key
     *
     * @param key   key
     * @param value value
     */
    private static void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    /**
     * get boolean value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    private static boolean getBoolean(String key, boolean defValue) {
        return mSharedPreferences.getBoolean(key, defValue);
    }


    /**
     * put long value with key
     *
     * @param key   key
     * @param value value
     */
    static void putLong(String key, long value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    /**
     * get value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    static long getLong(String key, long defValue) {
        return mSharedPreferences.getLong(key, defValue);
    }

    /**
     * put int value with key
     *
     * @param key   key
     * @param value value
     */
    private static void putInt(String key, int value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * get value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value
     */
    @SuppressWarnings("SameParameterValue")
    private static int getInt(String key, int defValue) {
        return mSharedPreferences.getInt(key, defValue);
    }

    /**
     * get the string value for key
     *
     * @param key      key
     * @param defValue default value
     * @return the value for key
     */
    @SuppressWarnings("SameParameterValue")
    public static String getString(String key, @Nullable String defValue) {
        return mSharedPreferences.getString(key, defValue);
    }


    /**
     * put long value with key
     *
     * @param key   key
     * @param value value
     */
    public static void putString(String key, @Nullable String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * remove the keys that are required to be removed on logout
     */
    public static void logout() {

        //reset the User Data
        mUser = new User();
        Utils.logoutFbSession();


        //clear user preferences
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(USER);
        editor.apply();

    }


    /**
     * Get the reference to last updated session
     *
     * @return the session
     */
    public static Session getSession() {

        //create the single instance of session
        if (mSession == null) {
            synchronized (AppPreferences.class) {
                if (mSession == null) {
                    String json = mSharedPreferences.getString(SESSION, null);
                    if (TextUtils.isEmpty(json)) {
                        mSession = new Session();
                    } else {
                        mSession = new Gson().fromJson(json, Session.class);
                    }
                }
            }
        }

        return mSession;
    }

    /**
     * Save the current session
     *
     * @param session session
     */
    public static void setSession(Session session) {
        mSession = session;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SESSION, new Gson().toJson(session));
        editor.apply();
    }


    /**
     * is fcm token send on server
     *
     * @return true/false
     */
    public static boolean isFCMTokenSendOnServer() {
        return mSharedPreferences.getBoolean(IS_FCM_TOKEN_SEND, false);
    }

    /**
     * mark fcm token send on server
     */
    public static void markFCMTokenSendOnServer() {
        putBoolean(IS_FCM_TOKEN_SEND, true);
    }


    /**
     * is fcm token send on server
     *
     * @return true/false
     */
    public static boolean getIsDataLoaded() {
        return mSharedPreferences.getBoolean(IS_DATA_LOADED, false);
    }

    /**
     * mark fcm token send on server
     */
    public static void setIsDataLoaded() {
        putBoolean(IS_DATA_LOADED, true);
    }

    /**
     * is fcm token send on server
     *
     * @return true/false
     */
    public static boolean isTabLayoutResized() {
        return mSharedPreferences.getBoolean(IS_TABLAYOUT_RESIZED, false);
    }

    /**
     * mark fcm token send on server
     */
    public static void setTabLayoutResized(boolean value) {
        putBoolean(IS_TABLAYOUT_RESIZED, value);
    }


    public static void setAutoplay(String autoplay){
        putString(SETTINGS,autoplay);
    }

    public static String getAutoPlay(){
      return getString(SETTINGS,Constants.AUTO_PLAY);
    }


    /**
     * Save the subscription
     *
     * @param termsAndPrivacy termsAndPrivacy
     */
    public static void setTermsAndConditon(TermsAndPrivacy termsAndPrivacy) {
        mTermsAndCondition = termsAndPrivacy;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(TERMS_AND_CONDITION, new Gson().toJson(mTermsAndCondition));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static TermsAndPrivacy getTermsAndCondition() {

        if (mTermsAndCondition == null) {
            String json = mSharedPreferences.getString(TERMS_AND_CONDITION, null);
            if (TextUtils.isEmpty(json)) {
                mTermsAndCondition = new TermsAndPrivacy();
            } else {
                mTermsAndCondition = new Gson().fromJson(json, TermsAndPrivacy.class);
            }
        }
        return mTermsAndCondition;
    }


    /**
     * Save the subscription
     *
     * @param searchKeywords SearchKeywords
     */
    public static void setSearchKeyword(SearchKeywords searchKeywords) {
        mSearchKeywords = searchKeywords;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SEARCH_KEYWORD, new Gson().toJson(mSearchKeywords));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static SearchKeywords getSearchKeyword() {

        if (mSearchKeywords == null) {
            String json = mSharedPreferences.getString(SEARCH_KEYWORD, null);
            if (TextUtils.isEmpty(json)) {
                mSearchKeywords = new SearchKeywords();
            } else {
                mSearchKeywords = new Gson().fromJson(json, SearchKeywords.class);
            }
        }
        return mSearchKeywords;
    }


    /**
     * Save the subscription
     *
     * @param termsAndPrivacy termsAndPrivacy
     */
    public static void setPrivacyPolicy(TermsAndPrivacy termsAndPrivacy) {
        mPrivacyPolicy = termsAndPrivacy;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(PRIVACY_POLICY, new Gson().toJson(mPrivacyPolicy));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static TermsAndPrivacy getPrivacyPolicy() {

        if (mPrivacyPolicy == null) {
            String json = mSharedPreferences.getString(PRIVACY_POLICY, null);
            if (TextUtils.isEmpty(json)) {
                mPrivacyPolicy = new TermsAndPrivacy();
            } else {
                mPrivacyPolicy = new Gson().fromJson(json, TermsAndPrivacy.class);
            }
        }
        return mPrivacyPolicy;
    }


    /**
     * Save the subscription
     *
     * @param subscription subscription
     */
    public static void setSubscriptionData(Subscription subscription) {
        mSubscription = subscription;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SUBSCRIPTION, new Gson().toJson(subscription));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static Subscription getSubscription() {

        if (mSubscription == null) {
            String json = mSharedPreferences.getString(SUBSCRIPTION, null);
            if (TextUtils.isEmpty(json)) {
                mSubscription = new Subscription();
            } else {
                mSubscription = new Gson().fromJson(json, Subscription.class);
            }
        }
        return mSubscription;
    }


    /**
     * Save the Categories Data
     *
     * @param categories Categories
     */
    public static void setCategories(Categories categories) {
        mCategories = categories;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CATEGORIES, new Gson().toJson(mCategories));
        editor.apply();
        if (categories.getCategory() != null) {
            setOnlineCategoryCount(categories.getCategory().size());
        }
    }

    // set categories json data
    public static void setCategoriesJson(String categories) {
        categoriesJson = categories;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CATEGORIES_JSON, categoriesJson);
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static Categories getCategories() {

        if (mCategories == null) {
            String json = mSharedPreferences.getString(CATEGORIES, null);
            if (TextUtils.isEmpty(json)) {
                mCategories = new Categories();
            } else {
                mCategories = new Gson().fromJson(json, Categories.class);
            }
        }
        return mCategories;
    }


    // to match categories
    public static String getCategoriesJson() {

        if (TextUtils.isEmpty(categoriesJson)) {
            String json = mSharedPreferences.getString(CATEGORIES_JSON, null);
            if (TextUtils.isEmpty(json)) {
                categoriesJson = null;
            } else {
                categoriesJson = json;
            }
        }
        return categoriesJson;
    }


    /**
     * Save the Online Categories Data Count
     *
     * @param categoryCount Categories
     */
    public static void setOnlineCategoryCount(int categoryCount) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(ONLINE_CATEGORY_COUNT, categoryCount);
        editor.apply();
    }


    /**
     * get the Online Categories Data Count
     *
     * @return the session
     */
    public static int getOnlineCategoryCount() {

        int onlineCategoryCount = mSharedPreferences.getInt(ONLINE_CATEGORY_COUNT, 0);

        return onlineCategoryCount;
    }


    /**
     * Save the User Data
     *
     * @param user user
     */
    public static void setUserData(User user) {
        mUser = user;

        String serviceCode;
        if(TextUtils.isEmpty(mUser.getService_code())){
            serviceCode = mUser.getServiceCode();
        }else{
            serviceCode = mUser.getService_code();
        }

        String savedServiceCode = getString(SERVICE_CODE,"service code");

        if(!TextUtils.isEmpty(serviceCode)){
            putString(SERVICE_CODE,serviceCode);
        }

        if(!savedServiceCode.equals("service code")){

            if(!savedServiceCode.equals(serviceCode)){
                // clear all downloads if old and new user are not same on a device
                Utils.getFetchInstance().deleteAll();
            }
        }


        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER, new Gson().toJson(user));
        editor.apply();
    }




    /**
     * Get the reference to last updated User Details
     *
     * @return the session
     */
    public static User getUser() {

        if (mUser == null) {
            String json = mSharedPreferences.getString(USER, null);
            if (TextUtils.isEmpty(json)) {
                mUser = new User();
            } else {
                mUser = new Gson().fromJson(json, User.class);
            }
        }
        return mUser;
    }


    /**
     * Save the User Credentials Data
     *
     * @param user user
     */
    public static void setUserCredentails(UserCredentials user) {
        mUserCredentials = user;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(USER_CREDENTIALS, new Gson().toJson(user));
        editor.apply();
    }


    /**
     * Get the User Credentials Data
     *
     * @return UserCredentials
     */
    public static UserCredentials getUserCredentials() {

        if (mUserCredentials == null) {
            String json = mSharedPreferences.getString(USER_CREDENTIALS, null);
            if (TextUtils.isEmpty(json)) {
                mUserCredentials = new UserCredentials();
            } else {
                mUserCredentials = new Gson().fromJson(json, UserCredentials.class);
            }
        }
        return mUserCredentials;
    }


    public static String getUserToken() {
        if (!TextUtils.isEmpty(getUser().getUserToken())) {
            return getUser().getUserToken();
        }
        return null;

    }


    public static String getUserId() {
        if (!TextUtils.isEmpty(getUser().getUserId())) {
            return getUser().getUserId();
        }
        return null;

    }


    /**
     * Provides LogIn Status
     *
     * @return boolean
     */
    public static boolean isLoggedIn() {

        return !(TextUtils.isEmpty(getUserToken()) || getUser() == null);

    }


    /**
     * Save the DashBoard Data
     *
     * @param dashboard DashBoard
     */
    public static void setDashBoard(DashBoard dashboard) {
        mDashboard = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD, new Gson().toJson(mDashboard));
        editor.apply();
    }


    public static void setTabDifferences(DashBoard mDashboard) {
        DashBoard newDashBoardData = mDashboard;
        DashBoard oldDashBoardData = AppPreferences.getDashBoard();

        // check for tab changes
        if (oldDashBoardData.getPlanSetting() != null) {
            if (newDashBoardData.isCourseTabVisible() != oldDashBoardData.isCourseTabVisible() || newDashBoardData.isMentorTabVisible() != oldDashBoardData.isMentorTabVisible() || newDashBoardData.isVideoTabVisible() != oldDashBoardData.isVideoTabVisible() || newDashBoardData.isCourseTabVisible() != oldDashBoardData.isCourseTabVisible() || newDashBoardData.isPurchaseTabVisible() != oldDashBoardData.isPurchaseTabVisible()) {
                putBoolean(IS_TAB_CHANGED, true);
            } else {
                putBoolean(IS_TAB_CHANGED, false);
            }
        } else {
            putBoolean(IS_TAB_CHANGED, true);
        }
    }

    public static boolean isTabChanged() {
        return getBoolean(IS_TAB_CHANGED, true);
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoard getDashBoard() {

        if (mDashboard == null) {
            String json = mSharedPreferences.getString(DASHBOARD, null);
            if (TextUtils.isEmpty(json)) {
                mDashboard = new DashBoard();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashboard = gson.fromJson(json, DashBoard.class);
            }
        }
        return mDashboard;
    }

    /**
     * Save the DashBoard Detail Data
     *
     * @param dashboard DashBoard
     */
    public static void setCourseDashBoardDetail(DashBoardCourseDetails dashboard) {
        mDashBoardCourseDetails = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_COURSE_DETAIL_DATA, new Gson().toJson(mDashBoardCourseDetails));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardCourseDetails getCourseDashBoardDetailData() {

        if (mDashBoardCourseDetails == null) {
            String json = mSharedPreferences.getString(DASHBOARD_COURSE_DETAIL_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardCourseDetails = new DashBoardCourseDetails();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashBoardCourseDetails = gson.fromJson(json, DashBoardCourseDetails.class);
            }
        }
        return mDashBoardCourseDetails;
    }


    /**
     * Save the WishList  Data
     *
     * @param dashboard DashBoard
     */
    public static void setCourseDashBoardWishList(DashBoardWishList dashboard) {
        mDashBoardWishList = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_WISH_LIST_DATA, new Gson().toJson(mDashBoardWishList));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardWishList getDashBoardWishList() {

        if (mDashBoardWishList == null) {
            String json = mSharedPreferences.getString(DASHBOARD_WISH_LIST_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardWishList = new DashBoardWishList();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashBoardWishList = gson.fromJson(json, DashBoardWishList.class);
            }
        }
        return mDashBoardWishList;
    }


    /**
     * Save the Mentor Detail Data
     *
     * @param dashboard DashBoard
     */
    public static void setMentorDashBoardDetail(DashBoardMentorDetails dashboard) {
        mDashBoardMentorDetails = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_MENTOR_DETAIL_DATA, new Gson().toJson(mDashBoardCourseDetails));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardMentorDetails getMentorDashBoardDetailData() {

        if (mDashBoardMentorDetails == null) {
            String json = mSharedPreferences.getString(DASHBOARD_MENTOR_DETAIL_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardMentorDetails = new DashBoardMentorDetails();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashBoardMentorDetails = gson.fromJson(json, DashBoardMentorDetails.class);
            }
        }
        return mDashBoardMentorDetails;
    }


    /**
     * Save the Video Detail Data
     *
     * @param dashboard DashBoard
     */
    public static void setVideoDashBoardDetail(DashBoardVideoDetails dashboard) {
        mDashBoardVideoDetails = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_VIDEO_DETAIL_DATA, new Gson().toJson(mDashBoardVideoDetails));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardVideoDetails getVideoDashBoardDetailData() {

        if (mDashBoardVideoDetails == null) {
            String json = mSharedPreferences.getString(DASHBOARD_VIDEO_DETAIL_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardVideoDetails = new DashBoardVideoDetails();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashBoardVideoDetails = gson.fromJson(json, DashBoardVideoDetails.class);
            }
        }
        return mDashBoardVideoDetails;
    }


    /**
     * Save the Video Detail Data
     *
     * @param dashboard DashBoard
     */
    public static void setEpisodeDashBoardDetail(DashBoardEpisodeDetails dashboard) {
        mDashBoardEpisodeDetails = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_EPISODE_DETAIL_DATA, new Gson().toJson(mDashBoardEpisodeDetails));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardEpisodeDetails getEpisodeDashBoardDetailData() {

        if (mDashBoardEpisodeDetails == null) {
            String json = mSharedPreferences.getString(DASHBOARD_EPISODE_DETAIL_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardEpisodeDetails = new DashBoardEpisodeDetails();
            } else {


                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
                final Gson gson = gsonBuilder.create();

                // Parse JSON to Java
                mDashBoardEpisodeDetails = gson.fromJson(json, DashBoardEpisodeDetails.class);
            }
        }
        return mDashBoardEpisodeDetails;
    }

    /**
     * Save the mentors DashBoard Data
     *
     * @param dashboard DashBoard
     */
    public static void setMentorDashBoard(DashBoardMentors dashboard) {
        mMentorDashBoard = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_MENTORS, new Gson().toJson(mMentorDashBoard));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardMentors getmMentorDashBoard() {

        if (mMentorDashBoard == null) {
            String json = mSharedPreferences.getString(DASHBOARD_MENTORS, null);
            if (TextUtils.isEmpty(json)) {
                mMentorDashBoard = new DashBoardMentors();
            } else {


                // Parse JSON to Java
                mMentorDashBoard = new Gson().fromJson(json, DashBoardMentors.class);
            }
        }
        return mMentorDashBoard;
    }

    /**
     * Save the videos DashBoard Data
     *
     * @param dashboard DashBoard
     */
    public static void setDashboardCourses(DashBoardCourses dashboard) {
        mDashBoardCourses = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_COURSES, new Gson().toJson(mDashBoardCourses));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardCourses getCoursesDashBoard() {

        if (mDashBoardCourses == null) {
            String json = mSharedPreferences.getString(DASHBOARD_COURSES, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardCourses = new DashBoardCourses();
            } else {


                // Parse JSON to Java
                mDashBoardCourses = new Gson().fromJson(json, DashBoardCourses.class);
            }
        }
        return mDashBoardCourses;
    }


    /**
     * Save the videos DashBoard Data
     *
     * @param dashboard DashBoard
     */
    public static void setDashboardVideos(DashBoardVideos dashboard) {
        mDashBoardVideos = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DASHBOARD_VIDEOS, new Gson().toJson(mDashBoardVideos));
        editor.apply();
    }


    /**
     * Get the reference to last updated subscription
     *
     * @return the session
     */
    public static DashBoardVideos getVideoDashBoard() {

        if (mDashBoardVideos == null) {
            String json = mSharedPreferences.getString(DASHBOARD_VIDEOS, null);
            if (TextUtils.isEmpty(json)) {
                mDashBoardVideos = new DashBoardVideos();
            } else {


                // Parse JSON to Java
                mDashBoardVideos = new Gson().fromJson(json, DashBoardVideos.class);
            }
        }
        return mDashBoardVideos;
    }


    public static void setImagesPath(String key, String path) {
        mImagesSet.put(key, path);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(IMAGES_SET, new Gson().toJson(mImagesSet));
        editor.apply();
    }


    public static String getImagesPath(String key) {

        if (mImagesSet.size() > 0) {
            return mImagesSet.get(key);
        } else {
            String json = mSharedPreferences.getString(IMAGES_SET, null);
            if (TextUtils.isEmpty(json)) {
                return null;
            } else {
                Type type = new TypeToken<HashMap<String, String>>() {
                }.getType();
                HashMap<String, String> storedHashMap = new Gson().fromJson(json, type);
                if (storedHashMap.size() > 0) {
                    return storedHashMap.get(key);
                } else {
                    return null;
                }
            }


        }
    }


    public static HashMap<String, String> getImagesHashMap() {
        if (mImagesSet.size() > 0) {
            return mImagesSet;
        } else {
            String json = mSharedPreferences.getString(IMAGES_SET, null);
            if (TextUtils.isEmpty(json)) {
                return null;
            } else {
                Type type = new TypeToken<HashMap<String, String>>() {
                }.getType();
                HashMap<String, String> storedHashMap = new Gson().fromJson(json, type);
                if (storedHashMap.size() > 0) {
                    return storedHashMap;
                } else {
                    return null;
                }
            }


        }
    }


    public static void clearImageCache() {

        mImagesSet = new HashMap<>();

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.remove(IMAGES_SET);
        editor.apply();
    }


    public static void setVideoResumeWindow(String key, int resumeWindow) {
        mVideoResumeWindowSet.put(key, resumeWindow);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(VIDEO_RESUME_WINDOW, new Gson().toJson(mVideoResumeWindowSet));
        editor.apply();
    }


    public static int getVideoResumeWindow(String key) {
        if (mVideoResumeWindowSet.size() > 0) {
            try {
                return mVideoResumeWindowSet.get(key);
            } catch (NullPointerException e) {
                return 0;
            }
        } else {
            String json = mSharedPreferences.getString(VIDEO_RESUME_WINDOW, null);
            if (TextUtils.isEmpty(json)) {
                return 0;
            } else {
                Type type = new TypeToken<HashMap<String, Integer>>() {
                }.getType();
                HashMap<String, Integer> storedHashMap = new Gson().fromJson(json, type);
                if (storedHashMap.size() > 0) {
                    try {
                        return storedHashMap.get(key);
                    } catch (NullPointerException e) {
                        return 0;
                    }
                } else {
                    return 0;
                }
            }


        }
    }

    public static void setVideoResumePosition(String key, long resumePosition) {
        mVideoResumePositionSet.put(key, resumePosition);

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(VIDEO_RESUME_POSITION, new Gson().toJson(mVideoResumePositionSet));
        editor.apply();
    }


    public static long getVideoResumePosition(String key) {
        if (mVideoResumePositionSet.size() > 0) {
            try {
                return mVideoResumePositionSet.get(key);
            } catch (NullPointerException e) {
                return 0L;
            }
        } else {
            String json = mSharedPreferences.getString(VIDEO_RESUME_POSITION, null);
            if (TextUtils.isEmpty(json)) {
                return 0L;
            } else {
                Type type = new TypeToken<HashMap<String, Long>>() {
                }.getType();
                HashMap<String, Long> storedHashMap = new Gson().fromJson(json, type);
                if (storedHashMap.size() > 0) {
                    try {
                        return storedHashMap.get(key);
                    } catch (NullPointerException e) {
                        return 0L;
                    }
                } else {
                    return 0L;
                }
            }


        }
    }


    /**
     * Save the customer  Data
     *
     * @param dashboard Customer
     */
    public static void setCustomerData(Customer dashboard) {
        mCustomerData = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CUSTOMER_DATA, new Gson().toJson(mCustomerData));
        editor.apply();
    }


    /**
     * Get the customer Data
     *
     * @return the session
     */
    public static Customer getCustomerData() {

        if (mCustomerData == null) {
            String json = mSharedPreferences.getString(CUSTOMER_DATA, null);
            if (TextUtils.isEmpty(json)) {
                mCustomerData = new Customer();
            } else {


                // Parse JSON to Java
                mCustomerData = new Gson().fromJson(json, Customer.class);
            }
        }
        return mCustomerData;
    }


    /**
     * Save the country  code
     *
     * @param dashboard Customer
     */
    public static void setCountryCode(List<CountryCode> dashboard) {
        mListCountryCode = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(COUNTRY_CODE, new Gson().toJson(mListCountryCode));
        editor.apply();
    }


    /**
     * Get the country code
     *
     * @return the session
     */
    public static List<CountryCode> getCountryCode() {

        if (mListCountryCode == null) {
            String json = mSharedPreferences.getString(COUNTRY_CODE, null);
            if (TextUtils.isEmpty(json)) {
                mListCountryCode = new ArrayList<>();
            } else {


                // Parse JSON to Java
                mListCountryCode = new Gson().fromJson(json, new TypeToken<List<CountryCode>>() {
                }.getType());
            }
        }
        return mListCountryCode;
    }









    /**
     * Save the support type list
     *
     * @param dashboard Customer
     */
    public static void setSupportType(List<SupportType> dashboard) {
        mListSupportType = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(SUPPORT_TYPE, new Gson().toJson(mListSupportType));
        editor.apply();
    }


    /**
     * Get the country code
     *
     * @return the session
     */
    public static List<SupportType> getSupportType() {

        if (mListSupportType == null) {
            String json = mSharedPreferences.getString(SUPPORT_TYPE, null);
            if (TextUtils.isEmpty(json)) {
                mListSupportType = new ArrayList<>();
            } else {


                // Parse JSON to Java
                mListSupportType = new Gson().fromJson(json, new TypeToken<List<SupportType>>() {
                }.getType());
            }
        }
        return mListSupportType;
    }

    /**
     * Save the CreditCard list
     *
     * @param creditCardList creditCardList
     */
    public static void setCreditCardList(List<CreditCard> creditCardList) {
        mListCreditCards = creditCardList;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(CARD_LIST, new Gson().toJson(mListCreditCards));
        editor.apply();
    }


    /**
     * Get the credit card list
     *
     * @return the session
     */
    public static List<CreditCard> getCreditCardList() {

        if (mListSupportType == null) {
            String json = mSharedPreferences.getString(CARD_LIST, null);
            if (TextUtils.isEmpty(json)) {
                mListCreditCards = new ArrayList<>();
            } else {

                // Parse JSON to Java
                mListCreditCards = new Gson().fromJson(json, new TypeToken<List<CreditCard>>() {
                }.getType());
            }
        }
        return mListCreditCards;
    }


    /**
     * Save the Deep Link Data
     *
     * @param dashboard Customer
     */
    public static void setDeepLinkData(DeepLink dashboard) {
        mDeepLink = dashboard;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(DEEP_LINK, new Gson().toJson(mDeepLink));
        editor.apply();
    }


    /**
     * Get the country code
     *
     * @return the session
     */
    public static DeepLink getDeepLink() {

        if (mDeepLink == null) {
            String json = mSharedPreferences.getString(DEEP_LINK, null);
            if (TextUtils.isEmpty(json)) {
                mDeepLink = new DeepLink();
            } else {


                // Parse JSON to Java
                mDeepLink = new Gson().fromJson(json, new TypeToken<DeepLink>() {
                }.getType());
            }
        }
        return mDeepLink;
    }


    public static void setWifiState(boolean value) {
        putBoolean(WIFI, value);
    }


    public static boolean getWifiState() {
        return getBoolean(WIFI, false);
    }


    public static void setNotificationState(boolean value) {
        putBoolean(NOTIFICATION, value);
    }


    public static boolean getNotificationState() {
        return getBoolean(NOTIFICATION, true);
    }


    public static void setMainActivityState(boolean value) {
        putBoolean(MAIN_ACTIVITY_STATE, value);
    }


    public static boolean getMainActivityState() {
        return getBoolean(MAIN_ACTIVITY_STATE, false);
    }


    public static void setLoginActivityState(boolean value) {
        putBoolean(LOGIN_ACTIVITY_STATE, value);
    }


    public static boolean getLoginActivityState() {
        return getBoolean(LOGIN_ACTIVITY_STATE, false);
    }

    public static void setVideoFullScreenActivityState(boolean value) {
        putBoolean(IS_FULL_SCREEN_ACTIVE, value);
    }


    public static boolean getFullScreenActivityState() {
        return getBoolean(IS_FULL_SCREEN_ACTIVE, false);
    }

    public static void setCustomerSupportActivityState(boolean value) {
        putBoolean(IS_CUSTOMER_SUPPORT_ACTIVE, value);
    }


    public static boolean getCustomerSupportActivityState() {
        return getBoolean(IS_CUSTOMER_SUPPORT_ACTIVE, false);
    }

    public static void setLiveChatActivityState(boolean value) {
        putBoolean(IS_LIVE_CHAT_ACTIVE, value);
    }


    public static boolean getLiveChatActivityState() {
        return getBoolean(IS_LIVE_CHAT_ACTIVE, false);
    }

    public static void setVideoFullScreenStatus(boolean value) {
        putBoolean(FULL_SCREEN_STATUS, value);
    }


    public static boolean getVideoFullScreenStatus() {
        return getBoolean(FULL_SCREEN_STATUS, false);
    }


    public static void setSubscriptionPopUpState(boolean value) {
        putBoolean(SHOW_POPUP, value);
    }


    public static boolean getSubscriptionPopUpState() {
        return getBoolean(SHOW_POPUP, true);
    }


    public static void setShowPopupDate() {
        putString(SHOW_POPUP_DATE, DateTimeUtils.getTodaysDate());
    }

    public static String getPopUpDate() {
        return getString(SHOW_POPUP_DATE, DateTimeUtils.getTodaysDate());
    }


    public static void setMaintainceModeCrossClicked(boolean value) {
        putBoolean(MAINTAINCE_MODE_CROSS_CLICK, value);
    }


    public static boolean getMaintainceModeCrossClicked() {
        return getBoolean(MAINTAINCE_MODE_CROSS_CLICK, false);
    }


    public static void setMaintainceModeUpdate(String value) {
        putString(MAINTAINCE_MODE_UPDATE, value);
    }


    public static String getMaintainceModeUpdate() {
        return getString(MAINTAINCE_MODE_UPDATE, null);
    }

    public static void setMaintainceMode(boolean value) {
        putBoolean(MAINTAINCE_MODE, value);
    }


    public static boolean getMaintainceMode() {
        return getBoolean(MAINTAINCE_MODE, false);
    }


    public static void setOpenContactSupport(boolean value) {
        putBoolean(OPEN_CONTACT_SUPPORT, value);
    }

    public static boolean getOpenContactSupport() {
        return getBoolean(OPEN_CONTACT_SUPPORT, false);
    }

    public static void setFingerprintDialogStatus(boolean value) {
        putBoolean(OPEN_FINGERPRINT_DIALOG, value);
    }


    public static boolean getFingerprintDialogStatus() {
        return getBoolean(OPEN_FINGERPRINT_DIALOG, false);
    }



    /**
     * Save the stored Video list
     *
     * @param downloadList Download Video List
     */
    public static void setDownloadList(List<DownloadFile> downloadList) {
        mDownloadFileList = downloadList;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(VIDEO_DOWNLOAD_LIST, new Gson().toJson(mDownloadFileList));
        editor.apply();
    }



    /**
     * Add the stored Video list
     *
     * @param downloadFile Download File
     */
    public static void addDownloadListItem(DownloadFile downloadFile) {
        mDownloadFileList.add(downloadFile);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(VIDEO_DOWNLOAD_LIST, new Gson().toJson(mDownloadFileList));
        editor.apply();
    }

    /**
     * remove the stored Video list
     *
     * @param downloadFile Download File
     */
    public static void removeDownloadListItem(DownloadFile downloadFile) {

        if(mDownloadFileList!=null || mDownloadFileList.size()>0) {
            for (DownloadFile downloadFiles : mDownloadFileList) {
                if (downloadFiles.getId().equals(downloadFile.getId())) {
                    mDownloadFileList.remove(downloadFiles);
                }
            }
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(VIDEO_DOWNLOAD_LIST, new Gson().toJson(mDownloadFileList));
            editor.apply();
        }
    }


    /**
     * Get the stored Video list
     *
     * @return the session
     */
    public static List<DownloadFile> getDownloadList() {



        if (mDownloadFileList.size()<=0 ) {
            String json = mSharedPreferences.getString(VIDEO_DOWNLOAD_LIST, null);
            if (TextUtils.isEmpty(json)) {
                mDownloadFileList = new ArrayList<>();
            } else {

                // Parse JSON to Java
                mDownloadFileList = new Gson().fromJson(json, new TypeToken<List<DownloadFile>>() {
                }.getType());
            }
        }
        return mDownloadFileList;
    }

}
