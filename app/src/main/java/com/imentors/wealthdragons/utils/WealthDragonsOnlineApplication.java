package com.imentors.wealthdragons.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.CategoryChooseActivity;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.activity.MaintenanceActivity;
import com.imentors.wealthdragons.activity.SplashActvity;
import com.imentors.wealthdragons.models.Maintenance;

import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;


/**
 * Created on 7/03/17.
 */
public class WealthDragonsOnlineApplication extends Application {

    /**
     * The default socket timeout in milliseconds
     */
    private static final int DEFAULT_TIMEOUT_MS = 10000;

    private final String TAG = WealthDragonsOnlineApplication.class.getSimpleName();

    private static WealthDragonsOnlineApplication mSharedInstance;

    private RequestQueue mRequestQueue;

    private boolean isInBackground;


    public static WealthDragonsOnlineApplication sharedInstance() {
        return mSharedInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NukeSSLCerts.nuke();
        mSharedInstance = this;
        AppPreferences.init(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }


    /**
     * Request Queue is provided to add new request
     *
     * @return Request Queue
     */
    public RequestQueue getRequestQueue() {

        if (mRequestQueue == null) {

            synchronized (this) {

                if (mRequestQueue == null) {

                    mRequestQueue = Volley.newRequestQueue(WealthDragonsOnlineApplication.sharedInstance());
                }
            }
        }

        return mRequestQueue;
    }


    /**
     * Add your request to request queue
     *
     * @param req Request
     * @param tag String
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }


    /**
     * Add your request to request queue
     *
     * @param req Request
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        req.setRetryPolicy(new DefaultRetryPolicy(
                DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }


    /**
     * Cancel your request from request queue
     *
     * @param tag String
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public void redirectToSplashActivity(Context context) {
        AppPreferences.logout();
        Utils.logoutFbSession();
        context.startActivity(new Intent(context, SplashActvity.class));
        ((Activity) context).overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);

        ((Activity) context).finish();
    }



    public void redirectToMaintanceActivity(Context context){
        AppPreferences.setMaintainceMode(true);
        AppPreferences.logout();
        Utils.logoutFbSession();
        Intent i = new Intent(context, MaintenanceActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
        ((Activity) context).overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
        ((Activity) context).finish();
    }



    public void redirectToCategoryChooserActivity(Context context){

        ActivityManager mngr = (ActivityManager) getSystemService( ACTIVITY_SERVICE );

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(1);

        if(taskList.get(0).numActivities == 1 && !taskList.get(0).topActivity.getClassName().equals(CategoryChooseActivity.class.getName())) {
            Intent i = new Intent(context, CategoryChooseActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(i);
            ((Activity) context).overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
            ((Activity) context).finish();
        }



    }



    // for certification
    public static class NukeSSLCerts {
        protected static final String TAG = "NukeSSLCerts";

        public static void nuke() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[] {
                        new X509TrustManager() {
                            public X509Certificate[] getAcceptedIssuers() {
                                X509Certificate[] myTrustedAnchors = new X509Certificate[0];
                                return myTrustedAnchors;
                            }

                            @Override
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}

                            @Override
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String arg0, SSLSession arg1) {
                        if (! arg0.equalsIgnoreCase("https://www.wealthdragonsonline.com"))
                            return true;
                        else if (! arg0.equalsIgnoreCase("http://www.wealthdragonsonline.com"))
                            return true;
                        else if (! arg0.equalsIgnoreCase("https://www.abusinesspage.com"))
                            return true;
                        else return !arg0.equalsIgnoreCase("http://www.abusinesspage.com");
                    }
                });
            } catch (Exception e) {
            }
        }
    }


    // top handle login activity animation
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) { // Works for Activity
            // Get called every-time when application went to background.
            isInBackground = true;


        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public boolean getBackGroundState(){
        if(isInBackground){
            isInBackground = false;
           return true;
        }

        return isInBackground;
    }

}
