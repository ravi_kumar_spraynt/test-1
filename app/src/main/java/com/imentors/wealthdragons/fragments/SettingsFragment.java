package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Settings;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import java.util.Map;


public class SettingsFragment extends BaseFragment {


    private LinearLayout mProgressBar;
    private Context mContext;
    private ImageView mIvWifi , mIvPlayNext , mIvNotification;
    private boolean isWifiSelected = false, isNotitficationSelected = false , isNextEpisodeSeleceted;
    private String selectedResolution = Constants.LOW  , nextEpisode ;





    public static SettingsFragment newInstance() {

        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_settings, container, false);



        mProgressBar = view.findViewById(R.id.progressBar);

//        mIvHighRes = view.findViewById(R.id.iv_selected_high_resolutoin);
//        mIvLowRes = view.findViewById(R.id.iv_selected_low_resolution);
//        mIvMedRes = view.findViewById(R.id.iv_selected_medium_resolution);
        mIvWifi = view.findViewById(R.id.iv_selected_wi_fi);
        mIvPlayNext = view.findViewById(R.id.iv_selected_play_next_video);
        mIvNotification = view.findViewById(R.id.iv_selected_notification);


        isNotitficationSelected = AppPreferences.getNotificationState();

        isWifiSelected = AppPreferences.getWifiState();


        mIvPlayNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNextEpisodeSeleceted = !isNextEpisodeSeleceted;


                if(!isNextEpisodeSeleceted) {
                    mIvPlayNext.setImageResource(R.drawable.radio_check_inactive);
                    nextEpisode = mContext.getString(R.string.manaul);
                    Utils.callEventLogApi("disabled <b>Auto Play Video Setting</b> from <b>Settings</b>");

                }
                else{
                    mIvPlayNext.setImageResource(R.drawable.radio_check_active);
                    nextEpisode = mContext.getString(R.string.auto_play);
                    Utils.callEventLogApi("enabled <b>Auto Play Video Setting</b> from <b>Settings</b>");

                }
                callSettingsApi(Api.SET_SETTINGS_API);

            }
        });



        mIvWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isWifiSelected = !isWifiSelected;

                if(isWifiSelected){
                    Utils.callEventLogApi("On <b>Wi-Fi</b> ");
                    mIvWifi.setImageResource(R.drawable.radio_check_active);
                }else{
                    Utils.callEventLogApi("OFF <b>Wi-Fi</b> ");
                    mIvWifi.setImageResource(R.drawable.radio_check_inactive);
                }

                AppPreferences.setWifiState(isWifiSelected);

            }
        });

        mIvNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNotitficationSelected = !isNotitficationSelected;

                if(isNotitficationSelected){
                    Utils.callEventLogApi("selected <b>Notification</b> ");
                    mIvNotification.setImageResource(R.drawable.radio_check_active);
                }else{
                    Utils.callEventLogApi("not selected <b>Notification</b> ");
                    mIvNotification.setImageResource(R.drawable.radio_check_inactive);
                }

                AppPreferences.setNotificationState(isNotitficationSelected);

                callSettingsApi(Api.SET_NOTIFICATION_SETTINGS_API);
            }
        });


        if(isNotitficationSelected){
            mIvNotification.setImageResource(R.drawable.radio_check_active);
        }else{
            mIvNotification.setImageResource(R.drawable.radio_check_inactive);
        }


        if(isWifiSelected){
            mIvWifi.setImageResource(R.drawable.radio_check_active);
        }else{
            mIvWifi.setImageResource(R.drawable.radio_check_inactive);
        }



        callSettingsApi(Api.GET_SETTINGS_API);



        return view;
    }

    //  refreshing api
    public void callSettingsApi(final String api) {

        if(api == Api.GET_SETTINGS_API) {
            mProgressBar.setVisibility(View.VISIBLE);
        }



        // calling setting api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    mProgressBar.setVisibility(View.GONE);

                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b> Setting api</b> error");
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                switch (api){
                    case Api.SET_SETTINGS_API:
                        params.put(Keys.DATA_USAGE, selectedResolution);
                        params.put(Keys.NEXT_EPISODE, nextEpisode);

                        break;
                    case Api.SET_NOTIFICATION_SETTINGS_API:
                        if(isNotitficationSelected){
                            params.put(Keys.ENABLE, "Yes");
                        }else{
                            params.put(Keys.ENABLE, "No");
                        }
                        params.put(Keys.TOKEN, FirebaseInstanceId.getInstance().getToken());
                        break;
                    default:
                        break;
                }


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    // handling setting api response
    @Override
    public void showDataOnView(String response)  {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {


        //  parse JSON to java
        Settings settingsData = new Gson().fromJson(response, Settings.class);

//        selectedResolution = settingsData.getData_use();
        nextEpisode = settingsData.getNext_episode();

        AppPreferences.setAutoplay(nextEpisode);

//        switch(selectedResolution){
//            case Constants.LOW:
//                mIvLowRes.setImageResource(R.drawable.radio_check_active);
//                mIvMedRes.setImageResource(R.drawable.radio_check_inactive);
//                mIvHighRes.setImageResource(R.drawable.radio_check_inactive);
//
//                break;
//            case Constants.HIGH:
//                mIvLowRes.setImageResource(R.drawable.radio_check_inactive);
//                mIvMedRes.setImageResource(R.drawable.radio_check_inactive);
//                mIvHighRes.setImageResource(R.drawable.radio_check_active);
//                break;
//            default:
//                mIvLowRes.setImageResource(R.drawable.radio_check_inactive);
//                mIvMedRes.setImageResource(R.drawable.radio_check_active);
//                mIvHighRes.setImageResource(R.drawable.radio_check_inactive);
//                break;
//        }


        switch(nextEpisode){
            case Constants.AUTO_PLAY:
                mIvPlayNext.setImageResource(R.drawable.radio_check_active);
                isNextEpisodeSeleceted = true;
                break;
            default:
                mIvPlayNext.setImageResource(R.drawable.radio_check_inactive);
                isNextEpisodeSeleceted = false;

                break;
        }


    }

    //  closed setting page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Setting </b> page");

    }
}
