package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.AboutUs;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class AboutUsFragment extends BaseFragment {


    private TextView mAboutUsContent;
    private TextView mApplicationVersion;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private LinearLayout mProgressBar;
    private Context mContext;
    private WealthDragonTextView mLoaderTitle;



    public static AboutUsFragment newInstance() {


        AboutUsFragment fragment = new AboutUsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_about, container, false);
        mProgressBar = view.findViewById(R.id.progressBar);
        mAboutUsContent = view.findViewById(R.id.tv_about_us_content);
        mApplicationVersion = view.findViewById(R.id.tv_application_version);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mLoaderTitle=view.findViewById(R.id.tv_search);

        mLoaderTitle.setText(R.string.loading_about);

        callAboutUsApi();
        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callAboutUsApi();
                Utils.callEventLogApi("refreshed <b>About Us</b> screen");

            }
        });


        return view;
    }



    public void callAboutUsApi() {

        //  set refreshing
        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
        }

        // calling about us api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.ABOUT_US_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>About us</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>About us</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        // Parse JSON to Java
        final AboutUs aboutUs = new Gson().fromJson(response, AboutUs.class);

        // setting data on view
        mAboutUsContent.setText(aboutUs.getAbout_us_text());
        mApplicationVersion.setText(" 4.0");


    }

     //  closed about us page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("<b>About Us</b> page left");
    }
}
