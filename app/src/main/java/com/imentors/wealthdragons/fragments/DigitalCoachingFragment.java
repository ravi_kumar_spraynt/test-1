package com.imentors.wealthdragons.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.media.session.PlaybackStateCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.DigitalDetailListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.ItemDecorater;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.facebook.FacebookSdk.getApplicationContext;


public class DigitalCoachingFragment extends BaseFragment implements DigitalDetailListAdapter.OnListItemClicked, PagingListener {


    private Context mContext;
    private RecyclerView mRecycleview;
    private List<DigitalCoaching> digitalCoachingArrayList = new ArrayList<>();
    private DigitalDetailListAdapter mDigitalCoachingAdapter;
    private String mVideoNameForListenerLog,mIsAutoPlay, mMentorImageUrl, mMentorName, mMentorId;
    private Timer saveSeekTime;


    private TextView mTvCourseTitle, mTvCoursePreferance, mVideoError, mTvEClassNameSmallVideo,mNextVideo,tvEClassName;
    private FrameLayout mVideoFrameLayout,mFullScreenButton, mDialogFullScreenButton;
    private ProgressBar mProgressBar;
    private ImageView mIvVideoPlay, mIvCourseImage,mFullScreenIcon;
    private PlayerView mExoPlayerView,mDialogplayerView;

    private SimpleExoPlayer mPlayer;

    private long mResumePosition;
    private boolean mExoPlayerFullscreen,isPotrait,isLandScape,isAutoPlay,isNextVideoTapped, mIsDownloaded;
    private Dialog mFullScreenDialog;

    private LinearLayout nextVideoFrame,mApiProgressBar;
    private RelativeLayout mLlHeadingContainer;
    private ImageView dialogPosterImage;
    private int mPosition = 0 , mTotalServerItem, mNextPage;
    private String mIsSubscribed,mSelectedUrl,mSelectedId,mDescription,mBannerImage,mGroupImage;
    private boolean isPaid;
    private DashBoardMentorDetails mDashBoardDetails;


    public static DigitalCoachingFragment newInstance(ArrayList<DigitalCoaching> digitalCoachingList,int totalServerItem, int nextPage,String isAutoPlay,String isSubscribed,String mentorName, String mentorImage,String mentorId
            , String selectedId,String descriptions,String bannerImage, String groupImage, boolean isDownloaded, DashBoardMentorDetails dashBoardMentorDetails) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putString(Constants.SUBSCRIPTION, isSubscribed);
        args.putString(Constants.IS_AUTOPLAY, isAutoPlay);
        args.putString(Constants.POSTER_IMAGE_URL, mentorImage);
        args.putString(Constants.CATEGORY_NAME, mentorName);
        args.putString(Constants.MENTORS_KEY, mentorId);
        args.putString(Constants.SELECTED_TAB, selectedId);
        args.putString(Constants.PUNCH_LINE,descriptions);
        args.putString(Constants.BANNER_PATH,bannerImage);
        args.putSerializable(Constants.MENTOR_DIGITAL_COACHING_LIST,digitalCoachingList);
        args.putInt(Constants.NEXT_PAGE,nextPage);
        args.putInt(Constants.TOTAL_ITEM,totalServerItem);
        args.putString(Constants.GROUP_IMAGE_PATH,groupImage);
        args.putBoolean(Constants.IS_DOWNLOADED,isDownloaded);
        args.putSerializable(Constants.DASHBOARD_VIDEO_DATA,dashBoardMentorDetails);


        DigitalCoachingFragment fragment = new DigitalCoachingFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsSubscribed =  getArguments().getString(Constants.SUBSCRIPTION);
        mIsAutoPlay =  getArguments().getString(Constants.IS_AUTOPLAY);
        mMentorImageUrl = getArguments().getString(Constants.POSTER_IMAGE_URL);
        mMentorName = getArguments().getString(Constants.CATEGORY_NAME);
        mMentorId = getArguments().getString(Constants.MENTORS_KEY);
        mSelectedId = getArguments().getString(Constants.SELECTED_TAB);
        digitalCoachingArrayList =(List<DigitalCoaching>) getArguments().getSerializable(Constants.MENTOR_DIGITAL_COACHING_LIST);
        mNextPage = getArguments().getInt(Constants.NEXT_PAGE);
        mTotalServerItem = getArguments().getInt(Constants.TOTAL_ITEM);
        mDescription=getArguments().getString(Constants.PUNCH_LINE);
        mBannerImage=getArguments().getString(Constants.BANNER_PATH);
        mGroupImage=getArguments().getString(Constants.GROUP_IMAGE_PATH);
        mIsDownloaded=getArguments().getBoolean(Constants.IS_DOWNLOADED);
        mDashBoardDetails= (DashBoardMentorDetails) getArguments().getSerializable(Constants.DASHBOARD_VIDEO_DATA);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.digital_coaching_detail, container, false);



        isPaid = mIsSubscribed.equals("Yes");
        mRecycleview = view.findViewById(R.id.listView);
        mRecycleview.setNestedScrollingEnabled(false);
        mRecycleview.setHasFixedSize(true);
        mApiProgressBar = view.findViewById(R.id.progressBar);
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCoursePreferance = view.findViewById(R.id.tv_course_desc);
        TextView tvMentorName  = view.findViewById(R.id.tv_mentor_name);
        ImageView ivMentor = view.findViewById(R.id.civ_mentor);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);

        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);
        mIvCourseImage = view.findViewById(R.id.iv_video_image);

        mExoPlayerView = view.findViewById(R.id.exoplayer);

        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);
        tvMentorName.setText(mMentorName);
        Glide.with(mContext).load(mMentorImageUrl).error(R.drawable.no_img_mentor).into(ivMentor);

//        callDcApi();




        GridLayoutManager manager = null;
        if (Utils.isTablet()) {

            manager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);

//            mRecycleview.addItemDecoration(new SpacesItemDecorationGrid(view.getContext(), 4, 3));



            mRecycleview.addItemDecoration(new ItemDecorater(
                    getResources().getDimensionPixelSize(R.dimen.margin_default),3));

        }
        else
        {
             manager = new GridLayoutManager(getContext(), 1, LinearLayoutManager.VERTICAL, false);
        }

        mRecycleview.setLayoutManager(manager);
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReloadPayResponseListener, new IntentFilter(Constants.BROADCAST_PAY_FINSIH));


        initFullscreenDialog();
        initFullscreenButton();

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(mExoPlayerView!=null && mExoPlayerView.getPlayer()!=null && mExoPlayerView.getPlayer().getPlayWhenReady()){
                    mExoPlayerView.getPlayer().setPlayWhenReady(false);
                    mExoPlayerView.setVisibility(GONE);
                    mIvCourseImage.setVisibility(VISIBLE);
                    mIvVideoPlay.setVisibility(VISIBLE);
                    mProgressBar.setVisibility(View.INVISIBLE);
                    return;
                }

            }
        });

        setData();


        return view;
    }


    private BroadcastReceiver mReloadPayResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callDcApi();
        }
    };


    private void startTimerToSendVideoSeekTime() {
        saveSeekTime = new Timer();

        saveSeekTime.schedule(new TimerTask() {
            @Override
            public void run() {
                saveSeekTime();
            }
        }, 100, 1000);

    }



    private void setDataInList() {


        //  parse JSON to java


        isAutoPlay  = TextUtils.equals("Auto Play", mIsAutoPlay);

        mDigitalCoachingAdapter = new DigitalDetailListAdapter(getContext(), this, digitalCoachingArrayList,
                this, mTotalServerItem, mNextPage,mSelectedId,mDescription,
                mMentorName,mBannerImage,mMentorId,mGroupImage,mRecycleview,mIsDownloaded,mDashBoardDetails,isPaid);
        mRecycleview.setAdapter(mDigitalCoachingAdapter);
        startTimerToSendVideoSeekTime();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mIsDownloaded){
                    mSelectedId = digitalCoachingArrayList.get(mPosition).getId();
                    mSelectedUrl = digitalCoachingArrayList.get(mPosition).getmSavedVideoAddress();
                    initExoPlayer();
                    showNextVideo();
                }else{
                    callVideoUrl(digitalCoachingArrayList.get(mPosition).getId(),mPosition);

                }

            }
        }, 1500);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReloadPayResponseListener);

        if ( mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
        Utils.releaseFullScreenPlayer();
        Utils.releaseDetailScreenPlayer();

        if (saveSeekTime != null) {
            saveSeekTime.cancel();
        }
    }

    @Override
    public void onItemClicked(DigitalCoaching item, int position) {

        if (Utils.isNetworkAvailable(mContext)) {


            if (mPlayer != null) {
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    AppPreferences.setVideoResumePosition(digitalCoachingArrayList.get(mPosition).getId(), 0);

                } else {
                    AppPreferences.setVideoResumePosition(digitalCoachingArrayList.get(mPosition).getId(), mPlayer.getContentPosition());

                }

                Utils.callEventLogApi("clicked <b>" + item.getTitle() + " </b>" + "in digital coaching");

            }


            mPosition = position;
            mIvVideoPlay.setVisibility(GONE);
            mIvCourseImage.setVisibility(GONE);

            mSelectedId = item.getId();

            if(mIsDownloaded){
                mSelectedUrl = item.getmSavedVideoAddress();
                initExoPlayer();
                showNextVideo();
            }else{
                mSelectedUrl = item.getVideo_url();
                callVideoUrl(digitalCoachingArrayList.get(mPosition).getId(), mPosition);

            }

        }

        else
        {

            if(mIsDownloaded){
                mPosition = position;
                mIvVideoPlay.setVisibility(GONE);
                mIvCourseImage.setVisibility(GONE);
                mSelectedId = item.getId();
                mSelectedUrl = item.getmSavedVideoAddress();
                initExoPlayer();
                showNextVideo();
            }else{
                Utils.showNetworkError(mContext, new NoConnectionError());
            }


        }

    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }



    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + "in digital coaching");

        isPotrait = false;
        isLandScape = true;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setText("Next Video >");
        if(digitalCoachingArrayList!=null && digitalCoachingArrayList.size()>1){
            mNextVideo.setVisibility(VISIBLE);
        }else{
            mNextVideo.setVisibility(GONE);
        }

        dialogPosterImage = dialogView.findViewById(R.id.iv_poster_image);
        dialogPosterImage.setVisibility(GONE);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNextVideoTapped = true;
                Utils.callEventLogApi("clicked <b>" + "Next Digital Coaching Video" + " </b> in digital coaching");
                playNextVideo();

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText(digitalCoachingArrayList.get(mPosition).getTitle());
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);
        dialogCourseImage.setVisibility(GONE);

        // on Next Click
        nextVideoFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + "in digital coaching");

        isPotrait = true;
        isLandScape = false;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if (isAutoPlay && digitalCoachingArrayList!=null && digitalCoachingArrayList.size()>1) {
                        mNextVideo.setVisibility(VISIBLE);
                    } else {
                        mNextVideo.setVisibility(GONE);
                    }
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }


    @Override
    public void onPause() {
        super.onPause();
        if(isLandScape) {

            if(Utils.isNetworkAvailable(mContext)){
                if(mDialogplayerView!=null && mDialogplayerView.getPlayer()!=null && mDialogplayerView.getPlayer().getPlayWhenReady()) {
                    mDialogplayerView.getPlayer().setPlayWhenReady(false);
                }
            }else{
                closeFullscreenDialog();
            }
        }else{

            if(mExoPlayerView!=null && mExoPlayerView.getPlayer()!=null && mExoPlayerView.getPlayer().getPlayWhenReady()) {
                mExoPlayerView.getPlayer().setPlayWhenReady(false);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(digitalCoachingArrayList.size()>0 && mPlayer ==null){

            mExoPlayerView.setVisibility(GONE);
            mIvCourseImage.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }else{
            if(mExoPlayerView!=null && mExoPlayerView.getPlayer()!=null && mExoPlayerView.getVisibility()== VISIBLE){
                mExoPlayerView.getPlayer().setPlayWhenReady(true);
            }

        }

    }





    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String view_type, String seeAllType) {
            callPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_DIGITAL_PAGINATION, view_type);
    }


    public void callPaginationApi(final int pageNumber, final String tasteId, String api, final String viewType) {

        // calling search top keywords pagination api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    addDataInAdapter(response);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if (!Utils.setErrorDialog(response, getContext())) {
                            setErrorMessage(viewType);
                        }
                    } catch (JSONException e1) {
                        Utils.callEventLogApi("getting <b>Pagination</b> error in api");

                        e1.printStackTrace();
                        setErrorMessage(viewType);
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Pagination</b> error in api");
                error.printStackTrace();
                if (error instanceof NoConnectionError) {
                    Utils.showNetworkError(mContext,new NoConnectionError());
                } else {
                    setErrorMessage(viewType);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if (mNextPage != 0) {
                    params.put(Keys.PAGE_NO, String.valueOf(mNextPage));
                    params.put(Keys.MENTOR_ID, mMentorId);

                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void addDataInAdapter(String response) {

        // Parse JSON to Java


        DashBoardMentorDetails mentorDetail = new Gson().fromJson(response, DashBoardMentorDetails.class);

        if (mentorDetail.getDigitalCoaching().getCoachings().size() > 0 && mentorDetail.getDigitalCoaching().getTotal_items() >= digitalCoachingArrayList.size()) {

            List<DigitalCoaching> newDigitalCoachingList = new ArrayList<>();

            for(DigitalCoaching digitalCoaching:mentorDetail.getDigitalCoaching().getCoachings()){
                newDigitalCoachingList.add(digitalCoaching);
                digitalCoachingArrayList.add(digitalCoaching);
            }


            mNextPage = mentorDetail.getDigitalCoaching().getNext_page();
            mDigitalCoachingAdapter.addItems(newDigitalCoachingList, mentorDetail.getDigitalCoaching().getTotal_items(), mentorDetail.getDigitalCoaching().getNext_page());




        }else{
            mDigitalCoachingAdapter.setServerError();

        }



    }


    //  setting server error
    private void setErrorMessage(String viewType) {
        mDigitalCoachingAdapter.setServerError();
    }









    private void showNextVideo() {
        // next video and timer according to video quantites
        if(isLandScape) {
            if (isAutoPlay) {
                mNextVideo.setVisibility(View.VISIBLE);

            } else {
                // case of intro video
                mNextVideo.setVisibility(View.GONE);
            }
        }
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mSelectedUrl);

        mVideoNameForListenerLog = digitalCoachingArrayList.get(mPosition).getTitle();
        mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);

        mResumePosition = AppPreferences.getVideoResumePosition(digitalCoachingArrayList.get(mPosition).getId());

        Glide.with(mContext).load(digitalCoachingArrayList.get(mPosition).getBanner()).error(R.drawable.no_img_course).into(mIvCourseImage);


        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching",false);


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();
        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video  <b>" + digitalCoachingArrayList.get(mPosition).getTitle()+" </b>" + "in digital coaching" );


        if(isLandScape){


            mDialogplayerView.setVisibility(VISIBLE);

            mDialogplayerView.setPlayer(mPlayer);

            tvEClassName.setText(digitalCoachingArrayList.get(mPosition).getTitle());


        }else{

            mExoPlayerView.setVisibility(View.VISIBLE);
            mIvCourseImage.setVisibility(View.INVISIBLE);
            mIvVideoPlay.setVisibility(View.INVISIBLE);

            mExoPlayerView.setPlayer(mPlayer);

        }


        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                int stat = mPlayer.getPlaybackState();
                if(mPlayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING ) {



                }
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if(playbackState == Player.STATE_ENDED ){

                    Utils.callEventLogApi("watched video <b>" + digitalCoachingArrayList.get(mPosition).getTitle()+" </b>" + "in digital coaching" );

                    if(isAutoPlay) {
                        if (playWhenReady == true && digitalCoachingArrayList!=null && digitalCoachingArrayList.size()>1){
                            isNextVideoTapped = true;
                             playNextVideo();
                    }else{
                            if(isLandScape){
                                closeFullscreenDialog();
                            }
                            mExoPlayerView.setVisibility(GONE);
                            mVideoError.setVisibility(View.INVISIBLE);
                            mProgressBar.setVisibility(View.INVISIBLE);
                            mIvCourseImage.setVisibility(View.VISIBLE);
                            mIvVideoPlay.setVisibility(VISIBLE);
                        }
                    }else{
                        if(mExoPlayerFullscreen){
                            dialogPosterImage.setVisibility(View.INVISIBLE);
                            mDialogplayerView.setVisibility(View.VISIBLE);
                        }


                        mExoPlayerView.setVisibility(GONE);
                        mVideoError.setVisibility(View.INVISIBLE);
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mIvCourseImage.setVisibility(View.VISIBLE);
                        mIvVideoPlay.setVisibility(VISIBLE);
                    }

                }

                if(playWhenReady){
                    Utils.callEventLogApi("played <b>"+mVideoNameForListenerLog+"</b> in digital coaching");

                }else{
                    Utils.callEventLogApi("paused <b>"+mVideoNameForListenerLog+"</b> in digital coaching");
                }


                if (playbackState == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mExoPlayerView.setVisibility(VISIBLE);
                    mExoPlayerView.setControllerAutoShow(true);
                    mExoPlayerView.setUseController(true);
                }


            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {
                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");
                }catch (Exception e){
                    e.printStackTrace();
                }
                // show error screen
                if(isLandScape){
                    if(!Utils.isNetworkAvailable(mContext)){
                        closeFullscreenDialog();

                        Utils.showNetworkError(mContext,new NoConnectionError());
                    }else{
                        mVideoError.setVisibility(VISIBLE);

                        mDialogplayerView.setVisibility(View.INVISIBLE);

                    }
                }else{
                    if(!Utils.isNetworkAvailable(mContext)){
                        Utils.showNetworkError(mContext,new NoConnectionError());
                    }

                    mVideoError.setVisibility(VISIBLE);
                    mExoPlayerView.setVisibility(View.INVISIBLE);

                }
                mProgressBar.setVisibility(GONE);


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch(reason){
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " " + "<b> " + mVideoNameForListenerLog + "</b> in digital coaching"  );

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }
            }
        });




        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        if(isNextVideoTapped){
            mResumePosition = 0;
            isNextVideoTapped = false;
        }

        mPlayer.seekTo(0, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);
    }

    private void playNextVideo() {
        if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
            AppPreferences.setVideoResumePosition(digitalCoachingArrayList.get(mPosition).getId(),0);

        } else {
            AppPreferences.setVideoResumePosition(digitalCoachingArrayList.get(mPosition).getId(),mPlayer.getContentPosition());

        }

        mPosition++;


        if(mPosition>= digitalCoachingArrayList.size()){
            mPosition =0;
        }




        if(mPosition< digitalCoachingArrayList.size() && isAutoPlay){
            mVideoNameForListenerLog = digitalCoachingArrayList.get(mPosition).getTitle();
            mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);
            mSelectedId = digitalCoachingArrayList.get(mPosition).getId();


            if(mIsDownloaded){
                mSelectedUrl = digitalCoachingArrayList.get(mPosition).getmSavedVideoAddress();
                initExoPlayer();
                showNextVideo();
            }else{
                mSelectedUrl = digitalCoachingArrayList.get(mPosition).getVideo_url();
                callVideoUrl(digitalCoachingArrayList.get(mPosition).getId(),mPosition);

            }



        }else{
            Utils.releaseDetailScreenPlayer();
            if(mDialogplayerView != null){
                mDialogplayerView.setVisibility(View.INVISIBLE);
            }
            mVideoError.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);
        }
    }






    private void callVideoUrl(final String mItemId, int position) {

        if(!TextUtils.isEmpty(digitalCoachingArrayList.get(position).getTitle())){
            mTvCourseTitle.setText(digitalCoachingArrayList.get(position).getTitle());

        }

        if(!TextUtils.isEmpty(digitalCoachingArrayList.get(position).getDescription())){
            mTvCoursePreferance.setText(digitalCoachingArrayList.get(position).getDescription());

        }

        mDigitalCoachingAdapter.setSelectedUrl(digitalCoachingArrayList.get(position).getId());


        if(!isPaid && digitalCoachingArrayList.get(position).getFree_paid().equals("Paid")) {

            if (!isPotrait && isLandScape) {
                closeFullscreenDialog();
            }

            if(mExoPlayerView!=null && mExoPlayerView.getPlayer()!=null){
                mExoPlayerView.getPlayer().setPlayWhenReady(false);
            }

            if(mDialogplayerView!=null){
                mDialogplayerView.setVisibility(View.INVISIBLE);
            }
            mVideoError.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mIvCourseImage.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);


            if (AppPreferences.getCreditCardList().size() > 0) {
                openDialog(PaymentCardListDialog.newInstance(mMentorId, Constants.TYPE_EXPERT, null));
            } else {
                openDialog(PayDialogFragment.newInstance("Ruppee", false, false, mMentorId, Constants.TYPE_EXPERT, null));
            }

        }else {




            if (!TextUtils.isEmpty(digitalCoachingArrayList.get(position).getVideo_url())
                    && !digitalCoachingArrayList.get(position).getVideo_url().equals("NoVideoExist")
                    &&(isPaid || !digitalCoachingArrayList.get(position).getFree_paid().equals("Paid"))
                    ){

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);
                            mSelectedUrl = jsonObject.getString("video_url");
                            initExoPlayer();
                            showNextVideo();

                        } catch (Exception e) {
                            e.printStackTrace();
                            mSelectedUrl = mSelectedUrl;
                            initExoPlayer();
                            showNextVideo();

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mSelectedUrl = mSelectedUrl;

                        initExoPlayer();
                        showNextVideo();

                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = Utils.getParams(false);
                        params.put(Keys.ITEM_ID, mItemId);

                        params.put(Keys.ITEM_TYPE, "DigitalCoaching");

                        return params;
                    }
                };

                WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


            }else{

                mVideoError.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.INVISIBLE);
                mIvCourseImage.setVisibility(View.INVISIBLE);
                mIvVideoPlay.setVisibility(View.INVISIBLE);
                if(isLandScape){
                    closeFullscreenDialog();
                }
                Toast.makeText(getApplicationContext(), "Video not available", Toast.LENGTH_SHORT).show();
            }


        }
    }


    // opened dialog
    private void openDialog(DialogFragment dialogFragment) {

        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }



    private VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                    mResumePosition = 0;
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                    mResumePosition = mPlayer.getContentPosition();
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("DigiCoaching");

                videoDataToBeSent.setVideoId(digitalCoachingArrayList.get(mPosition).getId());

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }

    public void saveSeekTime() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_VIDEO_TIME_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                VideoData mVideoData = getCurrentVideoData();

                if (mVideoData != null) {
                    params.put(Keys.VIDEOS_ID, mVideoData.getVideoId());
                    params.put(Keys.COUNTRY, mVideoData.getCountry());
                    params.put(Keys.TIME, mVideoData.getTime());
                    params.put(Keys.TYPE, mVideoData.getType());
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    public void callDcApi() {

        //  set refreshing

        mApiProgressBar.setVisibility(View.VISIBLE);


        // calling about us api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MENTOR_DETAIL_DIGITAL_COACHING_PAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mApiProgressBar.setVisibility(View.GONE);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Digital Coaching</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mApiProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Digital Coaching</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.MENTOR_ID, mMentorId);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        // Parse JSON to Java
        final DashBoardMentorDetails dashBoardItemOderingDigital = new Gson().fromJson(response, DashBoardMentorDetails.class);
        mNextPage = dashBoardItemOderingDigital.getDigitalCoaching().getNext_page();
        mTotalServerItem = dashBoardItemOderingDigital.getDigitalCoaching().getTotal_page();
        isPaid = dashBoardItemOderingDigital.getDigitalCoaching().getIs_subscribed().equals("Yes");

        digitalCoachingArrayList = dashBoardItemOderingDigital.getDigitalCoaching().getCoachings();

        setData();
    }

    // setting data on view

    private void setData(){

        for(int i=0;i<digitalCoachingArrayList.size();i++){
            DigitalCoaching digital = digitalCoachingArrayList.get(i);

            if(digital.getId().equals(mSelectedId)){
                mSelectedUrl = digital.getVideo_url();
                mPosition =i;
            }

            AppPreferences.setVideoResumePosition(digital.getId(),Utils.getTimeInMilliSeconds(digital.getVideo_seektime()));
        }

        mTvCourseTitle.setText(digitalCoachingArrayList.get(mPosition).getTitle());
        mTvCoursePreferance.setText(digitalCoachingArrayList.get(mPosition).getDescription());
        mIvVideoPlay.setVisibility(View.INVISIBLE);
        mIvVideoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mIsDownloaded){
                    mIvVideoPlay.setVisibility(GONE);
                    mIvCourseImage.setVisibility(GONE);
                    mSelectedUrl = digitalCoachingArrayList.get(mPosition).getmSavedVideoAddress();
                    initExoPlayer();
                    showNextVideo();
                }else {
                    if (Utils.isNetworkAvailable(mContext)) {
                        mIvVideoPlay.setVisibility(GONE);
                        mIvCourseImage.setVisibility(GONE);
                        callVideoUrl(digitalCoachingArrayList.get(mPosition).getId(), mPosition);
                    } else {
                        Utils.showNetworkError(mContext, new NoConnectionError());
                    }
                }
            }
        });

        mIvCourseImage.setVisibility(View.INVISIBLE);


        setDataInList();

    }


}

