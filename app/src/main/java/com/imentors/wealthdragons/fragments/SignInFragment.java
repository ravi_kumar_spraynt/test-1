package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.FingerPrintAuthenticationDialog;
import com.imentors.wealthdragons.dialogFragment.FingerPrintAuthenticationQuestionDialog;
import com.imentors.wealthdragons.interfaces.LoginFragmentChangingListener;
import com.imentors.wealthdragons.models.User;
import com.imentors.wealthdragons.models.UserCredentials;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class SignInFragment extends BaseFragment implements FingerPrintAuthenticationQuestionDialog.DialogDismissListener,FingerPrintAuthenticationDialog.DialogDismissListener{

    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private WelcomeModel mWelcomeModel;
    private int mItemPosition;
    private LoginFragmentChangingListener mListener;
    private TextView mTvSignIn , mTvFbSignIn ;
    private EditText mEtEmail , mEtPassword;
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private String name,email;
    private ProfileTracker profileTracker;
    private TextView tvFbSign;
    private ProgressBar mProgressBar;
    private String fbImage = null;
    private Serializable mNotificationData = null;
    private Context mContext;
    private String mEmail,mPassword;
    private boolean mIsFingerPrintAthentication = false;
    private boolean mIsFbLogin = false;
    private FingerPrintAuthenticationQuestionDialog fingerPrintAuthenticationQuestionDialog;
    private FingerPrintAuthenticationDialog fingerPrintAuthenticationDialog;



    public static SignInFragment newInstance(WelcomeModel mWelcomeModelData, int position, Serializable notificationData) {


        // put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.WELCOME_MODEL_DATA,mWelcomeModelData);
        args.putInt(Constants.ITEM_POSITION,position);
        if(notificationData!=null){
            args.putSerializable(Keys.NOTIFICATION,notificationData);

        }

        SignInFragment fragment = new SignInFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null){
            mWelcomeModel = (WelcomeModel) getArguments().getSerializable(Constants.WELCOME_MODEL_DATA);
            mItemPosition = getArguments().getInt(Constants.ITEM_POSITION);
            mNotificationData = getArguments().getSerializable(Keys.NOTIFICATION);

        }

        if (getActivity() instanceof LoginFragmentChangingListener) {
            mListener = (LoginFragmentChangingListener) getActivity();
        }

        callbackManager = CallbackManager.Factory.create();

        profileTracker.startTracking();

    }




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);


        LinearLayout ll_sign_up = view.findViewById(R.id.ll_sign_up);

        TextView txtSignUp = view.findViewById(R.id.tv_click_sign_up);

        TextView txtForgotPassword = view.findViewById(R.id.tv_click_forgot);

        mProgressBar = view.findViewById(R.id.progressBar);

        tvFbSign = view.findViewById(R.id.tv_blue_button);


        FrameLayout flOrange = view.findViewById(R.id.fl_btn_orange);
        FrameLayout flBlue =  view.findViewById(R.id.fl_btn_blue);


        mTvSignIn = view.findViewById(R.id.tv_orange_button);
        mTvFbSignIn = view.findViewById(R.id.tv_blue_button);

        mEtEmail = view.findViewById(R.id.et_sign_in_email);
        mEtPassword = view.findViewById(R.id.et_sign_in_password);
//        mEtEmail.setText("programming.devteam@gmail.com");
//        mEtPassword.setText("123456");

        loginButton = view.findViewById(R.id.login_button);
        loginButton.setFragment(this);


        mTvSignIn.setText(getString(R.string.sign_in));



        mEtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    mTvSignIn.requestFocus();
                    callSignInAPI(mEtEmail.getText().toString().trim(),mEtPassword.getText().toString().trim(),false);
                    return true;
                }
                return false;
            }
        });





        if(mWelcomeModel.getUserGroup().isFbButtonVisible()){
            flBlue.setVisibility(View.VISIBLE);
            mTvFbSignIn.setText(getString(R.string.sign_in_with_facebook));
        }else{
            flBlue.setVisibility(View.GONE);
        }



        if(!mWelcomeModel.getUserGroup().isSignUpButtonVisible()){
            ll_sign_up.setVisibility(View.GONE);
        }else{
            ll_sign_up.setVisibility(View.VISIBLE);
        }

        txtForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onForgotPasswordClicked();
            }
        });

        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSignUpClicked();
            }
        });

        flOrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSignInAPI(mEtEmail.getText().toString().trim(),mEtPassword.getText().toString().trim(),false);
            }
        });



        flBlue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.isNetworkAvailable(getContext())) {
                    mProgressBar.setVisibility(View.VISIBLE);

                    loginButton.performClick();
                }else{
                    Utils.showErrorDialog(getContext(), getString(R.string.error_api_no_internet_connection), getString(R.string.error_api_no_internet_connection_message), R.string.okay_text, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    },null);
                }
            }
        });

        // fb setup

        List< String > permissionNeeds = Arrays.asList("email",
                "public_profile");

        loginButton.setReadPermissions(permissionNeeds);

        loginButton.registerCallback(callbackManager,callback);


        view.getViewTreeObserver().addOnWindowFocusChangeListener(new ViewTreeObserver.OnWindowFocusChangeListener() {
            @Override
            public void onWindowFocusChanged(final boolean hasFocus) {
                // do your stuff here
                if (hasFocus) {
                    if (mEtEmail.hasFocus() ) {
                        Utils.showKeyboard(getContext(), mEtEmail);

                    }
                    if (mEtPassword.hasFocus() ) {
                        Utils.showKeyboard(getContext(), mEtPassword);
                    }
                }
            }
            });






        mEtEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (!AppPreferences.getFingerprintDialogStatus()) {
                    AppPreferences.setFingerprintDialogStatus(true);
                    if (AppPreferences.getUserCredentials().isCredentailsSaved()) {

                        if (Utils.isFingerPrintAvailable(getActivity())) {
                            fingerPrintAuthenticationDialog
                                    = new FingerPrintAuthenticationDialog();
                            fingerPrintAuthenticationDialog.setListener(SignInFragment.this);
                            fingerPrintAuthenticationDialog.show(getActivity().getFragmentManager(), DIALOG_FRAGMENT_TAG);
                        }
                    }

                }
                return false;
            }
        });



        return view;
    }



    public void callSignInAPI(String mailId, String pass, boolean isFingerprintAuthentication) {

        mIsFingerPrintAthentication = isFingerprintAuthentication;

            Utils.hideKeyboard(getContext());
            Utils.logoutFbSession();


            // check for validation

            mEmail = null;
            mPassword = null;

            if (TextUtils.isEmpty(mailId) && TextUtils.isEmpty(pass)) {

                mEtEmail.requestFocus();
                Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_email_password_blank), getString(R.string.okay_text), false, true);
                return;


            } else if (TextUtils.isEmpty(mailId)) {

                mEtEmail.requestFocus();
                Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_email_blank), getString(R.string.okay_text), false, true);
                return;

            } else if (!Utils.isValidEmail(mailId)) {

                mEtEmail.requestFocus();
                Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_email), getString(R.string.okay_text), false, true);
                return;

            } else if (TextUtils.isEmpty(pass)) {

                mEtPassword.requestFocus();
                Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_password_blank), getString(R.string.okay_text), false, true);
                return;

            }


            mTvSignIn.setText(getString(R.string.loading));
            mProgressBar.setVisibility(View.VISIBLE);

            disableTouch();

            mEmail = mailId;
            mPassword = pass;


        // calling sign in api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SIGN_IN_API, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    mProgressBar.setVisibility(View.GONE);

                    mTvSignIn.setText(getString(R.string.signing_in));
                    disableTouch();
                    try {
                        onGetDataAPIServerResponse(response);
                        mTvSignIn.setText(getString(R.string.sign_in));
                    } catch (Exception e) {

                        if (isAdded()) {
                            Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false, true);
                        }
                        mTvSignIn.setText(getString(R.string.sign_in));
                        enableTouch();

                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                    if (isAdded()) {
                        Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false, true);
                    }
                    mTvSignIn.setText(getString(R.string.sign_in));
                    enableTouch();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = Utils.getParams(true);
                    params.put(Keys.USER_EMAIL, mEmail);
                    params.put(Keys.USER_PASSWORD, mPassword);

                    return params;
                }
            };


            WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // handling sign in api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
//        Utils.logoutFbSession();

        if(response!=null) {

            JsonReader reader = new JsonReader(new StringReader(response));
            reader.setLenient(true);

            //  parse JSON to java
            User user = new Gson().fromJson(reader, User.class);

            //  set user data in app preferences
            AppPreferences.setUserData(user);

            if(!TextUtils.isEmpty(fbImage)) {
                callFbProfileApi();
            }


            // if user comes from fingerprint dialog then redirect it to dashboard otherwise show fingerprint authentication chooser dialog
            if(mIsFingerPrintAthentication || mIsFbLogin || !Utils.isFingerPrintAvailable(getActivity())){

                if(mIsFbLogin){
                    // save no credentails in case of fb login
                    saveNoUserCredentials();
                }else {
                    // for fingerprintAuthentication
                    saveUserCredentials();
                }

                redirectUserToDestintaion(user);

            }else{

                fingerPrintAuthenticationQuestionDialog
                        = new FingerPrintAuthenticationQuestionDialog();
                fingerPrintAuthenticationQuestionDialog.setListener(SignInFragment.this);
                fingerPrintAuthenticationQuestionDialog.show(getActivity().getFragmentManager(), DIALOG_FRAGMENT_TAG);

            }



        }else{
            mTvSignIn.setText(getString(R.string.sign_in));
            tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());
            enableTouch();
            if(isAdded()) {
                Utils.showAlertDialog(getContext(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
        }


    }

    private void redirectUserToDestintaion(User user) {
        if(user.isDashboard()){
            Intent intent = new Intent();
            Serializable notificationHashMap = ((LoginActivity)getActivity()).getNotificationData();
            if(notificationHashMap != null) {
                intent.putExtra(Keys.NOTIFICATION, notificationHashMap);
            }
            intent.setClass(getContext(), MainActivity.class);
            startActivity(intent);
            getActivity().overridePendingTransition(R.anim.slide_in_from_right,R.anim.no_animation);
            getActivity().finish();

        }else{
            SubscriptionFragment fragment  = SubscriptionFragment.newInstance(true,mNotificationData);
            addFragment(fragment,true);
            mTvSignIn.setText(getString(R.string.sign_in));
            tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());

        }

        setAllFields();
    }

    // save user credentials
    private void saveUserCredentials() {
        UserCredentials credentials = new UserCredentials();
        credentials.setEmail(mEmail);
        credentials.setPassword(mPassword);
        credentials.setCredentailsSavedStatus(true);
        AppPreferences.setUserCredentails(credentials);
    }


    @Override
    public void onActivityResult(int requestCode, int responseCode,
                                 Intent data) {
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }



    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {@Override
    public void onSuccess(LoginResult loginResult) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {@Override
                public void onCompleted(JSONObject object,
                                        GraphResponse response) {

                    Log.i("LoginActivity",
                            response.toString());

                    mProgressBar.setVisibility(View.GONE);

                    try {

                        name = object.getString("name");
                        email = object.getString("email");
//                        firstName = object.getString("first_name");
//                        lastName = object.getString("last_name");
                        JSONObject pictureData = object.getJSONObject("picture");
                        JSONObject imageData = pictureData.getJSONObject("data");
                        String imageUrl = imageData.getString("url");

                        Glide.with(WealthDragonsOnlineApplication.sharedInstance())
                                .load(imageUrl)
                                .asBitmap()
                                .fitCenter() //fits given dimensions maintaining ratio
                                .into(new SimpleTarget<Bitmap>() {
                                          @Override
                                          public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                              fbImage = Utils.getStringImage(resource);
                                          }
                                      }
                                );

                        tvFbSign.setText(getString(R.string.signing_in));
                        disableTouch();
                        callFbLoginApi();

                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(isAdded()) {
                            Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                        }
                        enableTouch();
                    }
                }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields",
                "name,email,first_name,last_name,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

        @Override
        public void onCancel() {
            System.out.println("onCancel");
            mProgressBar.setVisibility(View.GONE);

            enableTouch();

        }

        @Override
        public void onError(FacebookException exception) {
            System.out.println("onError");
            mProgressBar.setVisibility(View.GONE);
            Log.v("LoginActivity", exception.getCause().toString());
            if(isAdded()) {
                Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
            enableTouch();

        }
    };

    private void callFbLoginApi() {

        Utils.hideKeyboard(getContext());
//        Utils.logoutFbSession();

        mProgressBar.setVisibility(View.VISIBLE);

        // calling facebook login api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.FB_LOG_IN_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);

                try {
                    // we need not require to show fingerprint question dialog on fb login
                    mIsFbLogin = true;
                    onGetDataAPIServerResponse(response);
                    tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());


                } catch (Exception e) {
                    if(isAdded()) {
                        Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                    }
                    tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());
                    enableTouch();
                    e.printStackTrace();
                    Utils.logoutFbSession();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                error.printStackTrace();
                if(isAdded()) {
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
                tvFbSign.setText(mWelcomeModel.getUserGroup().getFbBtnText());
                enableTouch();
                Utils.logoutFbSession();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(true);

                params.put(Keys.EMAIL, email);
                params.put(Keys.FIRST_NAME, name);
//                params.put(Keys.LAST_NAME, lastName);
                if(!TextUtils.isEmpty(fbImage)){
                    params.put(Keys.IMAGE,fbImage);
                }
                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }


    // setting text fields
    private void setAllFields(){
        mEtEmail.setText("");
        mEtPassword.setText("");
        mTvSignIn.setText(getString(R.string.sign_in));
        if(mWelcomeModel.getUserGroup().isFbButtonVisible()){
            mTvFbSignIn.setText(getString(R.string.sign_in_with_facebook));
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        profileTracker.stopTracking();


    }


    @Override
    public void onDialogSuccessDismiss() {
        if(fingerPrintAuthenticationQuestionDialog !=  null) {
            // keyboard is appearing after dialog dismiss
            if (fingerPrintAuthenticationQuestionDialog.isVisible()) {
                fingerPrintAuthenticationQuestionDialog.dismiss();
            }

            saveUserCredentials();

            redirectUserToDestintaion(AppPreferences.getUser());

        }
    }

    @Override
    public void onDialogCancelDismiss() {
        if(fingerPrintAuthenticationQuestionDialog !=  null) {
            // keyboard is appearing after dialog dismiss
            if (fingerPrintAuthenticationQuestionDialog.isVisible()) {
                fingerPrintAuthenticationQuestionDialog.dismiss();
            }
            saveNoUserCredentials();


            redirectUserToDestintaion(AppPreferences.getUser());



        }
    }

    private void saveNoUserCredentials() {
        //  save user credentials for fingerprintAuthentication with false
        UserCredentials credentials = new UserCredentials();
        AppPreferences.setUserCredentails(credentials);
    }

    @Override
    public void onDialogDismiss() {
        if (fingerPrintAuthenticationDialog != null) {
            if (fingerPrintAuthenticationDialog.isVisible()) {
                fingerPrintAuthenticationDialog.dismiss();
            }

          callSignInAPI(AppPreferences.getUserCredentials().getEmail(), AppPreferences.getUserCredentials().getPassword(), true);

        }
    }

    private void callFbProfileApi() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_FB_IMAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    String res = response;

                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(false);

                if(!TextUtils.isEmpty(fbImage)){
                    params.put(Keys.IMAGE,fbImage);
                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }


}
