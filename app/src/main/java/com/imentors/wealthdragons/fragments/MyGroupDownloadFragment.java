package com.imentors.wealthdragons.fragments;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.adapters.DownLoadDetailGroupListAdapter;
import com.imentors.wealthdragons.courseDetailView.DetailItemListView;

import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.GroupDownloadFile;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2core.Func;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.VISIBLE;

public class MyGroupDownloadFragment extends BaseFragment  {


    private Context mContext;
    List<DownloadFile> list = AppPreferences.getDownloadList();
    private Fetch fetch;
    private List<GroupDownloadFile> mGroupDownloadList = new ArrayList<>();
    private List<Integer> mIdsList = new ArrayList<>();
    private View mProgressBar,mNoData;
    private LinearLayoutManager layoutManager;
    private TextView mLoaderTitle,mTvNoData;
    private boolean isWishlistHeadingVisible;
    private LinearLayout mLlScreenContainer;


    public static MyGroupDownloadFragment newInstance() {

        //  put data in bundle
        Bundle args = new Bundle();

        MyGroupDownloadFragment fragment = new MyGroupDownloadFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard_details, container, false);


        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        view.findViewById(R.id.swipeRefreshLayout).setEnabled(false);
        mLoaderTitle = view.findViewById(R.id.tv_search);
        mNoData = view.findViewById(R.id.no_data);
        mTvNoData = view.findViewById(R.id.tv_no_Data);

        mLoaderTitle.setText(getString(R.string.loading_download_list));

        return view;
    }

    private void showData() {
        mProgressBar.setVisibility(VISIBLE);

        if(mIdsList !=null && mIdsList.size()>0){

            mGroupDownloadList.clear();
            for(int i=0;i<mIdsList.size();i++){

                int id = mIdsList.get(i);

                final int finalI = i;
                fetch.getDownloadsInGroup(id, new Func<List<Download>>() {
                    @Override
                    public void call(@NotNull List<Download> result) {

                        if(result!=null && result.size()>0) {
                            Download download = result.get(0);

                            GroupDownloadFile groupDownloadFile = new GroupDownloadFile(download.getGroup(),
                                    download.getExtras().getString(Constants.GROUP_TYPE, Constants.TYPE_COURSE),
                                    result.size(),
                                    download.getExtras().getString(Constants.GROUP_TITLE, Constants.TYPE_COURSE),
                                    download.getExtras().getString(Constants.GROUP_IMAGE_PATH, Constants.TYPE_COURSE),
                                    download.getStatus(),
                                    download.getExtras().getString(Constants.DATA, Constants.TYPE_COURSE)
                            );
                            groupDownloadFile.setmDownloadItemList(result);

                            mGroupDownloadList.add(groupDownloadFile);
                        }

                        if(finalI == mIdsList.size()-1){
                            showList();
                        }

                    }
                });
            }
        }else{
            mNoData.setVisibility(View.VISIBLE);
            mTvNoData.setText(getString(R.string.no_download_videos));
            mProgressBar.setVisibility(View.GONE);
        }


    }

    private void showList() {
        mLlScreenContainer.removeAllViews();
        mNoData.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        isWishlistHeadingVisible = false;


         List<Article> articlesList = new ArrayList<>();
        List<Mentors> mentorsList = new ArrayList<>();


        for(GroupDownloadFile groupDownloadFile:mGroupDownloadList){
            GsonBuilder gsonBuilder = new GsonBuilder();

            // Parse JSON to Java
            gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
            final Gson gson = gsonBuilder.create();


            if(groupDownloadFile.getmType().equals(Constants.TYPE_COURSE)){
                DashBoardCourseDetails dashBoardCourseDetails = gson.fromJson(groupDownloadFile.getmSavedResponse(), DashBoardCourseDetails.class);

                articlesList.add(new Article(dashBoardCourseDetails.getCourseDetail().getId(),dashBoardCourseDetails.getCourseDetail().getTitle(),groupDownloadFile.getmGroupBannerAddress(),
                        dashBoardCourseDetails.getCourseDetail().getMentors().get(0).getName(),dashBoardCourseDetails.getCourseDetail().getMentors().get(0).getPunch_line(),groupDownloadFile.getmSavedResponse(),true));



            }

            if(groupDownloadFile.getmType().equals(Constants.TYPE_MENTOR) || groupDownloadFile.getmType().equals(Constants.TYPE_EXPERT)){
//                isWishlistHeadingVisible = true;
                DashBoardMentorDetails dashBoardMentorDetails = gson.fromJson(groupDownloadFile.getmSavedResponse(), DashBoardMentorDetails.class);

                mentorsList.add(new Mentors(dashBoardMentorDetails.getProfile().getId(),dashBoardMentorDetails.getProfile().getName()
                        ,groupDownloadFile.getmGroupBannerAddress(),dashBoardMentorDetails.getProfile().getPunch_line(),groupDownloadFile.getmSavedResponse(),true));


            }
        }



        if(articlesList.size()>0){
            isWishlistHeadingVisible = true;
            mLlScreenContainer.addView(new DetailItemListView(mContext, articlesList, null,mContext.getString(R.string.my_downloads), 0, 0, null, Constants.TYPE_WISHLLIST_FEATURED_COURSES,false,true));

        }


        if(mentorsList.size()>0){
            mLlScreenContainer.addView(new DetailItemListView(mContext, mentorsList, null,isWishlistHeadingVisible?"Mentors":mContext.getString(R.string.my_downloads),0, mentorsList.size(), Constants.TYPE_WISHLLIST_FEATURED_MENTORS,null,true));
            isWishlistHeadingVisible = true;

        }



    }

    @Override
    public void onResume() {
        super.onResume();
        if(mIdsList==null || mIdsList.size()<=0) {
            fetch = Utils.getFetchInstance();
            fetch.addListener(fetchListener);
            fetch.getAllGroupIds(new Func<List<Integer>>() {
                @Override
                public void call(@NotNull List<Integer> result) {
                    mIdsList = result;
                    showData();

                }
            });
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        fetch.removeListener(fetchListener);
    }





    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {



        }


        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {



        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {

        }


        @Override
        public void onRemoved(@NotNull Download download) {

            fetch.getAllGroupIds(new Func<List<Integer>>() {
                @Override
                public void call(@NotNull List<Integer> result) {
                    mIdsList = result;
                    showData();

                }
            });


        }

        @Override
        public void onDeleted(@NotNull Download download) {

            fetch.getAllGroupIds(new Func<List<Integer>>() {
                @Override
                public void call(@NotNull List<Integer> result) {
                    mIdsList = result;
                    showData();

                }
            });


        }




    };

}


