package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.DashBoardViewPagerAdapter;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.utils.Utils;


public class DashBoardPagerFragment extends BaseFragment  {


    /**
     * use always for slider while getting position  = position % itemList.size()
     */


    private Context mContext;
    private ViewPager mViewPager;
    private DashBoardViewPagerAdapter mDashBoardViewPagerAdapter;

    public static DashBoardPagerFragment newInstance() {

        Bundle args = new Bundle();
        DashBoardPagerFragment fragment = new DashBoardPagerFragment();
        fragment.setArguments(args);
        return fragment;

    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_dashboard_viewpager, container, false);


        mViewPager = view.findViewById(R.id.fragments_viewpager);
        mDashBoardViewPagerAdapter = new DashBoardViewPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager.setAdapter(mDashBoardViewPagerAdapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.setCurrentItem(0);
        Utils.hideKeyboard(mContext);



        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity activity = (MainActivity) getActivity();
                activity.setTabSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        return view;
    }


    public void onTabChanged(int itemsCount , DashBoard dashBoard) {

        if(dashBoard!=null){
        if(dashBoard.getPlanSetting()!= null && mDashBoardViewPagerAdapter!=null) {
            mViewPager.setOffscreenPageLimit(itemsCount);
            mDashBoardViewPagerAdapter.setCount(itemsCount, dashBoard);

        }
        }
    }


    public int getCurrentItem(){
       return mViewPager.getCurrentItem();
    }


    public void setCurrentItem(int position){
        Utils.hideKeyboard(mContext);
        mViewPager.setCurrentItem(position);
    }

    public Fragment getCurrentFragment(){
        return mDashBoardViewPagerAdapter.getCurrentFragment();
    }

}
