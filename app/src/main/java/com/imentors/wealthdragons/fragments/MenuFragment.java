package com.imentors.wealthdragons.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.MenuExpandableListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.SubscribeDialogFragment;
import com.imentors.wealthdragons.interfaces.CategoryItemInteraction;
import com.imentors.wealthdragons.models.Categories;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


/**
 * Created on 7/03/17.
 */
public class MenuFragment extends BaseFragment implements Utils.DialogInteraction {

    //activity fragment communication
    private CategoryItemInteraction mListener;
    private Context mContext;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ExpandableListView mListView;
    private ViewGroup headerView ;
    private Categories mData = null;
    private MenuExpandableListAdapter menuAdapter;
    private WealthDragonTextView mUserName;
    private ImageView mUserImageView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_menu, container, false);



        mListView = view.findViewById(R.id.listView);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);


        menuAdapter = new MenuExpandableListAdapter(getActivity());






        mListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if(mListener!=null){
                    if(Utils.isNetworkAvailable(mContext)){
                        mListener.onCategoryMenuClick((Category) menuAdapter.getGroup(groupPosition ));

                        menuAdapter.selctedItemPosition(((Category) menuAdapter.getGroup(groupPosition)).getTitle(), ((Category) menuAdapter.getGroup(groupPosition)).getId(), -1);

                    }else{

                        try{

                            int title = Integer.parseInt(((Category) menuAdapter.getGroup(groupPosition )).getTitle());
                            if(!getString(title).equals(getString(R.string.my_downloads))) {
                                Utils.showNetworkError(mContext, new NoConnectionError());
                            }else{
                                mListener.onCategoryMenuClick((Category) menuAdapter.getGroup(groupPosition ));

                                menuAdapter.selctedItemPosition(((Category) menuAdapter.getGroup(groupPosition)).getTitle(), ((Category) menuAdapter.getGroup(groupPosition)).getId(), -1);
                            }

                        }catch (NumberFormatException n){
                            Utils.showNetworkError(mContext, new NoConnectionError());

                        }

                    }

                }
                return false;
            }
        });



        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(mListener!=null){
                    mListener.onCategoryChildClick((Category.SubCategory) menuAdapter.getChild(groupPosition,childPosition));
                    menuAdapter.selctedItemPosition(((Category.SubCategory) menuAdapter.getChild(groupPosition,childPosition)).getTitle(),((Category.SubCategory) menuAdapter.getChild(groupPosition,childPosition)).getId(),groupPosition);
                }
                return false;
            }
        });


        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        mListView.setAdapter(menuAdapter);

        headerView = view.findViewById(R.id.layout_header);


        mUserName = headerView.findViewById(R.id.txt_name);
        mUserImageView = headerView.findViewById(R.id.imageView);
        ImageView closeDrawer = headerView.findViewById(R.id.imv_cross);

        closeDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("closed<b> Menu</b>");
                mListener.onCrossClick();
            }
        });

        View logoutFooterView = inflater.inflate(R.layout.category_footer,mListView,false);

        mListView.addFooterView(logoutFooterView,null,false);

        logoutFooterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Utils.callEventLogApi("clicked <b>Logout</b> from <b>Sidebar Menu</b>");

                showSignOutDialogBox();

            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callCategoriesApi(true);
                Utils.callEventLogApi("refreshed<b> Menu </b>");

            }
        });



        //  get data from app preferences
        mData = AppPreferences.getCategories();

        if(savedInstanceState==null) {
            callCategoriesApi(true);
            intializeDataView(new Categories(Utils.addStaticCategories(mData), null, null, null, 0));
        }


        return view;
    }

    public void callCategoriesApi(final boolean isRefreshingVisible) {

        //  set refreshing
        if(isRefreshingVisible) {

            mSwipeRefreshLayout.setRefreshing(true);
        }


        // calling categories api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.CATEGORIES_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String res = response;
                try {
                    mSwipeRefreshLayout.setRefreshing(false);
                    // not show error dialog on frequent call
                    if(isRefreshingVisible){
                        onGetDataAPIServerResponse(response);
                    }else{
                        showDataOnView(response);
                    }
                } catch (Exception e) {

                    Utils.callEventLogApi("getting <b>Menu </b> error in api " );

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (isRefreshingVisible) {

                    mSwipeRefreshLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Menu </b> error in api " );

                    error.printStackTrace();
                    if (isAdded()) {
                        if(!Utils.isNetworkAvailable(mContext)){
                            intializeDataView(new Categories(Utils.addStaticCategories(mData), null, null, null, 0));

                        }else{

                            Utils.showAlertDialog(mContext, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false, true);


                        }



                    }
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling categories api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);

            if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {

                WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(mContext);

            }else{

                //  parse JSON to java
                Categories categroies = new Gson().fromJson(response, Categories.class);

                if(categroies.showSubscriptionPopUp()){
                    SubscribeDialogFragment dialogFrag = SubscribeDialogFragment.newInstance();
                    dialogFrag.setCancelable(false);
                    dialogFrag.show(getFragmentManager(),"dialog");
                }


                if(categroies !=null && categroies.getUserData()!=null) {

                    String previousCategories = AppPreferences.getCategoriesJson();


                    if (!TextUtils.isEmpty(previousCategories)) {

                        if (previousCategories.equals(response)) {
                            //  required to do changes in list if list contains null items
                            if(menuAdapter.getGroupCount()>0){
                                // do not change
                            }else{
                                AppPreferences.setCategoriesJson(response);
                                AppPreferences.setCategories(categroies);
                                intializeDataView(categroies);
                            }

                        } else {
                            AppPreferences.setCategoriesJson(response);
                            AppPreferences.setCategories(categroies);
                            intializeDataView(categroies);
                        }
                    } else {
                        AppPreferences.setCategoriesJson(response);
                        AppPreferences.setCategories(categroies);
                        intializeDataView(categroies);
                    }
                }


            }
        }catch (JSONException e){
            e.printStackTrace();

        }



    }

    //  set data in view
    private void intializeDataView(Categories categroies) {


            menuAdapter.clearData();

            if (categroies.getUserData() != null) {

                headerView.setVisibility(View.VISIBLE);


                if (!TextUtils.isEmpty(categroies.getUserData().getUserName())) {
                    mUserName.setText(categroies.getUserData().getUserName() + " " + "(" + AppPreferences.getUser().getService_code() + ")");
                }
                Glide.with(mContext).load(categroies.getUserData().getUserImage()).into(mUserImageView);

            } else {
                headerView.setVisibility(View.GONE);
            }

            int unreadMessageCount = 0;

            if (categroies.getUserData() == null) {
                unreadMessageCount = 0;
            } else {
                unreadMessageCount = categroies.getUserData().getUnreadMessages();
            }


            menuAdapter.setItems(categroies.getAllCategory(), categroies.isIs_static_content(), categroies.getStaticCategoriesSize(), unreadMessageCount);
            mListView.setAdapter(menuAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext= context;
        if (context instanceof CategoryItemInteraction) {
            mListener = (CategoryItemInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    private void callSignOutApi() {

        final ProgressDialog pDialog = new ProgressDialog(mContext);
        pDialog.setMessage(mContext.getString(R.string.logging_out));
        pDialog.setCancelable(false);
        pDialog.show();


        // calling sign out api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SIGN_OUT_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pDialog.dismiss();
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    Utils.callEventLogApi("getting <b>Sign Out </b> error in api " );

                    e.printStackTrace();
                    WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(mContext);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Sign Out </b> error in api " );

                pDialog.dismiss();
                WealthDragonsOnlineApplication.sharedInstance().redirectToSplashActivity(mContext);

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    //  showing alert dialog
    private void showSignOutDialogBox() {
        Utils.callEventLogApi("opened <b>Logout</b> dialog in sidebar menu");
        Utils.showTwoButtonAlertDialog(this,mContext,getString(R.string.dialog_title),getString(R.string.dialog_message_logout),null,null);
    }
    @Override
    public void onPositiveButtonClick(AlertDialog dialog,boolean isNotification) {
        callSignOutApi();
        Utils.callEventLogApi("clicked <b>Yes</b> button for <b>Logout Confirmation</b>");
        dialog.dismiss();
    }

    @Override
    public void onNegativeButtonClick(AlertDialog dialog,boolean isNotification) {

        dialog.dismiss();
        mListener.onCrossClick();
        Utils.callEventLogApi("clicked <b>No</b> button for <b>Logout Confirmation</b>");

    }


    public void setSelectedItemInAdapter(){
        menuAdapter.selctedItemPosition(null,null,-1);
        intializeDataView(AppPreferences.getCategories());

    }


}
