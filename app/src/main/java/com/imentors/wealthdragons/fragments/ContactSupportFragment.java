package com.imentors.wealthdragons.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.LoginActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class ContactSupportFragment extends BaseFragment {

    private EditText mEtEmail, mEtFirstName, mEtLastName, mEtMessage, mEtSubject;
    private WealthDragonTextView mContactSupportButton;
    private ProgressBar mProgressBar;


    public static ContactSupportFragment newInstance() {
        ContactSupportFragment fragment = new ContactSupportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this.getContext()).registerReceiver(mDismissAlertDialog,
                new IntentFilter(Constants.BROADCAST_DISMISS_ALERT_DIALOG));

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_contact_support, container, false);

        Utils.callEventLogApi("opened <b>Contact Support</b> page" );

        Toolbar mToolbar = view.findViewById(R.id.login_toolbar);

        mProgressBar = view.findViewById(R.id.progressBar);

        TextView txtStatus = mToolbar.findViewById(R.id.tv_singing_status);

        txtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((LoginActivity)getActivity()).onSignInClicked();
                getActivity().onBackPressed();

            }
        });

        mToolbar.setNavigationIcon(R.drawable.back_arrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });


        mEtFirstName = view.findViewById(R.id.et_support_first_name);


        mEtLastName = view.findViewById(R.id.et_support_last_name);

        mEtEmail = view.findViewById(R.id.et_support_email_address);

        mEtSubject = view.findViewById(R.id.et_support_subject);

        mEtMessage = view.findViewById(R.id.et_support_message);

        mContactSupportButton = view.findViewById(R.id.tv_orange_button);


        mContactSupportButton.setText(getString(R.string.contact_support));

        mContactSupportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Contact Support</b> button" );
                callContactSupportAPI();

            }
        });


//        mEtFirstName.setText("Ravi");
//        mEtLastName.setText("Kumar");
//        mEtEmail.setText("programming.devteam@gmail.com");
//        mEtSubject.setText("complain");
//        mEtMessage.setText("activate my account");


        mEtFirstName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mEtLastName.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mEtLastName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mEtEmail.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mEtEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mEtSubject.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mEtSubject.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mEtMessage.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mEtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    mContactSupportButton.requestFocus();
                    callContactSupportAPI();
                    return true;
                }
                return false;
            }
        });


        return view;
    }

    private void callContactSupportAPI() {

        Utils.hideKeyboard(getContext());


        // check for validation

        String email = null;
        String lastName = null;
        String firstName = null;
        String message = null;
        String subject = null;


        if (TextUtils.isEmpty(mEtFirstName.getText().toString())) {

            mEtFirstName.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_first_name_blank), getString(R.string.okay_text), false,true);
            return;

        } else if (TextUtils.isEmpty(mEtLastName.getText().toString())) {

            mEtLastName.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_last_name_blank), getString(R.string.okay_text), false,true);
            return;

        } else if (TextUtils.isEmpty(mEtEmail.getText().toString())) {

            mEtEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_email_blank), getString(R.string.okay_text), false,true);
            return;

        } else if (!Utils.isValidEmail(mEtEmail.getText().toString())) {

            mEtEmail.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_incorrect_email), getString(R.string.okay_text), false,true);
            return;

        } else if (TextUtils.isEmpty(mEtSubject.getText().toString())) {

            mEtSubject.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_subject_blank), getString(R.string.okay_text), false,true);
            return;

        }  else if (TextUtils.isEmpty(mEtMessage.getText().toString())) {

            mEtMessage.requestFocus();
            Utils.showAlertDialog(this.getContext(), getString(R.string.error), getString(R.string.error_message_blank), getString(R.string.okay_text), false,true);
            return;

        }


        firstName = mEtFirstName.getText().toString();
        lastName = mEtLastName.getText().toString();
        email = mEtEmail.getText().toString();
        subject = mEtSubject.getText().toString();
        message = mEtMessage.getText().toString();


        final String finalEmail = email;
        final String finalSubject = subject;
        final String finalLastName = lastName;
        final String finalFirstName = firstName;
        final String finalMessage = message;

        mContactSupportButton.setText(getString(R.string.processing));
        mProgressBar.setVisibility(View.VISIBLE);
        disableTouch();


        // calling contact support api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.CONTACT_SUPPORT_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);

                try {
                    onGetDataAPIServerResponse(response);
                    mContactSupportButton.setText(getString(R.string.contact_support));
                    enableTouch();
                } catch (Exception e) {
                    mContactSupportButton.setText(getString(R.string.contact_support));
                    enableTouch();
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                    Utils.callEventLogApi("getting <b>Contact Support</b> error in api" );
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Contact Support</b> error in api" );
                error.printStackTrace();
                mContactSupportButton.setText(getString(R.string.contact_support));
                enableTouch();
                if(isAdded()) {
                    Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {


                Map<String, String> params = Utils.getParams(true);

                params.put(Keys.EMAIL, finalEmail);
                params.put(Keys.SUBJECT, finalSubject);
                params.put(Keys.FIRST_NAME, finalFirstName);
                params.put(Keys.LAST_NAME, finalLastName);
                params.put(Keys.MESSAGE, finalMessage);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    // handling contact support api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);


        JSONObject responseObject = null;
        try {
            // Parse JSON to Java
            responseObject = new JSONObject(response);


            //  show alert dialog
        if(!TextUtils.isEmpty(responseObject.optString(Keys.SUCCESS))){
            if(isAdded()) {
                Utils.showAlertDialog(getActivity(), getString(R.string.success), responseObject.getString(Keys.SUCCESS), getString(R.string.okay_text), true,false);
            }
            }

        } catch (JSONException e) {
            if(isAdded()) {
                Utils.showAlertDialog(getActivity(), getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), false,true);
            }
            e.printStackTrace();
        }


    }


    // setting data on view
    private void setAllFields(){
        mEtFirstName.setText("");
        mEtLastName.setText("");
        mEtEmail.setText("");
        mEtEmail.setText("");
        mEtMessage.setText("");
        mContactSupportButton.setText(getString(R.string.contact_support));
    }

    private BroadcastReceiver mDismissAlertDialog = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            popToRootFragment();
            setAllFields();


        }
    };

    //  closed contact support page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Contact Support</b> page" );
        LocalBroadcastManager.getInstance(this.getActivity()).unregisterReceiver(mDismissAlertDialog);
    }


}
