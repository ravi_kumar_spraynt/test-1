package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.SearchTabView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

public class SearchFragment extends BaseFragment {


    /**
     * use always for slider while getting position  = position % itemList.size()
     */


    private Context mContext;
    private RecyclerView mRecyclerViewpager;
    private LinearLayout mProgressBar,mNoDataFound;
    private LinearLayout mLlScreenContainer;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private String mSearchKey;
    private String mSearchId;
    private String mSearchType;
    private boolean isDataAvailable = false;
    private WealthDragonTextView mProgressBarText;




    public static SearchFragment newInstance(String  search, String id, String type) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.SEARCH_ITEM,search);

        if(!TextUtils.isEmpty(id)){
            args.putString(Constants.SEARCH_ID,id);

        }

        if(!TextUtils.isEmpty(type)){
            args.putString(Constants.SEARCH_TYPE,type);

        }

        SearchFragment fragment = new SearchFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean hasSearchItemKey = getArguments().containsKey(Constants.SEARCH_ITEM);
        boolean hasSearchTypeKey = getArguments().containsKey(Constants.SEARCH_TYPE);
        boolean hasSearchIdKey = getArguments().containsKey(Constants.SEARCH_ID);

        if(hasSearchItemKey){
            mSearchKey = getArguments().getString(Constants.SEARCH_ITEM);
        }

        if(hasSearchTypeKey){
            mSearchType = getArguments().getString(Constants.SEARCH_TYPE);
        }

        if(hasSearchIdKey){
            mSearchId = getArguments().getString(Constants.SEARCH_ID);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);




        mProgressBar = view.findViewById(R.id.progressBar);
        mNoDataFound = view.findViewById(R.id.no_data);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mRecyclerViewpager =  view.findViewById(R.id.pager);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mProgressBarText = view.findViewById(R.id.tv_search);
        mRecyclerViewpager.setVisibility(View.GONE);

        if(!TextUtils.isEmpty(mSearchKey)){
            callSearchAllApi();
        }

        mProgressBarText.setText(R.string.searching);

        mSwipeRefershLayout.setEnabled(false);


        return view;
    }



    //  refreshing api
    public void callSearchAllApi() {

        mProgressBar.setVisibility(View.VISIBLE);

        mNoDataFound.setVisibility(View.GONE);


        // calling search api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SEARCH_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("searched for <b>"+mSearchKey+"</b> from Search ");

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Search </b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Search </b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                    params.put(Keys.SEARCH_KEY,mSearchKey);

                    if(!TextUtils.isEmpty(mSearchId)){
                        params.put(Keys.ID,mSearchId);
                    }

                    if(!TextUtils.isEmpty(mSearchType)){
                        params.put(Keys.TYPE,mSearchType);
                    }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling search api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        final GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final DashBoard searchPagenationData = gson.fromJson(response, DashBoard.class);

        // set dashboard data in app preferences
        AppPreferences.setDashBoard(searchPagenationData);


        //  for each loop
        for (DashBoardItemOdering dashBoardItemOdering : searchPagenationData.getHeadingOrdering()) {

            //  get layout from model class
            String layoutType = dashBoardItemOdering.getLayout();

            switch (layoutType) {
                case Constants.TYPE_COURSE:

                    DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoardItemOdering;
                    if(dashBoardItemOderingArticle.getItems().size()>0) {

                        mLlScreenContainer.addView(new SearchTabView(mContext, dashBoardItemOderingArticle, mSearchKey , dashBoardItemOdering.getSee_all(),false,mSearchType,mSearchId));

                        isDataAvailable = true;
                    }

                    continue;

                case Constants.TYPE_PROGRAMMES:

                    DashBoardItemOderingArticle dashBoardItemOderingProgrammes = (DashBoardItemOderingArticle) dashBoardItemOdering;
                    if(dashBoardItemOderingProgrammes.getItems().size()>0) {

                        mLlScreenContainer.addView(new SearchTabView(mContext, dashBoardItemOderingProgrammes, mSearchKey , dashBoardItemOdering.getSee_all(),true,mSearchType,mSearchId));

                        isDataAvailable = true;
                    }

                    continue;

                case Constants.TYPE_VIDEO:

                    DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoardItemOdering;


                    if(dashBoardItemOderingVideo.getItems().size()>0) {

                        mLlScreenContainer.addView(new SearchTabView(mContext, dashBoardItemOderingVideo,mSearchKey,mSearchType,mSearchId));

                        isDataAvailable = true;

                    }


                    continue;


                case Constants.TYPE_MENTOR:
                    DashBoardItemOderingMentors dashBoardItemOderingMentors = (DashBoardItemOderingMentors) dashBoardItemOdering;

                    if(dashBoardItemOderingMentors.getItems().size()>0) {

                        mLlScreenContainer.addView(new SearchTabView(mContext, dashBoardItemOderingMentors, mSearchKey,mSearchType,mSearchId));

                        isDataAvailable = true;

                    }

                    continue;

                case Constants.TYPE_EVENT:
                    DashBoardItemOderingEvent dashBoardItemOderingEvents = (DashBoardItemOderingEvent) dashBoardItemOdering;

                    if(dashBoardItemOderingEvents.getItems().size()>0) {

                        mLlScreenContainer.addView(new SearchTabView(mContext, dashBoardItemOderingEvents,mSearchKey,mSearchType,mSearchId));

                        isDataAvailable = true;

                    }

                    continue;
            }

        }

        if(!isDataAvailable){
            mNoDataFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
