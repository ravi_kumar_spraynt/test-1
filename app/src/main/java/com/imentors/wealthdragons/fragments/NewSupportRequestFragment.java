package com.imentors.wealthdragons.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.CustomerSupportActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.CountryCodeFragment;
import com.imentors.wealthdragons.dialogFragment.ServiceTypeFragment;
import com.imentors.wealthdragons.models.Customer;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.Map;


public class NewSupportRequestFragment extends BaseFragment {


    private EditText mEtFirstName , mEtSecondName , mEtMobileNumber , mEtMessage , mEtEmail , mEtServiceCode;
    private TextView mEtCountryCode,mEtSericeType;
    private FrameLayout mBtSubmit , mBtCancel;
    private Customer mCustomerData;
    private Context mContext;
    private LinearLayout mProgressbarLayout;
    private String mServiceTypeId;


    public static NewSupportRequestFragment newInstance() {
        NewSupportRequestFragment fragment = new NewSupportRequestFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_support_request, container, false);

        mEtMessage = view.findViewById(R.id.et_support_message);
        mEtFirstName = view.findViewById(R.id.et_support_first_name);
        mEtCountryCode = view.findViewById(R.id.et_support_country_code);
        mEtSecondName = view.findViewById(R.id.et_support_last_name);
        mEtEmail = view.findViewById(R.id.et_support_email_address);
        mEtMobileNumber = view.findViewById(R.id.et_support_mobile_number);
        mEtServiceCode = view.findViewById(R.id.et_support_service_code);
        mEtSericeType = view.findViewById(R.id.et_support_service_type);

        mBtSubmit = view.findViewById(R.id.fl_submit);
        mBtCancel = view.findViewById(R.id.fl_cancel);

        mProgressbarLayout = view.findViewById(R.id.progress_bar);

        //  get data from app preferences
        mCustomerData = AppPreferences.getCustomerData();

        setDataOnScreen(mCustomerData);

        mBtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check for validation
                Utils.callEventLogApi("clicked <b>"+"Submit" + "</b> button");

                Utils.hideKeyboard(getContext());

                postDataToServer();
            }
        });


        mBtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("back from <b>"+"Cancel " + "</b>");

                CustomerSupportActivity activity = (CustomerSupportActivity) mContext;
                activity.finish();
            }
        });


        mEtCountryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                CountryCodeFragment dialogFrag = CountryCodeFragment.newInstance();
//                dialogFrag.setTargetFragment(NewSupportRequestFragment.this, Constants.COUNTRY_CODE_DIALOG);
//                dialogFrag.show(getActivity().getSupportFragmentManager(), "dialog");
                Utils.callEventLogApi("clicked<b>"+" Country Code"+"</b>");

                Utils.hideKeyboard(mContext);


                CountryCodeFragment fragment = CountryCodeFragment.newInstance();

                fragment.setTargetFragment(NewSupportRequestFragment.this, Constants.COUNTRY_CODE_DIALOG);


                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.fm_customer, fragment);
                ft.addToBackStack(null);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();

            }
        });


        mEtSericeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>"+" Support Type"+"</b>");
                Utils.hideKeyboard(mContext);

                //  opened service type dialog
                ServiceTypeFragment dialogFrag = ServiceTypeFragment.newInstance();
                dialogFrag.setTargetFragment(NewSupportRequestFragment.this, Constants.SERVICE_TYPE_DIALOG);
                dialogFrag.show(getActivity().getSupportFragmentManager(), "dialog");



//                ServiceTypeFragment fragment = ServiceTypeFragment.newInstance();
//
//                fragment.setTargetFragment(NewSupportRequestFragment.this, Constants.SERVICE_TYPE_DIALOG);
//
//
//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//                ft.add(R.id.fm_customer, fragment);
//                ft.addToBackStack(null);
//                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
//                ft.commit();

            }
        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        //  get data from app preferences
        mCustomerData = AppPreferences.getCustomerData();

        setDataOnScreen(mCustomerData);
    }

    //  set all data on screen
    private void setDataOnScreen(Customer mCustomerData) {

        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getFirst_name())){
            mEtFirstName.setText(mCustomerData.getCustomer().getFirst_name());
            mEtFirstName.setEnabled(false);


        }

        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getLast_name())) {
            mEtSecondName.setText(mCustomerData.getCustomer().getLast_name());
            mEtSecondName.setEnabled(false);
        }


        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getEmail())) {
            mEtEmail.setText(mCustomerData.getCustomer().getEmail());
            mEtEmail.setEnabled(false);
        }

        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getMobile())) {
            mEtMobileNumber.setText(mCustomerData.getCustomer().getMobile());
            mEtMobileNumber.setEnabled(false);
            mEtCountryCode.setEnabled(false);

        }else{
            mEtMobileNumber.setEnabled(true);
            mEtCountryCode.setEnabled(true);

        }


        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getUnique_id())) {
            mEtServiceCode.setText(mCustomerData.getCustomer().getUnique_id());
            mEtServiceCode.setEnabled(false);
        }else{
            mEtServiceCode.setEnabled(true);
        }


        if(!TextUtils.isEmpty(mCustomerData.getCustomer().getCountry_code())) {
            mEtCountryCode.setText(mCustomerData.getCustomer().getCountry_code());
        }else{
            mEtCountryCode.setEnabled(true);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case Constants.COUNTRY_CODE_DIALOG:

                if (resultCode == Activity.RESULT_OK) {
                    // After Ok code.
                    String code = data.getStringExtra(Constants.COUNTRY_CODE);
                    mEtCountryCode.setText(code);

                } else if (resultCode == Activity.RESULT_CANCELED){
                    // After Cancel code.
                }

                break;

            case Constants.SERVICE_TYPE_DIALOG:
                if (resultCode == Activity.RESULT_OK) {
                    // After Ok code.
                    String code = data.getStringExtra(Constants.SERVICE_TYPE);
                    mServiceTypeId = data.getStringExtra(Constants.SERVICE_TYPE_ID);

                    mEtSericeType.setText(code);

                } else if (resultCode == Activity.RESULT_CANCELED){
                    // After Cancel code.
                }

                break;


        }

    }


    private void postDataToServer() {

        // check for validation
        String message = null;
        String email = null;
        String lastName = null;
        String firstName = null;
        String serviceType = null;
        String serviceCode = null;
        String mobileNumber = null;
        String countryCode = null;


        if (TextUtils.isEmpty(mEtFirstName.getText().toString())) {

            mEtFirstName.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_first_name_blank), getString(R.string.okay_text),true);

            return;

        } else if (TextUtils.isEmpty(mEtSecondName.getText().toString())) {

            mEtSecondName.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_last_name_blank), getString(R.string.okay_text),true);

            return;

        } else if (TextUtils.isEmpty(mEtEmail.getText().toString())) {

            mEtEmail.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_email_blank), getString(R.string.okay_text),true);

            return;

        }else if (!Utils.isValidEmail(mEtEmail.getText().toString())) {

            mEtEmail.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_email_valid), getString(R.string.okay_text),true);

            return;

        } else if (TextUtils.isEmpty(mEtMobileNumber.getText().toString())) {

            mEtMobileNumber.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_mobile), getString(R.string.okay_text),true);

            return;

        }
        else if (TextUtils.isEmpty(mEtServiceCode.getText().toString())) {

            mEtServiceCode.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_service_code), getString(R.string.okay_text),true);

            return;

        }else if ((mContext.getString(R.string.support_type)).equals(mEtSericeType.getText().toString())) {

            mEtSericeType.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_service_type), getString(R.string.okay_text),true);


            return;

        }else if (TextUtils.isEmpty(mEtMessage.getText().toString())) {

            mEtMessage.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_customer_message), getString(R.string.okay_text),true);

            return;

        }

         message = mEtMessage.getText().toString();
         email = mEtEmail.getText().toString();
         lastName = mEtSecondName.getText().toString();
         firstName = mEtFirstName.getText().toString();
         serviceType = mServiceTypeId;
         serviceCode = mEtServiceCode.getText().toString();
         mobileNumber = mEtMobileNumber.getText().toString();
         countryCode = mEtCountryCode.getText().toString().replace("+","");

        mProgressbarLayout.setVisibility(View.VISIBLE);


        final String finalMobileNumber = mobileNumber;
        final String finalEmail = email;
        final String finalMessage = message;
        final String finalServiceType = serviceType;
        final String finalServiceCode = serviceCode;
        final String finalLastName = lastName;
        final String finalFirstName = firstName;


        final String finalCountryCode = countryCode;

        // calling new support request and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST,Api.ADD_SUPPORT_REQUESTS_API, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {


                mProgressbarLayout.setVisibility(View.GONE);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressbarLayout.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting<b>"+"Support Request"+"</b> error in api");
                    e.printStackTrace();
                }



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressbarLayout.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>"+"Support Request"+"</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.SUPPORT_FIRST_NAME, finalFirstName);
                params.put(Keys.SUPPORT_LAST_NAME, finalLastName);
                params.put(Keys.SUPPORT_EMAIL, finalEmail);
                params.put(Keys.SUPPORT_MOBILE, finalMobileNumber);
                params.put(Keys.SUPPORT_SERVICE_CODE, finalServiceCode);
                params.put(Keys.SUPPORT_SERVICE_TYPE_ID, finalServiceType);
                params.put(Keys.SUPPORT_CONTACT_MESSAGE, finalMessage);
                params.put(Keys.SUPPORT_COUNTRY_CODE, finalCountryCode);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling new support request api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        Utils.showSimpleAlertDialog(mContext, getString(R.string.thanku), getString(R.string.thanku_contact_support), getString(R.string.okay_text),false);


        resetData();


    }


    //  reset data
    private void resetData() {
        mEtSericeType.setText(mContext.getString(R.string.support_type));
        mEtMessage.setText("");
        mEtMessage.setHint(mContext.getString(R.string.message));
        mEtMobileNumber.setEnabled(false);
        mEtCountryCode.setEnabled(false);
        mEtEmail.setEnabled(false);
        mEtFirstName.setEnabled(false);
        mEtSecondName.setEnabled(false);
    }


}
