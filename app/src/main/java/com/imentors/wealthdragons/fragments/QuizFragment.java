package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.QuizListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Quiz;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class QuizFragment extends BaseFragment {


    private WealthDragonTextView mTvQuizName , mTvQuizQuestion , mTvButtonNext , mTvProgressBar;
    private LinearLayout mProgressBar , mNoDataFound;
    private Context mContext;
    private String mQuizId;
    private RecyclerView mRecyclerView;
    private int mStartQuestionCount;
    private QuizListAdapter mQuizListAdapter;
    private List<Quiz.Question> mQuizQuestionList;
    private FrameLayout mSaveAndExitButton , mNextButton;
    private Quiz.Question mCurrentQuestion;
    private static final int NEXT_BUTTON = 2;
    private static final int PAGE_LAOD_QUIZ_API =  0;
    private static final int SAVE_AND_EXIT =  1;
    private static final int RESULT =  3;
    private List<Integer> mSelectedListPosition ;
    private int mSelectedPosition;
    private Quiz.QuizData quiz;

    public static QuizFragment newInstance(String  quizId) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.QUIZ_ID,quizId);
        QuizFragment fragment = new QuizFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean hasKey = getArguments().containsKey(Constants.QUIZ_ID);
        if(hasKey){
            mQuizId = getArguments().getString(Constants.QUIZ_ID);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_quiz, container, false);


        mTvQuizName = view.findViewById(R.id.tv_quiz_name);
        mTvQuizQuestion = view.findViewById(R.id.tv_quiz_ques);
        mProgressBar = view.findViewById(R.id.progressBar);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mSaveAndExitButton = view.findViewById(R.id.fl_save_exit);
        mNextButton = view.findViewById(R.id.fl_next);
        mTvButtonNext = view.findViewById(R.id.tv_next);
        mTvProgressBar = view.findViewById(R.id.tv_search);
        mNoDataFound = view.findViewById(R.id.no_data);

        //  set layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mQuizListAdapter = new QuizListAdapter(mContext);

        callQuizApi(PAGE_LAOD_QUIZ_API);


        mSaveAndExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callQuizApi(SAVE_AND_EXIT);

                getActivity().onBackPressed();
            }
        });


        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                disableView();

                boolean isOptionsSelected = false;

                if(mCurrentQuestion.getType().equals(Constants.MULTIPLE_CHOICE_CHECKBOX)){
                    if(mQuizListAdapter.getSelectedItemList().size()>0){
                       isOptionsSelected = true;
                    }
                }else{
                    if(mQuizListAdapter.getSelecetedItem() >-1){
                        isOptionsSelected = true;
                    }
                }
                if(isOptionsSelected) {
                    mStartQuestionCount=mStartQuestionCount+1;
                        callQuizApi(NEXT_BUTTON);
                }else{
                    // show error dialog
                    Utils.showSimpleAlertDialog(mContext,getString(R.string.error),getString(R.string.select_answer),getString(R.string.okay_text),true);
                    if(mContext!=null){
                        enableView();
                    }
                }
            }
        });

        return view;
    }


    private void callQuizApi(final int clickType) {

        String api = null;

        if(mContext!=null){
            disableView();
        }

        switch(clickType){
            case NEXT_BUTTON:
                if(mTvButtonNext.getText().toString().equals(getString(R.string.finish))){
                    Utils.callEventLogApi("clicked <b>Finish Button</b>  from <b>"+quiz.getTitle()+"</b> Question of <b>"+quiz.getCourse_title()+"</b>");

                }else{
                    Utils.callEventLogApi("clicked <b>Next Button</b> from <b>"+quiz.getTitle()+"</b> Question of <b>"+quiz.getCourse_title()+"</b>");

                }


                api = Api.SAVE_QUESTION_FOR_USER_QUIZ_API;
                mTvProgressBar.setText(getString(R.string.saving_quiz_answer));
                break;
            case PAGE_LAOD_QUIZ_API:
                api = Api.QUIZ_API;
                mProgressBar.setVisibility(View.VISIBLE);
                mTvProgressBar.setText(getString(R.string.loading_quiz));
                break;

            case SAVE_AND_EXIT:
                Utils.callEventLogApi("clicked <b>Save & Exit Button </b> from <b>"+quiz.getTitle()+"</b> of <b>"+quiz.getCourse_title()+"</b>");
                api = Api.SAVE_CURRENT_QUIZ_API;
                mTvProgressBar.setText(getString(R.string.saving_quiz_state));
                break;

            case RESULT:
                api = Api.QUIZ_RESULT_API;
                break;
        }

        mProgressBar.setVisibility(View.VISIBLE);



        // calling quiz api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jsonObject = new JSONObject(response);

                    if(!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))){
                        if(mQuizQuestionList.size()<=mStartQuestionCount) {
                            moveToResultFragment();
                        }else {
                            mProgressBar.setVisibility(View.GONE);

                            if (mContext!=null) {
                                setDataInRecyclerView(mStartQuestionCount);
                            }
                        }
                    }else {
                        mProgressBar.setVisibility(View.GONE);

                        onGetDataAPIServerResponse(response);
                    }
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting  <b>Quiz </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting  <b>Quiz </b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.QUIZ_LIST_ID,mQuizId);

                if(clickType == NEXT_BUTTON){
                    params.put(Keys.QUESTION_ID,mCurrentQuestion.getId());

                    if(mCurrentQuestion.getType().equals(Constants.MULTIPLE_CHOICE_CHECKBOX)) {
                        mSelectedListPosition = mQuizListAdapter.getSelectedItemList();
                        for (int i =0; i < mSelectedListPosition.size();i++) {
                            params.put(Keys.ANSWER + "[" + i + "]", mCurrentQuestion.getOption().get(mSelectedListPosition.get(i)));
                        }
                    }else{
                        mSelectedPosition = mQuizListAdapter.getSelecetedItem();
                        params.put(Keys.ANSWER + "[" + 0 + "]", mCurrentQuestion.getOption().get(mSelectedPosition));
                    }

                    if(mQuizQuestionList.size() <= mStartQuestionCount) {
                        params.put(Keys.FINISH,getString(R.string.yes));
                    }

                }

                if(clickType == SAVE_AND_EXIT){
                    params.put(Keys.PAGE_NO,Integer.toString(mStartQuestionCount));
                }

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    // handling quiz api response
    @Override
    public void showDataOnView(String response)  {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {


        //  parse json to java
         Quiz quizData = new Gson().fromJson(response, Quiz.class);
         quiz = quizData.getQuiz();


         if(quiz!=null) {

             if(quiz.getQuestions() != null) {
                 if (quiz.getQuestions().size() > 0) {

                     mNoDataFound.setVisibility(View.GONE);

                     mTvQuizName.setText(quiz.getTitle());

                     mStartQuestionCount = Integer.parseInt(quiz.getCurrent_page());

                     mQuizQuestionList = quiz.getQuestions();
                     setDataInRecyclerView(mStartQuestionCount);

                 } else {

                     mNoDataFound.setVisibility(View.VISIBLE);

                 }
             }else{
                 mNoDataFound.setVisibility(View.VISIBLE);
             }
         }

    }

    //  set data in recycler view
    private void setDataInRecyclerView(int questionCount) {

        if(mQuizQuestionList.size()>questionCount) {


            if(questionCount ==  mQuizQuestionList.size()-1){
                mTvButtonNext.setText(getString(R.string.finish));
            }

            mCurrentQuestion = mQuizQuestionList.get(questionCount);
            mTvQuizQuestion.setText(mCurrentQuestion.getQuestion());
            mRecyclerView.setAdapter(mQuizListAdapter);
            mQuizListAdapter.setNewItems(mCurrentQuestion.getOption(), mCurrentQuestion.getType(),mCurrentQuestion.getQuestion());
        }


        if(mContext!=null){
            enableView();
        }


    }

    //  move to result
    private void moveToResultFragment(){
        Utils.callEventLogApi("visited <b>"+quiz.getTitle()+"</b> of <b>"+quiz.getCourse_title()+"</b>  result page in quiz");

        FragmentTransaction transaction =  getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.fm_main, QuizResultFragment.newInstance(mQuizId)).commit();

    }


    private void disableView(){
        mNextButton.setEnabled(false);
        mSaveAndExitButton.setEnabled(false);

    }

    private void enableView(){
        mNextButton.setEnabled(true);
        mSaveAndExitButton.setEnabled(true);
    }


}
