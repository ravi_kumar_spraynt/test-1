package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.imentors.wealthdragons.R;

import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonEditText;


public class  SearchContainerFragment extends BaseFragment {


    private Context mContext;
    private WealthDragonEditText mSearchView;
    private ImageView mSearchCross;
    private Handler handler = new Handler();
    private Runnable runnable ;
    private boolean mIsSearchPerformed = false;


    public static SearchContainerFragment newInstance() {


        SearchContainerFragment fragment = new SearchContainerFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_container, container, false);

        mSearchView = view.findViewById(R.id.tv_search);
        mSearchCross = view.findViewById(R.id.iv_cancel_search);


        if(savedInstanceState == null){

            replace(SearchTopListFragment.newInstance(false,null));
            mSearchView.setCursorVisible(true);

        }

        mSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Search </b> icon");

                mSearchView.setCursorVisible(true);

            }
        });



        mSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            //  cleared search by click search cross
            @Override
            public void afterTextChanged(Editable s) {
                int length = s.toString().length();
                if(length==0){
                    mSearchCross.setVisibility(View.INVISIBLE);
                    if (runnable != null) {
                        handler.removeCallbacks(runnable);
                    }
                    replace(SearchTopListFragment.newInstance(false,null));
                }else {
                    mSearchCross.setVisibility(View.VISIBLE);

                    if (length > 1 ) {
                        if(runnable!=null) {
                            handler.removeCallbacks(runnable);
                        }

                        runnable = new Runnable() {
                            @Override
                            public void run() {

                                BaseFragment topSearchFragment = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.search_container);
                                if(topSearchFragment instanceof SearchTopListFragment){
                                    ((SearchTopListFragment) topSearchFragment).setNewAutoSearchDataOnList(mSearchView.getText().toString().trim());
                                }else{
                                    if(mIsSearchPerformed){
                                        // do nothing
                                        mIsSearchPerformed = false;
                                    }else{
                                        SearchTopListFragment frag = SearchTopListFragment.newInstance(true,mSearchView.getText().toString().trim());
                                        replace(frag);
                                    }
                                }
                            }
                        };

                        handler.postDelayed(runnable,500);

                        }
                    }
                }

        });


        mSearchView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utils.hideKeyboard(mContext);
                    performSearch(mSearchView.getText().toString().trim());
                    return true;
                }

                mSearchView.setCursorVisible(false);
                if (event != null&& (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(mSearchView.getApplicationWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                }

                return false;
            }
        });


        //  cleared search
        mSearchCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("closed <b>Search </b>result");

                clearSearch();
            }
        });

        return view;
    }



    private void performSearch(String trim) {
        mSearchView.setCursorVisible(false);
        mIsSearchPerformed = true;
        if(!TextUtils.isEmpty(trim)){
            replace(SearchFragment.newInstance(trim,null,null));
        }

    }

    public void callretryApi(){
        BaseFragment topSearchFragment = (BaseFragment) getChildFragmentManager().findFragmentById(R.id.search_container);
        if(topSearchFragment instanceof SearchTopListFragment){
            ((SearchTopListFragment) topSearchFragment).searchTopList();
        }
    }



    private void clearSearch(){

        if(Utils.isKeyboardVisible()){
           Utils.hideKeyboard(mContext);
            mSearchView.setCursorVisible(false);
        }else{
            Utils.showKeyboard(mContext,mSearchView);
            mSearchView.setCursorVisible(true);
        }

        mSearchView.setText("");
        replace(SearchTopListFragment.newInstance(false,null));
    }


    public void replace(Fragment fragment){
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.search_container, fragment );
        transaction.setCustomAnimations(
                R.anim.slide_in_from_right, R.anim.slide_out_to_right);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

        transaction.commit();
    }


    //  setting text on search view
    public void setTextOnSearchView(String text){
        mSearchView.setText(text);
        mIsSearchPerformed = true;
    }

    //  closed keyboard
    public void closeKeyBoard(){
        mSearchView.setCursorVisible(false);
        Utils.hideKeyboard(mContext);
    }

}
