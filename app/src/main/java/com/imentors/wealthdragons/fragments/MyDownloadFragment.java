package com.imentors.wealthdragons.fragments;
import android.content.Context;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.DownLoadDetailListAdapter;
import com.imentors.wealthdragons.dialogFragment.FullScreenDownloadPlayerDialog;
import com.imentors.wealthdragons.listeners.DownloadActionListener;
import com.imentors.wealthdragons.models.DownloadFile;
import com.imentors.wealthdragons.models.GroupDownloadFile;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.DeleteSwipeRecyclerView;
import com.tonyodev.fetch2.AbstractFetchListener;
import com.tonyodev.fetch2.Download;
import com.tonyodev.fetch2.Error;
import com.tonyodev.fetch2.Fetch;
import com.tonyodev.fetch2.FetchListener;
import com.tonyodev.fetch2core.Func;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyDownloadFragment extends BaseFragment implements  DownloadActionListener,DeleteSwipeRecyclerView.RecyclerItemTouchHelperListener {


    private Context mContext;
    private RecyclerView mRecycleview;
    private DownLoadDetailListAdapter mDownloadDetailListAdapter;
    private LinearLayout mFragmentProgressBar,mNoDataFound;
    List<DownloadFile> list = AppPreferences.getDownloadList();
    private Fetch fetch;
    private List<Download> mResult;
    private String mGroupId,mGroupTitle;
    private TextView mVideoTitle;


    public static MyDownloadFragment newInstance(String mGroupId,String groupTitle) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.GROUP_TITLE, mGroupId);
        args.putSerializable(Constants.GROUP_ID, groupTitle);



        MyDownloadFragment fragment = new MyDownloadFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroupId = (String) getArguments().getSerializable(Constants.GROUP_TITLE);
        mGroupTitle = (String) getArguments().getSerializable(Constants.GROUP_ID);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.download_list, container, false);
        mNoDataFound = view.findViewById(R.id.no_data);
        mFragmentProgressBar = view.findViewById(R.id.progressBar);

        mFragmentProgressBar.setVisibility(View.VISIBLE);
        mVideoTitle=view.findViewById(R.id.tv_video_title);
        mRecycleview = view.findViewById(R.id.listView);

        return view;
    }

    private void showData() {

        if(mResult !=null && mResult.size()>0){
            mNoDataFound.setVisibility(View.GONE);
            mFragmentProgressBar.setVisibility(View.GONE);

            mVideoTitle.setText(mGroupTitle);

            mRecycleview.setNestedScrollingEnabled(false);
            mRecycleview.setHasFixedSize(true);


            Utils.callEventLogApi("opened <b>Feed Detail</b> page");


            mRecycleview.setLayoutManager(new GridLayoutManager(getContext(),
                    1,
                    LinearLayoutManager.VERTICAL,
                    false));

            mDownloadDetailListAdapter = new DownLoadDetailListAdapter(getContext(),mResult, this);
            mRecycleview.setAdapter(mDownloadDetailListAdapter);
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new DeleteSwipeRecyclerView(0, ItemTouchHelper.LEFT, this);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(mRecycleview);

            fetch.addListener(fetchListener);

        }else{
            mNoDataFound.setVisibility(View.VISIBLE);
            mFragmentProgressBar.setVisibility(View.GONE);

        }


    }

    @Override
    public void onResume() {
        super.onResume();
        fetch = Utils.getFetchInstance();

        fetch.getDownloadsInGroup(Integer.valueOf(mGroupId),new Func<List<Download>>() {
            @Override
            public void call(@NotNull List<Download> result) {
                mResult = result;
                showData();
            }
        });


    }

    @Override
    public void onPause() {
        super.onPause();
        fetch.removeListener(fetchListener);
    }


    private final FetchListener fetchListener = new AbstractFetchListener() {


        @Override
        public void onCompleted(@NotNull Download download) {
            mDownloadDetailListAdapter.updateProgress(download);

        }


        @Override
        public void onError(@NotNull Download download, @NotNull Error error, @org.jetbrains.annotations.Nullable Throwable throwable) {
            mDownloadDetailListAdapter.updateProgress(download);
        }



        @Override
        public void onProgress(@NotNull Download download, long etaInMilliSeconds, long downloadedBytesPerSecond) {
            mDownloadDetailListAdapter.updateProgress(download);

        }

        @Override
        public void onQueued(@NotNull Download download, boolean waitingOnNetwork) {
            mDownloadDetailListAdapter.updateProgress(download);

        }


        @Override
        public void onRemoved(@NotNull Download download) {
            mResult.remove(download);
            mDownloadDetailListAdapter.updateProgress(download);
        }

        @Override
        public void onDeleted(@NotNull Download download) {
            mResult.remove(download);
            mDownloadDetailListAdapter.updateProgress(download);
        }

        @Override
        public void onWaitingNetwork(@NotNull Download download) {
            mDownloadDetailListAdapter.updateProgress(download);

        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRemoveDownload(int id) {
        fetch.delete(id);
    }

    @Override
    public void onRetryDownload(int id) {
        fetch.retry(id);
    }

    @Override
    public void onItemClicked(Download download) {

    }

    @Override
    public void onDownloadItemClicked(List<Download> download,int position) {
        FullScreenDownloadPlayerDialog dialogFragment = FullScreenDownloadPlayerDialog.newInstance((Serializable)download,position);
        dialogFragment.setCancelable(true);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onGroupItemClicked(GroupDownloadFile download) {
        //nothing to do
    }

    @Override
    public void allItemsRemoved() {
        ((MainActivity)mContext).onBackPressed();
        mNoDataFound.setVisibility(View.VISIBLE);
        mFragmentProgressBar.setVisibility(View.GONE);

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        Toast.makeText(getApplicationContext(), R.string.download_removed, Toast.LENGTH_SHORT).show();
        mDownloadDetailListAdapter.removeSwippedItem(position);
    }
}


