package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.Messages;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;

import java.util.Map;

public class MessageDetailFragment extends BaseFragment {


    private WealthDragonTextView mTvMessageDate , mTvMessageTitle , mTvMessage;
    private LinearLayout mProgressBar;
    private Context mContext;
    private String mMessageId;



    public static MessageDetailFragment newInstance(String messageId) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.MESSAGE_ID,messageId);

        MessageDetailFragment fragment = new MessageDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMessageId =  getArguments().getString(Constants.MESSAGE_ID);


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_detail_message, container, false);



        mProgressBar = view.findViewById(R.id.progressBar);
        mTvMessage = view.findViewById(R.id.tv_message_detail_message);
        mTvMessageDate = view.findViewById(R.id.tv_message_detail_time);
        mTvMessageTitle = view.findViewById(R.id.tv_message_detail_heading);

        callMessageDetailApi();

        callMessageSeenApi();



        return view;
    }

    public void callMessageDetailApi() {

        mProgressBar.setVisibility(View.VISIBLE);


        // calling message detail api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MESSAGE_DETAIL_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Message Detail </b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Message Detail </b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.MESSAGE_ID,mMessageId);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling message detail api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        setData(response);
    }


    //  setting data on view
    private void setData(String response) {


        //  parse JSON to java
        Messages.Notification messageData = new Gson().fromJson(response,Messages.Notification.class);

        //  set data on view
        mTvMessageTitle.setText(messageData.getTitle());
        mTvMessage.setText(messageData.getMessage());
        mTvMessageDate.setText(Utils.getMessageDate(messageData.getDate()));

    }



    private void callMessageSeenApi() {



        //TODO implemented maintenance mode
        // calling message seen api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MESSAGE_SEEN_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
                Utils.callEventLogApi("getting <b>Message Seen </b> error in api");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ID,mMessageId);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    //  closed message detail page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Message Detail </b> page");

    }
}
