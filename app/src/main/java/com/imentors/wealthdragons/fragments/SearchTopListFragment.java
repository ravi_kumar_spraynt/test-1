package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.AutoSearchListAdapter;
import com.imentors.wealthdragons.adapters.SearchTopListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.SearchAutoComplete;
import com.imentors.wealthdragons.models.SearchKeywords;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class SearchTopListFragment extends BaseFragment implements PagingListener {

    private LinearLayout mProgressBar,mNoDataFound;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private SearchTopListAdapter mSearchTopListAdapter;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private SearchTopListAdapter.OnTopSearchesClick mListener;
    private AutoSearchListAdapter.OnTopSearchesClick mAutoSearchListener;
    private AutoSearchListAdapter mAutoSearchListAdapter;
    private WealthDragonTextView mTopSearchesHeading;
    private boolean mIsAutoSearchList = false;
    private String mSearchKey;



    public static SearchTopListFragment newInstance(boolean isAutoSearchList , String searchKey) {

        //  add data in bundle
        Bundle args = new Bundle();

        args.putBoolean(Constants.SEARCH_IS_AUTOSEARCH,isAutoSearchList);
        if(!TextUtils.isEmpty(searchKey)){
            args.putString(Constants.SEARCH_ITEM,searchKey);
        }

        SearchTopListFragment fragment = new SearchTopListFragment();
        fragment.setArguments(args);

        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsAutoSearchList = getArguments().getBoolean(Constants.SEARCH_IS_AUTOSEARCH);

        boolean hasSearchItemKey = getArguments().containsKey(Constants.SEARCH_ITEM);
        if(hasSearchItemKey){
            mSearchKey = getArguments().getString(Constants.SEARCH_ITEM);
        }



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if (context instanceof SearchTopListAdapter.OnTopSearchesClick) {
            mListener = (SearchTopListAdapter.OnTopSearchesClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


        if (context instanceof AutoSearchListAdapter.OnTopSearchesClick) {
            mAutoSearchListener = (AutoSearchListAdapter.OnTopSearchesClick) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_search_top_list, container, false);

        mProgressBar = view.findViewById(R.id.progress_bar);
        mNoDataFound = view.findViewById(R.id.no_data);
        mRecyclerView = view.findViewById(R.id.listView);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callSearchKeywordApi();
                Utils.callEventLogApi("refreshed <b> Search Keyword Screen </b>");

            }
        });
        mTopSearchesHeading = view.findViewById(R.id.tv_top_search_heading);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(RecyclerView.SCROLL_STATE_DRAGGING == newState){
                    Utils.hideKeyboard(getContext());
                }
            }


        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        searchTopList();


        return view;
    }

    public void searchTopList()
    {
        if(mIsAutoSearchList && !TextUtils.isEmpty(mSearchKey)){
            setNewAutoSearchDataOnList(mSearchKey);
        }else{
            callSearchKeywordApi();
        }
    }


    //  refreshing api
    private void callSearchKeywordApi() {

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);

        }else {
            mSwipeRefershLayout.setRefreshing(false);
            mProgressBar.setVisibility(View.VISIBLE);

        }


        // calling search top list api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SEARCH_TOP_KEYWORDS_SEARCH_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefershLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Search TopList</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);
                Utils.callEventLogApi("getting <b>Search TopList</b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        //  parse JSON to java
        SearchKeywords searchKeywords = new Gson().fromJson(response,SearchKeywords.class);


        //  set search keyword in app preferences
        AppPreferences.setSearchKeyword(searchKeywords);

        showSearchKeywordList(searchKeywords);
    }

    //  calling SearchTopList Adapter
    private void showSearchKeywordList(SearchKeywords searchKeywords) {
        mSearchTopListAdapter = new SearchTopListAdapter(searchKeywords.getKey_words(),this,mListener,searchKeywords.getTotal_items(),searchKeywords.getNext_page());
        mRecyclerView.setAdapter(mSearchTopListAdapter);
    }

    // setting data on new auto search data
    public void setNewAutoSearchDataOnList(String searchKey){
        if(mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
            mNoDataFound.setVisibility(View.GONE);
            mSwipeRefershLayout.setEnabled(false);
            mTopSearchesHeading.setVisibility(View.GONE);
            mSearchTopListAdapter = null;
            mRecyclerView.setAdapter(null);
            if (!TextUtils.isEmpty(searchKey)) {
                callSearchApi(searchKey);
            }

        }

    }


    private void callSearchApi(final String searchKey) {


        // TODO implemented maintenance
        // calling search auto complete api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SEARCH_AUTO_COMPLETE_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    mProgressBar.setVisibility(View.GONE);
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                        Type type = new TypeToken<List<SearchAutoComplete>>() {
                        }.getType();
                        List<SearchAutoComplete> searchAutoCompleteList = new Gson().fromJson(response, type);
                        if(searchAutoCompleteList.size()>0) {
                            mAutoSearchListAdapter = new AutoSearchListAdapter(searchAutoCompleteList, mAutoSearchListener,getContext());
                            mRecyclerView.setAdapter(mAutoSearchListAdapter);
                        }else{
                            mNoDataFound.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mNoDataFound.setVisibility(View.VISIBLE);


                    }


                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    mNoDataFound.setVisibility(View.VISIBLE);
                    Utils.callEventLogApi("getting <b>Search Auto Complete</b> error in api");
                    e.printStackTrace();

                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mNoDataFound.setVisibility(View.VISIBLE);
                Utils.callEventLogApi("getting <b>Search Auto Complete</b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.SEARCH_KEY, searchKey);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String view_type, String seeAllType) {
        if(view_type.equals(Constants.TYPE_SEARCH))
            callSeeAllPaginationApi(pageNumber,tasteId, Api.SEARCH_TOP_KEYWORDS_SEARCH_PAGINATION_API,view_type);
    }


    // TODO implemented maintenance mode
    public void callSeeAllPaginationApi(final int pageNumber , final String tasteId , String api , final String viewType) {

        // calling search top keywords pagination api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    addDataInAdapter(response);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if(!Utils.setErrorDialog(response,getContext())){
                            setErrorMessage(viewType);
                        }
                    } catch (JSONException e1) {
                        Utils.callEventLogApi("getting <b>Search Top Keyword</b> error in api");

                        e1.printStackTrace();
                        setErrorMessage(viewType);
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Search Top Keyword</b> error in api");
                error.printStackTrace();
                if (error instanceof NoConnectionError)
                {

                }else{
                    setErrorMessage(viewType);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if(pageNumber !=0){
                    params.put(Keys.PAGE_NO,String.valueOf(pageNumber));
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void addDataInAdapter(String response) {

        // Parse JSON to Java
        final SearchKeywords reviewPagenationData = new Gson().fromJson(response, SearchKeywords.class);


        mSearchTopListAdapter.addItems(reviewPagenationData.getKey_words(),reviewPagenationData.getTotal_items(),reviewPagenationData.getNext_page());

    }


    //  setting server error
    private void setErrorMessage(String viewType) {
        mSearchTopListAdapter.setServerError();
    }


}
