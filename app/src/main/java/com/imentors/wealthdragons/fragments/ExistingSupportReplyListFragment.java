package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.ExistingSupportReplyListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonEditText;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ExistingSupportReplyListFragment extends BaseFragment implements ExistingSupportReplyListAdapter.OnListClicked {



    private LinearLayout mProgressBar;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ExistingSupportReplyListAdapter mExistingSupportReplyListAdapter;
    private String mRequestId;
    private FrameLayout mSubmitBtn , mBackButton,mAttachment;
    private WealthDragonEditText mEtMessage;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private WealthDragonTextView mTvReply;


    public static ExistingSupportReplyListFragment newInstance(String requestId) {
        ExistingSupportReplyListFragment fragment = new ExistingSupportReplyListFragment();
        //  put data in bundle
        Bundle bundle = new Bundle();
        bundle.putString(Constants.SUPPORT,requestId);
        fragment.setArguments(bundle);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRequestId = getArguments().getString(Constants.SUPPORT);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;



    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_existing_support_reply, container, false);

        mProgressBar = view.findViewById(R.id.progress_bar);
        mRecyclerView = view.findViewById(R.id.listView);
        mSubmitBtn = view.findViewById(R.id.fl_submit);
        mBackButton = view.findViewById(R.id.fl_cancel);
        mEtMessage = view.findViewById(R.id.et_support_message);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mTvReply = view.findViewById(R.id.tv_reply);

        callExistingSupportApi(true);

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callExistingSupportApi(true);
                Utils.callEventLogApi("refreshed <b>Existing Support Reply Screen</b>");


            }
        });

        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Reply Button</b> in customer support ");
                addReplyMessage();
            }
        });


        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.callEventLogApi("clicked <b>Back Button </b> from customer support");
                popFragment();
            }
        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        Utils.hideKeyboard(mContext);
    }

    private void callExistingSupportApi(boolean showProgressBar) {

        //  set refreshing
        if(showProgressBar) {
            if (mSwipeRefershLayout.isRefreshing()) {
                mSwipeRefershLayout.setRefreshing(true);
                mProgressBar.setVisibility(View.GONE);

            } else {
                mSwipeRefershLayout.setRefreshing(false);
                mProgressBar.setVisibility(View.VISIBLE);

            }
        }



        // calling reply Existing Support list and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.REPLY_SUPPORT_LIST_DETAIL_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Customer Support</b> error in api");

                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);
                Utils.callEventLogApi("getting <b>Customer Support</b> error in api");

                error.printStackTrace();
                Utils.showCustomerSupportNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.REQUEST_ID,mRequestId);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    // handling Existing support list api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        //used to deserlize direct list
//        List<SupportType> existingSuppportList =  new Gson().fromJson(response,  new TypeToken<List<SupportType>>(){}.getType());

        //  parse JSON to java
        SupportType existingSuppportList =  new Gson().fromJson(response,  SupportType.class);


        if(existingSuppportList.getMessage()!=null) {

            if (existingSuppportList.getMessage().size() > 0) {

                    showSupportRequestList(existingSuppportList.getMessage());
            }
        }else{
            //  mExistingSupportReplyListAdapter.addNewItem(new SupportType.ExistingSupportMessage(mEtMessage.getText().toString()));
           //  set data on view
            mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount()-1);
            mEtMessage.setText("");
            mEtMessage.setHint(mContext.getString(R.string.reply_small));
            Utils.showSimpleAlertDialog(mContext, getString(R.string.thanku), getString(R.string.thanku_contact_support), getString(R.string.okay_text),false);
        }

    }

    //  showed support request list
    private void showSupportRequestList(List<SupportType.ExistingSupportMessage> supportTypeList) {
        mExistingSupportReplyListAdapter = new ExistingSupportReplyListAdapter(supportTypeList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mExistingSupportReplyListAdapter);
        mRecyclerView.scrollToPosition(mRecyclerView.getAdapter().getItemCount()-1);

    }


    //  add message
    private void addReplyMessage() {

        Utils.hideKeyboard(mContext);



        if(TextUtils.isEmpty(mEtMessage.getText().toString())){
            mEtMessage.requestFocus();
            Utils.showSimpleAlertDialog(mContext, getString(R.string.error), getString(R.string.error_reply), getString(R.string.okay_text),true);

            return;
        }
        else {
            mTvReply.setText(R.string.processing);

        }



        // calling reply support request and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.REPLY_SUPPORT_REQUEST_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                mTvReply.setText(R.string.reply);
                callExistingSupportApi(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Reply Support</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mTvReply.setText(R.string.reply);
                Utils.callEventLogApi("getting <b>Reply Support</b> error in api");
                error.printStackTrace();
                Utils.showCustomerSupportNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.REQUEST_ID,mRequestId);
                params.put(Keys.SUPPORT_CONTACT_MESSAGE, mEtMessage.getText().toString());


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //  clicked list items
    @Override
    public void OnListClicked(SupportType.ExistingSupportMessage item) {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fm_customer, CustomerAttachmentPagerFragment.newInstance(item));
        ft.addToBackStack(null);
        ft.commit();


    }


    public void refreshReplyList(){
        callExistingSupportApi(false);
    }
}
