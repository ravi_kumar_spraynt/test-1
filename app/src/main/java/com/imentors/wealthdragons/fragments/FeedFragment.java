package com.imentors.wealthdragons.fragments;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.support.v4.media.session.PlaybackStateCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.adapters.FeedDetailListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.Feeds;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class FeedFragment extends BaseFragment implements FeedDetailListAdapter.OnListItemClicked, PagingListener {


    private Context mContext;
    private RecyclerView mRecycleview;
    private List<Feeds.Feed> mFeedsList = new ArrayList<>();
    private FeedDetailListAdapter mFeedListAdapter;
    private String mMentorId;
    private LinearLayout mFragmentProgressBar;
    private WealthDragonTextView mLoaderTitle;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private Timer saveSeekTime;


    private TextView mTvCourseTitle, mTvCoursePunchLine, mTvCoursePreferance, mVideoError, mTvEClassNameSmallVideo;
    private FrameLayout mShareBtn;
    private ProgressBar mProgressBar;
    private FrameLayout mVideoFrameLayout;
    private ImageView mIvVideoPlay, mIvCourseImage;
    private PlayerView mExoPlayerView;


    private Activity mActivity;
    private MediaSource mVideoSource;
    private FrameLayout mFullScreenButton, mDialogFullScreenButton;
    private ImageView mFullScreenIcon, mPlayIcon;
    private SimpleExoPlayer mPlayer;
    private boolean isFromActivityResult = false, isPaused = false;

    private int mResumeWindow;
    private long mResumePosition;
    private OpenFullScreenVideoListener mOpenFullScreenVideoListener;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private Utils.DialogInteraction mDialogListener;
    private boolean mExoPlayerFullscreen, mIsVideoError = false;
    private Dialog mFullScreenDialog;
    private PlayerView mDialogplayerView;
    private boolean isPotrait, isBufferering = true;
    private boolean isLandScape;
    private ImageButton mPlayImageButton;


    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private RelativeLayout mLlHeadingContainer;
    private ImageView dialogPosterImage;
    private String mVideoNameForListenerLog;
    private boolean isAutoPlay;
    private TextView tvEClassName;
    private String mVideoUrl;
    private int mPosition = 0 ;
    private NestedScrollView mNestedScrollView;
    private boolean isNextVideoTapped;


    public static FeedFragment newInstance(String mentorId) {

        //  put data in bundle
        Bundle args = new Bundle();
        args.putSerializable(Constants.MENTORS_KEY, mentorId);

        FeedFragment fragment = new FeedFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMentorId = (String) getArguments().getSerializable(Constants.MENTORS_KEY);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.feed_list, container, false);

        mNestedScrollView = view.findViewById(R.id.nested_scroll_view);
        mRecycleview = view.findViewById(R.id.listView);
        mRecycleview.setNestedScrollingEnabled(false);
        mRecycleview.setHasFixedSize(true);
        mLoaderTitle = view.findViewById(R.id.tv_search);
        mFragmentProgressBar = view.findViewById(R.id.progressBar);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);


        mLoaderTitle.setText(getString(R.string.loading_feeds));

        Utils.callEventLogApi("opened <b>Feed Detail</b> page");

        GridLayoutManager manager = null;
        if (Utils.isTablet()) {
             manager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
        }
        else
        {
             manager = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        }

        mRecycleview.setLayoutManager(manager);

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callFeedsApi();
                Utils.callEventLogApi("refreshed <b>Feed Detail</b> screen");

            }
        });

        callFeedsApi();


        // heading
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCourseTitle.setVisibility(GONE);
        mTvCoursePunchLine = view.findViewById(R.id.tv_course_detail_punch_line);
        mTvCoursePunchLine.setVisibility(GONE);
        mTvCoursePreferance = view.findViewById(R.id.txt_course_detail_taste_preferance);
        mTvCoursePreferance.setVisibility(GONE);
        mShareBtn = view.findViewById(R.id.fl_btn_full_details);
        mShareBtn.setVisibility(GONE);
        mVideoError = view.findViewById(R.id.video_error);
        mProgressBar = view.findViewById(R.id.progress_bar);


        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);


        mIvVideoPlay = view.findViewById(R.id.iv_video_play);
        mIvVideoPlay.setVisibility(View.INVISIBLE);

        mIvVideoPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIvVideoPlay.setVisibility(GONE);
                mIvCourseImage.setVisibility(GONE);
                callVideoUrl(mFeedsList.get(mPosition).getId());
            }
        });


        mIvCourseImage = view.findViewById(R.id.iv_video_image);
        mIvCourseImage.setVisibility(View.INVISIBLE);
        mExoPlayerView = view.findViewById(R.id.exoplayer);
        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);

        initFullscreenDialog();
        initFullscreenButton();
        return view;
    }

    private void startTimerToSendVideoSeekTime() {
        saveSeekTime = new Timer();

        saveSeekTime.schedule(new TimerTask() {
            @Override
            public void run() {
                saveSeekTime();
            }
        }, 100, 1000);

    }

    private void callFeedsApi() {


        if (mSwipeRefershLayout.isRefreshing()) {
            mSwipeRefershLayout.setRefreshing(true);
            mFragmentProgressBar.setVisibility(View.GONE);

        } else {
            mSwipeRefershLayout.setRefreshing(false);
            mFragmentProgressBar.setVisibility(View.VISIBLE);

        }


        // calling badges api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.FEEDS_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mFragmentProgressBar.setVisibility(View.GONE);
                    mSwipeRefershLayout.setRefreshing(false);

                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {

                    mFragmentProgressBar.setVisibility(View.GONE);
                    mSwipeRefershLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Feeds </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mFragmentProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);
                Utils.callEventLogApi("getting <b>Feeds </b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.MENTOR_ID, mMentorId);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {


        //  parse JSON to java
        Feeds feeds = new Gson().fromJson(response, Feeds.class);

        mFeedsList.addAll(feeds.getFeeds().getFeeds());

        for(Feeds.Feed feed: mFeedsList){
            AppPreferences.setVideoResumePosition(feed.getId(),Utils.getTimeInMilliSeconds(feed.getSeektime()));
        }

        mVideoUrl = mFeedsList.get(0).getVideo();
        callVideoUrl(mFeedsList.get(0).getId());

        isAutoPlay = feeds.getFeeds().isAutoPlay();

        mFeedListAdapter = new FeedDetailListAdapter(getContext(), this, feeds.getFeeds().getFeeds(), this, feeds.getFeeds().getTotal_items(), feeds.getFeeds().getNext_page());
        mRecycleview.setAdapter(mFeedListAdapter);
        startTimerToSendVideoSeekTime();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (saveSeekTime != null) {
            saveSeekTime.cancel();
        }
    }

    @Override
    public void onItemClicked(Feeds.Feed item, int position) {

        if(mPlayer!=null) {
            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                AppPreferences.setVideoResumePosition(mFeedsList.get(mPosition).getId(), 0);

            } else {
                AppPreferences.setVideoResumePosition(mFeedsList.get(mPosition).getId(), mPlayer.getContentPosition());

            }
        }

        mPosition = position;
        mIvVideoPlay.setVisibility(GONE);
        mIvCourseImage.setVisibility(GONE);

        mVideoUrl = mFeedsList.get(mPosition).getVideo();

        callVideoUrl(mFeedsList.get(mPosition).getId());

        mNestedScrollView.post(new Runnable() {
            @Override
            public void run() {

                ObjectAnimator.ofInt(mNestedScrollView, "scrollY", 0).setDuration(100).start();
//                    mNestedScrollView.scrollTo(0,0);
            }
        });
    }


    private void initFullscreenButton() {

        mFullScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);

        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });
    }


    //  opened full screen video mode
    public void openFullScreenVideo(int rotationAngle) {
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
            }
        }
    }



    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + "Feeds");

        isPotrait = false;
        isLandScape = true;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mNextVideo.setText("Next Feed Video >");

        dialogPosterImage = dialogView.findViewById(R.id.iv_poster_image);
        dialogPosterImage.setVisibility(GONE);

        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);

        mNextVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isNextVideoTapped = true;
                Utils.callEventLogApi("clicked <b>" + "Next Feed Video" + " </b> in feed");
                playNextVideo();

            }
        });
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);
        nextVideoFrame.setVisibility(GONE);
        tvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);

        tvEClassName.setText(mFeedsList.get(mPosition).getTitle());
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);
        dialogCourseImage.setVisibility(GONE);

        // on Next Click
        nextVideoFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlayer.getNextWindowIndex() != -1) {
                    mPlayer.seekToDefaultPosition(mPlayer.getNextWindowIndex());

                }
            }
        });

        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();
        Log.d("orientation", "lanscape");

    }

    //  closed full screen dialog
    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + "Feeds");

        isPotrait = true;
        isLandScape = false;
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");

    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == VISIBLE) {
                    mLlHeadingContainer.setVisibility(VISIBLE);
                    if (isAutoPlay) {
                        mNextVideo.setVisibility(VISIBLE);
                    } else {
                        mNextVideo.setVisibility(GONE);
                    }
                } else {
                    mLlHeadingContainer.setVisibility(GONE);

                }
            }
        });
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }




    @Override
    public void onStop() {
        super.onStop();
        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mPlayer.release();
            mPlayer = null;
            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(mFeedsList.size()>0 && mPlayer ==null){

            mExoPlayerView.setVisibility(GONE);
            mIvCourseImage.setVisibility(VISIBLE);
            mIvVideoPlay.setVisibility(VISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String view_type, String seeAllType) {
        if (view_type.equals(Constants.TYPE_SEARCH))
            callPaginationApi(pageNumber, tasteId, Api.FEEDS_API_PAGINATION, view_type);
    }


    public void callPaginationApi(final int pageNumber, final String tasteId, String api, final String viewType) {

        // calling search top keywords pagination api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                    addDataInAdapter(response);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if (!Utils.setErrorDialog(response, getContext())) {
                            setErrorMessage(viewType);
                        }
                    } catch (JSONException e1) {
                        Utils.callEventLogApi("getting <b>Pagination</b> error in api");

                        e1.printStackTrace();
                        setErrorMessage(viewType);
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Pagination</b> error in api");
                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                } else {
                    setErrorMessage(viewType);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                if (pageNumber != 0) {
                    params.put(Keys.PAGE_NO, String.valueOf(pageNumber));
                    params.put(Keys.MENTOR_ID, mMentorId);
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void addDataInAdapter(String response) {

        // Parse JSON to Java
        Feeds feedsPaginationData = new Gson().fromJson(response, Feeds.class);

        if (feedsPaginationData.getFeeds().getFeeds().size() > 0) {
            mFeedsList.addAll(feedsPaginationData.getFeeds().getFeeds());
            mFeedListAdapter.addItems(feedsPaginationData.getFeeds().getFeeds(), feedsPaginationData.getFeeds().getTotal_items(), feedsPaginationData.getFeeds().getNext_page());
        } else {
            mFeedListAdapter.setServerError();

        }

    }


    //  setting server error
    private void setErrorMessage(String viewType) {
        mFeedListAdapter.setServerError();
    }









    private void showNextVideo() {
        // next video and timer according to video quantites
        if(isLandScape) {
            if (isAutoPlay) {
                mNextVideo.setVisibility(View.VISIBLE);

            } else {
                // case of intro video
                mNextVideo.setVisibility(View.GONE);
            }
        }
    }


    // for internal use
    private void initExoPlayer() {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        mVideoNameForListenerLog = mFeedsList.get(mPosition).getTitle();
        mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);

        mResumePosition = AppPreferences.getVideoResumePosition(mFeedsList.get(mPosition).getId());

        Glide.with(mContext).load(mFeedsList.get(mPosition).getBanner()).error(R.drawable.no_img_course).into(mIvCourseImage);


        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, null, false, Constants.TYPE_EXPERT, "Digital Coaching",false);


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();
        mPlayer.clearVideoSurface();
        Utils.callEventLogApi("started new video  <b>" + mFeedsList.get(mPosition).getTitle()+" </b>" + "in Feeds" );


        if(isLandScape){


            mDialogplayerView.setVisibility(VISIBLE);

            mDialogplayerView.setPlayer(mPlayer);

            tvEClassName.setText(mFeedsList.get(mPosition).getTitle());


        }else{

            mExoPlayerView.setVisibility(View.VISIBLE);

            mExoPlayerView.setPlayer(mPlayer);

        }


        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                int stat = mPlayer.getPlaybackState();
                if(mPlayer.getPlaybackState() == PlaybackStateCompat.STATE_PLAYING ) {



                }
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if(playbackState == Player.STATE_ENDED ){

                    Utils.callEventLogApi("watched video <b>" + mFeedsList.get(mPosition).getTitle()+" </b>" + "from Feeds" );

                    if(isAutoPlay) {
                        isNextVideoTapped = true;
                        playNextVideo();
                    }else{
                        if(mExoPlayerFullscreen){
                            dialogPosterImage.setVisibility(View.INVISIBLE);
                            mDialogplayerView.setVisibility(View.VISIBLE);
                        }


                        mExoPlayerView.setVisibility(GONE);
                        mVideoError.setVisibility(View.INVISIBLE);
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mIvCourseImage.setVisibility(View.VISIBLE);
                        mIvVideoPlay.setVisibility(VISIBLE);
                    }

                }

                if(playWhenReady){
                }else{
                    Utils.callEventLogApi("paused <b>"+mVideoNameForListenerLog+"</b> in Feeds");
                }


                if (playbackState == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mExoPlayerView.setVisibility(VISIBLE);
                    mExoPlayerView.setControllerAutoShow(true);
                    mExoPlayerView.setUseController(true);
                }

                if(!((BaseActivity)mActivity).isFragmentInBackStack(new DashBoardDetailFragment())){
                    Utils.releaseDetailScreenPlayer();
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {
                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mVideoNameForListenerLog + "</b>");
                }catch (Exception e){
                    e.printStackTrace();
                }
                // show error screen
                mVideoError.setVisibility(VISIBLE);
                if(isLandScape){
                    mDialogplayerView.setVisibility(View.INVISIBLE);
                }else{
                    mExoPlayerView.setVisibility(View.INVISIBLE);
                }
                mProgressBar.setVisibility(GONE);


            }

            @Override
            public void onPositionDiscontinuity(int reason) {

                // for handling log
                switch(reason){
                    case Player.DISCONTINUITY_REASON_SEEK:
                        Utils.callEventLogApi("scrolled to  " + "<b>" + Utils.getMinHourSecFromSeconds(mPlayer.getContentPosition() / 1000) + "</b>" + " " + "<b> " + mVideoNameForListenerLog + "</b> in Feeds"  );

                        break;
                }


            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING){
                    mProgressBar.setVisibility(VISIBLE);
                }else{
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mDialogplayerView.setVisibility(VISIBLE);
                    mDialogplayerView.setControllerAutoShow(true);
                    mDialogplayerView.setUseController(true);
                }
            }
        });




        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);

        if(isNextVideoTapped){
            mResumePosition = 0;
            isNextVideoTapped = false;
        }

        mPlayer.seekTo(0, mResumePosition);

        mVideoError.setVisibility(View.INVISIBLE);
        mProgressBar.setVisibility(View.INVISIBLE);


        mPlayer.setPlayWhenReady(true);
    }

    private void playNextVideo() {
        if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
            AppPreferences.setVideoResumePosition(mFeedsList.get(mPosition).getId(),0);

        } else {
            AppPreferences.setVideoResumePosition(mFeedsList.get(mPosition).getId(),mPlayer.getContentPosition());

        }

        mPosition++;


        if(mPosition>=mFeedsList.size()){
            mPosition =0;
        }

        if(mPosition<mFeedsList.size() && isAutoPlay){
            mVideoNameForListenerLog = mFeedsList.get(mPosition).getTitle();
            mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);
            mVideoUrl = mFeedsList.get(mPosition).getVideo();
            callVideoUrl(mFeedsList.get(mPosition).getId());
        }else{
            Utils.releaseDetailScreenPlayer();
            mDialogplayerView.setVisibility(View.INVISIBLE);
            mVideoError.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }






    private void callVideoUrl(final String mItemId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer();
                    showNextVideo();

                } catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = mVideoUrl;
                    initExoPlayer();
                    showNextVideo();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = mVideoUrl;

                initExoPlayer();
                showNextVideo();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mItemId);
                params.put(Keys.ITEM_TYPE, "Announcements");
                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    private VideoData getCurrentVideoData(){
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null) {
                String seekTime;
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                    seekTime = "0";
                    mResumePosition = 0;
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                    seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
                    mResumePosition = mPlayer.getContentPosition();
                }

                videoDataToBeSent.setTime(seekTime);
                videoDataToBeSent.setType("Feeds");

                videoDataToBeSent.setVideoId(mFeedsList.get(mPosition).getId());

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }

    public void saveSeekTime() {


        //TODO implemented maintenance mode
        // calling sendSeekTime api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.SAVE_VIDEO_TIME_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                VideoData mVideoData = getCurrentVideoData();

                if (mVideoData != null) {
                    params.put(Keys.VIDEOS_ID, mVideoData.getVideoId());
                    params.put(Keys.COUNTRY, mVideoData.getCountry());
                    params.put(Keys.TIME, mVideoData.getTime());
                    params.put(Keys.TYPE, mVideoData.getType());
                }


                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


}

