package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.QuizResult;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.QuizResultView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;

import java.util.Map;


public class QuizResultFragment extends BaseFragment implements QuizResultView.onExpandClick{


    private WealthDragonTextView mTvQuizName;
    private LinearLayout mProgressBar,mLlItemContainer;
    private Context mContext;
    private String mQuizId;
    private FrameLayout mFmRetakeBtn , mFmReturnToCourse;
    private QuizResult.QuizResultData quiz;




    public static QuizResultFragment newInstance(String  quizId) {

        //  put data in bundle
        Bundle args = new Bundle();

        args.putString(Constants.QUIZ_ID,quizId);
        QuizResultFragment fragment = new QuizResultFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean hasKey = getArguments().containsKey(Constants.QUIZ_ID);
        if(hasKey){
            mQuizId = getArguments().getString(Constants.QUIZ_ID);
        }

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_quiz_result, container, false);

        mProgressBar = view.findViewById(R.id.progressBar);
        mTvQuizName = view.findViewById(R.id.tv_quiz_title);
        mFmRetakeBtn = view.findViewById(R.id.fl_retake);
        mFmReturnToCourse = view.findViewById(R.id.fl_return_to_course);
        mLlItemContainer = view.findViewById(R.id.ll_item_container);


        mFmReturnToCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Return to Course</b> from <b>"+quiz.getQuiz_title()+"</b> of <b>"+quiz.getCourse_title()+"</b>  Result Page");

                getActivity().onBackPressed();
            }
        });

        mFmRetakeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Retake</b> from <b>"+quiz.getQuiz_title()+"</b> of <b>"+quiz.getCourse_title()+"</b>  Result Page");

                retakeQuizApi();
                moveToQuizFragment();
            }
        });

        callQuizApi();

        return view;
    }

    private void callQuizApi() {

        String api = Api.QUIZ_RESULT_API;

        mProgressBar.setVisibility(View.VISIBLE);


        // calling quiz result api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressBar.setVisibility(View.GONE);
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>Quiz Result </b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>Quiz Result </b> error in api");
                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.QUIZ_LIST_ID,mQuizId);


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }



     // handling quiz result api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {

        //  parse json to java
        QuizResult quizData = new Gson().fromJson(response, QuizResult.class);
        quiz = quizData.getQuiz();

        if(quiz != null){

            mTvQuizName.setText(quiz.getQuiz_title());

            if(quiz.getAttempt().size()>0) {
                setDataInRecyclerView(quiz,0);
            }
        }

    }


    // set quiz data in recycler view
    private void setDataInRecyclerView(QuizResult.QuizResultData quizData , int position) {

        mLlItemContainer.removeAllViews();

        for(int i=0;i<quizData.getAttempt().size();i++){
            if(i == position) {
                mLlItemContainer.addView(new QuizResultView(mContext, quizData.getAttempt().get(i), this, i, true));
            }else{
                mLlItemContainer.addView(new QuizResultView(mContext, quizData.getAttempt().get(i), this, i, false));
            }

        }

    }



    //  move to quiz
    private void moveToQuizFragment(){
        FragmentTransaction transaction =  getFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.fm_main, QuizFragment.newInstance(mQuizId)).commit();
    }


    @Override
    public void onExpandClick(int position) {
        setDataInRecyclerView(quiz,position);
    }



    //  retake quiz
    private void retakeQuizApi() {

        String api = Api.RETAKE_QUIZ_API;



        //TODO implemented maintenance mode
        // calling retake quiz api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>Quiz Retake </b> error in api");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.QUIZ_LIST_ID,mQuizId);


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

}
