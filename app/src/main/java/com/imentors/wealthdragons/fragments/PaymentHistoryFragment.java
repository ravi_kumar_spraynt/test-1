package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.PaymentHistoryListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.PaymentHistory;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PaymentHistoryFragment extends BaseFragment implements PaymentHistoryListAdapter.OnListItemClicked{


    private LinearLayout mProgressBar , mNoDataFound;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private PaymentHistoryListAdapter mPaymentHistoryAdapter;

    public static PaymentHistoryFragment newInstance() {
        PaymentHistoryFragment fragment = new PaymentHistoryFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_payment_history, container, false);



        mProgressBar = view.findViewById(R.id.progressBar);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mNoDataFound = view.findViewById(R.id.no_data);

        //  set layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mPaymentHistoryAdapter = new PaymentHistoryListAdapter(mContext,this);

        callPaymentHistoryApi();


        return view;
    }

    public void callPaymentHistoryApi() {

        mProgressBar.setVisibility(View.VISIBLE);

        // calling payment history api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.PAYMENT_HISTORY_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                        mProgressBar.setVisibility(View.GONE);
                        onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>payment history</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>payment history</b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    //handling payment history api response
    @Override
    public void showDataOnView(String response)  {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {


        //  parse json to java
        PaymentHistory paymentHistory = new Gson().fromJson(response, PaymentHistory.class);

        if(paymentHistory.getOrderData() !=null){
            if(paymentHistory.getOrderData().size()>0){
                mNoDataFound.setVisibility(View.GONE);
                mPaymentHistoryAdapter.setNewItems(paymentHistory.getOrderData());
                mRecyclerView.setAdapter(mPaymentHistoryAdapter);
            }else{
                mNoDataFound.setVisibility(View.VISIBLE);
            }
        }else{
            mNoDataFound.setVisibility(View.VISIBLE);
        }

    }


    //  opened payment history detail
    @Override
    public void onItemClicked(PaymentHistory.OrderData item) {
        addFragment(PaymentHistoryDetailFragment.newInstance(item.getId()),true);
    }
    

}
