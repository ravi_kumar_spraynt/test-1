package com.imentors.wealthdragons.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.PaymentInVoice;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.FileDownloader;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Map;



public class PaymentHistoryDetailFragment extends BaseFragment {


    private WealthDragonTextView mTvPaymentPrice , mTvPaymentTotalPrice , mTvInvoice  ,mTvPaymentType , mTvPaymentConfirm , mTvPaymentDate ,mTvShippingTitle ,mTvShippingAddress, mTvShippingTown, mTvShippingPhn;
    private Context mContext;
    private LinearLayout mProgressBar,mProgress_bar;
    private StringBuilder mShippingTown = null;
    private String mOrderId;
    private ImageView mPdfViewer, mPdfDownloader, mPdfMailer;
    public static int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION =2;
    private PaymentInVoice mPaymentInvoice = null;


    public static PaymentHistoryDetailFragment newInstance(String orderId) {

        //  put data in bundle
        Bundle args= new Bundle();

        args.putString(Constants.ORDER_DATA,orderId);

        PaymentHistoryDetailFragment fragment = new PaymentHistoryDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOrderId =  getArguments().getString(Constants.ORDER_DATA);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_payment_history_detail, container, false);

        Utils.callEventLogApi("opened <b>Payment History Detail </b> page");



        mTvPaymentTotalPrice = view.findViewById(R.id.tv_total_price);
        mTvPaymentPrice = view.findViewById(R.id.tv_price);
        mTvInvoice = view.findViewById(R.id.tv_invoice);
        mTvPaymentType = view.findViewById(R.id.tv_member_type);
        mTvPaymentConfirm = view.findViewById(R.id.tv_payment_confirm);
        mTvPaymentDate = view.findViewById(R.id.tv_date);
        mProgressBar=view.findViewById(R.id.progressBar);

        mTvShippingTitle=view.findViewById(R.id.tv_shipping);
        mTvShippingAddress=view.findViewById(R.id.tv_shipping_address);
        mTvShippingTown=view.findViewById(R.id.tv_shipping_town);
        mTvShippingPhn=view.findViewById(R.id.tv_shipping_phone);

        mPdfDownloader = view.findViewById(R.id.iv_download);
        mPdfViewer = view.findViewById(R.id.iv_view);
        mPdfMailer = view.findViewById(R.id.iv_mail);
        mProgress_bar=view.findViewById(R.id.progress_bar);

        callPaymentInVoice();


        return view;
    }

    public void callPaymentInVoice() {
        mProgressBar.setVisibility(View.VISIBLE);

        // calling payment history api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.INVOICE_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgressBar.setVisibility(View.GONE);
                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>payment history</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>payment history</b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ORDER_ID,mOrderId);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        if(!TextUtils.isEmpty(response)) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if(!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))){
                    return;
                }else{
                    handleDashBoardResponse(response);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    private void handleDashBoardResponse(String response) {


        //  parse json to java
        PaymentInVoice paymentInVoice = new Gson().fromJson(response, PaymentInVoice.class);
        mPaymentInvoice = paymentInVoice;

        setDataOnView(paymentInVoice);

    }

    private void setDataOnView(final PaymentInVoice paymentInVoice) {
        //  set data on view
        mTvPaymentDate.setText(paymentInVoice.getDate());
        mTvPaymentType.setText(paymentInVoice.getItem_name());
        mTvInvoice.setText(getString(R.string.invoice)+" "+paymentInVoice.getInvoice_id());
        mTvPaymentTotalPrice.setText(paymentInVoice.getCurrencyIcon() +paymentInVoice.getPrice());
        mTvPaymentPrice.setText(paymentInVoice.getCurrencyIcon()+paymentInVoice.getPrice());
        mTvPaymentConfirm.setText(getString(R.string.payment_confirm) + " "+paymentInVoice.getCurrencyIcon()+ paymentInVoice.getPrice());

        if(paymentInVoice.isShippingDetailsVisible()){
            mTvShippingTitle.setVisibility(View.VISIBLE);

            if(!TextUtils.isEmpty(paymentInVoice.getShipping_address())){
                mTvShippingAddress.setVisibility(View.VISIBLE);
                mTvShippingAddress.setText(paymentInVoice.getShipping_address());
            }

            if(!TextUtils.isEmpty(paymentInVoice.getShipping_town()) || !TextUtils.isEmpty(paymentInVoice.getShipping_county()) || !TextUtils.isEmpty(paymentInVoice.getShipping_zipcode())){
                mTvShippingTown.setVisibility(View.VISIBLE);

                addComa(paymentInVoice.getShipping_town());
                addComa(paymentInVoice.getShipping_county());

                mTvShippingTown.setText(mShippingTown + paymentInVoice.getShipping_zipcode());
            }

            if(!TextUtils.isEmpty(paymentInVoice.getShipping_phone())){
                mTvShippingPhn.setVisibility(View.VISIBLE);
                mTvShippingPhn.setText("Phone: "+ paymentInVoice.getShipping_phone());
            }



        }


        mPdfViewer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(paymentInVoice.getPdf_file())) {
                    Utils.callEventLogApi("opened <b>" + paymentInVoice.getPdf_file() + " document</b>");

                    Utils.openDocument(paymentInVoice.getPdf_file(), mContext);
                }else{
                    Toast.makeText(mContext,"No invoice available",Toast.LENGTH_SHORT).show();
                }
            }
        });


        mPdfDownloader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(paymentInVoice.getPdf_file())) {
                    Utils.callEventLogApi("opened <b>" + paymentInVoice.getPdf_file() + " document</b>");

                    int writeExternalStoragePermission = ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE);
// If do not grant write external storage permission.
                    if(writeExternalStoragePermission!= PackageManager.PERMISSION_GRANTED)
                    {
                        // Request user to grant write external storage permission.
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                    }else {

                        saveFile(paymentInVoice);

                    }
                }else{
                    Toast.makeText(mContext,"No Invoice available",Toast.LENGTH_SHORT).show();
                }
            }
        });

        mPdfMailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(paymentInVoice.getPdf_file())) {
                    if (!TextUtils.isEmpty(paymentInVoice.getEmail())) {
                        Utils.callEventLogApi("clicked <b>Email Icon</b> in payment history detail");
                        ResendInvoice();

                    }else{
                        Toast.makeText(mContext,"No Email available",Toast.LENGTH_SHORT).show();
                    }
                    }else{
                    Toast.makeText(mContext,"No Invoice available",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void saveFile(){
        saveFile(mPaymentInvoice);
    }

    private void saveFile(PaymentInVoice paymentInVoice) {
        Toast.makeText(mContext,"Downloading Invoice",Toast.LENGTH_SHORT).show();

        final StringBuilder fileName = new StringBuilder();
        if (!TextUtils.isEmpty(paymentInVoice.getInvoice_id())) {
            fileName.append("Invoice");
            fileName.append(paymentInVoice.getInvoice_id());
            fileName.append(".pdf");
        }else{
            fileName.append("Invoice");
            fileName.append(System.currentTimeMillis());
            fileName.append(".pdf");
        }

        new DownloadFile().execute(paymentInVoice.getPdf_file(), fileName.toString());
    }


    private void addComa(String address){
        if(!TextUtils.isEmpty(address)){
            mShippingTown.append(address);
            mShippingTown.append(", ");
        }
    }


    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            File extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                try {
                    File pdfFile = new File(extStorageDirectory,fileName);
                    FileDownloader.downloadFile(fileUrl, pdfFile, getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            return null;

        }
    }



    public void ResendInvoice()
    {
        mProgress_bar.setVisibility(View.VISIBLE);
        // calling resend invoice api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.RESEND_INVOICE_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mProgress_bar.setVisibility(View.GONE);
                    Toast.makeText(mContext,"E-mail sent sucessfully",Toast.LENGTH_SHORT).show();
                    onGetDataAPIServerResponse(response);

                } catch (Exception e) {
                    mProgress_bar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>resend invoice</b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress_bar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>payment history</b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ORDER_ID,mOrderId);

                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }

}

