package com.imentors.wealthdragons.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.rubensousa.gravitysnaphelper.GravityPagerSnapHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.SlidingImageAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.TabChangingListener;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardItemOderingArticle;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.MaintainceNotification;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.ItemTypeDeserilization;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.DashBoardTabView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class DashBoardFragment extends BaseFragment {


    /**
     * use always for slider while getting position  = position % itemList.size()
     */


    private Context mContext;
    private long mRequestStartTime;
    private long mResponseTime;

    private static int currentWidth = 0;
    private static boolean isTouchEnabled=true;
    private RecyclerView mRecyclerViewpager;
    private SlidingImageAdapter mSlidingImageAdapter;
    private LinearLayout mLlScreenContainer,mLlSliderContainer,mProgressBar,mNoRecordFound;
    private LinearLayoutManager layoutManager;
    private Timer swipeTimer;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private Runnable runnable;
    private Handler handler;
    private boolean isTablet;
    private Toolbar mToolbar;
    private boolean isDown = false;
    private DashBoard mDashBoard;
    private TabChangingListener mTabChangingListeners;
    private WealthDragonTextView mSearchLayout;
    private SlidingImageAdapter.OnItemClickListener mSliderImageClickListner;
    private FrameLayout mNoInternetConnection;




    public static DashBoardFragment newInstance() {

        Bundle args = new Bundle();

        DashBoardFragment fragment = new DashBoardFragment();
        fragment.setArguments(args);
        return fragment;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        if(mContext instanceof TabChangingListener){
            mTabChangingListeners = (TabChangingListener) mContext;
        }

        if(mContext instanceof SlidingImageAdapter.OnItemClickListener){
            mSliderImageClickListner = (SlidingImageAdapter.OnItemClickListener) mContext;
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);
        Utils.callEventLogApi("opened <b>Dashboard Screen</b>");


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        mProgressBar = view.findViewById(R.id.progressBar);
        mLlScreenContainer = view.findViewById(R.id.ll_screen_container);
        mRecyclerViewpager =  view.findViewById(R.id.pager);
        layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
        mLlSliderContainer = view.findViewById(R.id.image_slider_layout);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSearchLayout = view.findViewById(R.id.tv_search);
        mNoRecordFound = view.findViewById(R.id.no_record_available);
        mNoInternetConnection = view.findViewById(R.id.no_internet);
        mSearchLayout.setVisibility(View.VISIBLE);


        view.findViewById(R.id.tv_downloads).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addFragment(MyGroupDownloadFragment.newInstance(),true);

            }
        });


        view.findViewById(R.id.tv_retry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callDashBoardApi();
            }
        });

        callDashBoardApi();

        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!AppPreferences.getMaintainceModeCrossClicked()){
                    callMaintainceNotificaitonApi();
                }
                callDashBoardApi();
                Utils.callEventLogApi("refreshed <b>DashBoard Screen</b>" );

            }
        });

        LocalBroadcastManager.getInstance(mContext).registerReceiver(mReloadPayResponseListener, new IntentFilter(Constants.BROADCAST_PAY_FINSIH));

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if(!Utils.isNetworkAvailable(mContext)){
                addFragment(MyGroupDownloadFragment.newInstance(),true);
        }
    }

    // set refreshing
    public void callDashBoardApi() {
        mNoInternetConnection.setVisibility(View.GONE);


        if(swipeTimer!=null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);

        }


        mRequestStartTime = System.currentTimeMillis();
        // calling dashboard api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.DASHBOARD_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mResponseTime = System.currentTimeMillis() - mRequestStartTime;
                mProgressBar.setVisibility(View.GONE);
                if(mSwipeRefershLayout.isRefreshing()){
                    mSwipeRefershLayout.setRefreshing(false);
                }
                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    Utils.callEventLogApi("getting <b>DashBoard api</b> error in api" );
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                Utils.callEventLogApi("getting <b>DashBoard api</b> error in api" );
                mSwipeRefershLayout.setRefreshing(false);

                error.printStackTrace();

                if(!Utils.isNetworkAvailable(mContext)) {
                    mNoInternetConnection.setVisibility(View.VISIBLE);
                }else{
                    Utils.showNetworkError(mContext, error);
                }



            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling dashboard api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);
        handleDashBoardResponse(response);
    }

    private void handleDashBoardResponse(String response) {
        mLlScreenContainer.removeAllViews();


        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(new Intent(Constants.BROADCAST_DASHBOARD_TAB_LIST_UPDATED));

        final GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.registerTypeAdapter(DashBoardItemOdering.class, new ItemTypeDeserilization());
        final Gson gson = gsonBuilder.create();

        // Parse JSON to Java
        final DashBoard dashBoard = gson.fromJson(response, DashBoard.class);
        mDashBoard = dashBoard;

        AppPreferences.setTabDifferences(mDashBoard);
        AppPreferences.setAutoplay(mDashBoard.getNext_episode());

        //  set dashboard from app preferences
        AppPreferences.setDashBoard(dashBoard);

        if(AppPreferences.isTabChanged()){
            mTabChangingListeners.onTabChanged();
        }


        if(dashBoard.getSlider()!=null && dashBoard.getSlider().size()>0) {
            mLlScreenContainer.addView(mLlSliderContainer);

            setupSlider(dashBoard);
        }



        //  for each loop
        for (DashBoardItemOdering dashBoardItemOdering : dashBoard.getHeadingOrdering()) {

            String layoutType = dashBoardItemOdering.getLayout();

            switch (layoutType) {
                case Constants.TYPE_COURSE:

                    DashBoardItemOderingArticle dashBoardItemOderingArticle = (DashBoardItemOderingArticle) dashBoardItemOdering;
                    if(dashBoardItemOderingArticle.getItems().size()>0) {
                        mLlScreenContainer.addView(new DashBoardTabView(mContext, dashBoardItemOderingArticle,false));
                    }

                    continue;

                case Constants.TYPE_PROGRAMMES:

                    DashBoardItemOderingArticle dashBoardItemOderingProgramme = (DashBoardItemOderingArticle) dashBoardItemOdering;
                    if(dashBoardItemOderingProgramme.getItems().size()>0) {
                        mLlScreenContainer.addView(new DashBoardTabView(mContext, dashBoardItemOderingProgramme,true));
                    }

                    continue;

                case Constants.TYPE_VIDEO:

                    DashBoardItemOderingVideo dashBoardItemOderingVideo = (DashBoardItemOderingVideo) dashBoardItemOdering;


                    if(dashBoardItemOderingVideo.getItems().size()>0) {
                        mLlScreenContainer.addView(new DashBoardTabView(mContext, dashBoardItemOderingVideo));
                    }


                    continue;


                case Constants.TYPE_MENTOR:
                    DashBoardItemOderingMentors dashBoardItemOderingMentors = (DashBoardItemOderingMentors) dashBoardItemOdering;

                    if(dashBoardItemOderingMentors.getItems().size()>0) {

                        mLlScreenContainer.addView(new DashBoardTabView(mContext, dashBoardItemOderingMentors,dashBoard.getContetnType()));
                    }

                    continue;

                case Constants.TYPE_EVENT:
                    DashBoardItemOderingEvent dashBoardItemOderingEvents = (DashBoardItemOderingEvent) dashBoardItemOdering;

                    if(dashBoardItemOderingEvents.getItems().size()>0) {

                        mLlScreenContainer.addView(new DashBoardTabView(mContext, dashBoardItemOderingEvents));
                    }

                    continue;
            }

        }

//        if(AppPreferences.getIsDataLoaded()){
//            mProgressBar.setVisibility(View.GONE);
//            startSliderAutoScroll();
//        }else {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    startSliderAutoScroll();
//                }
//            }, 7000);
//            AppPreferences.setIsDataLoaded();
//        }


        if(mLlScreenContainer.getChildCount()>0){
            mNoRecordFound.setVisibility(View.GONE);

        }else{
            mLlScreenContainer.addView(mNoRecordFound);
            mNoRecordFound.setVisibility(View.VISIBLE);
        }

    }


    //handling scrolling slider
    private void setupSlider(final DashBoard dashBoard) {

        isTouchEnabled = true;

        if(swipeTimer!=null) {
            swipeTimer.cancel();
            swipeTimer = null;

        }




        if(mSlidingImageAdapter!=null){
            mSlidingImageAdapter.clearItems();
        }


        mSlidingImageAdapter = new SlidingImageAdapter(mSliderImageClickListner,mContext);
        mRecyclerViewpager.setLayoutManager(layoutManager);

        mRecyclerViewpager.setAdapter(mSlidingImageAdapter);

        if(dashBoard.getSlider()!=null) {
            mSlidingImageAdapter.setItems(dashBoard.getSlider());
        }

        if(isTablet){
            layoutManager.scrollToPosition(2);

        }else{
            layoutManager.scrollToPosition(1);
        }


        PagerSnapHelper snapHelper = new GravityPagerSnapHelper(Gravity.START);
        snapHelper.attachToRecyclerView(mRecyclerViewpager);

        mRecyclerViewpager.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(!isTablet) {

                    // animate if item count larger than 4
                    if(dashBoard.isSldingAnimation()) {
                        int firstItemVisible = layoutManager.findFirstVisibleItemPosition();
                        if (firstItemVisible > 0 && firstItemVisible % (mSlidingImageAdapter.getItemCount() - 1) == 0) {
                            if (recyclerView != null) {
                                recyclerView.scrollToPosition(1);
                            }
                        } else if (firstItemVisible == 0) {
                            if (recyclerView != null) {
                                recyclerView.scrollToPosition(mSlidingImageAdapter.getItemCount() - 1);
                            }
                        }

                    }
                } else {

                    // animate if item count larger than 2
                    if (dashBoard.isSldingAnimation()) {
                        int lastItemVisible = layoutManager.findLastCompletelyVisibleItemPosition();
                        int firstItemVisible = layoutManager.findFirstCompletelyVisibleItemPosition();

                        if (lastItemVisible > 0 && lastItemVisible % (mSlidingImageAdapter.getItemCount() - 1) == 0) {
                            if (recyclerView != null) {
                                recyclerView.scrollToPosition(2);
                            }
                        } else if (firstItemVisible == 0) {
                            if (recyclerView != null) {
                                recyclerView.scrollToPosition(mSlidingImageAdapter.getItemCount() - 3);
                            }
                        }

                    }
                }


            }


        });


        mRecyclerViewpager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                return !isTouchEnabled;

            }
        });

        if(getActivity()!=null) {
            if (!getActivity().isDestroyed()) {
                if (dashBoard.isSldingAnimation()) {
                    autoScroll();
                }
            }
        }



        if(getActivity()!=null) {
            if (!getActivity().isDestroyed()) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                        if (dashBoard.isSldingAnimation()) {
                            startSliderAutoScroll();
                        }
            }
        }, 2000);
    }}
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void autoScroll() {

        final int widthTobeScrolled;

        if(isTablet){

            widthTobeScrolled = (int)Utils.getScreenWidth(getActivity())/2;

        }else {
            widthTobeScrolled = (int)Utils.getScreenWidth(getActivity());

        }

        if(handler!=null) {
            handler.removeCallbacks(runnable);
        }


        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                if(getActivity()!=null) {
                    if (!getActivity().isDestroyed() && handler!=null) {

                        if (isTablet) {

                                mRecyclerViewpager.scrollBy(16, 0);
                                currentWidth += 16;

                                // disable touch during animation
                                isTouchEnabled = false;

                                if (currentWidth >= widthTobeScrolled) {
                                    handler.removeCallbacks(this);

                                    // enable touch afer animation
                                    isTouchEnabled = true;
                                    currentWidth = 0;
                                } else {
                                    handler.postDelayed(this, 0);
                                }

                        } else {

                            mRecyclerViewpager.scrollBy(20, 0);
                            currentWidth += 20;

                            // disable touch during animation
                            isTouchEnabled = false;

                            if (currentWidth >= widthTobeScrolled) {
                                handler.removeCallbacks(this);

                                // enable touch afer animation
                                isTouchEnabled = true;
                                currentWidth = 0;
                            } else {
                                handler.postDelayed(this, 0);
                            }

                        }
                    }
                }
            }
        };



    }


    // scrolling auto slider
    private void startSliderAutoScroll() {

        swipeTimer = new Timer();

        if(swipeTimer!=null) {
            if (isTablet) {
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        if (mRecyclerViewpager.getScrollState() == RecyclerView.SCROLL_STATE_IDLE && isTouchEnabled) {
                            handler.post(runnable);
                        }

                    }
                }, 5000, 3000);
            } else {
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        if (mRecyclerViewpager.getScrollState() == RecyclerView.SCROLL_STATE_IDLE && isTouchEnabled) {
                            handler.post(runnable);
                        }

                    }
                }, 5000, 3000);
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mReloadPayResponseListener);


        if(swipeTimer!=null) {
            swipeTimer.cancel();
            swipeTimer = null;
        }
    }





    public void startSlider(){

        try {
            if (handler != null) {
                handler = null;
                runnable = null;
                currentWidth = 0;
            }

            // checking for null
            if (mDashBoard.getPlanSetting() != null) {
                setupSlider(mDashBoard);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }


    public void hideSlider(boolean isVisible){
            if (!isVisible) {
                if (mLlSliderContainer.getVisibility() == View.VISIBLE) {
                    mLlSliderContainer.setVisibility(View.GONE);
                }
            } else {
                if (mLlSliderContainer.getVisibility() == View.GONE) {
                    mLlSliderContainer.setVisibility(View.VISIBLE);
                }
            }
    }

    private BroadcastReceiver mReloadPayResponseListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callDashBoardApi();
        }
    };

    private void callMaintainceNotificaitonApi() {
        AppPreferences.setMaintainceModeUpdate(null);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MAINTENANCE_NOTIFICATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MaintainceNotification maintainceNotification = new Gson().fromJson(response,MaintainceNotification.class);
                if(maintainceNotification.getMain_status().equals("1")){
                    WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(getActivity());
                }else if(maintainceNotification.getStatus().equals("1")){
                    showMaintainaceMarquee(maintainceNotification.getNotification_text());
                    AppPreferences.setMaintainceModeUpdate(maintainceNotification.getNotification_text());
//                    Utils.showSimpleAlertDialog(MainActivity.this, getString(R.string.maintaince_mode), maintainceNotification.getNotification_text(), getString(R.string.okay_text));
                }else if(maintainceNotification.getStatus().equals("2")){
                    showMaintainaceMarquee(null);
                    AppPreferences.setMaintainceModeUpdate(null);
                }
                // no reaction
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // no reaction
                error.printStackTrace();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void showMaintainaceMarquee(String maintainanceMessage) {

        LinearLayout mMarkview= getActivity().findViewById(R.id.marquee);
        TextView mTvMaintainceMode = mMarkview.findViewById(R.id.tv_marquee);

        if(!TextUtils.isEmpty(maintainanceMessage)) {
            mMarkview.setVisibility(View.VISIBLE);
            mTvMaintainceMode.setText(maintainanceMessage);
            mMarkview.setSelected(true);
        }else{
            mMarkview.setVisibility(View.GONE);
        }
    }

}
