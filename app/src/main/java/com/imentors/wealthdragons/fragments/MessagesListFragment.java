package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.MessagesListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.Messages;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MessagesListFragment extends BaseFragment implements MessagesListAdapter.onMessagesItemClicked{



    private LinearLayout mProgressBar,mNoData;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private MessagesListAdapter mMessageListAdapter;
    private SwipeRefreshLayout mSwipeRefershLayout;
    private Messages messageListData;



    public static MessagesListFragment newInstance() {
        MessagesListFragment fragment = new MessagesListFragment();
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_messages_list, container, false);
        mProgressBar = view.findViewById(R.id.progressBar);
        mRecyclerView = view.findViewById(R.id.listView);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callMessageApi();
                Utils.callEventLogApi("refreshed<b> Message List Screen</b>");

            }
        });
        mNoData = view.findViewById(R.id.no_data);

        callMessageApi();


        return view;
    }


    //  set refreshing
    public void callMessageApi() {

        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);

        }else {
            mSwipeRefershLayout.setRefreshing(false);
            mProgressBar.setVisibility(View.VISIBLE);

        }


        // calling message api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MESSAGE_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefershLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Message List </b> error in api");

                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);
                Utils.callEventLogApi("getting <b>Message List </b> error in api");

                error.printStackTrace();
                Utils.showNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling message api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        //  parse JSON to java
        messageListData = new Gson().fromJson(response, Messages.class);
        showMessageList(messageListData);

    }

    //  show message list
    private void showMessageList(Messages response) {

        if(response.getNotifications().size()>0) {

            mNoData.setVisibility(View.GONE);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            mRecyclerView.setLayoutManager(mLayoutManager);
            mMessageListAdapter = new MessagesListAdapter(response.getNotifications(),this,mContext);
            mRecyclerView.setAdapter(mMessageListAdapter);
        }else{
            mNoData.setVisibility(View.VISIBLE);
        }
    }

    //  clicked message item
    @Override
    public void onMessagetemClick(Messages.Notification item , int position) {
        messageListData.getNotifications().get(position).setSeen(getString(R.string.read));
        showMessageList(messageListData);
        mMessageListAdapter.notifyDataSetChanged();
        addFragment(MessageDetailFragment.newInstance(item.getId()),true);
    }


    //  closed message list page
    @Override
    public void onStop() {
        super.onStop();
        Utils.callEventLogApi("closed <b>Message List </b> page");

    }
}
