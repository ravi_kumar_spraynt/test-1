package com.imentors.wealthdragons.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.ExistingSupportRequestListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ExistingSupportRequestListFragment extends BaseFragment implements ExistingSupportRequestListAdapter.OnViewDetailClick {



    private LinearLayout mProgressBar , mNoDataFound;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ExistingSupportRequestListAdapter mExistingSupportRequestListAdapter;
    private SwipeRefreshLayout mSwipeRefershLayout;




    public static ExistingSupportRequestListFragment newInstance() {
        ExistingSupportRequestListFragment fragment = new ExistingSupportRequestListFragment();
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;



    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_existing_support_request, container, false);

        mProgressBar = view.findViewById(R.id.progress_bar);
        mRecyclerView = view.findViewById(R.id.listView);
        mSwipeRefershLayout = view.findViewById(R.id.swipeRefreshLayout);
        mNoDataFound = view.findViewById(R.id.no_data);


        mSwipeRefershLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                callExistingSupportApi();
                Utils.callEventLogApi("refreshed <b>Existing Support Request Reply Screen</b>");

            }
        });

        callExistingSupportApi();

        return view;
    }


    public void callExistingSupportApi() {

        //  set refreshing
        if(mSwipeRefershLayout.isRefreshing()){
            mSwipeRefershLayout.setRefreshing(true);
            mProgressBar.setVisibility(View.GONE);

        }else {
            mSwipeRefershLayout.setRefreshing(false);
            mProgressBar.setVisibility(View.VISIBLE);

        }


        // calling existing support request api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.EXISTING_SUPPORT_REQUESTS_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);

                try {
                    onGetDataAPIServerResponse(response);
                } catch (Exception e) {
                    mProgressBar.setVisibility(View.GONE);
                    mSwipeRefershLayout.setRefreshing(false);
                    Utils.callEventLogApi("getting <b>Existing Support</b> error in api");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefershLayout.setRefreshing(false);
                Utils.callEventLogApi("getting <b>Existing Support</b> error in api");
                error.printStackTrace();
                Utils.showCustomerSupportNetworkError(mContext, error);
                mSwipeRefershLayout.setRefreshing(false);


            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    //handling existing support request api response
    @Override
    public void showDataOnView(String response) {
        super.showDataOnView(response);

        //used to deserlize direct list
//        List<SupportType> existingSuppportList =  new Gson().fromJson(response,  new TypeToken<List<SupportType>>(){}.getType());

        //  parse JSON to java
        SupportType existingSuppportList =  new Gson().fromJson(response,  SupportType.class);


        //  show message or no data found
        if(existingSuppportList.getMessage().size()>0){
            mNoDataFound.setVisibility(View.GONE);
            showSupportRequestList(existingSuppportList.getMessage());
        }else{
            mNoDataFound.setVisibility(View.VISIBLE);
        }

    }

    //  show recycler list
    private void showSupportRequestList(List<SupportType.ExistingSupportMessage> supportTypeList) {
        mExistingSupportRequestListAdapter = new ExistingSupportRequestListAdapter(supportTypeList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mExistingSupportRequestListAdapter);
    }

    // clicked detail
    @Override
    public void onViewDetailClicked(SupportType.ExistingSupportMessage item) {
        // move to detail fragment

        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fm_customer, ExistingSupportRequestDetailFragment.newInstance(item));
        ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }
}
