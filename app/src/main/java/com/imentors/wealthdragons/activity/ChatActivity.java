package com.imentors.wealthdragons.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.dialogFragment.ChatCommentDialog;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.LiveChatErrorDialog;
import com.imentors.wealthdragons.dialogFragment.LiveChatThankYouDialog;
import com.imentors.wealthdragons.utils.AppPreferences;

import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

public class ChatActivity extends BaseActivity implements LiveChatErrorDialog.onRetryButtonClicked {


    private PlayerView mExoPlayerView;

    private SimpleExoPlayer mPlayerHeader;

    private String mVideoUrls = "";


    private TextView mTvConnecting;
    private TextView mTvLiveCount;


    private String notificationId;
    private String mThankYouMessage;
    private String mLiveChatErrorTitle;
    private String mExpertId;
    private String mLiveDigiCoachingId;
    private String mLiveSassionRowId;


    private ChatCommentDialog dialogChatFrag;
    private LiveChatThankYouDialog dialogLiveChatThank;
    private LiveChatErrorDialog dialogLiveChatError;

    private ImageView mIvClose;
    private boolean mIsReload;

    private Timer mLiveUserCountTimer;

    private int mLiveUserCount = 0;
    private BroadcastReceiver mDismissAlertDialog = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            callNotificationApi();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_live_chat);

        Utils.getScreenHeight(this);

        Utils.callEventLogApi("opened <b> Live Chat</b> page");


        AppPreferences.setLiveChatActivityState(true);

        LocalBroadcastManager.getInstance(this).registerReceiver(mDismissAlertDialog,
                new IntentFilter(Constants.BROADCAST_DISMISS_ALERT_DIALOG));

        mExoPlayerView = findViewById(R.id.exoplayer);
        mTvConnecting = findViewById(R.id.tv_connecting);
        mIvClose = findViewById(R.id.iv_close);
        FrameLayout mBtLive = findViewById(R.id.bt_live);
        FrameLayout mBtLiveCount = findViewById(R.id.bt_live_people);
        mTvLiveCount = findViewById(R.id.tv_live_count);

        mIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        handleNotification();

        dialogChatFrag = ChatCommentDialog.newInstance();
        dialogChatFrag.show(getSupportFragmentManager(), Constants.DIALOG);
        dialogChatFrag.setCancelable(false);
        dialogChatFrag.setListener(isCrossVisible -> {
            if (isCrossVisible) {
                mIvClose.setVisibility(View.GONE);
                mBtLive.setVisibility(View.GONE);
                mBtLiveCount.setVisibility(View.GONE);

            } else {
                mIvClose.setVisibility(View.VISIBLE);
                mBtLive.setVisibility(View.VISIBLE);
                mBtLiveCount.setVisibility(View.VISIBLE);


            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        callNotificationApi();

    }

    private void handleNotification() {
        //checking for intent
        Intent intentData = getIntent();
        //checking for extras in form of bundle
        Bundle bundleData = intentData.getExtras();


        // checking empty condition
        if (!bundleData.isEmpty()) {
            boolean hasNotification = bundleData.containsKey(Keys.NOTIFICATION);
            boolean hasChatId = bundleData.containsKey(Keys.CHAT_USER_ID);
            boolean hasExpertId = bundleData.containsKey(Keys.EXPERT_ID);
            boolean hasReloadPage = bundleData.containsKey(Keys.RELOAD_PAGE);
            boolean hasLiveDigiCoachingId = bundleData.containsKey(Keys.LIVE_DIGI_COACHING_ID);

            if (hasNotification) {

                HashMap<String, String> map = (HashMap<String, String>) bundleData.getSerializable(Keys.NOTIFICATION);
                notificationId = map.get(Keys.ID);
                mExpertId = map.get(Keys.EXPERT_ID);
            }

            if (hasChatId) {
                notificationId = bundleData.getString(Keys.CHAT_USER_ID);

            }


            if (hasExpertId) {
                mExpertId = bundleData.getString(Keys.EXPERT_ID);
            }

            if (hasReloadPage) {
                mIsReload = bundleData.getBoolean(Keys.RELOAD_PAGE);

            }

            if (hasLiveDigiCoachingId) {
                mLiveDigiCoachingId = bundleData.getString(Keys.LIVE_DIGI_COACHING_ID);
            }


        }
    }

    private void setPlayerListener() {
        mPlayerHeader.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                // no use
            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                // no use
            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
                // no use
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == Player.STATE_READY) {
                    mTvConnecting.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {
                // no use
            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {
                // no use
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

                if (Utils.isNetworkAvailable(ChatActivity.this)) {

                    if (isBehindLiveWindow(error)) {
                        // Re-initialize player at the live edge.
                        initilizePlayer(getApplicationContext(), mVideoUrls);
                    } else if (error.getSourceException().getMessage().contains("403") || error.getSourceException().getMessage().contains("404") || error.getSourceException().getMessage().contains("410")) {

                        mIvClose.setVisibility(View.GONE);
                        mTvConnecting.setVisibility(View.GONE);
                        dialogLiveChatThank = LiveChatThankYouDialog.newInstance(mThankYouMessage);
                        dialogLiveChatThank.show(getSupportFragmentManager(), Constants.DIALOG);
                        dialogLiveChatThank.setCancelable(false);

                    } else {

                        initilizePlayer(getApplicationContext(), mVideoUrls);

                    }


                } else {

                    mIvClose.setVisibility(View.GONE);
                    mTvConnecting.setVisibility(View.GONE);
                    dialogLiveChatError = LiveChatErrorDialog.newInstance(false, mLiveChatErrorTitle);
                    dialogLiveChatError.show(getSupportFragmentManager(), Constants.DIALOG);
                    dialogLiveChatError.setListener(ChatActivity.this);
                    dialogLiveChatError.setCancelable(false);


                }
            }

            @Override
            public void onPositionDiscontinuity(int reason) {
                // no use
            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                // no use
            }

            @Override
            public void onSeekProcessed() {
                // no use
            }
        });
    }

    //  closed about us page
    @Override
    public void onStop() {

        callLiveSessionClosed(mExpertId);

        super.onStop();

        if (mLiveUserCountTimer != null) {
            mLiveUserCountTimer.cancel();
        }
        AppPreferences.setLiveChatActivityState(false);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDismissAlertDialog);


        Utils.callEventLogApi("<b>Live Chat</b> page left");
        if (mPlayerHeader != null) {
            mPlayerHeader.release();
            mPlayerHeader = null;
        }


    }

    @Override
    public void onBackStackChanged() {
        throw new UnsupportedOperationException();
    }

    private void initilizePlayer(Context mContext, String mVideoUrls) {
        mTvConnecting.setVisibility(View.VISIBLE);


        if (mPlayerHeader != null) {
            mPlayerHeader.release();
        }

        mPlayerHeader = null;

        if (!TextUtils.isEmpty(mVideoUrls)) {
            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
            LoadControl loadControl = new DefaultLoadControl();
            mPlayerHeader = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(mContext), trackSelector, loadControl);

            attachPlayerWithMediaSource(mVideoUrls, mContext);

            setPlayerListener();

        } else {

            callNotificationApi();

        }


    }

    private void callLiveUserCount(final String expertId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LIVE_USER_COUNT, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);


                if (!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))) {

                    String message = jsonObject.getString(Keys._MAINTAINANCE);
                    if (message.equals(Keys.ON)) {
                        AppPreferences.setMaintainceMode(true);
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(ChatActivity.this);
                        return;
                    }

                } else if (!TextUtils.isEmpty(jsonObject.optString(Constants.STATUS))) {

                    String message = jsonObject.getString(Constants.STATUS);
                    mLiveUserCount = jsonObject.getInt(Keys.LIVE_AUDIENCE);

                    if (message.equals("Succes") && mTvLiveCount != null && mTvLiveCount.getVisibility() == View.VISIBLE) {
                        mTvLiveCount.setText(String.valueOf(mLiveUserCount));

                    }
                }

            } catch (Exception e) {
                Logger.getLogger(Constants.LOG, e.toString());
            }

        }, error -> Logger.getLogger(Constants.LOG, error.toString())

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ITEM_ID, mLiveDigiCoachingId);
                params.put(Keys.MENTOR_ID, expertId);

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void startTimerForUserCount() {
        mLiveUserCountTimer = new Timer();

        mLiveUserCountTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                callLiveUserCount(mExpertId);
            }
        }, 100, 1000);
    }

    private void attachPlayerWithMediaSource(String eClassesList, Context mContext) {

        if (!TextUtils.isEmpty(eClassesList)) {
            String userAgent = Util.getUserAgent(mContext, mContext.getApplicationContext().getApplicationInfo().packageName);

            // Produces DataSource instances through which media data is loaded.
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(mContext, userAgent);

            Uri daUri = Uri.parse(eClassesList);
            HlsMediaSource hlsMediaSource = new HlsMediaSource.Factory(dataSourceFactory).setAllowChunklessPreparation(true).createMediaSource(daUri);


            mPlayerHeader.prepare(hlsMediaSource);
            mPlayerHeader.clearVideoSurface();

            mExoPlayerView.setPlayer(mPlayerHeader);
            mPlayerHeader.setPlayWhenReady(true);

        }


    }

    private boolean isBehindLiveWindow(ExoPlaybackException e) {
        if (e.type != ExoPlaybackException.TYPE_SOURCE) {
            return false;
        }
        Throwable cause = e.getSourceException();
        while (cause != null) {
            if (cause instanceof BehindLiveWindowException) {
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }

    private void callNotificationApi() {

        mTvConnecting.setVisibility(View.VISIBLE);


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.WATCH_LIVE, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);


                if (!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))) {

                    String message = jsonObject.getString(Keys._MAINTAINANCE);
                    if (message.equals(Keys.ON)) {
                        AppPreferences.setMaintainceMode(true);
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(ChatActivity.this);
                        return;
                    }

                } else {

                    onGetDataAPIServerResponse(response);


                }

            } catch (Exception e) {
                Logger.getLogger(Constants.LOG, e.toString());
            }

        }, error -> {
            Logger.getLogger(Constants.LOG, error.toString());
            if (error instanceof NoConnectionError) {
                Utils.showInternetError(ChatActivity.this, true);
            } else {
                Utils.showAlertDialog(ChatActivity.this, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), true, true);
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.ID, notificationId);

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }

    // using only for chat activity

    @Override
    public void finish() {

        Intent returnIntent = new Intent();
        returnIntent.putExtra(Keys.RELOAD_PAGE, mIsReload);
        returnIntent.putExtra(Keys.MENTOR_ID, mExpertId);

        if (TextUtils.isEmpty(mExpertId)) {
            setResult(RESULT_CANCELED, returnIntent);
        } else {
            setResult(RESULT_OK, returnIntent);
        }

        super.finish();
    }

    public void onGetDataAPIServerResponse(String response) throws JSONException {


        JSONObject jsonObject = new JSONObject(response);


        if (!TextUtils.isEmpty(jsonObject.optString(Keys.ERROR))) {

            String message = jsonObject.getString(Keys.ERROR);


            Utils.showAlertDialog(this, getString(R.string.error), message, getString(R.string.okay_text), true, true);


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys.INACTIVE))) {

            String message = jsonObject.getString(Keys.INACTIVE);


            Utils.showAlertDialog(this, getString(R.string.account_inactive), message, getString(R.string.error_contact_support), false, true);


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys._INACTIVE))) {

            String message = jsonObject.getString(Keys._INACTIVE);


            Utils.showAlertDialog(this, getString(R.string.inactive), message, getString(R.string.error_contact_support), false, true);


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys._ERROR))) {

            String message = jsonObject.getString(Keys._ERROR);

            Utils.showAlertDialog(this, getString(R.string.error), message, getString(R.string.okay_text), true, true);


        } else if (!TextUtils.isEmpty(jsonObject.optString(Keys._CATEGORY))) {

            WealthDragonsOnlineApplication.sharedInstance().redirectToCategoryChooserActivity(this);

        } else {

            handleResponse(response);
        }
    }

    private void handleResponse(String response) throws JSONException {


        JSONObject jsonObject = new JSONObject(response);


        if (!TextUtils.isEmpty(jsonObject.optString(Constants.STATUS)) && jsonObject.getString(Constants.STATUS).equals("Success")) {

            mVideoUrls = jsonObject.getString(Keys.STREAMING_URL);
            String mChatToken = jsonObject.getString(Keys.CHAT_TOKEN);
            String mUserId = jsonObject.getString(Keys.CHAT_USER_ID);
            mThankYouMessage = jsonObject.getString(Keys.THANK_YOU_MSG);
            String mUserType = jsonObject.getString(Keys.USER_TYPE);
            mLiveChatErrorTitle = jsonObject.getString(Keys.CANCEL_MESSAGE_TITLE);
            mExpertId = jsonObject.getString(Keys.EXPERT_ID_CHAT);
            mLiveDigiCoachingId = jsonObject.getString(Keys.LIVE_DIGI_COACHING_ID);
            mLiveSassionRowId = jsonObject.getString(Keys.LIVE_SESSION_ROW_ID);
            startTimerForUserCount();

            if (dialogChatFrag != null && dialogChatFrag.isVisible()) {
                dialogChatFrag.setData(mChatToken, mUserId, mUserType, mLiveChatErrorTitle);
            }

            initilizePlayer(getApplicationContext(), mVideoUrls);


        } else if (!TextUtils.isEmpty(jsonObject.optString("Message"))) {
            mTvConnecting.setVisibility(View.VISIBLE);
            mTvConnecting.setText(jsonObject.getString("Message"));
            new Handler().postDelayed(() -> {
                if (!ChatActivity.this.isDestroyed()) {
                    onBackPressed();
                }
            }, 2000);

        } else {
            mTvConnecting.setVisibility(View.VISIBLE);

            mTvConnecting.setText("Error in Live Video");
            new Handler().postDelayed(() -> {
                if (!ChatActivity.this.isDestroyed()) {
                    onBackPressed();
                }
            }, 2000);
        }

    }

    @Override
    public void onRetryClicked() {

        mIvClose.setVisibility(View.VISIBLE);
        initilizePlayer(getApplicationContext(), mVideoUrls);

    }


    private void callLiveSessionClosed(final String expertId) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LIVE_USER_CLOSE_SESSION,
                response -> Logger.getLogger(Constants.LOG, response)
                , error -> Logger.getLogger(Constants.LOG, error.toString())
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.LIVE_DIGI_COACHING_ID, mLiveDigiCoachingId);
                params.put(Keys.MENTOR_ID, expertId);
                params.put(Keys.LIVE_SESSION_ROW_ID, mLiveSassionRowId);


                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


}
