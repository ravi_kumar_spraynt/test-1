package com.imentors.wealthdragons.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.BaseFragment;
import com.imentors.wealthdragons.fragments.CustomerAttachmentPagerFragment;
import com.imentors.wealthdragons.fragments.CustomerPagerFragment;
import com.imentors.wealthdragons.fragments.ExistingSupportReplyListFragment;
import com.imentors.wealthdragons.fragments.ExistingSupportRequestListFragment;
import com.imentors.wealthdragons.models.Customer;
import com.imentors.wealthdragons.models.MaintainceNotification;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class CustomerSupportActivity extends BaseActivity {

    private Toolbar mToolbar;
    private Toolbar mToolbarAttchments;
    private WealthDragonTextView mTvNewSupportRequest;
    private WealthDragonTextView mTvExisitingSupportRequest;
    private boolean mIsNewSupportSelected = true;

    private FragmentManager fm;
    private LinearLayout mTabLayout;
    private String notifcationLayout;
    private String notificationId;
    private LinearLayout mMarkView;
    private WealthDragonTextView mTvMaintainceMode;
    private Intent notificationIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        Utils.hideKeyboard(this);

        setContentView(R.layout.activity_customer_support);
        AppPreferences.setCustomerSupportActivityState(true);

        mToolbar = findViewById(R.id.customer_support_toolbar);
        mToolbarAttchments = findViewById(R.id.customer_support_attachemnts_toolbar);
        mTabLayout = findViewById(R.id.ll_tab_item);
        ImageView mIvBackButton = mToolbar.findViewById(R.id.iv_back_button);
        ImageView mIvBackButtonAttachment = mToolbarAttchments.findViewById(R.id.iv_back_button_attachment);

        mTvExisitingSupportRequest = findViewById(R.id.tv_existing_support_request);
        mTvNewSupportRequest = findViewById(R.id.tv_new_support_request);
        mMarkView = findViewById(R.id.marquee);

        mTvMaintainceMode = mMarkView.findViewById(R.id.tv_marquee);
        ImageView mIvCloseMaintaince = mMarkView.findViewById(R.id.iv_close_marquee);
        mIvCloseMaintaince.setVisibility(View.GONE);


        mIvCloseMaintaince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("closed <b> marquee </b> from clicked cross icon ");
                mMarkView.setVisibility(View.GONE);
                AppPreferences.setMaintainceModeUpdate(null);
                AppPreferences.setMaintainceModeCrossClicked(true);
            }
        });

        mMarkView.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(AppPreferences.getMaintainceModeUpdate())) {
            showMaintainaceMarquee(AppPreferences.getMaintainceModeUpdate());
        }


        setSelectedBackground(mTvNewSupportRequest);


        mIvBackButtonAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>closed icon on customer attachments</b>");
                onBackPressed();
            }
        });

        mIvBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Customer Support Back</b> Icon");
                finish();
            }
        });


        mTvNewSupportRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>New Support Request </b>");

                mIsNewSupportSelected = true;

                setSelectedBackground(mTvNewSupportRequest);
                setDefaultBackground(mTvExisitingSupportRequest);


                setCurrentItemInViewPager(0);

            }
        });


        mTvExisitingSupportRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsNewSupportSelected = false;
                Utils.callEventLogApi("clicked <b>Existing Support Tickets </b>");

                setSelectedBackground(mTvExisitingSupportRequest);
                setDefaultBackground(mTvNewSupportRequest);


                setCurrentItemInViewPager(1);

                CustomerPagerFragment topFrag = (CustomerPagerFragment) getCustomerTopFragment();
                Fragment pagerTopFrag = topFrag.getCurrentItemOnPager();
                if (pagerTopFrag instanceof ExistingSupportRequestListFragment) {
                    ((ExistingSupportRequestListFragment) pagerTopFrag).callExistingSupportApi();
                }


            }
        });

        //  calling customer pager fragment
        if (savedInstanceState == null) {

            fm = getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            CustomerPagerFragment fragment = CustomerPagerFragment.newInstance();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fm_customer, fragment);
            fragmentTransaction.commit();
        }

        //checking for intent
        Intent intentData = getIntent();
        //checking for extras in form of bundle
        Bundle bundleData = intentData.getExtras();


        // checking empty condition
        if (!bundleData.isEmpty()) {
            boolean hasNotification = bundleData.containsKey(Keys.NOTIFICATION);
            if (hasNotification) {

                HashMap<String, String> map = (HashMap<String, String>) bundleData.getSerializable(Keys.NOTIFICATION);
                notificationId = map.get(Keys.ID);
                setCurrentItemInViewPager(1);
                mIsNewSupportSelected = false;
                addNewFragmentInCustomerSupport(ExistingSupportReplyListFragment.newInstance(notificationId));

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        callUserDataApi(Api.CUSTOMER_DATA_API);


        if (!AppPreferences.getCustomerSupportActivityState()) {
            callMaintainceNotificaitonApi();
        }
    }

    //  set background color
    private void setSelectedBackground(TextView textView) {
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        textView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));

    }

    //  set default background color
    private void setDefaultBackground(TextView textView) {
        textView.setTextColor(ContextCompat.getColor(this, R.color.textColorblack));
        textView.setBackgroundColor(ContextCompat.getColor(this, R.color.backgroundGray));
    }


    //  set theme and attachment
    @Override
    public void onBackStackChanged() {
        if (getCustomerTopFragment() instanceof CustomerAttachmentPagerFragment) {

            CustomerAttachmentPagerFragment customerAttachmentPagerFragment = (CustomerAttachmentPagerFragment) getCustomerTopFragment();

            setTheme(R.style.AppTheme_NoActionBar_CustomerSupportAttachments);
            mToolbar.setVisibility(View.GONE);
            mToolbarAttchments.setVisibility(View.VISIBLE);
            WealthDragonTextView heading = mToolbarAttchments.findViewById(R.id.tv_singing_status);
            if (!customerAttachmentPagerFragment.isItemMoreThanOne()) {
                heading.setText(getString(R.string.attachment));
            } else {
                heading.setText(getString(R.string.attachments));
            }
            mTabLayout.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
            }
        } else {
            setTheme(R.style.AppTheme_NoActionBar_CustomerSupport);
            mToolbar.setVisibility(View.VISIBLE);
            mToolbarAttchments.setVisibility(View.GONE);
            mTabLayout.setVisibility(View.VISIBLE);
            if (!mIsNewSupportSelected) {
                setSelectedBackground(mTvExisitingSupportRequest);
                setDefaultBackground(mTvNewSupportRequest);
            } else {
                setSelectedBackground(mTvNewSupportRequest);
                setDefaultBackground(mTvExisitingSupportRequest);
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(getResources().getColor(R.color.textColorblue));
            }


        }
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        notificationIntent = intent;

        if (intent.hasExtra(Keys.NOTIFICATION) && intent.getSerializableExtra(Keys.NOTIFICATION) instanceof HashMap) {
            HashMap<String, String> map = (HashMap<String, String>) intent.getSerializableExtra(Keys.NOTIFICATION);
            if (map != null) {
                if (getCustomerTopFragment() instanceof ExistingSupportReplyListFragment) {

                    ExistingSupportReplyListFragment existingSupportReplyListFragment = (ExistingSupportReplyListFragment) getCustomerTopFragment();
                    existingSupportReplyListFragment.refreshReplyList();

                } else {

                    new Handler().postDelayed(() -> {
                        if (!CustomerSupportActivity.this.isDestroyed()) {
                            onHandleNotification(intent);
                        }
                    }, 2000);
                }
            }
        }

    }


    private void onHandleNotification(Intent intent) {
        HashMap<String, String> map = (HashMap<String, String>) intent.getSerializableExtra(Keys.NOTIFICATION);
        if (map != null) {
            String dialogHeader = null;
            notifcationLayout = map.get(Keys.TYPE);
            dialogHeader = map.get(Keys.TITLE);
            notificationId = map.get(Keys.ID);
            final String body = map.get(Keys.BODY);

            Utils.showTwoButtonAlertDialog(new Utils.DialogInteraction() {
                @Override
                public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {

                    if (notifcationLayout.equals(Constants.CUSTOMER_SUPPORT_NOTIFICATION)) {
                        setCurrentItemInViewPager(1);
                        mIsNewSupportSelected = false;
                        addNewFragmentInCustomerSupport(ExistingSupportReplyListFragment.newInstance(notificationId));
                    } else {
                        redirectUserToDashBoardActivity();

                    }

                    dialog.dismiss();
                }

                @Override
                public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {
                    dialog.dismiss();
                }
            }, this, dialogHeader, body, getString(R.string.view), getString(R.string.cancel));

        }
    }


    private void redirectUserToDashBoardActivity() {


        Intent intent = new Intent();
        intent.putExtra(Keys.NOTIFICATION, notificationIntent.getSerializableExtra(Keys.NOTIFICATION));
        intent.putExtra(Constants.SHOW_NOTIFICATION_DIALOG, "false");

        intent.setClass(this, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.no_animation);
    }


    public void addFragment(Fragment fragment) {

        FragmentTransaction transaction = fm.beginTransaction();
        if (transaction.isAddToBackStackAllowed()) {
            transaction.addToBackStack(null);
            transaction.add(R.id.fm_main, fragment).commit();
        }
    }


    //  set current item in view pager
    private void setCurrentItemInViewPager(int position) {

        if (getCustomerTopFragment() instanceof CustomerPagerFragment) {
            CustomerPagerFragment topFrag = (CustomerPagerFragment) getCustomerTopFragment();
            topFrag.setCurrentItem(position);
        } else {
            if (getCustomerTopFragment() != null && fm != null) {
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                CustomerPagerFragment topFrag = (CustomerPagerFragment) getCustomerTopFragment();
                topFrag.setCurrentItem(position);
            }
        }
    }

    private BaseFragment getCustomerTopFragment() {
        return (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fm_customer);
    }


    @Override
    protected void onStop() {
        super.onStop();
        AppPreferences.setCustomerSupportActivityState(false);

    }


    private void addNewFragmentInCustomerSupport(Fragment fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fm_customer, fragment);
        ft.setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_right);
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }


    private void showMaintainaceMarquee(String maintainanceMessage) {

        if (!TextUtils.isEmpty(maintainanceMessage)) {
            mMarkView.setVisibility(View.VISIBLE);
            mTvMaintainceMode.setText(maintainanceMessage);
            mMarkView.setSelected(true);
        } else {
            mMarkView.setVisibility(View.GONE);
        }
    }


    private void callMaintainceNotificaitonApi() {
        AppPreferences.setMaintainceModeUpdate(null);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MAINTENANCE_NOTIFICATION, response -> {
            MaintainceNotification maintainceNotification = new Gson().fromJson(response, MaintainceNotification.class);
            if (maintainceNotification.getMain_status().equals("1")) {
                WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(CustomerSupportActivity.this);
            } else if (maintainceNotification.getStatus().equals("1")) {
                showMaintainaceMarquee(maintainceNotification.getNotification_text());
                AppPreferences.setMaintainceModeUpdate(maintainceNotification.getNotification_text());
            } else if (maintainceNotification.getStatus().equals("2")) {
                showMaintainaceMarquee(null);
                AppPreferences.setMaintainceModeUpdate(null);
            }
            // no reaction
        }, error -> Logger.getLogger(Constants.LOG, error.toString())
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void callUserDataApi(final String api) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, response -> {


            try {
                if (Utils.checkForMaintainceMode(response)) {
                    WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(CustomerSupportActivity.this);
                    return;
                }
                Customer customer = new Gson().fromJson(response, Customer.class);

                if (customer.getCustomer() != null) {
                    AppPreferences.setCustomerData(customer);
                }
            } catch (Exception e) {
                Logger.getLogger(Constants.LOG, e.toString());
            }

        }, error -> {

            // no use
        }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return Utils.getParams(false);

            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }
}
