package com.imentors.wealthdragons.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.ContactSupportFragment;
import com.imentors.wealthdragons.fragments.SubscriptionFragment;
import com.imentors.wealthdragons.fragments.WelcomePagerFragment;
import com.imentors.wealthdragons.interfaces.LoginFragmentChangingListener;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;

import java.io.Serializable;
import java.util.List;

public class  LoginActivity extends AppCompatActivity implements LoginFragmentChangingListener {


    // Signing page Detail
    private WelcomeModel mWelcomeModel;
    private Serializable notificationHashMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_login);
        AppPreferences.setLoginActivityState(true);

        //checking for intent
        Intent intentData = getIntent();
        //checking for extras in form of bundle
        Bundle bundleData = intentData.getExtras();


        // checking empty condition
        if (!bundleData.isEmpty()) {
            boolean hasData = bundleData.containsKey(Constants.SIGNING_MODEL_DATA);
            boolean hasNotification = bundleData.containsKey(Keys.NOTIFICATION);

            // condition for our data key
            if (hasData) {
                mWelcomeModel = (WelcomeModel) bundleData.getSerializable(Constants.SIGNING_MODEL_DATA);
            }

            if (hasNotification) {
                notificationHashMap = intentData.getSerializableExtra(Keys.NOTIFICATION);
            }

        }

        Toolbar mToolbar = findViewById(R.id.login_toolbar);

        // hide toolbar here
        mToolbar.setVisibility(View.GONE);


        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // disable home button , back arrow and title in action bar
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }


        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        if ((AppPreferences.isLoggedIn()) && (!AppPreferences.getUser().isDashboard())) {
            SubscriptionFragment fragment;
            if (notificationHashMap != null) {
                fragment = SubscriptionFragment.newInstance(true, notificationHashMap);
            } else {
                fragment = SubscriptionFragment.newInstance(true, null);
            }
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fm_main, fragment);
            fragmentTransaction.commit();

        } else {
            WelcomePagerFragment fragment = WelcomePagerFragment.newInstance(mWelcomeModel, false, 0, notificationHashMap, true);
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(
                    R.anim.slide_in_from_right, R.anim.slide_out_to_right);
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            fragmentTransaction.replace(R.id.fm_main, fragment);
            fragmentTransaction.commit();
        }


        if (AppPreferences.getOpenContactSupport()) {
            AppPreferences.setOpenContactSupport(false);

            ContactSupportFragment termFragment = ContactSupportFragment.newInstance();

            FragmentTransaction ft = fm.beginTransaction();

            ft.setCustomAnimations(
                    R.anim.slide_in_from_right, R.anim.slide_out_to_right);

            ft.add(R.id.fm_main, termFragment);

            ft.addToBackStack(null);

            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);

            ft.commit();

        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Utils.getAppVersionCodeFromServer(this);

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        onHandleNotification(intent);

    }


    private void onHandleNotification(Intent intent) {
        //checking for extras in form of bundle
        Bundle bundleData = intent.getExtras();
        // checking empty condition
        if (bundleData != null && !bundleData.isEmpty()) {

                boolean hasNotification = bundleData.containsKey(Keys.NOTIFICATION);

                if (hasNotification) {
                    notificationHashMap = intent.getSerializableExtra(Keys.NOTIFICATION);
                    onSignInClicked();
                }

        }

    }



    public Serializable getNotificationData() {
        if (notificationHashMap != null) {
            return notificationHashMap;
        }
        return null;
    }



    @Override
    public void onSignInClicked() {
        if (isFragmentInBackStack(new WelcomePagerFragment())) {
            WelcomePagerFragment frag = (WelcomePagerFragment) getFragmentFromBackStack(WelcomePagerFragment.class.getName());
            frag.openSignInFragment();
        }
    }

    @Override
    public void onSignUpClicked() {
        if (isFragmentInBackStack(new WelcomePagerFragment())) {
            WelcomePagerFragment frag = (WelcomePagerFragment) getFragmentFromBackStack(WelcomePagerFragment.class.getName());
            frag.openSignUpFragment();
        }
    }

    @Override
    public void onForgotPasswordClicked() {
        if (isFragmentInBackStack(new WelcomePagerFragment())) {
            WelcomePagerFragment frag = (WelcomePagerFragment) getFragmentFromBackStack(WelcomePagerFragment.class.getName());
            frag.openForgotPasswordFragment();
        }
    }


    public boolean isFragmentInBackStack(Fragment frag) {
        List<Fragment> fragentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragentList) {

            if (fragment.getClass().isAssignableFrom(frag.getClass())) {
                return true;
            }

        }
        return false;

    }

    public Fragment getFragmentFromBackStack(String frag) {
        List<Fragment> fragentList = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragentList) {

            if (fragment.getClass().getName().equals(frag)) {
                return fragment;
            }

        }
        return null;
    }


}
