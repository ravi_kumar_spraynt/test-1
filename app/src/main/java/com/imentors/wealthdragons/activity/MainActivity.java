package com.imentors.wealthdragons.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;

import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.AutoSearchListAdapter;
import com.imentors.wealthdragons.adapters.EClassesListAdapter;
import com.imentors.wealthdragons.adapters.SearchTopListAdapter;
import com.imentors.wealthdragons.adapters.SlidingImageAdapter;
import com.imentors.wealthdragons.adapters.SubCategoryListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.EnrollmentDialogFragment;
import com.imentors.wealthdragons.dialogFragment.FullScreenPlayerDialog;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.fragments.AboutUsFragment;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.fragments.DashBoardFragment;
import com.imentors.wealthdragons.fragments.DashBoardPagerFragment;
import com.imentors.wealthdragons.fragments.DigitalCoachingFragment;
import com.imentors.wealthdragons.fragments.FeedFragment;
import com.imentors.wealthdragons.fragments.MenuFragment;
import com.imentors.wealthdragons.fragments.MessageDetailFragment;
import com.imentors.wealthdragons.fragments.MessagesListFragment;
import com.imentors.wealthdragons.fragments.MyBadgesFragment;
import com.imentors.wealthdragons.fragments.MyGroupDownloadFragment;
import com.imentors.wealthdragons.fragments.MyPurchaseFragment;
import com.imentors.wealthdragons.fragments.OtherTabFragment;
import com.imentors.wealthdragons.fragments.PaymentHistoryDetailFragment;
import com.imentors.wealthdragons.fragments.PaymentHistoryFragment;
import com.imentors.wealthdragons.fragments.QuizFragment;
import com.imentors.wealthdragons.fragments.QuizResultFragment;
import com.imentors.wealthdragons.fragments.SearchContainerFragment;
import com.imentors.wealthdragons.fragments.SearchFragment;
import com.imentors.wealthdragons.fragments.SeeAllFragment;
import com.imentors.wealthdragons.fragments.SettingsFragment;
import com.imentors.wealthdragons.fragments.SubscriptionFragment;
import com.imentors.wealthdragons.fragments.TastePreferancesFragment;
import com.imentors.wealthdragons.fragments.WishListFragment;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.CategoryItemInteraction;
import com.imentors.wealthdragons.interfaces.DigitalCoachingClickListener;
import com.imentors.wealthdragons.interfaces.EventClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.MenuItemInteraction;
import com.imentors.wealthdragons.interfaces.OpenFullScreenVideoListener;
import com.imentors.wealthdragons.interfaces.SeeAllClickListener;
import com.imentors.wealthdragons.interfaces.TabChangingListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Category;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.CountryCode;
import com.imentors.wealthdragons.models.Customer;
import com.imentors.wealthdragons.models.DashBoard;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.DashBoardItemOdering;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DashBoardSlider;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.DeepLink;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.MaintainceNotification;
import com.imentors.wealthdragons.models.MenuItem;
import com.imentors.wealthdragons.models.SearchAutoComplete;
import com.imentors.wealthdragons.models.SubCategory;
import com.imentors.wealthdragons.models.SupportType;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.DateTimeUtils;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.TabBar;
import com.imentors.wealthdragons.views.WealthDragonDrawerLayout;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.imentors.wealthdragons.utils.Constants.IS_COURSEDETAIL;
import static com.imentors.wealthdragons.utils.Constants.STATE_RESUME_POSITION;
import static com.imentors.wealthdragons.utils.Constants.STATE_RESUME_WINDOW;
import static com.imentors.wealthdragons.utils.Constants.TYPE_TAG;
import static com.imentors.wealthdragons.utils.Constants.VIDEO_URL_LIST;


public class MainActivity extends BaseActivity implements SlidingImageAdapter.OnItemClickListener, CategoryItemInteraction, TabChangingListener, SeeAllClickListener, AutoSearchListAdapter.OnTopSearchesClick, SearchTopListAdapter.OnTopSearchesClick, SubCategoryListAdapter.onSubCategoryClick, ArticleClickListener, MentorClickListener, VideoClickListener, DigitalCoachingClickListener, OpenFullScreenVideoListener, EClassesListAdapter.OnEClassesClick, EventClickListener, Utils.DialogInteraction {


    //Reference to drawer layout to open or hide the menu
    private WealthDragonDrawerLayout mDrawerLayout;

    //bottom tab bar
    private TabBar mTabBar;
    private Toolbar mToolbar;
    private FragmentManager fm;
    private ImageView mIvSearchView;
    private DashBoard mNewDashBoardData;
    private int backStackEntryCount;
    private String notificationId;
    private String notifcationLayout;
    private MenuFragment mMenuFragment;
    private Intent notificationIntent = null;
    private LinearLayout mMarkview;
    private WealthDragonTextView mTvMaintainceMode;
    private FullScreenPlayerDialog dialogFragment;
    private boolean isCategoryChooser;
    private BroadcastReceiver mDismissAlertDialog = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Fragment fragment = getTopFragment();

            boolean position = intent.getBooleanExtra("isClearResponse", false);


            if (fragment instanceof DashBoardPagerFragment) {

                DashBoardPagerFragment dashBoardPagerFragment = (DashBoardPagerFragment) fragment;
                if (dashBoardPagerFragment.getCurrentFragment() instanceof DashBoardFragment) {
                    DashBoardFragment dashBoardFrag = (DashBoardFragment) dashBoardPagerFragment.getCurrentFragment();
                    dashBoardFrag.callDashBoardApi();
                } else if (dashBoardPagerFragment.getCurrentFragment() instanceof OtherTabFragment) {
                    OtherTabFragment otherFragment = (OtherTabFragment) dashBoardPagerFragment.getCurrentFragment();
                    otherFragment.callTabApi();
                }
            } else if (fragment instanceof SeeAllFragment) {
                SeeAllFragment seeAllFragment = (SeeAllFragment) fragment;
                seeAllFragment.callSeeAllApi();

            } else if (fragment instanceof OtherTabFragment) {
                OtherTabFragment otherFragment = (OtherTabFragment) fragment;
                otherFragment.callTabApi();
            } else if (fragment instanceof DashBoardDetailFragment) {
                DashBoardDetailFragment dashBoardDetailFragment = (DashBoardDetailFragment) fragment;
                if (position) {
                    dashBoardDetailFragment.clearResponse();
                }
                dashBoardDetailFragment.reload();

            } else if (fragment instanceof WishListFragment) {
                WishListFragment wishListFragment = (WishListFragment) fragment;
                wishListFragment.callWishListApi();

            } else if (fragment instanceof MessagesListFragment) {
                MessagesListFragment messagesListFragment = (MessagesListFragment) fragment;
                messagesListFragment.callMessageApi();


            } else if (fragment instanceof TastePreferancesFragment) {
                TastePreferancesFragment tastePreferancesFragment = (TastePreferancesFragment) fragment;
                tastePreferancesFragment.callTastePreferancesApi();


            } else if (fragment instanceof AboutUsFragment) {
                AboutUsFragment aboutUsFragment = (AboutUsFragment) fragment;
                aboutUsFragment.callAboutUsApi();


            } else if (fragment instanceof PaymentHistoryFragment) {
                PaymentHistoryFragment paymentHistoryFragment = (PaymentHistoryFragment) fragment;
                paymentHistoryFragment.callPaymentHistoryApi();

            } else if (fragment instanceof MyBadgesFragment) {
                MyBadgesFragment myBadgesFragment = (MyBadgesFragment) fragment;
                myBadgesFragment.callBadgesApi();

            } else if (fragment instanceof MyPurchaseFragment) {
                MyPurchaseFragment myPurchaseFragment = (MyPurchaseFragment) fragment;
                myPurchaseFragment.myPurchaseApi();

            } else if (fragment instanceof MessageDetailFragment) {
                MessageDetailFragment messageDetailFragment = (MessageDetailFragment) fragment;
                messageDetailFragment.callMessageDetailApi();

            } else if (fragment instanceof PaymentHistoryDetailFragment) {
                PaymentHistoryDetailFragment paymentHistoryDetailFragment = (PaymentHistoryDetailFragment) fragment;
                paymentHistoryDetailFragment.callPaymentInVoice();

            } else if (fragment instanceof SearchFragment) {
                SearchFragment searchFragment = (SearchFragment) fragment;
                searchFragment.callSearchAllApi();

            } else if (fragment instanceof SearchContainerFragment) {
                SearchContainerFragment searchTopListFragment = (SearchContainerFragment) fragment;
                searchTopListFragment.callretryApi();

            } else if (fragment instanceof DigitalCoachingFragment) {
                DigitalCoachingFragment searchTopListFragment = (DigitalCoachingFragment) fragment;
                searchTopListFragment.callDcApi();

            }


        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDismissAlertDialog,
                new IntentFilter(Constants.BROADCAST_DISMISS_ALERT_DIALOG));


        //checking for intent
        Intent intentData = getIntent();
        notificationIntent = getIntent();
        //checking for extras in form of bundle
        Bundle bundleData = intentData.getExtras();


        // checking empty condition
        if (!bundleData.isEmpty()) {
            Log.d("notifications", "res");
            boolean hasNotification = bundleData.containsKey(Keys.NOTIFICATION);
            isCategoryChooser = bundleData.getBoolean(Constants.IS_CATEGORY_CHOOSER);
            if (hasNotification) {

                new Handler().postDelayed(() -> {
                    Log.d("notifications", "message");
                    if (!MainActivity.this.isDestroyed()) {
                        onHandleNotification(getIntent());
                    }
                }, 1000);
            }

        }


        setContentView(R.layout.activity_main);

        mMarkview = findViewById(R.id.marquee);
        mTvMaintainceMode = mMarkview.findViewById(R.id.tv_marquee);
        ImageView mIvCloseMaintaince = mMarkview.findViewById(R.id.iv_close_marquee);
        mIvCloseMaintaince.setVisibility(View.GONE);


        mIvCloseMaintaince.setOnClickListener(v -> {
            Utils.callEventLogApi("closed <b> marquee </b> from clicked cross icon ");
            mMarkview.setVisibility(View.GONE);
            AppPreferences.setMaintainceModeUpdate(null);
            AppPreferences.setMaintainceModeCrossClicked(true);

        });

        mMarkview.setVisibility(View.GONE);


        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mMenuFragment = (MenuFragment) (getSupportFragmentManager()
                .findFragmentById(R.id.menuFragment));

        ImageView openDrawerButton = mToolbar.findViewById(R.id.imv_drawer);
        mIvSearchView = mToolbar.findViewById(R.id.imv_search);

        openDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("tapped <b>menu </b>button");

                if (getTopFragment() instanceof DashBoardPagerFragment) {
                    mMenuFragment.setSelectedItemInAdapter();
                }

                mMenuFragment.callCategoriesApi(false);

                openDrawers();
            }
        });

        //  clicked search tab
        mIvSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("opened <b>Search </b>tab");

                // move to search Fragment
                addFragment(SearchContainerFragment.newInstance());
            }
        });


        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayShowTitleEnabled(false);

        final View view = findViewById(R.id.main_container);


        //save user data to preferences
        callUserDataApi(Api.CUSTOMER_DATA_API);

        //save customer Data
        callUserDataApi(Api.COUNTRY_CODES_API);

        //save Support Type
        callUserDataApi(Api.SUPPORT_TYPES_API);


        mDrawerLayout = findViewById(R.id.drawer_layout);


        ViewGroup.LayoutParams params = mMenuFragment.getView().getLayoutParams();
        params.width = Utils.getDrawerWidth(MainActivity.this);
        params.height = mMenuFragment.getView().getLayoutParams().MATCH_PARENT;
        mMenuFragment.getView().setLayoutParams(params);


        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(/* host Activity */
                this,
                /* DrawerLayout object */
                mDrawerLayout,
                0, 0) {


            @Override
            public boolean onOptionsItemSelected(android.view.MenuItem item) {
                if (item != null && item.getItemId() == android.R.id.home) {
                    if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        mDrawerLayout.closeDrawer(Gravity.END);
                    } else {
                        mDrawerLayout.openDrawer(Gravity.RIGHT);
                    }
                }
                return false;
            }

            //  drawer slide
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                view.setTranslationX(-slideOffset * drawerView.getWidth());
                mDrawerLayout.bringChildToFront(drawerView);
                mDrawerLayout.requestLayout();
                Utils.hideKeyboard(MainActivity.this);

            }

        };


        mDrawerLayout.addDrawerListener(mDrawerToggle);

        mDrawerToggle.syncState();

        mTabBar = findViewById(R.id.tab_bar);

        // needs to be added after dashboard response
        onTabChanged();

        mTabBar.setOnMenuClickListener(new MenuItemInteraction() {

            //  clicked tab bar
            @Override
            public void onMenuClick(MenuItem menuItem) {


                switch (menuItem.textResId) {

                    case R.string.dashboard:
                        setCurrentItemInViewPager(0);

                        mTabBar.setSelectedDetailPage(mNewDashBoardData.getContetnType());

                        break;

                    case R.string.all_mentors:
                        Utils.callEventLogApi("opened <b>Mentor </b> tab");

                        if (mNewDashBoardData != null) {
                            if (mNewDashBoardData.isMentorTabVisible()) {
                                setCurrentItemInViewPager(1);
                            }
                        } else {
                            setCurrentItemInViewPager(1);
                        }
                        break;

                    case R.string.events:
                        Utils.callEventLogApi("opened <b>Event </b> tab");
                        if (mNewDashBoardData != null) {
                            if (!mNewDashBoardData.isMentorTabVisible()) {
                                setCurrentItemInViewPager(1);
                            } else {
                                setCurrentItemInViewPager(2);
                            }
                        } else {
                            setCurrentItemInViewPager(2);
                        }
                        break;

                    case R.string.all_courses:
                        Utils.callEventLogApi("opened <b>Course </b> tab");
                        if (mNewDashBoardData != null) {
                            if (!mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible()) {
                                setCurrentItemInViewPager(1);
                            } else if (!mNewDashBoardData.isMentorTabVisible() || !mNewDashBoardData.isVideoTabVisible()) {
                                setCurrentItemInViewPager(2);
                            } else {
                                setCurrentItemInViewPager(3);

                            }
                        } else {
                            setCurrentItemInViewPager(3);
                        }

                        break;

                    case R.string.purchase:

                        Utils.callEventLogApi("opened <b>Purchase </b> tab");
                        if (mNewDashBoardData != null) {
                            if (!mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible() && !mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(1);
                            }

                            // for position 2
                            else if (mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible() && !mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(2);
                            } else if (!mNewDashBoardData.isMentorTabVisible() && mNewDashBoardData.isVideoTabVisible() && !mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(2);
                            } else if (!mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible() && mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(2);
                            }

                            // for position 3
                            else if (!mNewDashBoardData.isMentorTabVisible() && mNewDashBoardData.isVideoTabVisible() && mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(3);
                            } else if (mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible() && mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(3);
                            } else if (mNewDashBoardData.isMentorTabVisible() && mNewDashBoardData.isVideoTabVisible() && !mNewDashBoardData.isCourseTabVisible()) {
                                setCurrentItemInViewPager(3);
                            }

                            // for position 4
                            else {
                                setCurrentItemInViewPager(4);

                            }
                        } else {
                            setCurrentItemInViewPager(4);
                        }

                        break;

                    default:
                        // no use
                        break;
                }
            }

            @Override
            public void onPopClick() {
                MainActivity.this.onPopClick();
            }
        });


        if (savedInstanceState == null) {

            fm = getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            DashBoardPagerFragment fragment = DashBoardPagerFragment.newInstance();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fm_main, fragment);
            fragmentTransaction.commit();

            mTabBar.setSelectedIndex(0);
        }


    }

    @Override
    protected void onNewIntent(final Intent intent) {

        if (this.getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {

            if (getTopFragment() instanceof DashBoardDetailFragment) {
                DashBoardDetailFragment dashBoardDetailFragment = (DashBoardDetailFragment) getTopFragment();
                dashBoardDetailFragment.closeFullScreenDialog();
            }

            if (dialogFragment != null && dialogFragment.isVisible()) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                dialogFragment.dismiss();
            }


            if (getTopFragment() instanceof FeedFragment) {
                FeedFragment dashBoardDetailFragment = (FeedFragment) getTopFragment();
                dashBoardDetailFragment.closeFullscreenDialog();
            }

        }

        super.onNewIntent(intent);
        notificationIntent = intent;

        // to handle customer support notification in case it is opened.. transferring of intent
        if (AppPreferences.getCustomerSupportActivityState() && intent.hasExtra(Keys.NOTIFICATION) && intent.getSerializableExtra(Keys.NOTIFICATION) instanceof HashMap) {

            openCustomerSupportActivity(notificationIntent);

        } else {
            new Handler().postDelayed(() -> {
                if (!MainActivity.this.isDestroyed()) {
                    onHandleNotification(intent);
                }
            }, 2000);
        }
    }

    private void onHandleNotification(Intent intent) {

        if (intent.hasExtra(Keys.NOTIFICATION) && intent.getSerializableExtra(Keys.NOTIFICATION) instanceof HashMap) {

            HashMap<String, String> map = (HashMap<String, String>) intent.getSerializableExtra(Keys.NOTIFICATION);
            if (map != null) {
                String dialogHeader = null;
                notifcationLayout = map.get(Keys.TYPE);
                dialogHeader = map.get(Keys.TITLE);
                notificationId = map.get(Keys.ID);
                final String body = map.get(Keys.BODY);

                String notificationString = "true";
                if (!TextUtils.isEmpty(intent.getExtras().getString(Constants.SHOW_NOTIFICATION_DIALOG))) {
                    notificationString = intent.getExtras().getString(Constants.SHOW_NOTIFICATION_DIALOG);
                }

                if (map.get(Constants.SHOW_NOTIFICATION_DIALOG).equals("false") || notificationString.equals("false")) {
                    // for handling customer support notification
                    if (notifcationLayout.equals(Constants.CUSTOMER_SUPPORT_NOTIFICATION)) {
                        openCustomerSupportActivity(notificationIntent);
                    } else if (notifcationLayout.equals(Constants.LIVE_NOTIFICATION_DIALOG)) {
                        openLiveChatActivity(notificationIntent);
                    } else {
                        addFragment(DashBoardDetailFragment.newInstance(notificationId, notifcationLayout, null, false, null));
                    }
                } else {
                    Utils.showTwoButtonAlertDialog(this, this, dialogHeader, body, getString(R.string.view), getString(R.string.cancel));
                }


            }
        }

    }

    @Override
    public void onBackStackChanged() {

        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            if (getTopFragment() instanceof DashBoardDetailFragment) {
                DashBoardDetailFragment detailFragment = (DashBoardDetailFragment) getTopFragment();
                selectTabDetailPage(detailFragment.getDetailPageType());
            } else {
                mTabBar.setSelectedIndex(-1);
            }

            if (getTopFragment() instanceof MyPurchaseFragment) {
                mTabBar.setSelectedIndex(mTabBar.getTotalItems());
            }

            if (getTopFragment() instanceof QuizFragment || getTopFragment() instanceof QuizResultFragment) {
                selectTabDetailPage(Constants.TYPE_COURSE);
            }


            mToolbar.setNavigationIcon(R.drawable.back_arrow);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onBackPressed();
                }
            });
        } else {


            if (getTopFragment() instanceof DashBoardPagerFragment) {
                DashBoardPagerFragment dashBoardFragment = (DashBoardPagerFragment) getTopFragment();
                setTabSelected(dashBoardFragment.getCurrentItem());
            }

            mToolbar.setNavigationIcon(null);
        }

        if (getTopFragment() instanceof SearchContainerFragment) {
            mIvSearchView.setVisibility(View.GONE);
        } else {
            mIvSearchView.setVisibility(View.VISIBLE);
        }

        if (getTopFragment() instanceof WishListFragment) {
            WishListFragment frag = (WishListFragment) getTopFragment();
            frag.callWishListApi();
        }


        if (isFragmentInBackStack(DashBoardFragment.newInstance()) && getTopFragment() instanceof DashBoardPagerFragment) {

            DashBoardFragment frag = (DashBoardFragment) getFragmentFromBackStack(DashBoardFragment.newInstance());
            if (frag != null) {
                frag.startSlider();
            }

        }

        if (backStackEntryCount != getSupportFragmentManager().getBackStackEntryCount()) {

            backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();

            if (isFragmentInBackStack(new DashBoardDetailFragment())) {


                Intent intent = new Intent(Constants.BROADCAST_SAVE_VIDEO_POSITION);

                intent.putExtra(Constants.IS_TOP_FRAGMENT, getTopFragment() instanceof DashBoardDetailFragment);

                LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

            }
        }

        closeDrawers();

    }

    //  opening drawer menu item
    @Override
    public void onCategoryMenuClick(Category item) {

        String selectedIndex = mNewDashBoardData.getContetnType();
        if (!selectedIndex.equals(Constants.TYPE_OLD)) {
            selectedIndex = mTabBar.getSelectedIndexType();
        }


        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {


            closeDrawers();
            // to check it is static menu or not applied try catch exception
            try {
                int title = Integer.parseInt(item.getTitle());

                if (getString(title).equals(getString(R.string.about))) {
                    Utils.callEventLogApi("visited <b>About Us Screen</b>");
                    addFragment(AboutUsFragment.newInstance());
                }

                if (getString(title).equals(getString(R.string.subscription))) {
                    Utils.callEventLogApi("visited <b>Subscription Screen</b>");
                    addFragment(SubscriptionFragment.newInstance(false, null));
                }

                if (getString(title).equals(getString(R.string.my_wishlist))) {
                    Utils.callEventLogApi("visited <b>Wishlist Screen</b>");
                    addFragment(WishListFragment.newInstance());
                }

                if (getString(title).equals(getString(R.string.messages))) {
                    Utils.callEventLogApi("visited <b>Messages Screen</b>");
                    addFragment(MessagesListFragment.newInstance());
                }

                if (getString(title).equals(getString(R.string.customer_support))) {
                    Utils.callEventLogApi("visited <b>Customer Support Screen</b>");
                    Intent intent = new Intent();
                    intent.setClass(this, CustomerSupportActivity.class);
                    startActivity(intent);
                }

                if (getString(title).equals(getString(R.string.taste_perferance))) {
                    addFragment(TastePreferancesFragment.newInstance());

                }

                if (getString(title).equals(getString(R.string.settings))) {
                    Utils.callEventLogApi("visited <b>Setting Screen</b>");
                    addFragment(SettingsFragment.newInstance());

                }

                if (getString(title).equals(getString(R.string.purchase))) {
                    Utils.callEventLogApi("visited <b>Purchase Screen</b>");
                    addFragment(MyPurchaseFragment.newInstance());

                }

                if (getString(title).equals(getString(R.string.payment_history))) {
                    addFragment(PaymentHistoryFragment.newInstance());
                }

                if (getString(title).equals(getString(R.string.my_badges))) {
                    Utils.callEventLogApi("visited <b>My Badges</b>");
                    addFragment(MyBadgesFragment.newInstance());

                }
                if (getString(title).equals(getString(R.string.my_downloads))) {
                    Utils.callEventLogApi("visited <b>My downloads</b>");
                    addFragment(MyGroupDownloadFragment.newInstance());

                }

            } catch (NumberFormatException e) {

                if (getTopFragment() instanceof SeeAllFragment) {
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                    addFragment(SeeAllFragment.newInstance(item.getId(), null, item.getTitle(), selectedIndex));
                } else {
                    addFragment(SeeAllFragment.newInstance(item.getId(), null, item.getTitle(), selectedIndex));
                }

            }

        } else {
            openDrawers();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        isCategoryChooser = false;
    }

    @Override
    protected void onResume() {
        super.onResume();


        callUserDataApi(Api.CUSTOMER_DATA_API);

        Utils.getAppVersionCodeFromServer(this);

        if (!AppPreferences.getCustomerSupportActivityState()) {

            AppPreferences.setMaintainceModeCrossClicked(false);
            callMaintainceNotificaitonApi();

        } else {
            if (AppPreferences.getMaintainceModeCrossClicked()) {
                showMaintainaceMarquee(AppPreferences.getMaintainceModeUpdate());
            }
        }

        AppPreferences.setMainActivityState(true);


        AppPreferences.setLiveChatActivityState(false);


        if (AppPreferences.getLoginActivityState() || AppPreferences.getCustomerSupportActivityState() || AppPreferences.getVideoFullScreenStatus()) {
            AppPreferences.setLoginActivityState(false);
            AppPreferences.setCustomerSupportActivityState(false);
            AppPreferences.setLiveChatActivityState(false);
            AppPreferences.setVideoFullScreenStatus(false);
        } else {

            callLoginHistoryApi();
        }


        // for deep linking

        new Handler().postDelayed(() -> {
            if (!MainActivity.this.isDestroyed()) {
                setDeepLink();
            }
        }, 3000);

    }

    private void setDeepLink() {
        if (AppPreferences.getDeepLink() != null && !TextUtils.isEmpty(AppPreferences.getDeepLink().getType())) {


            DeepLink deepLink = AppPreferences.getDeepLink();
            notifcationLayout = deepLink.getType();
            notificationId = deepLink.getId();

            if (!isFinishing()) {
                addFragment(DashBoardDetailFragment.newInstance(notificationId, notifcationLayout, null, false, null));
            }
            // setting null that its open only for once not everytime
            AppPreferences.setDeepLinkData(null);

        }
    }

    @Override
    public void onCategoryChildClick(Category.SubCategory item) {
        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            closeDrawers();


            String selectedIndex = mNewDashBoardData.getContetnType();
            if (!selectedIndex.equals(Constants.TYPE_OLD)) {
                selectedIndex = mTabBar.getSelectedIndexType();
            }


            if (getTopFragment() instanceof SeeAllFragment) {
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                addFragment(SeeAllFragment.newInstance(item.getId(), null, item.getTitle(), selectedIndex));
            } else {
                addFragment(SeeAllFragment.newInstance(item.getId(), null, item.getTitle(), selectedIndex));
            }

        } else {
            openDrawers();
        }
    }

    private void onPopClick() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onCrossClick() {
        closeDrawers();
    }

    private void openDrawers() {

        Utils.hideKeyboard(this);
        mDrawerLayout.openDrawer(Gravity.RIGHT);
    }

    /**
     * close drawer on screen layout
     */
    private void closeDrawers() {

        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(Gravity.END);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDismissAlertDialog);

        // case when video is not active
        if (!AppPreferences.getFullScreenActivityState()) {
            // to handle notification case
            AppPreferences.setMainActivityState(false);

            // for subscription pop up
            if (AppPreferences.getCategories() != null) {
                String popupDuration = AppPreferences.getCategories().getSubs_popup_duration();
                if (!TextUtils.isEmpty(popupDuration)) {
                    switch (popupDuration) {
                        case Constants.EVERY_TIME:
                            AppPreferences.setSubscriptionPopUpState(true);
                            break;
                        case Constants.EVERY_DAY:
                            if (DateTimeUtils.getDaysDifference(AppPreferences.getPopUpDate()) > 0) {
                                AppPreferences.setSubscriptionPopUpState(true);
                            }
                            break;
                        case Constants.EVERY_WEEK:
                            if (DateTimeUtils.getDaysDifference(AppPreferences.getPopUpDate()) > 7) {
                                AppPreferences.setSubscriptionPopUpState(true);
                            }
                            break;
                        case Constants.ALTERNATE_DAY:
                            if (DateTimeUtils.getDaysDifference(AppPreferences.getPopUpDate()) > 1) {
                                AppPreferences.setSubscriptionPopUpState(true);
                            }
                            break;
                        default:
                            Log.e("pop up duration error", popupDuration);
                            break;
                    }
                }
            }

        }

    }

    @Override
    public void onTabChanged() {
        mNewDashBoardData = AppPreferences.getDashBoard();
        List<MenuItem> items = new ArrayList<>();
        int tabCount = 1;
        mTabBar.removeAllViews();
        if (!TextUtils.isEmpty(mNewDashBoardData.getContetnType())) {
            mTabBar.setSelectedDetailPage(mNewDashBoardData.getContetnType());

        }


        items.add(new MenuItem(R.string.dashboard, R.drawable.tab_icon_dashboard));

        // checking for null
        if (mNewDashBoardData.isMentorTabVisible()) {
            items.add(new MenuItem(R.string.all_mentors, R.drawable.tab_icon_mentors));
            tabCount++;
        }

        if (mNewDashBoardData.isVideoTabVisible()) {
            items.add(new MenuItem(R.string.events, R.drawable.tab_icon_video_new));
            tabCount++;
        }

        if (mNewDashBoardData.isCourseTabVisible()) {
            items.add(new MenuItem(R.string.all_courses, R.drawable.tab_icon_courses));
            tabCount++;
        }

        if (mNewDashBoardData.isPurchaseTabVisible()) {
            items.add(new MenuItem(R.string.purchase, R.drawable.ic_video_purchase));
            tabCount++;
        }

        mTabBar.setMenuItems(items);
        mTabBar.setSelectedIndex(0);

        if (getTopFragment() instanceof DashBoardPagerFragment) {
            DashBoardPagerFragment topFrag = (DashBoardPagerFragment) getTopFragment();
            topFrag.onTabChanged(tabCount, mNewDashBoardData);
        }

    }

    @Override
    public void onSeeAllClicked(String key, DashBoardItemOdering item) {
        switch (key) {
            case Constants.COURSES_KEY:
                if (mNewDashBoardData != null) {
                    if (!mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible()) {
                        mTabBar.setSelectedIndex(1);
                    } else if (!mNewDashBoardData.isMentorTabVisible() || !mNewDashBoardData.isVideoTabVisible()) {
                        mTabBar.setSelectedIndex(2);
                    } else {
                        mTabBar.setSelectedIndex(3);

                    }
                } else {
                    mTabBar.setSelectedIndex(3);
                }

                break;
            case Constants.MENTORS_KEY:
                mTabBar.setSelectedIndex(1);
                break;
            case Constants.VIDEOS_KEY:
                if (mNewDashBoardData != null) {
                    if (!mNewDashBoardData.isMentorTabVisible()) {
                        mTabBar.setSelectedIndex(1);
                    } else {
                        mTabBar.setSelectedIndex(2);
                    }
                } else {
                    mTabBar.setSelectedIndex(2);
                }
                break;
            case Constants.COURSES_YOU_MAY_LIKE:
                // no category id is required for courses you may like list
                Fragment coursesYouMayLikeFragment = SeeAllFragment.newInstance(null, item.getSee_all(), item.getTitle(), null);
                FragmentTransaction fragTransaction = fm.beginTransaction();
                fragTransaction.addToBackStack(null);
                fragTransaction.add(R.id.fm_main, coursesYouMayLikeFragment).commit();
                break;
            default:
                if (!TextUtils.isEmpty(item.getCat_id())) {
                    // case for simple see all  with sub category in top list
                    Fragment seeAllFragment = SeeAllFragment.newInstance(item.getCat_id(), item.getSee_all(), item.getTitle(), null);
                    FragmentTransaction transaction = fm.beginTransaction();
                    transaction.addToBackStack(null);
                    transaction.add(R.id.fm_main, seeAllFragment).commit();

                } else {
                    // case for see all tab with sub category in all list
                    if (item.getSee_all().equals(Constants.ALL_EVENTS_KEY)) {
                        Fragment seeAllFragment = OtherTabFragment.newInstance(Constants.SEE_ALL_EVENT_TAB, item.getSee_all());
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.add(R.id.fm_main, seeAllFragment).commit();
                    } else {
                        Fragment seeAllFragment = OtherTabFragment.newInstance(Constants.SEE_ALL_COURSES_TAB, item.getSee_all());
                        FragmentTransaction transaction = fm.beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.add(R.id.fm_main, seeAllFragment).commit();
                    }


                }

        }
    }


    public void setTabSelected(int postion) {
        mTabBar.setSelectedIndex(postion);

    }

    private void setCurrentItemInViewPager(int position) {

        Utils.releaseDetailScreenPlayer();

        if (getTopFragment() instanceof DashBoardPagerFragment) {
            DashBoardPagerFragment topFrag = (DashBoardPagerFragment) getTopFragment();
            topFrag.setCurrentItem(position);
        } else {
            if (getTopFragment() != null) {
                fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                DashBoardPagerFragment topFrag = (DashBoardPagerFragment) getTopFragment();
                topFrag.setCurrentItem(position);
            }
        }
    }


    public void addFragment(Fragment fragment) {

        FragmentTransaction transaction = fm.beginTransaction();
        if (transaction.isAddToBackStackAllowed()) {
            transaction.addToBackStack(null);
            transaction.add(R.id.fm_main, fragment).commitAllowingStateLoss();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (fm == null) {
            fm = getSupportFragmentManager();
        }
    }

    @Override
    public void onTopSearchItemClick(String item) {
        if (getTopFragment() instanceof SearchContainerFragment) {
            SearchContainerFragment topFrag = (SearchContainerFragment) getTopFragment();
            topFrag.replace(SearchFragment.newInstance(item, null, null));
            topFrag.setTextOnSearchView(item);
            topFrag.closeKeyBoard();
        }
    }


    @Override
    public void onTopSearchItemClick(SearchAutoComplete item) {
        if (getTopFragment() instanceof SearchContainerFragment) {
            SearchContainerFragment topFrag = (SearchContainerFragment) getTopFragment();
            topFrag.closeKeyBoard();

            if (!item.getType().equals(TYPE_TAG)) {
                addFragment(DashBoardDetailFragment.newInstance(item.getId(), item.getType(), null, false, null));
            } else {
                topFrag.replace(SearchFragment.newInstance(item.getName(), item.getId(), item.getType()));
                topFrag.setTextOnSearchView(item.getName());
            }

        }
    }


    @Override
    public void onSubCategoryItemClick(SubCategory item, String viewType) {
        // not to be used
    }

    @Override
    public void onSubCategorySeeAllFilterClick(SubCategory item, String parentId) {

        if (getTopFragment() instanceof SeeAllFragment) {
            SeeAllFragment topFrag = (SeeAllFragment) getTopFragment();
            if (item.getId() == null) {
                //show all condition
                topFrag.callSeeAllApi(Api.SEE_ALL_API, parentId, null, item.getTitle());
            } else {
                topFrag.callSeeAllApi(Api.SEE_ALL_API_FILTER, item.getId(), parentId, item.getTitle());
            }
        }

    }

    @Override
    public void onArticleClick(String detailType, String detailId) {
        addFragment(DashBoardDetailFragment.newInstance(detailId, detailType, null, false, null));
        mTabBar.setSelectedDetailPage(Constants.TYPE_COURSE);
    }


    @Override
    public void onFullScreenVideoButtonClick(int resumeWindow, long resumePosition, List<String> url, DashBoardCourseDetails dashBoardCourseDetails, DashBoardVideoDetails dashBoardVideoDetails, String title, boolean isCourseDetail, String thumbUrl, String posterImage) {
        Intent intent = new Intent();
        intent.setClass(this, FullScreenVideoActivity.class);
        intent.putExtra(STATE_RESUME_POSITION, resumePosition);
        intent.putExtra(STATE_RESUME_WINDOW, resumeWindow);
        intent.putStringArrayListExtra(VIDEO_URL_LIST, (ArrayList<String>) url);
        intent.putExtra(Constants.DASHBOARD_COURSE_DATA, dashBoardCourseDetails);
        intent.putExtra(Constants.DASHBOARD_VIDEO_DATA, dashBoardVideoDetails);
        intent.putExtra(Constants.VIDEO_TITLE, title);
        intent.putExtra(Constants.THUMB_URL, thumbUrl);
        intent.putExtra(Constants.POSTER_IMAGE_URL, posterImage);
        intent.putExtra(IS_COURSEDETAIL, isCourseDetail);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onEClassesClick(final Chapters.EClasses item, final int position, String coursePrice, final String courseId, String itemType, String courseTitle, boolean isProgramme, boolean isDownloaded) {

        if (Utils.isNetworkAvailable(this)) {
            if (getTopFragment() instanceof DashBoardDetailFragment) {
                final DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();

                switch (item.getFree_paid()) {
                    case "Free":
                        switch (item.getType()) {

                            case Constants.TYPE_VIDEO:

                                if (!TextUtils.isEmpty(item.getFile())) {

                                    topFrag.startNewVideo(item.getId(), position);
                                }
                                break;

                            case Constants.TYPE_DETAIL_QUIZ:
                                if (!TextUtils.isEmpty(item.getFile())) {

                                    Utils.callEventLogApi("accessed <b>" + item.getTitle() + "</b>");
                                } else {
                                    Utils.callEventLogApi("accessed <b>" + item.getTitle() + "</b> " + "quiz " + item.getFile());

                                }
                                topFrag.stopVideo();
                                addFragment(QuizFragment.newInstance(item.getQuiz_list_id()));
                                Utils.releaseDetailScreenPlayer();

                                break;


                            case Constants.TYPE_DETAIL_AUDIO:
                                if (!TextUtils.isEmpty(item.getFile())) {

                                    topFrag.stopVideo();
                                    Utils.releaseDetailScreenPlayer();

                                }
                                break;

                            default:
                                if (!TextUtils.isEmpty(item.getFile())) {
                                    Utils.callEventLogApi("opened <b>" + item.getTitle() + " document</b>");

                                    topFrag.stopVideo();
                                    Utils.releaseDetailScreenPlayer();
                                    Utils.openDocument(item.getFile(), this);
                                }
                                break;
                        }
                        break;
                    case "Paid":
                        topFrag.clearScreen();

                        if (!AppPreferences.getCreditCardList().isEmpty()) {
                            openDialog(PaymentCardListDialog.newInstance(courseId, itemType, item.getId()));
                        } else {
                            openDialog(PayDialogFragment.newInstance("Ruppee", false, false, courseId, itemType, item.getId()));
                        }
                        break;
                    default:
                        topFrag.clearScreen();

                        openDialog(EnrollmentDialogFragment.newInstance(item.getId(), courseId, courseTitle, isProgramme));

                        Utils.callEventLogApi("clicked " + " icon of eclasses from <b>" + item.getTitle() + "</b>");


                        break;
                }


            }
        } else {
            if (isDownloaded) {

                if (getTopFragment() instanceof DashBoardDetailFragment) {
                    final DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();

                    if (item.getFree_paid().equals("Free") && item.getType().equals(Constants.TYPE_VIDEO)) {

                        topFrag.startNewVideo(item.getId(), position);
                    } else {
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }

            } else {
                Utils.showNetworkError(this, new NoConnectionError());
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.setDialogValue();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utils.callEventLogApi("Pressed<b> Back Button</b>");


        if (isFragmentInBackStack(new QuizFragment())) {

            List<Fragment> fragentList = getSupportFragmentManager().getFragments();
            for (Fragment fragment : fragentList) {

                if (fragment.getClass().isAssignableFrom(QuizFragment.class)) {
                    fm.popBackStackImmediate();
                }

                if (fragment.getClass().isAssignableFrom(QuizResultFragment.class)) {
                    fm.popBackStackImmediate();
                }

            }

            checkForTopFrag();

        }


        if (getTopFragment() instanceof DashBoardDetailFragment) {
            DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();
            topFrag.reload();
        }

    }


    private void checkForTopFrag() {
        if (getTopFragment() instanceof DashBoardDetailFragment) {
            // keep going remove till dashboard detail
        } else {
            fm.popBackStackImmediate();
            checkForTopFrag();
        }
    }

    @Override
    public void onMentorClick(String detailType, String detailId) {
        addFragment(DashBoardDetailFragment.newInstance(detailId, detailType, null, false, null));
        mTabBar.setSelectedDetailPage(Constants.TYPE_MENTOR);
    }

    @Override
    public void onVideoClick(String detailType, String detailId, String episodeId) {
        addFragment(DashBoardDetailFragment.newInstance(detailId, detailType, episodeId, false, null));
    }

    @Override
    public void onDigitalCoachingClick(ArrayList<DigitalCoaching> digitalCoachingArrayList, int totalServerItem,
                                       int nextPage, String detailType, String detailId, String mIsSubscribed,
                                       String isAutoPlay, String freePaid, boolean showpayDialog, String mentorName,
                                       String mentorUrl, String selectedId, String description,
                                       String bannerimage, String groupImage, boolean isDownloaded,
                                       DashBoardMentorDetails dashBoardMentorDetails) {


        if (isDownloaded) {
            if (getTopFragment() instanceof DashBoardDetailFragment) {
                DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();
                topFrag.clearResponse();
                topFrag.reload();
            }


            Utils.releaseDetailScreenPlayer();
            Utils.releaseFullScreenPlayer();
            addFragment(DigitalCoachingFragment.newInstance(digitalCoachingArrayList, totalServerItem, nextPage, isAutoPlay, mIsSubscribed, mentorName, mentorUrl, detailId, selectedId, description, bannerimage, groupImage, isDownloaded, dashBoardMentorDetails));

        } else {


            if (mIsSubscribed.equals("No") && freePaid.equals("Paid")) {

                if (showpayDialog) {
                    if (AppPreferences.getCreditCardList() != null && !AppPreferences.getCreditCardList().isEmpty()) {
                        openDialog(PaymentCardListDialog.newInstance(detailId, Constants.TYPE_EXPERT, null));
                    } else {
                        openDialog(PayDialogFragment.newInstance("Ruppee", false, false, detailId, Constants.TYPE_EXPERT, null));
                    }
                } else {
                    addFragment(DashBoardDetailFragment.newInstance(detailId, detailType, null, false, null));

                }


            } else {
                if (Utils.isNetworkAvailable(this)) {
                    if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                        Utils.showTwoButtonAlertDialog(this, this, this.getString(R.string.notification), this.getString(R.string.check_wifi_status), this.getString(R.string.okay_text), this.getString(R.string.go_to_setting));

                    } else {


                        if (getTopFragment() instanceof DashBoardDetailFragment) {
                            DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();
                            topFrag.clearResponse();
                            topFrag.reload();
                        }


                        Utils.releaseDetailScreenPlayer();
                        Utils.releaseFullScreenPlayer();
                        addFragment(DigitalCoachingFragment.newInstance(digitalCoachingArrayList, totalServerItem, nextPage, isAutoPlay, mIsSubscribed, mentorName, mentorUrl, detailId, selectedId, description, bannerimage, groupImage, isDownloaded, dashBoardMentorDetails));

                    }
                } else {
                    Utils.showNetworkError(this, new NoConnectionError());
                }
            }
        }


    }

    // mobile
    // clothes & shoes
    // electrnoics
    // bikes
    // cars
    // decorative items
    // furnture
    // Flat
    // House
    // sports equipemnt
    // books

    @Override
    public void onEventClick(String detailType, String detailId) {
        addFragment(DashBoardDetailFragment.newInstance(detailId, detailType, null, false, null));
    }

    private void callUserDataApi(final String api) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, response -> {

            switch (api) {
                case Api.CUSTOMER_DATA_API:
                    try {
                        if (Utils.checkForMaintainceMode(response)) {
                            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(MainActivity.this);
                            return;
                        }
                        Customer customer = new Gson().fromJson(response, Customer.class);

                        if (customer.getCustomer() != null) {
                            AppPreferences.setCustomerData(customer);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(Constants.LOG, e.toString());
                    }
                    break;


                case Api.COUNTRY_CODES_API:

                    try {
                        if (Utils.checkForMaintainceMode(response)) {
                            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(MainActivity.this);
                            return;
                        }
                        List<CountryCode> countryCodeList = new Gson().fromJson(response, new TypeToken<List<CountryCode>>() {
                        }.getType());

                        if (!countryCodeList.isEmpty()) {
                            AppPreferences.setCountryCode(countryCodeList);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(Constants.LOG, e.toString());
                    }
                    break;

                case Api.SUPPORT_TYPES_API:
                    try {
                        if (Utils.checkForMaintainceMode(response)) {
                            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(MainActivity.this);
                            return;
                        }
                        List<SupportType> supportTypeList = new Gson().fromJson(response, new TypeToken<List<SupportType>>() {
                        }.getType());

                        if (!supportTypeList.isEmpty()) {
                            AppPreferences.setSupportType(supportTypeList);
                        }
                    } catch (Exception e) {
                        Logger.getLogger(Constants.LOG, e.toString());
                    }


                    break;

                default:
                    // nothing to do
                    break;

            }

        }, error -> {
            // no use
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return Utils.getParams(false);

            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public void onPositiveButtonClick(AlertDialog dialog, boolean isNotification) {

        // handling notification dialog condition
        if (isNotification) {

            if (notifcationLayout.equals(Constants.CUSTOMER_SUPPORT_NOTIFICATION)) {
                openCustomerSupportActivity(notificationIntent);
            } else if (notifcationLayout.equals(Constants.LIVE_NOTIFICATION_DIALOG)) {
                openLiveChatActivity(notificationIntent);
            } else {
                if (AppPreferences.getCustomerSupportActivityState()) {
                    onBackPressed();
                }
                addFragment(DashBoardDetailFragment.newInstance(notificationId, notifcationLayout, null, false, null));

            }
        }
        dialog.dismiss();

    }

    @Override
    public void onNegativeButtonClick(AlertDialog dialog, boolean isNotification) {

        // handling notification dialog condition
        if (!isNotification) {
            addFragment(SettingsFragment.newInstance());
        }

        dialog.dismiss();
    }

    @Override
    public void onSliderImageClick(DashBoardSlider item, int position) {
        addFragment(DashBoardDetailFragment.newInstance(item.getItem_id(), item.getType(), null, false, null));

        if (item.getType().equals(Constants.TYPE_COURSE)) {
            mTabBar.setSelectedDetailPage(Constants.TYPE_COURSE);

        } else {
            mTabBar.setSelectedDetailPage(mNewDashBoardData.getContetnType());

        }


    }


    private void callLoginHistoryApi() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.LOGIN_HISTORY, response -> {
            try {
                if (Utils.checkForMaintainceMode(response)) {
                    WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(MainActivity.this);
                    return;
                }
                JSONObject jsonObject = new JSONObject(response);

                if (!TextUtils.isEmpty(jsonObject.optString(Keys._CATEGORY)) && !isCategoryChooser) {

                    WealthDragonsOnlineApplication.sharedInstance().redirectToCategoryChooserActivity(MainActivity.this);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, error -> {
            // no reaction

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                params.put(Keys.LOGIN_WITH, "Dashboard");
                String manufractureName = android.os.Build.MANUFACTURER;
                manufractureName = manufractureName.substring(0, 1).toUpperCase() + manufractureName.substring(1);
                params.put(Keys.DEVICE_NAME, manufractureName + " " + android.os.Build.MODEL);
                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);
    }


    // used for customer support notifications
    private void openCustomerSupportActivity(Intent notificationIntent) {
        Utils.callEventLogApi("visited <b>Customer Support Screen</b>");
        Intent intent = new Intent();
        intent.putExtra(Keys.NOTIFICATION, notificationIntent.getSerializableExtra(Keys.NOTIFICATION));
        intent.setClass(this, CustomerSupportActivity.class);
        startActivity(intent);
    }

    // used for customer support notifications
    private void openLiveChatActivity(Intent notificationIntent) {

        if (getTopFragment() instanceof DashBoardDetailFragment) {
            DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();
            topFrag.clearResponse();
        }


        Utils.callEventLogApi("visited <b>Live Chat Screen</b>");
        Intent intent = new Intent();
        intent.putExtra(Keys.NOTIFICATION, notificationIntent.getSerializableExtra(Keys.NOTIFICATION));
        intent.putExtra(Keys.RELOAD_PAGE, false);

        intent.setClass(this, ChatActivity.class);
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (data.getBooleanExtra(Keys.RELOAD_PAGE, false)) {

                if (getTopFragment() instanceof DashBoardDetailFragment) {
                    DashBoardDetailFragment topFrag = (DashBoardDetailFragment) getTopFragment();
                    topFrag.reload();
                }
            } else {

                addFragment(DashBoardDetailFragment.newInstance(data.getStringExtra(Keys.MENTOR_ID), Constants.TYPE_MENTOR, null, false, null));

            }
        }

    }

    private void callMaintainceNotificaitonApi() {
        AppPreferences.setMaintainceModeUpdate(null);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.MAINTENANCE_NOTIFICATION, response -> {
            MaintainceNotification maintainceNotification = new Gson().fromJson(response, MaintainceNotification.class);
            if (maintainceNotification.getMain_status().equals("1")) {
                WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(MainActivity.this);
            } else if (maintainceNotification.getStatus().equals("1")) {
                showMaintainaceMarquee(maintainceNotification.getNotification_text());
                AppPreferences.setMaintainceModeUpdate(maintainceNotification.getNotification_text());
            } else if (maintainceNotification.getStatus().equals("2")) {
                showMaintainaceMarquee(null);
                AppPreferences.setMaintainceModeUpdate(null);
            }
        }, error -> Logger.getLogger(Constants.LOG, error.toString())

        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void showMaintainaceMarquee(String maintainanceMessage) {

        if (!TextUtils.isEmpty(maintainanceMessage)) {
            mMarkview.setVisibility(View.VISIBLE);
            mTvMaintainceMode.setText(maintainanceMessage);
            mMarkview.setSelected(true);
        } else {
            mMarkview.setVisibility(View.GONE);
        }
    }


    // opening payment dialog
    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(getSupportFragmentManager(), "dialog");

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PaymentHistoryDetailFragment.REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            int grantResultsLength = grantResults.length;
            if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "You grant write external storage permission. Please click original button again to continue.", Toast.LENGTH_LONG).show();
                if (getTopFragment() instanceof PaymentHistoryDetailFragment) {
                    PaymentHistoryDetailFragment paymentHistoryDetailFragment = (PaymentHistoryDetailFragment) getTopFragment();
                    paymentHistoryDetailFragment.saveFile();

                }
            } else {
                Toast.makeText(getApplicationContext(), "You denied write external storage permission.", Toast.LENGTH_LONG).show();
            }
        }
    }


    private void selectTabDetailPage(String layoutType) {
        switch (layoutType) {
            case Constants.TYPE_COURSE:
                if (mNewDashBoardData != null && mNewDashBoardData.isCourseTabVisible()) {
                    if (!mNewDashBoardData.isMentorTabVisible() && !mNewDashBoardData.isVideoTabVisible()) {
                        mTabBar.setSelectedIndex(1);
                    } else if (!mNewDashBoardData.isMentorTabVisible() || !mNewDashBoardData.isVideoTabVisible()) {
                        mTabBar.setSelectedIndex(2);
                    } else {
                        mTabBar.setSelectedIndex(3);

                    }
                } else {
                    mTabBar.setSelectedIndex(-1);
                }

                break;
            case Constants.TYPE_MENTOR:
                if (mNewDashBoardData != null && mNewDashBoardData.isMentorTabVisible()) {
                    mTabBar.setSelectedIndex(1);
                } else {
                    mTabBar.setSelectedIndex(-1);
                }

                break;
            case Constants.TYPE_EXPERT:
                if (mNewDashBoardData != null && mNewDashBoardData.isMentorTabVisible()) {
                    mTabBar.setSelectedIndex(1);
                } else {
                    mTabBar.setSelectedIndex(-1);
                }
                break;
            case Constants.TYPE_EVENT:
                if (mNewDashBoardData != null && mNewDashBoardData.isVideoTabVisible()) {
                    if (!mNewDashBoardData.isMentorTabVisible()) {
                        mTabBar.setSelectedIndex(1);
                    } else {
                        mTabBar.setSelectedIndex(2);
                    }
                } else {
                    mTabBar.setSelectedIndex(-1);
                }
                break;
            case Constants.TYPE_VIDEO:
                if (mNewDashBoardData != null && mNewDashBoardData.isVideoTabVisible()) {
                    if (!mNewDashBoardData.isMentorTabVisible()) {
                        mTabBar.setSelectedIndex(1);
                    } else {
                        mTabBar.setSelectedIndex(2);
                    }
                } else {
                    mTabBar.setSelectedIndex(-1);
                }
                break;
            default:
                // no use
                break;
        }
    }


}
