package com.imentors.wealthdragons.activity;

import android.content.BroadcastReceiver;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;


import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.models.WelcomeModel;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


public class SplashActvity extends AppCompatActivity {


    private ProgressBar mProgressBar;
    private boolean isNotificationAvailable;
    private Intent notificationIntent;
    private BroadcastReceiver mDismissAlertDialog = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getWelcomePageDetails();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WealthDragonsOnlineApplication.NukeSSLCerts.nuke();
        if (AppPreferences.getMaintainceMode()) {
            WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(this);
            return;
        }
        Utils.hideKeyboard(this);

        LocalBroadcastManager.getInstance(this).registerReceiver(mDismissAlertDialog,
                new IntentFilter(Constants.BROADCAST_DISMISS_ALERT_DIALOG));

        setContentView(R.layout.activity_splash);
        mProgressBar = findViewById(R.id.progressBar);
        AppPreferences.setTabLayoutResized(false);
        notificationIntent = getIntent();
        Uri url = notificationIntent.getData();
        if (url != null) {
            Log.e("url", url.toString());
            Utils.callDeepLinkApi(url);
        }
        isNotificationAvailable = notificationIntent.hasExtra(Keys.NOTIFICATION);

        if (AppPreferences.isLoggedIn()) {
            //no need to call api as user will be redirected to desired screen in Login Activity
            redirectToInnerScreen();
        } else {
            getWelcomePageDetails();
        }
    }

    private void redirectToInnerScreen() {


        // condition to match if user should be directed to dashboard or subscription
        if (AppPreferences.getUser().isDashboard()) {
            redirectUserToDashBoardActivity();
        } else {
            redirectToLoginActivity(null);
        }
    }

    private void redirectUserToDashBoardActivity() {


        Intent intent = new Intent();
        if (isNotificationAvailable) {
            intent.putExtra(Keys.NOTIFICATION, notificationIntent.getSerializableExtra(Keys.NOTIFICATION));
            if (AppPreferences.getFullScreenActivityState()) {
                onBackPressed();
            }
        }

        // to handle customer support notification in case it is opened.. transferring of intent
        if (AppPreferences.getCustomerSupportActivityState()) {
            intent.setClass(this, CustomerSupportActivity.class);
        } else {
            intent.setClass(this, MainActivity.class);
        }

        if (!isNotificationAvailable) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.no_animation);
        finish();
    }

    private void getWelcomePageDetails() {
        // show progress Bar

        mProgressBar.setVisibility(View.VISIBLE);

        // url for homepage
        String url = Api.HOMEPAGE_API;


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, response -> {
            try {

                JSONObject jsonObject = new JSONObject(response);


                if (!TextUtils.isEmpty(jsonObject.optString(Keys._MAINTAINANCE))) {

                    String message = jsonObject.getString(Keys._MAINTAINANCE);
                    if (message.equals(Keys.ON)) {
                        AppPreferences.setMaintainceMode(true);
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(SplashActvity.this);
                        return;
                    }

                } else {

                    onGetDataAPIServerResponse(new Gson().fromJson(response, WelcomeModel.class));


                }

            } catch (Exception e) {
                Logger.getLogger(Constants.LOG, e.toString());
            }

        }, error -> {
            Logger.getLogger(Constants.LOG, error.toString());
            mProgressBar.setVisibility(View.GONE);
            if (error instanceof NoConnectionError) {
                Utils.showInternetError(SplashActvity.this, true);
            } else {
                Utils.showAlertDialog(SplashActvity.this, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), true, true);
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                if (WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet)) {
                    params.put(Keys.DEVICE, Keys.TABLET);

                } else {
                    params.put(Keys.DEVICE, Keys.ANDROID);

                }

                return params;
            }
        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);


    }

    private void onGetDataAPIServerResponse(WelcomeModel response) {


        // send user to login screen if data comes here
        if (response != null) {

            //set data in bundle
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.SIGNING_MODEL_DATA, response);

            redirectToLoginActivity(bundle);
        } else {

            Utils.showAlertDialog(SplashActvity.this, getString(R.string.error), getString(R.string.error_api_try_again), getString(R.string.okay_text), true, true);

        }

    }

    private void redirectToLoginActivity(Bundle bundle) {
        Intent intent = new Intent();
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (isNotificationAvailable) {
            intent.putExtra(Keys.NOTIFICATION, notificationIntent.getSerializableExtra(Keys.NOTIFICATION));
        }

        intent.setClass(this, LoginActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_from_right, R.anim.no_animation);
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mDismissAlertDialog);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mDismissAlertDialog);
        }
    }

    public void getHashkey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());

                Log.i("Base64", Base64.encodeToString(md.digest(), Base64.NO_WRAP));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("Name not found", e.getMessage(), e);

        } catch (NoSuchAlgorithmException e) {
            Log.d("Error", e.getMessage(), e);
        }

    }

}
