package com.imentors.wealthdragons.courseDetailView;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.RatingDetail;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class CourseDetailRatings extends RelativeLayout {


    private Context mContext;
    private FrameLayout mGetInsantAccess , mAddToWishList;
    private WealthDragonTextView mRatingCount , mRatingSmallCount , mRatingOneCount , mRatingTwoCount , mRatingThreeCount , mRatingFourCount ,mRatingFiveCount;
    private View mFiveStarView , mFourStarview,mThreeStarView , mTwoStarView , mOneStarView;
    private RatingBar mRatingBar;





    public CourseDetailRatings(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailRatings(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailRatings(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseDetailRatings(Context context, DashBoardCourseDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(DashBoardCourseDetails dashBoardDetails) {

        RatingDetail ratings = dashBoardDetails.getCourseDetail().getRatingDetail();

        mRatingCount.setText((int) ratings.getRating() +".O");

        mRatingBar.setRating(ratings.getRating());


        mRatingSmallCount.setText(ratings.getRating_persons()+" " + mContext.getString(R.string.ratings));
        mRatingOneCount.setText(Integer.toString(ratings.getOne_count()));
        mRatingTwoCount.setText(Integer.toString(ratings.getTwo_count()));
        mRatingThreeCount.setText(Integer.toString(ratings.getThree_count()));
        mRatingFourCount.setText(Integer.toString(ratings.getFour_count()));
        mRatingFiveCount.setText(Integer.toString(ratings.getFive_count()));

    }

    //  initialize course detail ratings view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_ratings, this);
        mContext = context;

        mRatingCount = view.findViewById(R.id.tv_rating_count);
        mRatingSmallCount = view.findViewById(R.id.tv_rating_count_small);
        mRatingOneCount = view.findViewById(R.id.tv_one_count);
        mRatingTwoCount = view.findViewById(R.id.tv_two_count);
        mRatingThreeCount = view.findViewById(R.id.tv_three_count);
        mRatingFourCount = view.findViewById(R.id.tv_four_count);
        mRatingFiveCount = view.findViewById(R.id.tv_five_count);
        mRatingBar = findViewById(R.id.ratingBar);





    }



}
