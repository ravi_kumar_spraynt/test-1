package com.imentors.wealthdragons.courseDetailView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.text.Html;
import android.text.Layout;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.MentorDetail;
import com.imentors.wealthdragons.utils.Utils;
//import com.imentors.wealthdragons.views.WealthDragonFlowTextView;
import com.imentors.wealthdragons.views.WealthDragonMentorTextView;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class CourseDetailMentorDescription extends LinearLayout {


    private Context mContext;
    private FrameLayout mBtFullDetails;
    private WealthDragonTextView mTvMentorShortDescription, mTvMentorFullDescription , mTvFullDetailBtn;
//    private WealthDragonFlowTextView mTvFlowTextView;
//    private WealthDragonMentorTextView mTvMentorShortDescription;
    private boolean isFullVisible = false;
    private ImageView mMentorImage;
    private View mUnderLine;




    public CourseDetailMentorDescription(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailMentorDescription(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailMentorDescription(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    // calling constructor
    public CourseDetailMentorDescription(Context context, DashBoardCourseDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data in view
    private void bindView(final DashBoardCourseDetails dashBoardDetails) {

        final MentorDetail mentorDetail = dashBoardDetails.getCourseDetail().getMentors().get(0);

        if(!TextUtils.isEmpty(mentorDetail.getBanner())) {
            Glide.with(mContext).load(mentorDetail.getBanner()).error(R.drawable.no_img_mentor).into(mMentorImage);
        }



        if(!TextUtils.isEmpty(mentorDetail.getLong_details())){
            mBtFullDetails.setVisibility(View.GONE);
        }else{
            mBtFullDetails.setVisibility(View.GONE);
        }


        final String mentorShortDetail = mentorDetail.getShort_details();
        final String mentorLongDetail = mentorDetail.getLong_details();


        if(!TextUtils.isEmpty(mentorShortDetail)){
            mTvMentorShortDescription.setVisibility(View.VISIBLE);

//            mTvFlowTextView.setVisibility(View.VISIBLE);
//            setText(mentorDetail);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                mTvFlowTextView.setText(Html.fromHtml(mentorShortDetail,Html.FROM_HTML_MODE_LEGACY));
                mTvMentorShortDescription.setText(Html.fromHtml(mentorShortDetail,Html.FROM_HTML_MODE_LEGACY));
            } else {
//                mTvFlowTextView.setText(Html.fromHtml(mentorShortDetail));

                mTvMentorShortDescription.setText(Html.fromHtml(mentorShortDetail));
            }



//            int charcterCountInShortDescription = 0;
//
//
//            int end = mTvMentorShortDescription.getLayout().getLineEnd(mTvMentorShortDescription.getLineCount() - 1);
//
//
//
//            String displayed = mTvMentorShortDescription.getText().toString().substring(0, end);
//
//            charcterCountInShortDescription = displayed.length();




//            int getVisibleTextCount = getVisibleText(mTvMentorShortDescription,mentorShortDetail);
//            int textciunt = mentorDetail.getShort_details().length();
//            if(getVisibleTextCount < mentorDetail.getShort_details().length()) {
//                mentorLongDetail.startsWith(mentorShortDetail.substring(getVisibleTextCount));
//            }



        }else{
            mTvMentorShortDescription.setVisibility(View.GONE);
//            mTvFlowTextView.setVisibility(View.GONE);
            mBtFullDetails.setVisibility(View.GONE);
        }






            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                mTvMentorFullDescription.requestFocus();
                mTvMentorFullDescription.setText(Html.fromHtml(mentorLongDetail, Html.FROM_HTML_MODE_LEGACY));
            } else {
                mTvMentorFullDescription.setText(Html.fromHtml(mentorLongDetail));
            }




//        mTvMentorShortDescription.setMovementMethod(LinkMovementMethod.getInstance());


        mBtFullDetails.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isFullVisible){
                    Utils.callEventLogApi("clicked <b>Full Biography button</b> from <b>"+dashBoardDetails.getCourseDetail().getTitle());
                    mTvMentorFullDescription.setVisibility(View.VISIBLE);
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        mTvMentorFullDescription.requestFocus();
                        mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details(),Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        mTvMentorFullDescription.setText(Html.fromHtml(mentorDetail.getLong_details()));
                    }

                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( Utils.dpToPx(94), Utils.dpToPx(30));
                    params.setMargins(Utils.dpToPx(4),Utils.dpToPx(10),0,0);
                    mBtFullDetails.setLayoutParams(params);
                    mTvFullDetailBtn.setText(mContext.getString(R.string.show_less));
                }else{
                    Utils.callEventLogApi("clicked <b>Show Less button</b> from <b>"+dashBoardDetails.getCourseDetail().getTitle());
                    mTvMentorFullDescription.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams( Utils.dpToPx(94), Utils.dpToPx(30));
                    params.setMargins(Utils.dpToPx(104),Utils.dpToPx(10),0,0);
                    mBtFullDetails.setLayoutParams(params);
                    mTvFullDetailBtn.setText(mContext.getString(R.string.full_bio));
                }

                isFullVisible = !isFullVisible;
            }
        });

        mTvMentorFullDescription.setMovementMethod(LinkMovementMethod.getInstance());


//        if(dashBoardDetails.getCourseDetail().getRelated_Courses()!=null && dashBoardDetails.getCourseDetail().getRelated_Programme()!=null) {
//            if (dashBoardDetails.getCourseDetail().getRelated_Courses().getCourses() != null && dashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes() != null) {
//                if (dashBoardDetails.getCourseDetail().getRelated_Courses().getCourses().size() > 0 && dashBoardDetails.getCourseDetail().getRelated_Programme().getProgrammes().size()>0) {
//                    mUnderLine.setVisibility(VISIBLE);
//                }else{
//                    mUnderLine.setVisibility(GONE);
//                }
//            }else{
//                mUnderLine.setVisibility(GONE);
//            }
//        }else{
//            mUnderLine.setVisibility(GONE);
//        }

    }

    //  initialize course detail mentor description view
    private void initViews(Context context ) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_mentor_description, this);
        mContext = context;

        mBtFullDetails = view.findViewById(R.id.fl_btn_full_details);

        mMentorImage = view.findViewById(R.id.iv_mentor_image);

        mTvMentorFullDescription = view.findViewById(R.id.tv_mentor_full_description);
        mTvMentorShortDescription = view.findViewById(R.id.tv_mentor_small_description);

//        mTvFlowTextView = view.findViewById(R.id.tv_flow);
//        mTvFlowTextView.setTextSize(getResources().getDimension(R.dimen.text_size_small));


        mTvFullDetailBtn = view.findViewById(R.id.tv_full_detail_btn);
        mUnderLine = view.findViewById(R.id.view_underline);

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

//    private void setText(MentorDetail mentorDetail){
//
//        int totalheight;
//        final float[] widths = new float[1];
//        if(!TextUtils.isEmpty(mentorDetail.getLong_details())) {
//            totalheight =  Utils.dpToPx(30) + Utils.dpToPx(94) + (int)mContext.getResources().getDimension(R.dimen.margin_default);
//        }else{
//            totalheight =   Utils.dpToPx(94) + (int)mContext.getResources().getDimension(R.dimen.margin_default);
//        }
//
//        mTvMentorShortDescription.getLayoutParams().height = totalheight;
//
//        // number of lines in textView
//
//        int linesPerScreen = mTvMentorShortDescription.getLayoutParams().height/(mTvMentorShortDescription.getLineHeight() + (int)mTvMentorShortDescription.getLineSpacingExtra());
//
//
//        // Measure how much text will fit across the TextView
//
//        Paint paint = mTvMentorShortDescription.getPaint();
//
//
//        mTvMentorShortDescription.measure(0, 0);
//        int b =  mTvMentorShortDescription.getMeasuredWidth();
//        float c = mTvMentorShortDescription.getMaxWidth();
//        //must call measure!
//
//        int textWidth  = paint.breakText(mentorDetail.getShort_details(), true, mTvMentorShortDescription.getMeasuredWidth(), null);
//
//        // Total amount of text to fill the TextView is approximately:
//
//
//        int totalText = textWidth * (linesPerScreen+1) *2;
//
//
//        int le = mentorDetail.getShort_details().length();
//
//
//        if(totalText < mentorDetail.getShort_details().length()){
//            mTvMentorShortDescription.setText(mentorDetail.getShort_details().substring(0,totalText));
//            mTvMentorFullDescription.setVisibility(View.VISIBLE);
//            mTvMentorFullDescription.setText(mentorDetail.getShort_details().substring(totalText,mentorDetail.getShort_details().length()));
//        }else{
//            mTvMentorFullDescription.setVisibility(View.GONE);
//            mTvMentorShortDescription.setText(mentorDetail.getShort_details());
//
//        }
//
//    }

//    private Canvas getCanvas(){
//        Canvas canvas = new Canvas();
//        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paint.setColor(Color.TRANSPARENT);
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//        paint.setAntiAlias(true);
//        canvas.drawPath(getPath(),paint);
//        return canvas;
//    }
//
//    private Path getPath() {
//        Path path = new Path();
//        path.moveTo(Utils.dpToPx(98), 0);
//        path.lineTo(Utils.dpToPx(98), Utils.dpToPx(98));
//        path.lineTo(0, Utils.dpToPx(98));
//        path.lineTo(0,this.getMeasuredHeight());
//        path.lineTo(this.getMeasuredWidth(),this.getMeasuredHeight());
//        path.lineTo(this.getMeasuredWidth(),0);
//        path.close();
//
//        return path;
//    }
//
//
//    public int getVisibleText(TextView textView, String info) {
//        // test that we have a textview and it has text
//
//        int linesPerScreen = textView.getLayoutParams().height/(textView.getLineHeight() + (int)textView.getLineSpacingExtra());
//
//
//        // Measure how much text will fit across the TextView
//
//        Paint paint = textView.getPaint();
//
//
//        textView.measure(0, 0);
//        int b =  textView.getMeasuredWidth();
//        float c = textView.getMaxWidth();
//        //must call measure!
//
//        int textWidth  = paint.breakText(info, true, textView.getMeasuredWidth(), null);
//
//        // Total amount of text to fill the TextView is approximately:
//
//
//        return textWidth * (linesPerScreen);
//
//
//    }


}
