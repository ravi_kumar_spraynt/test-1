package com.imentors.wealthdragons.courseDetailView;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.adapters.DetailItemListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.interfaces.ArticleClickListener;
import com.imentors.wealthdragons.interfaces.DigitalCoachingClickListener;
import com.imentors.wealthdragons.interfaces.EventClickListener;
import com.imentors.wealthdragons.interfaces.MentorClickListener;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.interfaces.VideoClickListener;
import com.imentors.wealthdragons.models.Article;
import com.imentors.wealthdragons.models.CourseDetail;
import com.imentors.wealthdragons.models.DashBoardItemOderingEvent;
import com.imentors.wealthdragons.models.DashBoardItemOderingMentors;
import com.imentors.wealthdragons.models.DashBoardItemOderingVideo;
import com.imentors.wealthdragons.models.DashBoardMentorDetails;
import com.imentors.wealthdragons.models.DashBoardVideoDetails;
import com.imentors.wealthdragons.models.DashBoardWishList;
import com.imentors.wealthdragons.models.DigitalCoaching;
import com.imentors.wealthdragons.models.Event;
import com.imentors.wealthdragons.models.Mentors;
import com.imentors.wealthdragons.models.SubCategory;
import com.imentors.wealthdragons.models.Video;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DetailItemListView extends LinearLayout implements DetailItemListAdapter.OnItemClickListener, PagingListener {

    private RecyclerView mRecyclerView, mSubCategoryRecyclerView;
    private WealthDragonTextView mRecylerViewHeading, mTxtViewAll, mSubCategoryName,mRecyclerViewHeadingOrange;
    private DetailItemListAdapter detailListAdapter;
    private ImageView mHeadingImage, mSubCategoryUpIcon;
    private LinearLayout mHeadingContainer, mSubCategoryContainer;
    private LinearLayoutManager mLinearLayoutManager;
    private List<SubCategory> mSubCategoryList = new ArrayList<>();
    private String mTasteId;
    private boolean isSubCategoryListVisible = false, mIsDownloaded;
    private boolean isSubCategoryFilter = false, isTablet ,mIsProgrammeType, mShowPayDialog;
    private Context mContext;
    private String mMentorId;
    private ArticleClickListener mArticleClickListeners;
    private MentorClickListener mMentorClickListener;
    private VideoClickListener mVideoClickListener;
    private EventClickListener mEventClickListener;
    private DigitalCoachingClickListener mDigitalCoachingListener;
    private ArrayList<DigitalCoaching> mDigitalCoachingArrayList = new ArrayList<>();
    private String mIsSubscribed , mIsAutoPlay, mMentorName, mMentorImageUrl;
    private int mTotalServerItem, mNextPage;
    private DetailItemListView.OnDigiVideoPlayListener mOnDigiVideoPlayListener = null;
    private DashBoardMentorDetails mDashBoardDetails;



    private static final int MAX_ITEM_COUNT = 1;


    public DetailItemListView(Context context) {
        super(context);
    }

    public DetailItemListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public DetailItemListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public DetailItemListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //  calling constructor
    public DetailItemListView(Context context, DashBoardItemOderingMentors mentorList, String mentorId, String title, int nextPage, int itemsCountOnServer , String viewType,OnDigiVideoPlayListener onDigiVideoPlayListener) {
        super(context);
        initViews(context);


        setMentorList(context, mentorList, title,nextPage,itemsCountOnServer,viewType);

    }

    //  set mentor list
    private void setMentorList(Context context, DashBoardItemOderingMentors mentorList, String title,int nextPage, int itemsCountOnServer,String viewType) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if (TextUtils.isEmpty(title)) {
            mRecylerViewHeading.setText(mentorList.getTitle());
        } else {
            mRecylerViewHeading.setText(title);

        }

        if (TextUtils.isEmpty(mentorList.getBanner())) {
            mHeadingImage.setVisibility(View.GONE);
        } else {
            mHeadingImage.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(mentorList.getBanner()).into(mHeadingImage);
        }

        if(itemsCountOnServer == 0) {

            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, false,false);
        }else{
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, true, false);

        }


        if (!TextUtils.isEmpty(title)) {
            detailListAdapter.setItems(null, mentorList.getExperts(), null, null,null, nextPage, itemsCountOnServer, null, null,mIsProgrammeType);

        }


        mRecyclerView.setAdapter(detailListAdapter);


    }


    public DetailItemListView(Context context, List<Mentors> mentorList, String mentorId, String title, int nextPage, int itemsCountOnServer , String viewType,OnDigiVideoPlayListener onDigiVideoPlayListener, boolean isDownloaded) {
        super(context);
        initViews(context);


        setMentorList(context, mentorList, title,nextPage,itemsCountOnServer,viewType,isDownloaded);

    }

    //  set mentor list
    private void setMentorList(Context context,  List<Mentors> mentorList, String title,int nextPage, int itemsCountOnServer,String viewType, boolean isDownloaded) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(title)) {

            mRecylerViewHeading.setText(title);

        }

            mHeadingImage.setVisibility(View.GONE);


        if(itemsCountOnServer == 0) {

            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, false,isDownloaded);
        }else{
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, true, isDownloaded);

        }


        if (!TextUtils.isEmpty(title)) {
            detailListAdapter.setItems(null, mentorList, null, null,null, nextPage, itemsCountOnServer, null, null,mIsProgrammeType);

        }


        mRecyclerView.setAdapter(detailListAdapter);


    }

    //  calling constructor
    public DetailItemListView(Context context, DashBoardItemOderingEvent eventList, String mentorId, String title, int nextPage, int itemsCountOnServer , String viewType) {
        super(context);

        initViews(context);
        if (!TextUtils.isEmpty(mentorId)) {
            mMentorId = mentorId;
        }

        setEventList(context, eventList, title,nextPage,itemsCountOnServer,viewType);
    }

    //  set event list
    private void setEventList(Context context, DashBoardItemOderingEvent eventList, String title,int nextPage, int itemsCountOnServer,String viewType) {


        mRecylerViewHeading.setVisibility(View.VISIBLE);

        mRecylerViewHeading.setText(title);

        mHeadingImage.setVisibility(View.GONE);

        if(itemsCountOnServer == 0) {
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType,false,false);
        }else{
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType,true,false);

        }



        if (!TextUtils.isEmpty(title)) {

            detailListAdapter.setItems(null, null, null, eventList.getSmallLeterEvents(),null, nextPage, itemsCountOnServer, null, null,mIsProgrammeType);
        }


        mRecyclerView.setAdapter(detailListAdapter);



    }

    //  calling constructor
    public DetailItemListView(Context context, List<Video> videoList,String mentorId, String title,int nextPage, int itemsCountOnServer , String viewType) {
        super(context);

        initViews(context);
        if (!TextUtils.isEmpty(mentorId)) {
            mMentorId = mentorId;
        }

        setVideoList(context, videoList, title,nextPage,itemsCountOnServer,viewType);
    }

    //  set video list
    private void setVideoList(Context context, List<Video> videoList, String title,int nextPage, int itemsCountOnServer,String viewType) {


        mRecylerViewHeading.setVisibility(View.VISIBLE);

        mRecylerViewHeading.setText(title);

        mHeadingImage.setVisibility(View.GONE);

        if(itemsCountOnServer == 0) {
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, false,false);
        }else{
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType, true,false);
        }



        if (!TextUtils.isEmpty(title)) {

            detailListAdapter.setItems(null, null, videoList, null,null,  nextPage, itemsCountOnServer, null, null,mIsProgrammeType);
        }


        mRecyclerView.setAdapter(detailListAdapter);



    }

    //  calling constructor
    public DetailItemListView(Context context, final List<DigitalCoaching> videoList, final String mentorId,
                              String title, String titleHeading, int nextPage, int itemsCountOnServer ,
                              String isSubscribed, String isAutoPlay, String showPayDialog,
                              String mentorName, String mentorImageUrl, OnDigiVideoPlayListener onDigiVideoPlayListener,
                              boolean isDownloaded, DashBoardMentorDetails dashBoardMentorDetails) {
        super(context);


        mTotalServerItem = itemsCountOnServer;
        mNextPage = nextPage;
        mMentorName = mentorName;
        mMentorImageUrl = mentorImageUrl;
        mOnDigiVideoPlayListener=onDigiVideoPlayListener;
        mIsDownloaded = isDownloaded;
        mDashBoardDetails = dashBoardMentorDetails;


        initViews(context);
        if (!TextUtils.isEmpty(mentorId)) {
            mMentorId = mentorId;
        }

        if(!TextUtils.isEmpty(showPayDialog) && TextUtils.equals(showPayDialog,"No")){
            mShowPayDialog = true;
        }

        mIsSubscribed = isSubscribed;
        mDigitalCoachingArrayList.addAll(videoList);
        for(DigitalCoaching digitalCoaching : videoList) {
                AppPreferences.setVideoResumePosition(digitalCoaching.getId(), Utils.getTimeInMilliSeconds(digitalCoaching.getVideo_seektime()));
            }

        mIsAutoPlay = isAutoPlay;

        setDigitalItemList(context, videoList, title,titleHeading,nextPage,itemsCountOnServer,mIsSubscribed,mIsDownloaded);
    }

    public interface OnDigiVideoPlayListener {
        void onDigiVideoPlayed();

    }

    //  set video list
    private void setDigitalItemList(Context context, List<DigitalCoaching> videoList, String title,String titleHeading,int nextPage, int itemsCountOnServer,String isSubscribed, boolean isDownloaded) {



        if (TextUtils.isEmpty(title) && TextUtils.isEmpty(titleHeading))
        {
            mRecylerViewHeading.setVisibility(View.GONE);
            mRecyclerViewHeadingOrange.setVisibility(GONE);
        }

        else
        {
            mRecylerViewHeading.setVisibility(View.VISIBLE);
            mRecylerViewHeading.setText(title);
            mRecyclerViewHeadingOrange.setVisibility(VISIBLE);
            mRecyclerViewHeadingOrange.setText(titleHeading);
            mRecyclerViewHeadingOrange.setTextColor(getResources().getColor(R.color.btnBGcolorOrange));
        }

        mHeadingImage.setVisibility(View.GONE);

        if(itemsCountOnServer == 0) {
            detailListAdapter = new DetailItemListAdapter(this, this, context, Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, false, isDownloaded);
        }else{
            if(isDownloaded){
                detailListAdapter = new DetailItemListAdapter(this, this, context,  Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, false, isDownloaded);

            }else{
                detailListAdapter = new DetailItemListAdapter(this, this, context,  Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, true, isDownloaded);

            }
        }



        if (!TextUtils.isEmpty(title)) {

            detailListAdapter.setItems(null, null, null, null,videoList, nextPage, itemsCountOnServer, null, null,mIsProgrammeType);
        }


        detailListAdapter.setDigitalCoachingSubscription(isSubscribed);


        mRecyclerView.setAdapter(detailListAdapter);



    }





    // mentor Id required for detail page list
    //  calling constructor
    public DetailItemListView(Context context, List<Article> articleList,String mentorId, String title,int nextPage, int itemsCountOnServer , String tasteId, String viewType ,boolean isProgramme, boolean isDownloaded) {
        super(context);
        if (!TextUtils.isEmpty(mentorId)) {
            mMentorId = mentorId;
        }

        mIsProgrammeType = isProgramme;
        mTasteId = tasteId;

        initViews(context);

        setArticleList(context, articleList, title,nextPage,itemsCountOnServer,tasteId, viewType, isDownloaded);
    }

    //  set article list
    private void setArticleList(Context context, List<Article> articleList, String title,int nextPage, int itemsCountOnServer , String tasteId, String viewType, boolean isDownloaded) {

        mRecylerViewHeading.setVisibility(View.VISIBLE);

        mRecylerViewHeading.setText(title);

        mHeadingImage.setVisibility(View.GONE);

        if(itemsCountOnServer == 0) {
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType,false,isDownloaded);
        }else{
            detailListAdapter = new DetailItemListAdapter(this, this, context, viewType,true,isDownloaded);

        }

        switch (viewType) {
            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_RELATED_COURSE:
                detailListAdapter.setItems(articleList, null, null, null,null,  nextPage, itemsCountOnServer, tasteId, null,mIsProgrammeType);
                break;
            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_PROGRAMME_COURSE:
                detailListAdapter.setItems(articleList, null, null, null,null,  nextPage, itemsCountOnServer, tasteId, null,mIsProgrammeType);
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE:
                detailListAdapter.setItems(articleList, null, null, null,null,  nextPage, itemsCountOnServer, tasteId, null,mIsProgrammeType);
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME:
                detailListAdapter.setItems(articleList, null, null, null,null,  nextPage, itemsCountOnServer, tasteId, null,mIsProgrammeType);
                break;
            default:
                // for wishlilst
                detailListAdapter.setItems(articleList, null, null, null,null, nextPage, itemsCountOnServer, tasteId, null,mIsProgrammeType);
                break;
        }


        mRecyclerView.setAdapter(detailListAdapter);

    }

    //  initialize detail item list view
    private void initViews(final Context context) {

        mContext = context;


        if (context instanceof ArticleClickListener) {
            mArticleClickListeners = (ArticleClickListener) context;
        }


        if(context instanceof MentorClickListener){
            mMentorClickListener = (MentorClickListener) context;
        }

        if(context instanceof VideoClickListener){
            mVideoClickListener = (VideoClickListener) context;
        }

        if(context instanceof EventClickListener){
            mEventClickListener = (EventClickListener) context;
        }
        if(context instanceof DigitalCoachingClickListener){
            mDigitalCoachingListener = (DigitalCoachingClickListener) context;
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dashboard_detail_item_view, this);

        mLinearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mLinearLayoutManager.setInitialPrefetchItemCount(8);
        mLinearLayoutManager.setItemPrefetchEnabled(true);
        mRecyclerView = view.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mRecylerViewHeading = view.findViewById(R.id.txt_header_name);
        mRecyclerViewHeadingOrange=view.findViewById(R.id.txt_header_name_orange);
        mHeadingImage = view.findViewById(R.id.image_view_header);
        mHeadingContainer = view.findViewById(R.id.ll_header_view);
        mTxtViewAll = view.findViewById(R.id.txt_view_all);

        mSubCategoryContainer = view.findViewById(R.id.subcategory_container);
        mSubCategoryName = view.findViewById(R.id.tv_subcategory);
        mSubCategoryRecyclerView = view.findViewById(R.id.recycler_view_subcategory);
        mSubCategoryUpIcon = view.findViewById(R.id.iv_subcategory_up_icon);
        isTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);


        if (mSubCategoryList.size() > 0) {

            mSubCategoryContainer.setVisibility(View.VISIBLE);
            mSubCategoryName.setText(mSubCategoryList.get(0).getTitle());
        }


        mSubCategoryContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSubCategoryListVisible) {
                    hideSubCategory();
                } else {
                    mSubCategoryRecyclerView.setVisibility(View.VISIBLE);
                    mSubCategoryUpIcon.setVisibility(View.VISIBLE);

                    if (isTablet) {
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
                    } else {
                        mSubCategoryRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
                    }
                    mSubCategoryRecyclerView.requestLayout();
                    isSubCategoryListVisible = !isSubCategoryListVisible;

                }

            }
        });

        mTxtViewAll.setVisibility(View.GONE);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);


        // set dimensions
//        mRecylerViewHeading.setPadding(0,0,0,(int)mContext.getResources().getDimension(R.dimen.padding_default));


    }

    //  clicked article items
    @Override
    public void onArticleClick(Article item, int position) {

        if(item.isDownloaded()){
            ((MainActivity)mContext).addFragment(DashBoardDetailFragment.newInstance(item.getId(), Constants.TYPE_COURSE,
                    null,true,item.getSavedResponse()));

        }else {
            if (item.getLayout().equals(Constants.TYPE_PROGRAMMES)) {
                mArticleClickListeners.onArticleClick(Constants.TYPE_PROGRAMMES, item.getId());

            } else {
                mArticleClickListeners.onArticleClick(Constants.TYPE_COURSE, item.getId());

            }
        }
    }

    //  clicked mentor items
    @Override
    public void onMentorClick(Mentors item, int position) {
        if(item.isDownloaded()){
            ((MainActivity)mContext).addFragment(DashBoardDetailFragment.newInstance(item.getId(), Constants.TYPE_MENTOR,
                    null,true,item.getSavedResponse()));

        }else{
            mMentorClickListener.onMentorClick(Constants.TYPE_MENTOR,item.getId());

        }
    }


    //  clicked video items
    @Override
    public void onVideoClick(Video item, int position) {
        if(!TextUtils.isEmpty(item.getEpisode())) {
            if (item.isEpisode()) {
                mVideoClickListener.onVideoClick(Constants.TYPE_EPISODE, item.getVideo_id(), item.getId());
            } else {
                mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO, item.getId(), null);
            }
        }else{
            mVideoClickListener.onVideoClick(Constants.TYPE_VIDEO, item.getId(), null);
        }

    }

    //  clicked event click
    @Override
    public void onEventClick(Event item, int position) {
        mEventClickListener.onEventClick(Constants.TYPE_EVENT , item.getId());
    }
    //  clicked event click
    @Override
    public void onDigitalCoachingClick(DigitalCoaching item, int position) {
        mOnDigiVideoPlayListener.onDigiVideoPlayed();
        Utils.callEventLogApi("clicked " + "<b>" + item.getTitle() + "</b> digital coaching video from mentor detail");
        mDigitalCoachingListener.onDigitalCoachingClick(mDigitalCoachingArrayList,mTotalServerItem,mNextPage,Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING , item.getMentor_id(),
                mIsSubscribed, mIsAutoPlay,item.getFree_paid(),mShowPayDialog,mMentorName,
                mMentorImageUrl,item.getId(),item.getDescription(),item.getBanner(),mMentorImageUrl,mIsDownloaded,mDashBoardDetails);
    }


    @Override
    public void setHeader(String title, String imageHeader, int listSize) {

    }


    public void callSeeAllPaginationApi(final int pageNumber, final String tasteId, String api, final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    addDataInAdapter(response, viewType);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if (!Utils.setErrorDialog(response, getContext())) {
                            detailListAdapter.setServerError();
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        detailListAdapter.setServerError();
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error instanceof NoConnectionError) {

                } else {
                    detailListAdapter.setServerError();
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);


                if (!TextUtils.isEmpty(tasteId)) {
                    params.put(Keys.CURRENT_COURSE_ID, tasteId);
                    params.put(Keys.MENTOR_ID, mMentorId);
                } else if(!TextUtils.isEmpty(mMentorId)){

                    params.put(Keys.MENTOR_ID, mMentorId);
                }


                if (pageNumber != 0) {
                    params.put(Keys.PAGE_NO, String.valueOf(pageNumber));
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String viewType, String seeAllType) {

        switch (viewType) {
            case Constants.TYPE_COURSE:
                if (!TextUtils.isEmpty(seeAllType)) {
                    switch (seeAllType) {
                        // for different types of courses
                        case Constants.ALL_PROGRAMMES_KEY:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.PROGRAMME_PAGENTATION, viewType);
                            break;
                        case Constants.ALL_UPCOMING_COURSE:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.UPCOMING_COURSE_PAGENTATION, viewType);
                            break;
                        case Constants.ON_SALE_KEY:
                            // missing api
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.ON_SALE_PAGENTATION, viewType);
                            break;
                        default:
                            callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_PAGENATION, viewType);
                            break;
                    }
                } else {
                    callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_PAGENATION, viewType);
                }

                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_COURSE_PAGINATION, viewType);
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_PROGRAMME_PAGINATION, viewType);
                break;
            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_VIDEO_PAGINATION, viewType);
                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_EVENT:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_EVENT_PAGINATION, viewType);
                break;
            case Constants.TYPE_VIDEO:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.VIDEO_PAGENTATION, viewType);
                break;

            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_RELATED_COURSE:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_DETAIL_RELATED_COURSES_PAGINATION, viewType);
                break;

            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_PROGRAMME_COURSE:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.COURSE_DETAIL_RELATED_PROGRAMME_PAGINATION, viewType);
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_COURSES:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.WISHLIST_COURSE_PAGINATION, viewType);
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_PROGRAMMES:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.WISHLIST_PROGRAMME_PAGINATION, viewType);
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_EVENTS:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.WISHLIST_EVENT_PAGENATION, viewType);
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_VIDEOS:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.WISHLIST_VIDEO_PAGENATION, viewType);
                break;


            case Constants.TYPE_WISHLLIST_FEATURED_MENTORS:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.WISHLIST_MENTOR_PAGENATION, viewType);
                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MENTOR_DETAIL_DIGITAL_PAGINATION, viewType);
                break;


            case Constants.TYPE_PURCHASE_FEATURED_COURSES:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MY_PURCHASE_PAGINATION_API, viewType);
                break;


            case Constants.TYPE_PURCHASE_FEATURED_PROGRAMMES:
                callSeeAllPaginationApi(pageNumber, tasteId, Api.MY_PURCHASE_PROGRAMME_PAGINATION_API, viewType);
                break;


            default:
                break;
        }
    }


    private void addDataInAdapter(String response, String viewType)  {

        CourseDetail coursesDetails;

        CourseDetail.RelatedCourses relatedCourses;



        switch (viewType) {


            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_RELATED_COURSE:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get related courses from course detail
                relatedCourses = coursesDetails.getRelated_Courses();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;
            case Constants.TYPE_COURSE_DASHBOARD_DETAIL_PROGRAMME_COURSE:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get related courses from course detail
                relatedCourses = coursesDetails.getRelated_Courses();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;
                case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_COURSE:
                    //  parse json to java
                    coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                    //  get courses from course detail
                    relatedCourses = coursesDetails.getCourses();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_PROGRAMME:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get courses from course detail
                relatedCourses = coursesDetails.getCourses();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_VIDEO:
                //  parse json to java
                DashBoardVideoDetails videoDetails = new Gson().fromJson(response, DashBoardVideoDetails.class);
                //  get videos from video detail
                DashBoardVideoDetails.RelatedVideos relatedVideos = videoDetails.getVideos();

                if (relatedVideos.getVideos().size() > 0) {

                    detailListAdapter.addItems(null, null, relatedVideos.getVideos(), null,null, relatedVideos.getNext_page(), relatedVideos.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_EVENT:
                //  parse json to java
                try {
                JSONObject jsonObject = new JSONObject(response);


                if(!TextUtils.isEmpty(jsonObject.optString("DigitalCoaching"))) {

                        JSONObject  dashBoardItemOderingDigital = jsonObject.getJSONObject("DigitalCoaching");



                }
                }  catch (JSONException e) {

            e.printStackTrace();

        }

                break;

            case Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING:
                //  parse json to java

                DashBoardMentorDetails mentorDetail = new Gson().fromJson(response, DashBoardMentorDetails.class);

                if (mentorDetail.getDigitalCoaching().getCoachings().size() > 0) {

                    for(DigitalCoaching digitalCoaching:mentorDetail.getDigitalCoaching().getCoachings()){
                        mDigitalCoachingArrayList.add(digitalCoaching);
                    }

                    detailListAdapter.addItems(null, null, null, null,mentorDetail.getDigitalCoaching().getCoachings(), mentorDetail.getDigitalCoaching().getNext_page(), mentorDetail.getDigitalCoaching().getTotal_items(), mTasteId);

                }
                break;
            case Constants.TYPE_WISHLLIST_FEATURED_COURSES:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get featured course from course detail
                relatedCourses = coursesDetails.getFeaturedCourse();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_PROGRAMMES:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get featured program from course details
                DashBoardWishList.RelatedProgramme relatedProgrammes = coursesDetails.getFeaturedProgramme();

                if (relatedProgrammes.getProgrammes().size() > 0) {

                    detailListAdapter.addItems(relatedProgrammes.getProgrammes(), null, null, null,null, relatedProgrammes.getNext_page(), relatedProgrammes.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_EVENTS:

                coursesDetails = new Gson().fromJson(response, CourseDetail.class);

                //  parse json to java
                DashBoardItemOderingEvent dashBoardItemOderingEvents =  new Gson().fromJson(response, DashBoardItemOderingEvent.class);

                if (dashBoardItemOderingEvents.getEvents().size() > 0) {

                    detailListAdapter.addItems(null, null, null, dashBoardItemOderingEvents.getEvents(),null, dashBoardItemOderingEvents.getNext_page(), dashBoardItemOderingEvents.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_VIDEOS:


                //  parse json to java


                DashBoardItemOderingVideo dashBoardItemOderingVideo =   (new Gson().fromJson(response, CourseDetail.class).getWishlistVideos());


                if (dashBoardItemOderingVideo.getSmallLetterVideos().size() > 0) {

                    detailListAdapter.addItems(null, null, dashBoardItemOderingVideo.getSmallLetterVideos(), null,null, dashBoardItemOderingVideo.getNext_page(), dashBoardItemOderingVideo.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_WISHLLIST_FEATURED_MENTORS:


                //  parse json to java
                DashBoardItemOderingMentors dashBoardItemOderingMentors =  (new Gson().fromJson(response, CourseDetail.class).getExpert());

                if (dashBoardItemOderingMentors.getExperts().size() > 0) {

                    detailListAdapter.addItems(null, dashBoardItemOderingMentors.getExperts(), null, null,null, dashBoardItemOderingMentors.getNext_page(), dashBoardItemOderingMentors.getTotal_items(), mTasteId);

                }
                break;


            case Constants.TYPE_PURCHASE_FEATURED_COURSES:
                //  parse json to java
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get featured course from course detail
                relatedCourses = coursesDetails.getFeaturedCourse();

                if (relatedCourses.getCourses().size() > 0) {

                    detailListAdapter.addItems(relatedCourses.getCourses(), null, null, null,null, relatedCourses.getNext_page(), relatedCourses.getTotal_items(), mTasteId);

                }
                break;

            case Constants.TYPE_PURCHASE_FEATURED_PROGRAMMES:
                //  parse json to java
                coursesDetails = new Gson().fromJson(response, CourseDetail.class);
                //  get featured program from course details
                relatedProgrammes = coursesDetails.getFeaturedProgramme();

                if (relatedProgrammes.getProgrammes().size() > 0) {

                    detailListAdapter.addItems(relatedProgrammes.getProgrammes(), null, null, null,null, relatedProgrammes.getNext_page(), relatedProgrammes.getTotal_items(), mTasteId);

                }
                break;

            default:
                break;
        }
    }


    //  hide sub category
    private void hideSubCategory() {
        mSubCategoryRecyclerView.setVisibility(View.GONE);
        mSubCategoryUpIcon.setVisibility(View.GONE);
        isSubCategoryListVisible = !isSubCategoryListVisible;
    }

    //  removed title
    private void removeTitle() {
        mRecylerViewHeading.setVisibility(View.GONE);
        mHeadingContainer.setVisibility(View.GONE);
        mHeadingImage.setVisibility(View.GONE);
        mTxtViewAll.setVisibility(View.GONE);

    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mArticleClickListeners = null;
        mMentorClickListener = null;
        mVideoClickListener = null;
        mEventClickListener = null;

    }
}
