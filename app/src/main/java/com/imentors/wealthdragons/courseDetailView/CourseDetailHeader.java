package com.imentors.wealthdragons.courseDetailView;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Handler;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.BaseActivity;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.EnrollmentDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.VideoData;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.facebook.FacebookSdk.getApplicationContext;

public class CourseDetailHeader extends LinearLayout {


    private Context mContext;
    private WealthDragonTextView mTvCourseTitle, mTvCoursePunchLine, mTvCoursePreferance, mVideoError, mTvEnrollCount,mTvDigitalCoachingText;
    private ImageView mIvVideoPlay;
    private ImageView mIvCourseImage;
    private PlayerView mExoPlayerView;
    private FrameLayout mVideoFrameLayout;
    private FrameLayout mFullScreenButton, mDialogFullScreenButton;
    private ImageView mFullScreenIcon, mPlayIcon,mSmallScreenIcon;
    private SimpleExoPlayer mPlayer;
    private DashBoardCourseDetails mDashBoardDetails;
    private List<String> mVideoUrls = new ArrayList<>();
    private List<Chapters.EClasses> eClassesArrayList = new ArrayList<>();
    private String mVideoNameForListenerLog;

    private int mResumeWindow , mItemPosition;
    private long mResumePosition = 0;
    private String mVideoUrl;
    private boolean mIsTablet;
    private Dialog mFullScreenDialog;
    private boolean mExoPlayerFullscreen = false, mIsVideoError;
    private Activity mActivity;
    private boolean isLandScape = false, isPotrait = false, isEclassesEnable = false, isAudioPlayerStarted = false;
    private String mType;
    private ImageButton mPlayImageButton;

    private Utils.DialogInteraction mDialogListener;
    private PlayerView mDialogplayerView;
    private Timer mTimerNextVideo = new Timer();
    final Handler mNextVideoHandler = new Handler();
    private String imageUrl = null;
    private LinearLayout nextVideoFrame;
    private TextView mNextVideo;
    private TextView mTvEClassName, mTvEClassNameSmallVideo;
    private RelativeLayout mLlHeadingContainer;
    private boolean mShowNextVideoWithImageFrame = false;
    private String mEClassesTitleDialog;
    String thumbUrl = null;
    private boolean mShowController;
    private boolean mShowProgressBarDefinite;
    private int mProgressBarPosition;
    private ProgressBar mDefiniteProgressBar;
    private Thread mProgreeBarThread;
    private Handler mProgressBarHandler = new Handler();
    private View mVideoErrorDialog;
    private ProgressBar mProgressBar;
    private boolean isBufferering = true;
    private String mFirstVideoFreePaid = null;
    private boolean isFirstFreePaidVideo = false;
    private boolean mIsIntroVideoVisible = false;
    private ImageView mArticleWishlist;
    private boolean mIsItemAdded = false;
    private boolean isNextTapped = false;
    private VideoData mVideoDataToBeSaveInCourse = null;
    private CourseDetailBelowVideo.OnItemClickListener mOnItemClickListener = null;
    private boolean mIsDownloaded;


    public CourseDetailHeader(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseDetailHeader(Context context, DashBoardCourseDetails dashBoardDetails, Activity activity, OnDashBoardDetailReload dashBoardDetailReload, CourseDetailBelowVideo.OnItemClickListener onItemClickListener, boolean isDownloaded) {
        super(context);
        mActivity = activity;
        this.mOnItemClickListener = onItemClickListener;
        mIsDownloaded = isDownloaded;
        initViews(context);
        bindView(dashBoardDetails);

    }

    /**
     * different click handled by onItemClick
     */
    //  interface
    public interface OnDashBoardDetailReload {
        void onItemClicked();
    }


    //  set data in view
    private void bindView(DashBoardCourseDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;


        if (mIsTablet) {
            imageUrl =dashBoardDetails.getCourseDetail().getBig_banner();
           Glide.with(mContext).load(dashBoardDetails.getCourseDetail().getBig_banner()).thumbnail(Glide.with(mContext).load(dashBoardDetails.getCourseDetail().getLow_qty_big_banner()).dontAnimate()).dontAnimate().error(R.drawable.no_img_tab).into(mIvCourseImage);

        } else {
            imageUrl =dashBoardDetails.getCourseDetail().getBanner();

            Glide.with(mContext).load(dashBoardDetails.getCourseDetail().getBanner()).dontAnimate().error(R.drawable.no_img_mob).into(mIvCourseImage);

        }



        if (mDashBoardDetails.getCourseDetail().getChapters() != null) {
            if (mDashBoardDetails.getCourseDetail().getChapters().size() > 0) {

                for (Chapters chapters : mDashBoardDetails.getCourseDetail().getChapters()) {
                    for (Chapters.EClasses eClasses : chapters.getE_classes()) {
                        isEclassesEnable = eClasses.isAction();
                        break;
                    }
                }
            }
        }


        if(!mDashBoardDetails.isEclassVisible() && mDashBoardDetails.isIs_intro_video() && !TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getVideo()) && !TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getPlay_url()) && !dashBoardDetails.getCourseDetail().getPlay_url().equals("NoVideoExist")){
            mIsIntroVideoVisible = true;

        }


        if (mIsIntroVideoVisible) {
            // add only intro video in list
            mVideoUrl = dashBoardDetails.getCourseDetail().getPlay_url();
            mVideoUrls.add(mVideoUrl);

            mVideoNameForListenerLog = mDashBoardDetails.getCourseDetail().getTitle();
        }


        // check for subscribed condition
        if (isEclassesEnable) {
            if (mDashBoardDetails.getCourseDetail().getChapters() != null) {
                if (mDashBoardDetails.getCourseDetail().getChapters().size() > 0) {

                    for (Chapters chapters : mDashBoardDetails.getCourseDetail().getChapters()) {
                        for (Chapters.EClasses eClasses : chapters.getE_classes()) {
                            if (eClasses.getType().equals(Constants.TYPE_DETAIL_VIDEO) && !TextUtils.isEmpty(eClasses.getPlay_url()) && !eClasses.getPlay_url().equals("NoVideoExist") ) {
                                if(!isFirstFreePaidVideo) {
                                    mFirstVideoFreePaid = eClasses.getFree_paid();
                                    isFirstFreePaidVideo = true;
                                    if(eClasses.getFree_paid().equals("Free")){
                                        mVideoUrl = eClasses.getPlay_url();
                                    }
                                }
                                mVideoUrls.add(eClasses.getPlay_url());
                                eClassesArrayList.add(eClasses);
                            }
                        }
                    }
                }
            }
        }


        if (mDashBoardDetails.isProgrammes()) {
            if (mDashBoardDetails.isEclassVisible()) {
                mType = Constants.CHAPTER;
            } else {
                mType = Constants.TYPE_PROGRAMMES;
            }
        } else {
            if (mDashBoardDetails.isEclassVisible()) {
                mType = Constants.CHAPTER;
            } else {
                mType = Constants.TYPE_COURSE;
            }
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getTitle())) {
            mTvCourseTitle.setVisibility(View.VISIBLE);
            mTvCourseTitle.setText(dashBoardDetails.getCourseDetail().getTitle());
        } else {
            mTvCourseTitle.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getPunch_line())) {
            mTvCoursePunchLine.setVisibility(View.VISIBLE);
            mTvCoursePunchLine.setText(dashBoardDetails.getCourseDetail().getPunch_line());
        } else {
            mTvCoursePunchLine.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getTaste_preference())) {
            mTvCoursePreferance.setVisibility(View.VISIBLE);
            mTvCoursePreferance.setText(dashBoardDetails.getCourseDetail().getCategory_label() + ": " + dashBoardDetails.getCourseDetail().getTaste_preference());
        } else {
            mTvCoursePreferance.setVisibility(View.GONE);
        }





        showPlayIcon();


        initFullscreenButton();

    }

    private void showPlayIcon() {
        if (mDashBoardDetails.isEclassVisible()) {
            // handle non-paid courses
            if (mVideoUrls.size() > 0) {
                mIvVideoPlay.setVisibility(View.VISIBLE);

            } else {
                mIvVideoPlay.setVisibility(View.INVISIBLE);
                mVideoError.setVisibility(INVISIBLE);

            }
        } else {
            // handle paid courses

            if (mVideoUrls.size() > 0 && mIsIntroVideoVisible) {
                mIvVideoPlay.setVisibility(View.VISIBLE);


            } else {
                mIvVideoPlay.setVisibility(View.INVISIBLE);
                mVideoError.setVisibility(INVISIBLE);

            }

            mIvCourseImage.setVisibility(VISIBLE);

        }


    }

    //  initialize course detail header view
    private void initViews(Context context) {
        mIsTablet = WealthDragonsOnlineApplication.sharedInstance().getResources().getBoolean(R.bool.is_tablet);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (context instanceof Utils.DialogInteraction) {
            mDialogListener = (Utils.DialogInteraction) context;
        }

        View view = inflater.inflate(R.layout.course_detail_header, this);
        mContext = context;

        // heading
        mTvCourseTitle = view.findViewById(R.id.tv_course_detail_title);
        mTvCoursePunchLine = view.findViewById(R.id.tv_course_detail_punch_line);
        mTvCoursePreferance = view.findViewById(R.id.txt_course_detail_taste_preferance);
        mVideoError = view.findViewById(R.id.video_error);
        // video Frame
        mVideoFrameLayout = view.findViewById(R.id.frame_video);
        mVideoFrameLayout.getLayoutParams().height = (int) Utils.getDetailScreenImageHeight((Activity) mContext);
        mProgressBar = view.findViewById(R.id.progress_bar);
        mArticleWishlist = view.findViewById(R.id.iv_add_wishlist);

        mIvVideoPlay = view.findViewById(R.id.iv_video_play);

        mExoPlayerView = findViewById(R.id.exoplayer);
        mTvEClassNameSmallVideo = mExoPlayerView.findViewById(R.id.tv_eclasses_title);
        mTvDigitalCoachingText=view.findViewById(R.id.tv_digital_coching_view);



        mIvCourseImage = view.findViewById(R.id.iv_video_image);

        mTvEnrollCount = view.findViewById(R.id.tv_enrollcount);

        mTvDigitalCoachingText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mIsDownloaded) {

                    if(Utils.isNetworkAvailable(mContext)){
                        onDigitalCoachingClick();

                    }else{
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    onDigitalCoachingClick();
                }

            }
        });


        mIvVideoPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Utils.isNetworkAvailable(mContext)) {
                    if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

                        Utils.callEventLogApi("showed <b>" + mContext.getString(R.string.check_wifi_status) + "</b> ");
                        Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

                    } else {
                        onPlayIconClick();

                    }
                } else {

                    if(mIsDownloaded){
                        onPlayIconClick();

                    }else{
                        if(isLandScape){
                            closeFullscreenDialog();
                        }
                        Utils.showNetworkError(mContext, new NoConnectionError());
                    }

                }
            }
        });


        LocalBroadcastManager.getInstance(context).registerReceiver(mSaveVideoPosition, new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));

         }

    private void onDigitalCoachingClick() {
        boolean mShowPayDialog = false;

        if (!TextUtils.isEmpty(mDashBoardDetails.getCourseDetail().getDigi_coaching_screen()) && TextUtils.equals(mDashBoardDetails.getCourseDetail().getDigi_coaching_screen(), "No")) {
            mShowPayDialog = true;
        }

        if (mShowPayDialog) {
            if (AppPreferences.getCreditCardList() != null && AppPreferences.getCreditCardList().size() > 0) {
                openDialog(PaymentCardListDialog.newInstance(mDashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_EXPERT, null));
            } else {
                openDialog(PayDialogFragment.newInstance("Ruppee", false, false, mDashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_EXPERT, null));
            }
        } else {
            if (mContext != null) {
                ((MainActivity) mContext).addFragment(DashBoardDetailFragment.newInstance(mDashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, null, false, null));
            }
        }


        Utils.callEventLogApi("clicked <b>" + mDashBoardDetails.getCourseDetail().getDc_button_title() + " </b>" + "from Course Detail");

        pauseVideo();
    }

    private void onPlayIconClick() {
        // if course is paid but intro video visible
        if (!mDashBoardDetails.isEclassVisible() &&  mIsIntroVideoVisible) {

            Utils.callEventLogApi("clicked <b> play button </b> of  " + "<b>" + mDashBoardDetails.getCourseDetail().getTitle() + "</b>");

            isAudioPlayerStarted = false;

            // true means we have to play videos only on wifi connection or else play video


            // if there is video error because video error listener is called as page is loaded and may be play button pressed afterwards


            if (!TextUtils.isEmpty(mVideoUrl)) {


                mItemPosition = -1;

                if(mIsDownloaded && !TextUtils.isEmpty(mDashBoardDetails.getCourseDetail().getDownLoadedVideoAddress())){
                    mVideoUrl = mDashBoardDetails.getCourseDetail().getDownLoadedVideoAddress();
                    initExoPlayer(true);
                }else{
                    callVideoUrl(mDashBoardDetails.getCourseDetail().getCourse_intro_id(),true,mVideoUrls.get(0));

                }


                    // hide everything in case of error
                showExoplayer();

            }






        }

        // if course is free
        else if (!mDashBoardDetails.getCourseDetail().getFree_paid().equals("Enrollment") && mFirstVideoFreePaid.equals("Free")) {

            Utils.callEventLogApi("clicked <b> play button </b> of  " + "<b>" + mDashBoardDetails.getCourseDetail().getTitle() + "</b>");

            isAudioPlayerStarted = false;


                // if there is video error because video error listener is called as page is loaded and may be play button pressed afterwards


            if (!TextUtils.isEmpty(mVideoUrl)) {


                // if intro video is available condition
                String videoId = null;

                Chapters.EClasses eclass = eClassesArrayList.get(0);
                videoId = eclass.getId();

                // mark eClasses selected in video
                markEClassSelected(videoId);

                mItemPosition = 0;



                if(mIsDownloaded && !TextUtils.isEmpty(eclass.getDownloadedVideoAddress())){
                    mVideoUrl = eclass.getDownloadedVideoAddress();
                    initExoPlayer(false);
                }else{
                    callVideoUrl(eclass.getId(),false,eclass.getPlay_url());

                }

                        // hide everything in case of error
                showExoplayer();


            }

        } else {


                openEnrollmentDialog(EnrollmentDialogFragment.newInstance(mDashBoardDetails.getCourseDetail().getChapters().get(0).getE_classes().get(0).getId(), mDashBoardDetails.getCourseDetail().getId(), mDashBoardDetails.getCourseDetail().getTitle(),mDashBoardDetails.isProgrammes()));


        }
    }

    private void showExoplayer() {

            mIsVideoError = false;
            if (mExoPlayerView!=null) {
                mExoPlayerView.setVisibility(VISIBLE);


            }
            mIvCourseImage.setVisibility(INVISIBLE);
            mIvVideoPlay.setVisibility(INVISIBLE);
            mVideoError.setVisibility(INVISIBLE);
            mProgressBar.setVisibility(INVISIBLE);

    }


    private void bufferingHandler(){
        if (mExoPlayerView != null && !isAudioPlayerStarted) {

            // top make visible only when exoplayer view is invisible
            if (mExoPlayerView.getVisibility() == View.INVISIBLE) {
                mProgressBar.setVisibility(VISIBLE);
                mIvCourseImage.setVisibility(VISIBLE);
            }
        }else{
            closePlayer();
        }
    }


    private void bufferingStopHandler(){
        if (Utils.isNetworkAvailable(mContext)) {

            if (mExoPlayerView != null && !isAudioPlayerStarted) {

                if (mIsVideoError) {
                    // in some cases video error is called before to jsut handle those issue
                    mPlayImageButton.setActivated(false);
                    mIvCourseImage.setVisibility(View.INVISIBLE);
                    mIvVideoPlay.setVisibility(View.INVISIBLE);
                    mVideoError.setVisibility(VISIBLE);
                    mExoPlayerView.setVisibility(View.INVISIBLE);
                    mProgressBar.setVisibility(INVISIBLE);
                } else {

                    // using is buffering for loader visbility
                    isBufferering = false;
                    if (mExoPlayerView.getVisibility() == View.INVISIBLE) {

                        mProgressBar.setVisibility(INVISIBLE);
                        mExoPlayerView.setVisibility(VISIBLE);
                        mExoPlayerView.setControllerAutoShow(true);
                        mExoPlayerView.setUseController(true);
                        mIvCourseImage.setVisibility(INVISIBLE);
                    }
                }
            }
        } else {
            if(mIsDownloaded){

            }else{
                if(isLandScape){
                    closeFullscreenDialog();
                }
                Utils.showNetworkError(mContext, new NoConnectionError());
            }

        }
    }


    private void markEClassSelected(String videoId) {
        // send video id in broadcast
        Intent intent = new Intent(Constants.BROADCAST_MARK_RUNNING_TRACK);
        intent.putExtra(Constants.ECLASSES_VIDEO_ID, videoId);

        // send boolean is played in broadcast
        intent.putExtra(Constants.IS_VIDEO_PLAYED, true);

        LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcast(intent);
    }





    private BroadcastReceiver mSaveVideoPosition = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isDetailOnTop = intent.getBooleanExtra(Constants.IS_TOP_FRAGMENT, false);
            cancelNextVideoTimer();

            if (mExoPlayerView != null && mPlayer != null) {
                mResumeWindow = mPlayer.getCurrentWindowIndex();
                AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
                mResumePosition = Math.max(0, mPlayer.getContentPosition());
                AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            }

            if (!isDetailOnTop) {
                Utils.releaseDetailScreenPlayer();
                Utils.releaseFullScreenPlayer();
                if (mExoPlayerView != null) {
                    mExoPlayerView.getVideoSurfaceView().setVisibility(GONE);
                }
                mExoPlayerView = null;
                mPlayer = null;
            }


        }
    };





    private void errorHandler(){
        mIsVideoError = true;
        cancelNextVideoTimer();

        if (mPlayer != null) {

            if (mPlayer.getPlayWhenReady()) {
                try {
                    if (isLandScape) {
                        closeFullscreenDialog();
                    }

                    mPlayImageButton.setActivated(false);
                    mIvCourseImage.setVisibility(View.INVISIBLE);
                    mIvVideoPlay.setVisibility(View.INVISIBLE);
                    mVideoError.setVisibility(VISIBLE);
                    mExoPlayerView.setVisibility(View.INVISIBLE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }







    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelNextVideoTimer();
        if (mExoPlayerView != null && mPlayer != null && mExoPlayerView.getPlayer() != null) {
            mResumeWindow = mPlayer.getCurrentWindowIndex();
            AppPreferences.setVideoResumeWindow(mVideoUrl, mResumeWindow);
            mResumePosition = Math.max(0, mPlayer.getContentPosition());
            AppPreferences.setVideoResumePosition(mVideoUrl, mResumePosition);
            mPlayer.release();
            mPlayer.clearVideoSurface();
            mPlayer = null;

            // releasing full screen player too
            Utils.releaseFullScreenPlayer();
            Utils.releaseDetailScreenPlayer();
        }

        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mSaveVideoPosition);
    }


    private void initFullscreenButton() {
        initFullscreenDialog();
        mPlayImageButton = mExoPlayerView.findViewById(R.id.exo_play);
        mSmallScreenIcon = mExoPlayerView.findViewById(R.id.exo_fullscreen_icon);
        mSmallScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        mFullScreenButton = mExoPlayerView.findViewById(R.id.exo_fullscreen_button);
        mFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFullScreenVideo(Surface.ROTATION_0);
            }

        });


    }


    //  opened full screen video
    public void openFullScreenVideo(int rotationAngle) {
        if (!mExoPlayerFullscreen) {
            if (!isLandScape) {
                openFullscreenDialog(rotationAngle);
            }
        } else {
            if (!isPotrait) {
                closeFullscreenDialog();
                cancelNextVideoTimer();
            }
        }

    }

    public boolean getPlayBackState() {

        return mPlayer.getPlayWhenReady();
    }


    // for internal use
    private void initExoPlayer(final boolean isIntroVideo) {

        // get position from preferences

        Utils.releaseDetailScreenPlayer();
        mPlayer = null;
        ArrayList<String> mVideoUrlList = new ArrayList<>();
        mVideoUrlList.add(mVideoUrl);

        Utils.initilizeDetailScreenConcatinatedPlayer(mContext, mVideoUrlList, eClassesArrayList, mDashBoardDetails.isAutoPlay(), mType, mVideoNameForListenerLog,mIsIntroVideoVisible);


        mPlayer = Utils.getDetailScreenConcatinatedPlayer();
        mPlayer.clearVideoSurface();

        if(isLandScape){
            mDialogplayerView.setPlayer(mPlayer);

        }else{
            if (mExoPlayerView!=null) {
                mExoPlayerView.setPlayer(mPlayer);
            }

        }

        if (mDashBoardDetails.getCourseDetail().getDc_button_show().equals("Yes"))
        {
            mTvDigitalCoachingText.setText(mDashBoardDetails.getCourseDetail().getDc_button_title());

        }
        else
        {
            mTvDigitalCoachingText.setVisibility(INVISIBLE);
        }


        showVideoTitle(isIntroVideo);


        if (!mIsIntroVideoVisible) {

            if (!TextUtils.isEmpty(eClassesArrayList.get(mItemPosition).getSeek_time())) {
                mResumePosition = Utils.getTimeInMilliSeconds(eClassesArrayList.get(mItemPosition).getSeek_time());
            } else {
                mResumePosition = 0;
            }
        } else {
            if (!TextUtils.isEmpty(mDashBoardDetails.getCourseDetail().getSeek_time())) {
                mResumePosition = Utils.getTimeInMilliSeconds(mDashBoardDetails.getCourseDetail().getSeek_time());
            } else {
                mResumePosition = 0;
            }
        }



        mPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);




        mPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                if(playbackState == Player.STATE_ENDED && mDashBoardDetails.isAutoPlay() && !isIntroVideo){
                    Utils.callEventLogApi("watched video of <b>" +mEClassesTitleDialog+" <b>" + "in Course Detail" );
                    isNextTapped = true;
                    playNextVideo();
                }

                if(playbackState == Player.STATE_ENDED && isIntroVideo && playWhenReady) {
                    if(isLandScape){
                        closeFullscreenDialog();
                    }
                    mOnItemClickListener.onSubScriptionButtonClick(true);
                }


                if(playWhenReady){

                }else{
                    Utils.callEventLogApi("paused <b>"+mEClassesTitleDialog+"</b> in Course Detail ");
                }


                if (playbackState == Player.STATE_BUFFERING){
                    bufferingHandler();
                }else{
                    bufferingStopHandler();
                }


                if(!((BaseActivity)mActivity).isFragmentInBackStack(new DashBoardDetailFragment())){
                    Utils.releaseDetailScreenPlayer();
                }

            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                try {

                    Utils.callEventLogApi(error.getSourceException().getMessage() + " in " + mEClassesTitleDialog + "</b>");
                    mTvDigitalCoachingText.setVisibility(GONE);


                }
                catch (Exception e)
                {
                    e.printStackTrace();

                }
                errorHandler();
            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {
                postionChangedListener();

                if (mPlayer.getPlaybackState() == Player.STATE_BUFFERING){
                    bufferingHandler();
                }else{
                    bufferingStopHandler();
                }
            }
        });


        // start from zero if video automatically started or from next video tap
        if(isNextTapped){
            mResumePosition =0;
            isNextTapped = false;
        }


        boolean haveResumePosition = mResumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            mPlayer.seekTo(0, mResumePosition);
        }

        showExoplayer();

        mPlayer.setPlayWhenReady(true);

        mExoPlayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if(visibility == View.VISIBLE ){
                    mTvDigitalCoachingText.setVisibility(INVISIBLE);
                }else{
                    if(mDashBoardDetails.getCourseDetail().getDc_button_show().equals("Yes")) {
                        if(mIsDownloaded){
                            mTvDigitalCoachingText.setVisibility(GONE);

                        }else{
                            mTvDigitalCoachingText.setVisibility(VISIBLE);

                        }
                    }
                }
            }
        });
    }

    private void showVideoTitle(boolean isIntroVideoPlaying) {
        if(!isIntroVideoPlaying) {
            mEClassesTitleDialog = eClassesArrayList.get(mItemPosition).getTitle();
            if (isLandScape) {
                mTvEClassName.setText(mEClassesTitleDialog);
            } else {
                mTvEClassNameSmallVideo.setText(mEClassesTitleDialog);
            }

            if (TextUtils.isEmpty(mEClassesTitleDialog)) {
                mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);

            }
        }else{
            mEClassesTitleDialog = mDashBoardDetails.getCourseDetail().getTitle();
            if (isLandScape) {
                mTvEClassName.setText(mEClassesTitleDialog);
            } else {
                mTvEClassNameSmallVideo.setText(mEClassesTitleDialog);
            }

            if (TextUtils.isEmpty(mEClassesTitleDialog)) {
                mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);

            }
        }
    }


    private void playNextVideo() {
        setSeekTime();

        mItemPosition++;

        if(mItemPosition>=eClassesArrayList.size()){
            mItemPosition =0;
        }
        if(eClassesArrayList.get(mItemPosition).getFree_paid().equals("Free")) {

            if (mItemPosition < eClassesArrayList.size()) {
                showVideoTitle(false);
                markEClassSelected(eClassesArrayList.get(mItemPosition).getId());

                Chapters.EClasses eClasses = eClassesArrayList.get(mItemPosition);
                if(mIsDownloaded && !TextUtils.isEmpty(eClasses.getDownloadedVideoAddress())){
                    mVideoUrl = eClasses.getDownloadedVideoAddress();
                    initExoPlayer(false);
                }else{
                    callVideoUrl(eClasses.getId(),false,eClasses.getPlay_url());

                }
            } else {
                Utils.releaseDetailScreenPlayer();
                showPlayIcon();

            }
        }else{

            Utils.releaseDetailScreenPlayer();

            Intent intent = new Intent(Constants.BROADCAST_CHECK_FREE_PAID_TRACK);
            intent.putExtra(Constants.ECLASSES_VIDEO_ID, eClassesArrayList.get(mItemPosition).getId());


            LocalBroadcastManager.getInstance(WealthDragonsOnlineApplication.sharedInstance()).sendBroadcastSync(intent);

        }
    }


    // called from eclasses video

    public void startNewPlayer(String id, int position) {

        mShowNextVideoWithImageFrame = false;
        isAudioPlayerStarted = false;


        setSeekTime();
        mIsIntroVideoVisible = false;

        if (AppPreferences.getWifiState() && !Utils.IsWifiConnected()) {

            Utils.showTwoButtonAlertDialog(mDialogListener, mContext, mContext.getString(R.string.notification), mContext.getString(R.string.check_wifi_status), mContext.getString(R.string.okay_text), mContext.getString(R.string.go_to_setting));

        } else {
            if (!TextUtils.isEmpty(id)) {
                int pos = 0;
                for (int i = 0; i < eClassesArrayList.size(); i++) {
                    String urls = eClassesArrayList.get(i).getId();

                    if (urls.equals(id)) {
                        pos = i;
                        mItemPosition = pos;
                        mEClassesTitleDialog = eClassesArrayList.get(i).getTitle();
                        if (isLandScape) {
                            mTvEClassName.setText(mEClassesTitleDialog);
                        } else {
                            mTvEClassNameSmallVideo.setText(mEClassesTitleDialog);
                        }

                        if (TextUtils.isEmpty(mEClassesTitleDialog)) {
                            mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);

                        }


                        Chapters.EClasses eClasses = eClassesArrayList.get(i);
                        if(mIsDownloaded && !TextUtils.isEmpty(eClasses.getDownloadedVideoAddress())){
                            mVideoUrl = eClasses.getDownloadedVideoAddress();
                            initExoPlayer(false);
                        }else{
                            callVideoUrl(id,false,eClassesArrayList.get(i).getPlay_url());

                        }

                        Utils.callEventLogApi("accessed <b>" + eClassesArrayList.get(pos).getTitle() + "</b> of " + "<b>" + mDashBoardDetails.getCourseDetail().getTitle() + "</b>");


                    }
                }




            }
        }

    }


    //in case audio player is active and on changing seek this will call Exo player listener and broadcast will be active
    public void closePlayer() {
        isAudioPlayerStarted = true;
        if (mExoPlayerView!=null) {
            mExoPlayerView.setVisibility(View.INVISIBLE);
        }
        mIvCourseImage.setVisibility(View.VISIBLE);
        mVideoError.setVisibility(INVISIBLE);
        mProgressBar.setVisibility(GONE);

        showPlayIcon();
        if (mPlayer != null) {
            mPlayer.seekTo(0, 0);
            mPlayer.setPlayWhenReady(false);
        }

    }


    // in case paid type eclasses clicked
    public void clearPlayer() {
        mIvCourseImage.setVisibility(View.VISIBLE);
        mVideoError.setVisibility(INVISIBLE);
        mProgressBar.setVisibility(GONE);

        showPlayIcon();



        if(mPlayer!=null){
            mPlayer.setPlayWhenReady(false);
        }

    }



    public void pauseVideo() {
        if (mPlayer != null && mPlayer.getPlayWhenReady() == true) {
            mPlayer.setPlayWhenReady(false);
        }
    }

    public VideoData getCurrentVideoData() {
        VideoData videoDataToBeSent = new VideoData();

        try {

            if (mPlayer != null && mPlayer.getPlaybackState() == Player.STATE_READY) {
                if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                    videoDataToBeSent.setTime("0");
                } else {
                    videoDataToBeSent.setTime(Long.toString(mPlayer.getContentPosition() / 1000));
                }
                if (mDashBoardDetails.isProgrammes()) {
                    if (!mIsIntroVideoVisible) {
                        videoDataToBeSent.setType(Constants.CHAPTER);
                        videoDataToBeSent.setVideoId(eClassesArrayList.get(mItemPosition).getId());

                    } else {
                        videoDataToBeSent.setType(Constants.TYPE_PROGRAMMES);
                        videoDataToBeSent.setVideoId(mDashBoardDetails.getCourseDetail().getId());

                    }
                } else {
                    if (!mIsIntroVideoVisible) {
                        videoDataToBeSent.setType(Constants.CHAPTER);
                        videoDataToBeSent.setVideoId(eClassesArrayList.get(mItemPosition).getId());

                    } else {
                        videoDataToBeSent.setType(Constants.TYPE_COURSE);
                        videoDataToBeSent.setVideoId(mDashBoardDetails.getCourseDetail().getId());
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return videoDataToBeSent;

    }



    private void setSeekTime(){
        if (mPlayer != null ) {
            String seekTime;
            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 5 * 1000) {
                seekTime = "0";
            } else {
                seekTime = Long.toString(mPlayer.getContentPosition() / 1000);
            }

            mVideoDataToBeSaveInCourse = new VideoData();
            mVideoDataToBeSaveInCourse.setTime(seekTime);


            if (mDashBoardDetails.isProgrammes()) {
                if (!mIsIntroVideoVisible) {
                    mVideoDataToBeSaveInCourse.setVideoId( eClassesArrayList.get(mItemPosition).getId());

                } else {
                    mVideoDataToBeSaveInCourse.setVideoId( mDashBoardDetails.getCourseDetail().getId());

                }
            } else {
                if (!mIsIntroVideoVisible) {
                    mVideoDataToBeSaveInCourse.setVideoId( eClassesArrayList.get(mItemPosition).getId());

                } else {
                    mVideoDataToBeSaveInCourse.setVideoId( mDashBoardDetails.getCourseDetail().getId());

                }
            }

        }
    }


    public VideoData returnSeekTimeForCourse(){
        return mVideoDataToBeSaveInCourse;

    }

    public void emptyVideoData(){
        mVideoDataToBeSaveInCourse = null;
    }

    private void initFullscreenDialog() {

        mFullScreenDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (mExoPlayerFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }

            @Override
            protected void onStop() {
                super.onStop();

            }
        };


    }


    private void openFullscreenDialog(int rotationAngle) {
        Utils.callEventLogApi("clicked <b>" + "Full Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getCourseDetail().getTitle());
        isPotrait = false;
        isLandScape = true;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.activity_video_fullscreen, null);
        mDialogplayerView = dialogView.findViewById(R.id.exoplayer_fullScreen);
        mNextVideo = dialogView.findViewById(R.id.tv_nxtvideo);
        mLlHeadingContainer = dialogView.findViewById(R.id.header_holder);
        mVideoErrorDialog = findViewById(R.id.video_error);
        nextVideoFrame = dialogView.findViewById(R.id.next_video_image_holder);


        // next video and timer according to video quantites
        if ( mDashBoardDetails.isEclassVisible() && !mIsIntroVideoVisible) {
            mNextVideo.setVisibility(VISIBLE);
            startNextVideoTimer();

            // for handling when to show progress bar definite
            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && !mIsIntroVideoVisible) {
                mShowProgressBarDefinite = false;
            }


            getProgressBarThread();

            try {
                if (mProgreeBarThread.getState() == Thread.State.NEW)
                    mProgreeBarThread.start();

            } catch (Exception e) {
                e.printStackTrace();
            }


        } else {

            // case of intro video

            mNextVideo.setVisibility(GONE);
            nextVideoFrame.setVisibility(GONE);

        }


        mNextVideo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {



                mItemPosition++;

                if(mItemPosition>=eClassesArrayList.size()){
                    mItemPosition = 0;
                }



                Chapters.EClasses eclass = eClassesArrayList.get(mItemPosition);

                if(eclass.getFree_paid().equals("Paid")){
                    mDialogplayerView.getPlayer().setPlayWhenReady(false);


                    if(isLandScape){
                        closeFullscreenDialog();
                    }
                    mOnItemClickListener.onSubScriptionButtonClick(true);
                }else{
                    markEClassSelected(eclass.getId());
                    isNextTapped = true;
                    startNewPlayer(eclass.getId(), 0);
                }


            }
        });
        mDefiniteProgressBar = dialogView.findViewById(R.id.circularProgressbar);

        mTvEClassName = dialogView.findViewById(R.id.tv_eclasses_title);


        if (TextUtils.isEmpty(mEClassesTitleDialog)) {
            mTvEClassName.setText(mVideoNameForListenerLog);
        } else {
            mTvEClassName.setText(mEClassesTitleDialog);
        }
        ImageView dialogCourseImage = dialogView.findViewById(R.id.iv_video_image);



        Glide.with(mContext).load(imageUrl).error(R.drawable.no_img_course).into(dialogCourseImage);

        // on Next Click
        nextVideoFrame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemPosition++;

                if(mItemPosition>=eClassesArrayList.size()){
                 mItemPosition = 0;
                }
                isNextTapped = true;

                Chapters.EClasses eclass = eClassesArrayList.get(mItemPosition);
                markEClassSelected(eclass.getId());
                startNewPlayer(eclass.getId(), 0);


            }
        });
        initDialogFullScreenButton();
        PlayerView.switchTargetView(mExoPlayerView.getPlayer(), mExoPlayerView, mDialogplayerView);
        mFullScreenDialog.addContentView(dialogView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mExoPlayerFullscreen = true;
        mFullScreenDialog.show();

    }

    public void closeFullscreenDialog() {
        Utils.callEventLogApi("clicked <b>" + "Compact Screen Video" + " Mode</b> in Player from " + mDashBoardDetails.getCourseDetail().getTitle());
        isPotrait = true;
        isLandScape = false;
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        PlayerView.switchTargetView(mDialogplayerView.getPlayer(), mDialogplayerView, mExoPlayerView);
        if (TextUtils.isEmpty(mEClassesTitleDialog)) {
            mTvEClassNameSmallVideo.setText(mVideoNameForListenerLog);
        } else {
            mTvEClassNameSmallVideo.setText(mEClassesTitleDialog);
        }
        mFullScreenDialog.dismiss();
        mExoPlayerFullscreen = false;
        mFullScreenIcon.setVisibility(INVISIBLE);
        mSmallScreenIcon.setVisibility(VISIBLE);
        mSmallScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_expand));
        Log.d("orientation", "potrait");
    }


    private void initDialogFullScreenButton() {

        mDialogplayerView.setControllerVisibilityListener(new PlayerControlView.VisibilityListener() {
            @Override
            public void onVisibilityChange(int visibility) {
                if (visibility == View.VISIBLE) {
                    mShowController = true;

                    mLlHeadingContainer.setVisibility(VISIBLE);

                    if (mItemPosition != -1 && mDashBoardDetails.isAutoPlay() && !mIsIntroVideoVisible) {
                        mNextVideo.setVisibility(VISIBLE);
                    } else {
                        mNextVideo.setVisibility(GONE);
                    }
                    disableNextVideoLabel(true);
                } else {
                    mShowController = false;

                    mLlHeadingContainer.setVisibility(GONE);
                    if (mShowNextVideoWithImageFrame) {
                        showNextVideoLabel();
                    } else {
                        disableNextVideoLabel(true);
                    }
                }
            }
        });

        mSmallScreenIcon.setVisibility(INVISIBLE);
        mFullScreenIcon = mDialogplayerView.findViewById(R.id.exo_fullscreen_icon);
        mFullScreenIcon.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_fullscreen_skrink));
        mDialogFullScreenButton = mDialogplayerView.findViewById(R.id.exo_fullscreen_button);
        mDialogFullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeFullscreenDialog();
            }

        });

    }

    private void startNextVideoTimer() {

        mTimerNextVideo = new Timer();
        mTimerNextVideo.schedule(new TimerTask() {
            @Override
            public void run() {
                mNextVideoHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mPlayer != null) {
                            // next video option will be shown before 10 seconds from end
                            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 10 * 1000 && mItemPosition != -1 && mDashBoardDetails.isAutoPlay()) {
                                mShowNextVideoWithImageFrame = true;
                                if (!mShowController) {
                                    showNextVideoLabel();
                                }

                            } else {
                                mShowNextVideoWithImageFrame = false;
                                disableNextVideoLabel(false);
                            }
                        }

                    }
                });
            }
        }, 0, 500);

    }


    private void cancelNextVideoTimer() {
        if (mTimerNextVideo!=null) {
            mTimerNextVideo.cancel();
            mTimerNextVideo.purge();
        }

        mTimerNextVideo=null;
        disableNextVideoLabel(false);
    }

    private void showNextVideoLabel() {
        nextVideoFrame.setVisibility(VISIBLE);
        if (mShowProgressBarDefinite) {
            mProgressBarPosition = 0;
            mDefiniteProgressBar.setProgress(mProgressBarPosition);
            if (!mProgreeBarThread.isAlive()) {
                mProgreeBarThread = getProgressBarThread();
                mProgreeBarThread.start();
            }

        }
    }


    private void disableNextVideoLabel(boolean isFromControllerVisibility) {

        if (nextVideoFrame != null) {
            nextVideoFrame.setVisibility(GONE);
        }

        // do not disable in case of controller visibilty
        if (!isFromControllerVisibility) {
            mShowProgressBarDefinite = true;
        }

    }




    private void postionChangedListener(){
        if (isLandScape) {
            if (mPlayer.getContentPosition() > mPlayer.getDuration() - 11 * 1000 && mItemPosition != -1 && mDashBoardDetails.isAutoPlay()) {
                mShowProgressBarDefinite = false;
                mProgressBarPosition = 0;
                mDefiniteProgressBar.setProgress(mProgressBarPosition);
            } else {
                mShowProgressBarDefinite = true;
            }
        }
    }


    private Thread getProgressBarThread() {
        return mProgreeBarThread = new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (mProgressBarPosition < 100) {
                    mProgressBarPosition += 1;

                    mProgressBarHandler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mDefiniteProgressBar.setProgress(mProgressBarPosition);
                            mShowProgressBarDefinite = false;

                        }
                    });
                    try {
                        // Sleep for 100 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(100); //thread will take approx 10 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void pressFullScreenDialogButton() {

        mDialogFullScreenButton.performClick();

    }


    public boolean isFullScreen(){
        return  mExoPlayerFullscreen;
    }


    private void openEnrollmentDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }







    private void callVideoUrl(final String id , final boolean isIntroVideo, final String oldUrl) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Api.NEW_VIDEO_URL, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    mVideoUrl = jsonObject.getString("video_url");
                    initExoPlayer(isIntroVideo);
                } catch (Exception e) {
                    e.printStackTrace();
                    mVideoUrl = oldUrl;
                    initExoPlayer(isIntroVideo);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mVideoUrl = oldUrl;

                initExoPlayer(isIntroVideo);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);
                if (isIntroVideo) {
                    params.put(Keys.ITEM_TYPE, "CourseIntroVideo");
                    params.put(Keys.ITEM_ID, id);

                } else {
                    params.put(Keys.ITEM_TYPE, "Chapter");
                    params.put(Keys.ITEM_ID, id);


                }



                return params;

            }

        };
        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }


}
