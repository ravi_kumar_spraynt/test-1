package com.imentors.wealthdragons.courseDetailView;

import android.content.Context;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.ReviewListAdapter;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.interfaces.PagingListener;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.models.ReviewPagentaionItem;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;

import java.util.Map;

public class CourseDetailReviewList extends RelativeLayout implements PagingListener{


    private Context mContext;
    private ReviewListAdapter mReviewListAdapter;
    private RecyclerView mRecyclerView;
    private String mCourseId;




    public CourseDetailReviewList(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailReviewList(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailReviewList(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseDetailReviewList(Context context, DashBoardCourseDetails dashBoardDetails) {
        super(context);
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set review list adapter
    private void bindView(DashBoardCourseDetails dashBoardDetails) {

        mCourseId = dashBoardDetails.getCourseDetail().getId();

            if(dashBoardDetails.getCourseDetail().getReviewList().size()>0 && mRecyclerView !=null){
                mReviewListAdapter = new ReviewListAdapter(this,mContext);
                mRecyclerView.setAdapter(mReviewListAdapter);
                mReviewListAdapter.setItems(dashBoardDetails.getCourseDetail().getReviewList(),dashBoardDetails.getCourseDetail().getRatingDetail().getNext_page(),dashBoardDetails.getCourseDetail().getRatingDetail().getTotal_items());

            }

    }

    //  initialize  course detail review list view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_reviews_list, this);
        mContext = context;
        mRecyclerView = view.findViewById(R.id.listView);
        mRecyclerView.setLayoutManager (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemViewCacheSize(20);
        mRecyclerView.setDrawingCacheEnabled(true);
        mRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mRecyclerView.setNestedScrollingEnabled(false);


    }

    @Override
    public void onGetItemFromApi(int pageNumber, String tasteId, String viewType,String seeAllType) {

        if(viewType.equals(Constants.TYPE_REVIEW))
                 callSeeAllPaginationApi(pageNumber,tasteId, Api.RATING_PAGINATION,viewType);

    }

    //TODO implemented maintenance mode
    public void callSeeAllPaginationApi(final int pageNumber , final String tasteId , String api , final String viewType) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }
                    addDataInAdapter(response);

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if(!Utils.setErrorDialog(response,getContext())){
                            setErrorMessage(viewType);
                        }
                    } catch (Exception e1) {
                        Utils.callEventLogApi("getting <b>get Course Detail</b> error in api");

                        e1.printStackTrace();
                        setErrorMessage(viewType);
                    }

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.callEventLogApi("getting <b>get Course Detail</b> error in api");
                error.printStackTrace();
                if (error instanceof NoConnectionError)
                {

                }else{
                    setErrorMessage(viewType);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.COURSE_ID,mCourseId);


                if(pageNumber !=0){
                    params.put(Keys.PAGE_NO,String.valueOf(pageNumber));
                }

                return params;
            }
        };


        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }

    //  add data in adapter
    private void addDataInAdapter(String response) {

        // Parse JSON to Java
        final ReviewPagentaionItem reviewPagenationData = new Gson().fromJson(response, ReviewPagentaionItem.class);


        mReviewListAdapter.addItems(reviewPagenationData.getCourseDetail().getReviewList(),reviewPagenationData.getCourseDetail().getRatingDetail().getNext_page(),reviewPagenationData.getCourseDetail().getRatingDetail().getTotal_items());

    }


    //  set error message
    private void setErrorMessage(String viewType) {
        mReviewListAdapter.setServerError();
    }



}
