package com.imentors.wealthdragons.courseDetailView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;

import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.activity.MainActivity;
import com.imentors.wealthdragons.api.Api;
import com.imentors.wealthdragons.api.Keys;
import com.imentors.wealthdragons.dialogFragment.PayDialogFragment;
import com.imentors.wealthdragons.dialogFragment.PaymentCardListDialog;
import com.imentors.wealthdragons.fragments.DashBoardDetailFragment;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.utils.WealthDragonsOnlineApplication;
import com.imentors.wealthdragons.views.WealthDragonTextView;

import org.json.JSONObject;

import java.util.Map;

public class CourseDetailBelowVideo extends RelativeLayout {


    private Context mContext;
    private FrameLayout mGetInstantAccess, mAddToWishList,mDigitalCoaching;
    private DashBoardCourseDetails mDashBoardDetails;
    private WealthDragonTextView mTvAddToWishList, mTvRemoveWishList, mTvProcessing, mTvTakeEvent, mTvGetInstantAccess, mTvCost, mTvFinalCost, mTvPreBookingDate,mTvEnrollCount,mTvDightalCoachings;
    private String mItemId;
    private boolean mIsItemAdded = false, mIsCourse = true;
    private CourseDetailBelowVideo.OnItemClickListener mOnItemClickListener = null;


    /**
     * different click handled by onItemClick
     */
    public interface OnItemClickListener {
        void onSubScriptionButtonClick(boolean isVideoEnded);

    }


    public CourseDetailBelowVideo(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailBelowVideo(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    //  calling constructor
    public CourseDetailBelowVideo(Context context, DashBoardCourseDetails dashBoardDetails, CourseDetailBelowVideo.OnItemClickListener onItemClickListener, boolean isCourse) {
        super(context);
        this.mOnItemClickListener = onItemClickListener;
        mIsCourse = isCourse;
        initViews(context);
        bindView(dashBoardDetails);
    }

    //  set data on view
    private void bindView(final DashBoardCourseDetails dashBoardDetails) {

        mDashBoardDetails = dashBoardDetails;


        mItemId = dashBoardDetails.getCourseDetail().getId();

        if (dashBoardDetails.getCourseDetail().getDc_button_show().equals("Yes"))
        {
            mDigitalCoaching.setVisibility(VISIBLE);
            mTvDightalCoachings.setText(dashBoardDetails.getCourseDetail().getDc_button_title());

        }else
        {
            mDigitalCoaching.setVisibility(GONE);
        }





        if (dashBoardDetails.isWishlist()) {
            mAddToWishList.setVisibility(View.VISIBLE);

        } else {
            mAddToWishList.setVisibility(View.GONE);

        }

        if (dashBoardDetails.getCourseDetail().getWishlist().equals(mContext.getString(R.string.no))) {

            mTvAddToWishList.setVisibility(VISIBLE);
            mTvRemoveWishList.setVisibility(GONE);

            mIsItemAdded = false;

        } else {

            mTvRemoveWishList.setVisibility(VISIBLE);
            mTvAddToWishList.setVisibility(GONE);

            mIsItemAdded = true;

        }

        mAddToWishList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddToWishList.setVisibility(GONE);
                if(mIsItemAdded ) {
                    mTvProcessing.setVisibility(VISIBLE);
                    callWishListApi(mItemId);

                }else{
                    mTvProcessing.setVisibility(VISIBLE);
                    callWishListApi(mItemId);

                }
                mIsItemAdded = !mIsItemAdded;
            }
        });

        mDigitalCoaching.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean mShowPayDialog= false;

                if(!TextUtils.isEmpty(mDashBoardDetails.getCourseDetail().getDigi_coaching_screen()) && TextUtils.equals(mDashBoardDetails.getCourseDetail().getDigi_coaching_screen(),"No")){
                    mShowPayDialog = true;
                }

                if(mShowPayDialog){
                    if (AppPreferences.getCreditCardList() != null && AppPreferences.getCreditCardList().size() > 0) {
                        openDialog(PaymentCardListDialog.newInstance(dashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_EXPERT, null));
                    } else {
                        openDialog(PayDialogFragment.newInstance("Ruppee", false, false, dashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_EXPERT, null));
                    }
                }else{
                    if(mContext!=null) {
                        ((MainActivity) mContext).addFragment(DashBoardDetailFragment.newInstance(dashBoardDetails.getCourseDetail().getMentor_id(), Constants.TYPE_MENTOR_DASHBOARD_DETAIL_DIGITAL_COACHING, null,false,null));
                    }
                }



                Utils.callEventLogApi("clicked <b>" +dashBoardDetails.getCourseDetail().getDc_button_title()+" </b>" + "from Course Detail" );


            }
        });




        if (dashBoardDetails.isEclassVisible()) {
            mGetInstantAccess.setVisibility(View.GONE);

        } else {

            if (dashBoardDetails.getCourseDetail().getPush_live().equals("2")) {
                if (dashBoardDetails.getCourseDetail().getItem_pre_booking_status().equals("Yes")) {

                    showBuyNow(dashBoardDetails);
                    if (!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getItem_pre_booking_date())) {
                        mTvPreBookingDate.setVisibility(VISIBLE);
                        mTvPreBookingDate.setText("Pre-Booking till" + " " + dashBoardDetails.getCourseDetail().getItem_pre_booking_date());

                    } else {
                        mTvPreBookingDate.setVisibility(GONE);

                    }

                } else {

                    mGetInstantAccess.setVisibility(View.GONE);


                }
            } else {
                showBuyNow(dashBoardDetails);

            }
        }

              //TODO sorry chuityr ki vjh s comment krna pda
//            if (dashBoardDetails.getCourseDetail().isDiscountAvailable() && dashBoardDetails.getCourseDetail().getPurchase().equals("No")) {
//                float finalCost = Float.parseFloat(dashBoardDetails.getCourseDetail().getFinal_cost());
//                float actualCost = Float.parseFloat(dashBoardDetails.getCourseDetail().getItem_orignal_cost());
//                int discountPercentage = 0;
//
//                if(finalCost!= actualCost){
//                    discountPercentage = (int)((finalCost/actualCost)*100);
//                    mTvPreBookingDate.setVisibility(View.VISIBLE);
//
//                    mTvPreBookingDate.setText(100-discountPercentage + "% off");
//                }else{
//                    mTvPreBookingDate.setVisibility(View.GONE);
//                }
//
//            } else {
//                mTvPreBookingDate.setVisibility(View.GONE);
//            }





        mGetInstantAccess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.callEventLogApi("clicked <b>Buy Now</b> button in course detail");
                mOnItemClickListener.onSubScriptionButtonClick(false);
            }
        });

        if (dashBoardDetails.isStudent_enroll_section())
        {
        if (!TextUtils.isEmpty(dashBoardDetails.getCourseDetail().getNo_of_enrollment())) {
            if (Integer.parseInt(dashBoardDetails.getCourseDetail().getNo_of_enrollment()) > 0) {
                mTvEnrollCount.setVisibility(VISIBLE);
                String student  = (Integer.parseInt(dashBoardDetails.getCourseDetail().getNo_of_enrollment())>1)?"Students Enrolled":"Student Enrolled";
                mTvEnrollCount.setText(dashBoardDetails.getCourseDetail().getNo_of_enrollment()+" " +student);

            } else {
                mTvEnrollCount.setVisibility(GONE);

            }
        } else {
            mTvEnrollCount.setVisibility(GONE);
        }
      }
        else
        {
            mTvEnrollCount.setVisibility(GONE);

        }
        }

    private void showBuyNow(DashBoardCourseDetails dashBoardDetails) {
        mGetInstantAccess.setVisibility(View.VISIBLE);
        mTvTakeEvent.setText("Buy Now");
        if (!dashBoardDetails.getCourseDetail().getItem_orignal_cost().equals("0")) {
            if (dashBoardDetails.getCourseDetail().getItem_orignal_cost().equals(dashBoardDetails.getCourseDetail().getFinal_cost())) {
                mTvCost.setVisibility(GONE);
                mTvFinalCost.setVisibility(VISIBLE);
                mTvFinalCost.setText(dashBoardDetails.getCourseDetail().getCurrency() + dashBoardDetails.getCourseDetail().getFinal_cost());
            } else {
                mTvCost.setVisibility(VISIBLE);
                mTvCost.setText(dashBoardDetails.getCourseDetail().getCurrency() + dashBoardDetails.getCourseDetail().getItem_orignal_cost());
                mTvCost.setPaintFlags(mTvCost.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                mTvFinalCost.setVisibility(VISIBLE);
                mTvFinalCost.setText(dashBoardDetails.getCourseDetail().getCurrency() + dashBoardDetails.getCourseDetail().getFinal_cost());

            }

        } else {

            mTvCost.setVisibility(GONE);
            mTvFinalCost.setVisibility(GONE);

        }

    }

    //  initialize course detail below video view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_below_video, this);
        mContext = context;

        mGetInstantAccess = view.findViewById(R.id.fl_take_this_event);
        mDigitalCoaching=view.findViewById(R.id.fl_digital_coaching_button);
        mAddToWishList = view.findViewById(R.id.fl_btn_wishlist);
        mAddToWishList.setVisibility(GONE);
        mTvAddToWishList = view.findViewById(R.id.tv_add_to_wishlist);
        mTvRemoveWishList = view.findViewById(R.id.tv_remove_wishlist);
        mTvProcessing = view.findViewById(R.id.tv_processing);
        mTvGetInstantAccess = view.findViewById(R.id.tv_get_instant_view);
        mTvCost = view.findViewById(R.id.tv_cost);
        mTvFinalCost = view.findViewById(R.id.tv_final_cost);
        mTvTakeEvent = view.findViewById(R.id.tv_take_event);
        mTvPreBookingDate = view.findViewById(R.id.tv_prebooking_date);
        mTvEnrollCount=view.findViewById(R.id.tv_enrollcount);
        mTvDightalCoachings=view.findViewById(R.id.tv_digital_coaching_button);

        if (Utils.isTablet())
        {

            double reuiredButtonWidth = Utils.getScreenWidth((Activity)mContext)/1.5;

            int marginFromStart = (int)(Utils.getScreenWidth((Activity)mContext) - reuiredButtonWidth)/2;

            LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params4.setMarginStart(marginFromStart);
            mTvFinalCost.setLayoutParams(params4);
            mTvPreBookingDate.setLayoutParams(params4);

            LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.END);
            params5.setMarginEnd(marginFromStart);
            mTvEnrollCount.setLayoutParams(params5);

//            FrameLayout.LayoutParams Params = new FrameLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60) );
//            mTvAddToWishList.setLayoutParams(Params);
//            mTvRemoveWishList.setLayoutParams(Params);
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams((int) (reuiredButtonWidth), Utils.dpToPx(60));
            Params.gravity = Gravity.CENTER_HORIZONTAL;
            mAddToWishList.setLayoutParams(Params);
            mDigitalCoaching.setLayoutParams(Params);
            mGetInstantAccess.setLayoutParams(Params);

        }
        else {
            LinearLayout.LayoutParams Params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Utils.dpToPx(50));
//            mAddToWishList.setLayoutParams(Params);
            mGetInstantAccess.setLayoutParams(Params);
            mDigitalCoaching.setLayoutParams(Params);
            mAddToWishList.setLayoutParams(Params);

        }

    }

    public void callWishListApi(final String mItemId) {
        String api = null;


        if (mIsItemAdded) {
            api = Api.REMOVE_WISHLIST_API;

        } else {
            api = Api.ADD_WISHLIST_API;
        }


        //TODO implemented maintenance mode
        // calling add wishlist and remove wishlist api and maintaining response
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mTvProcessing.setVisibility(GONE);
                mAddToWishList.setVisibility(VISIBLE);

                try {

                    if (Utils.checkForMaintainceMode(response)) {
                        WealthDragonsOnlineApplication.sharedInstance().redirectToMaintanceActivity(mContext);
                        return;
                    }

                    JSONObject jsonObject = new JSONObject(response);

                    if (!TextUtils.isEmpty(jsonObject.optString(Keys.SUCCESS))) {


                        if (mIsItemAdded) {

                            Toast.makeText(mContext, "Added to Wishlist", Toast.LENGTH_SHORT).show();
                            mTvAddToWishList.setVisibility(GONE);
                            mTvRemoveWishList.setVisibility(VISIBLE);
                        }else{
                            mTvAddToWishList.setVisibility(VISIBLE);
                            mTvRemoveWishList.setVisibility(GONE);
                        }

                    }


                } catch (Exception e) {

                    if (mIsItemAdded) {
                        Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                        mTvAddToWishList.setVisibility(VISIBLE);
                        mTvRemoveWishList.setVisibility(GONE);

                    } else {

                        Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                        mTvRemoveWishList.setVisibility(VISIBLE);
                        mTvAddToWishList.setVisibility(GONE);


                    }
                    mIsItemAdded = !mIsItemAdded;
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTvProcessing.setVisibility(GONE);
                mAddToWishList.setVisibility(VISIBLE);

                if (mIsItemAdded) {
                    Utils.callEventLogApi("remove <b>Wishlist </b> error in api ");
                    mTvAddToWishList.setVisibility(VISIBLE);
                    mTvRemoveWishList.setVisibility(GONE);


                } else {

                    Utils.callEventLogApi("add <b>Wishlist </b> error in api");
                    mTvRemoveWishList.setVisibility(VISIBLE);
                    mTvAddToWishList.setVisibility(GONE);


                }
                mIsItemAdded = !mIsItemAdded;
                error.printStackTrace();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = Utils.getParams(false);

                params.put(Keys.ITEM_ID, mItemId);
                if (mDashBoardDetails.isProgrammes()) {
                    params.put(Keys.TYPE, Constants.TYPE_PROGRAMMES);

                } else {
                    params.put(Keys.TYPE, Constants.TYPE_COURSE);

                }


                return params;
            }
        };

        WealthDragonsOnlineApplication.sharedInstance().addToRequestQueue(stringRequest);

    }


    private void openDialog(DialogFragment dialogFragment) {
        dialogFragment.setCancelable(false);
        dialogFragment.show(((MainActivity)mContext).getSupportFragmentManager(), "dialog");

    }




}
