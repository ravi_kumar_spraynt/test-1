package com.imentors.wealthdragons.courseDetailView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.imentors.wealthdragons.R;
import com.imentors.wealthdragons.adapters.EClassesListAdapter;
import com.imentors.wealthdragons.models.Chapters;
import com.imentors.wealthdragons.models.DashBoardCourseDetails;
import com.imentors.wealthdragons.utils.AppPreferences;
import com.imentors.wealthdragons.utils.Constants;
import com.imentors.wealthdragons.utils.Utils;
import com.imentors.wealthdragons.views.WealthDragonTextView;

public class CourseDetailEClassesview extends RelativeLayout {


    private Context mContext;
    private EClassesListAdapter mEClassesListAdapter;
    private RecyclerView mRecyclerView;
    private EClassesListAdapter.OnEClassesClick mOnEClassesClick;
    private WealthDragonTextView mEclassesTitle;
    private Chapters chapter;
    private boolean mSelectFirstEclass = false, mIsProgramme;
    private String mCoursePrice, mCourseId , mItemType , mCourseTitle,mBannerImage,mPunchLine,mMentorName;
    private boolean mIsVisible, mIsDownloaded;
    private ImageView mDownloadGreyIcon,mDownloadOrangeIcon;
    private ProgressBar mProgressDownloadBar;
    private EClassesListAdapter.ShowDownlaodIcon mShowDownloadIcon;
    private DashBoardCourseDetails mDashBoardCourseDetails;




    public CourseDetailEClassesview(Context context) {
        super(context);
        mContext = context;
    }

    public CourseDetailEClassesview(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public CourseDetailEClassesview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public interface OnAllDownload {
        void onDownnloadClick();
        void onCancelDownloadClick();

    }

    public interface OnClickDownload {
        void onDownloadList();

    }


    //  calling constructor
    public CourseDetailEClassesview(Context context, Chapters dashBoardDetails , String eClassesTitle , boolean selectFirstItem, boolean isEclassesVisible, String courseId , String coursePrice, String itemType, String courseTitle, String mSelectedId,
                                    boolean isProgramme, String bannerImage, String pinchLine, String mentorName,
                                    boolean isDownloaded, DashBoardCourseDetails dashBoardCourseDetails, EClassesListAdapter.ShowDownlaodIcon showDownlaodIcon ) {
        super(context);
        mSelectFirstEclass = selectFirstItem;
        mCourseId = courseId;
        mCoursePrice = coursePrice;
        mItemType = itemType;
        mCourseTitle=courseTitle;
        mIsProgramme = isProgramme;
        mBannerImage=bannerImage;
        mIsVisible = false;
        mPunchLine=pinchLine;
        mMentorName=mentorName;
        mIsDownloaded = isDownloaded;
        mDashBoardCourseDetails = dashBoardCourseDetails;
        mShowDownloadIcon = showDownlaodIcon;

        initViews(context);
        bindView(dashBoardDetails,eClassesTitle,isEclassesVisible,mSelectedId,mBannerImage,mPunchLine,mMentorName);
    }

    //  set data in view
    private void bindView(Chapters dashBoardDetails , String eClassesTitle, boolean isEclassesVisible, final String selectedId,final String mBannerImage,final String mPunchLine,final String mMentorName) {
        mEclassesTitle.setText(eClassesTitle);

        chapter = dashBoardDetails;

        if(mRecyclerView!=null){
            if(mSelectFirstEclass) {
                mEClassesListAdapter = new EClassesListAdapter(mOnEClassesClick, mContext, 0,mCoursePrice,mCourseId,mItemType,mCourseTitle,mIsProgramme,mBannerImage,mPunchLine,mMentorName,mRecyclerView,mShowDownloadIcon, mDashBoardCourseDetails,mIsDownloaded);
            }else{
                mEClassesListAdapter = new EClassesListAdapter(mOnEClassesClick, mContext, -1,mCoursePrice,mCourseId,mItemType,mCourseTitle,mIsProgramme,mBannerImage,mPunchLine,mMentorName,mRecyclerView,mShowDownloadIcon, mDashBoardCourseDetails,mIsDownloaded);
            }
                mEClassesListAdapter.setItems(dashBoardDetails);
                mRecyclerView.setAdapter(mEClassesListAdapter);


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(mRecyclerView.findViewHolderForAdapterPosition(0) != null) {
                        if(mIsVisible){
                            playVideo(selectedId, chapter);
                        }
                    }
                }
            }, 500);

        }

    }

    public void downloadAllVideos(){
        mEClassesListAdapter.startDownload();
    }


    public void cancelAllVideos(){
        mEClassesListAdapter.cancelVideo();
    }

    private void playVideo(String selectedId,Chapters mChapter) {

        int position = 0;

        if(!TextUtils.isEmpty(selectedId)){
            for(Chapters.EClasses eclass:mChapter.getE_classes()){
                if(eclass.getId().equals(selectedId)){
                    mRecyclerView.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.iv_e_classes_play_icon).performClick();
                    return;
                }
                position++;
            }
        }


    }

    //  initialize course detail Eclasses view
    private void initViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.course_detail_ebook, this);
        mContext = context;

        if(mContext instanceof EClassesListAdapter.OnEClassesClick){
            mOnEClassesClick = (EClassesListAdapter.OnEClassesClick) mContext;
        }


        mEclassesTitle = view.findViewById(R.id.tv_eclasses_title);
        mDownloadGreyIcon=view.findViewById(R.id.iv_download);
        mDownloadOrangeIcon=view.findViewById(R.id.iv_download_grey);
        mProgressDownloadBar=view.findViewById(R.id.download_bar);

        mRecyclerView = view.findViewById(R.id.listView);
        mRecyclerView.setLayoutManager (new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setNestedScrollingEnabled(false);


        LocalBroadcastManager.getInstance(context).registerReceiver(mDisableList,new IntentFilter(Constants.BROADCAST_SAVE_VIDEO_POSITION));
        LocalBroadcastManager.getInstance(context).registerReceiver(mSelectedVideo,new IntentFilter(Constants.BROADCAST_MARK_RUNNING_TRACK));
        LocalBroadcastManager.getInstance(context).registerReceiver(mDisableAudio,new IntentFilter(Constants.BROADCAST_IS_AUDIO_ACTIVE));

        LocalBroadcastManager.getInstance(context).registerReceiver(mRefreshList,new IntentFilter(Constants.BROADCAST_REFRESH_LIST));

    }

    private BroadcastReceiver mDisableList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

                mEClassesListAdapter.disableAudio();
                mEClassesListAdapter.deselectRow();

        }
    };

    private BroadcastReceiver mRefreshList = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            mEClassesListAdapter.notifyDataSetChanged();

        }
    };

    private BroadcastReceiver mDisableAudio = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            mEClassesListAdapter.disableAudio();

        }
    };

    private BroadcastReceiver mSelectedVideo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String videoId = intent.getStringExtra(Constants.ECLASSES_VIDEO_ID);
            boolean isVideoIconTapped = intent.getBooleanExtra(Constants.IS_VIDEO_PLAYED,false);
            // boolean is required to handle the case of multiple eclasses thus to deselect older one we require boolean value
             boolean isVideoAvaialble = false;

             if(!TextUtils.isEmpty(videoId)) {
                 for (int i = 0; i < chapter.getE_classes().size(); i++) {
                     Chapters.EClasses eClasses = chapter.getE_classes().get(i);
                     if (eClasses.getType().equals(Constants.TYPE_VIDEO) && !TextUtils.isEmpty(eClasses.getPlay_url())) {
                         if(eClasses.getFree_paid().equals("Free")){
                         if (eClasses.getId().equals(videoId)) {
                             isVideoAvaialble = true;
                             mEClassesListAdapter.selectRow(i,isVideoIconTapped);
                             break;
                         }
                         }else{
                            if(AppPreferences.getFullScreenActivityState()){

                            } else{

                            }
                         }
                     }
                 }
             }

            if(!isVideoAvaialble){
                mEClassesListAdapter.deselectRow();
            }

        }
    };

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mIsVisible = true;
        // releasing audio player
        mEClassesListAdapter.disableAudio();
        Utils.releaseAudioPlayer();
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mSelectedVideo);
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mDisableAudio);
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mRefreshList);
        LocalBroadcastManager.getInstance(this.getContext()).unregisterReceiver(mDisableList);

    }

}
